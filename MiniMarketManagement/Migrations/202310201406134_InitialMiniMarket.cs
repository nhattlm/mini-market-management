﻿namespace MiniMarketManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMiniMarket : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Account",
                c => new
                    {
                        Username = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        Password = c.String(maxLength: 10, fixedLength: true),
                        EmployeeID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                    })
                .PrimaryKey(t => t.Username)
                .ForeignKey("dbo.Employee", t => t.EmployeeID)
                .Index(t => t.EmployeeID);
            
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        EmployeeID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        EmployeeName = c.String(nullable: false, maxLength: 255),
                        EmployeePhone = c.String(nullable: false, maxLength: 10, fixedLength: true, unicode: false),
                        EmployeeBirth = c.DateTime(nullable: false, storeType: "date"),
                        PositionID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        Image = c.Binary(storeType: "image"),
                    })
                .PrimaryKey(t => t.EmployeeID)
                .ForeignKey("dbo.Position", t => t.PositionID)
                .Index(t => t.PositionID);
            
            CreateTable(
                "dbo.ImportProductCard",
                c => new
                    {
                        ImportProductID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        SupplierID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        ImportProductDate = c.DateTime(nullable: false, storeType: "date"),
                        EmployeeID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                    })
                .PrimaryKey(t => t.ImportProductID)
                .ForeignKey("dbo.Supplier", t => t.SupplierID)
                .ForeignKey("dbo.Employee", t => t.EmployeeID)
                .Index(t => t.SupplierID)
                .Index(t => t.EmployeeID);
            
            CreateTable(
                "dbo.DetailImportProductCard",
                c => new
                    {
                        ImportProductID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        ProductID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        Quantity = c.Double(nullable: false),
                        PriceTotal = c.Double(nullable: false),
                    })
                .PrimaryKey(t => new { t.ImportProductID, t.ProductID })
                .ForeignKey("dbo.Products", t => t.ProductID)
                .ForeignKey("dbo.ImportProductCard", t => t.ImportProductID)
                .Index(t => t.ImportProductID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        ProductID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        ProductName = c.String(nullable: false, maxLength: 255),
                        ProductTypeID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        BuyPrice = c.Double(nullable: false),
                        CalculationUnitName = c.String(maxLength: 255),
                        SellPrice = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.ProductID)
                .ForeignKey("dbo.CalculationUnit", t => t.CalculationUnitName)
                .ForeignKey("dbo.ProductType", t => t.ProductTypeID)
                .Index(t => t.ProductTypeID)
                .Index(t => t.CalculationUnitName);
            
            CreateTable(
                "dbo.CalculationUnit",
                c => new
                    {
                        CalculationUnitName = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.CalculationUnitName);
            
            CreateTable(
                "dbo.DetailInvoice",
                c => new
                    {
                        InvoiceID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        CalculationUnitName = c.String(nullable: false, maxLength: 255),
                        CustomerID = c.String(maxLength: 10, fixedLength: true),
                        Price = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        Quantity = c.Double(nullable: false),
                        ProductID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                    })
                .PrimaryKey(t => new { t.InvoiceID, t.CalculationUnitName })
                .ForeignKey("dbo.Customers", t => t.CustomerID)
                .ForeignKey("dbo.Invoice", t => t.InvoiceID)
                .ForeignKey("dbo.CalculationUnit", t => t.CalculationUnitName)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.InvoiceID)
                .Index(t => t.CalculationUnitName)
                .Index(t => t.CustomerID)
                .Index(t => t.ProductID);
            
            CreateTable(
                "dbo.Customers",
                c => new
                    {
                        CusTomerID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        CustomerName = c.String(nullable: false, maxLength: 255),
                        PhoneNumber = c.Int(nullable: false),
                        Point = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CusTomerID);
            
            CreateTable(
                "dbo.Invoice",
                c => new
                    {
                        InvoiceID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        Date = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        PriceTotal = c.Double(nullable: false),
                        EmployeeID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        MinusPrice = c.Double(),
                    })
                .PrimaryKey(t => t.InvoiceID)
                .ForeignKey("dbo.Employee", t => t.EmployeeID)
                .Index(t => t.EmployeeID);
            
            CreateTable(
                "dbo.DetailReturnProductCard",
                c => new
                    {
                        ProductID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        ReturnProductID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        Quantity = c.Double(nullable: false),
                    })
                .PrimaryKey(t => new { t.ProductID, t.ReturnProductID })
                .ForeignKey("dbo.ReturnProductsCard", t => t.ReturnProductID)
                .ForeignKey("dbo.Products", t => t.ProductID)
                .Index(t => t.ProductID)
                .Index(t => t.ReturnProductID);
            
            CreateTable(
                "dbo.ReturnProductsCard",
                c => new
                    {
                        ReturnProductID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        SupplierID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        ReturnProductDate = c.DateTime(nullable: false, storeType: "date"),
                        EmployeeID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                    })
                .PrimaryKey(t => t.ReturnProductID)
                .ForeignKey("dbo.Supplier", t => t.SupplierID)
                .ForeignKey("dbo.Employee", t => t.EmployeeID)
                .Index(t => t.SupplierID)
                .Index(t => t.EmployeeID);
            
            CreateTable(
                "dbo.Supplier",
                c => new
                    {
                        SupplierID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        SupplierName = c.String(nullable: false, maxLength: 255),
                        PhoneNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SupplierID);
            
            CreateTable(
                "dbo.ProductType",
                c => new
                    {
                        ProductTypeID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        ProductTypeName = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.ProductTypeID);
            
            CreateTable(
                "dbo.Position",
                c => new
                    {
                        PositionID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        PositionName = c.String(nullable: false, maxLength: 255),
                    })
                .PrimaryKey(t => t.PositionID);
            
            CreateTable(
                "dbo.Statistics",
                c => new
                    {
                        DateEnd = c.DateTime(nullable: false, storeType: "date"),
                        DateStart = c.DateTime(nullable: false, storeType: "date"),
                        PriceTotal = c.Double(nullable: false),
                        ExpendsTotal = c.Double(nullable: false),
                        ProfitTotal = c.Double(nullable: false),
                        EmployeeID = c.String(nullable: false, maxLength: 10, fixedLength: true),
                    })
                .PrimaryKey(t => new { t.DateEnd, t.DateStart })
                .ForeignKey("dbo.Employee", t => t.EmployeeID)
                .Index(t => t.EmployeeID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Statistics", "EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.ReturnProductsCard", "EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.Employee", "PositionID", "dbo.Position");
            DropForeignKey("dbo.Invoice", "EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.ImportProductCard", "EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.DetailImportProductCard", "ImportProductID", "dbo.ImportProductCard");
            DropForeignKey("dbo.Products", "ProductTypeID", "dbo.ProductType");
            DropForeignKey("dbo.DetailReturnProductCard", "ProductID", "dbo.Products");
            DropForeignKey("dbo.ReturnProductsCard", "SupplierID", "dbo.Supplier");
            DropForeignKey("dbo.ImportProductCard", "SupplierID", "dbo.Supplier");
            DropForeignKey("dbo.DetailReturnProductCard", "ReturnProductID", "dbo.ReturnProductsCard");
            DropForeignKey("dbo.DetailInvoice", "ProductID", "dbo.Products");
            DropForeignKey("dbo.DetailImportProductCard", "ProductID", "dbo.Products");
            DropForeignKey("dbo.Products", "CalculationUnitName", "dbo.CalculationUnit");
            DropForeignKey("dbo.DetailInvoice", "CalculationUnitName", "dbo.CalculationUnit");
            DropForeignKey("dbo.DetailInvoice", "InvoiceID", "dbo.Invoice");
            DropForeignKey("dbo.DetailInvoice", "CustomerID", "dbo.Customers");
            DropForeignKey("dbo.Account", "EmployeeID", "dbo.Employee");
            DropIndex("dbo.Statistics", new[] { "EmployeeID" });
            DropIndex("dbo.ReturnProductsCard", new[] { "EmployeeID" });
            DropIndex("dbo.ReturnProductsCard", new[] { "SupplierID" });
            DropIndex("dbo.DetailReturnProductCard", new[] { "ReturnProductID" });
            DropIndex("dbo.DetailReturnProductCard", new[] { "ProductID" });
            DropIndex("dbo.Invoice", new[] { "EmployeeID" });
            DropIndex("dbo.DetailInvoice", new[] { "ProductID" });
            DropIndex("dbo.DetailInvoice", new[] { "CustomerID" });
            DropIndex("dbo.DetailInvoice", new[] { "CalculationUnitName" });
            DropIndex("dbo.DetailInvoice", new[] { "InvoiceID" });
            DropIndex("dbo.Products", new[] { "CalculationUnitName" });
            DropIndex("dbo.Products", new[] { "ProductTypeID" });
            DropIndex("dbo.DetailImportProductCard", new[] { "ProductID" });
            DropIndex("dbo.DetailImportProductCard", new[] { "ImportProductID" });
            DropIndex("dbo.ImportProductCard", new[] { "EmployeeID" });
            DropIndex("dbo.ImportProductCard", new[] { "SupplierID" });
            DropIndex("dbo.Employee", new[] { "PositionID" });
            DropIndex("dbo.Account", new[] { "EmployeeID" });
            DropTable("dbo.Statistics");
            DropTable("dbo.Position");
            DropTable("dbo.ProductType");
            DropTable("dbo.Supplier");
            DropTable("dbo.ReturnProductsCard");
            DropTable("dbo.DetailReturnProductCard");
            DropTable("dbo.Invoice");
            DropTable("dbo.Customers");
            DropTable("dbo.DetailInvoice");
            DropTable("dbo.CalculationUnit");
            DropTable("dbo.Products");
            DropTable("dbo.DetailImportProductCard");
            DropTable("dbo.ImportProductCard");
            DropTable("dbo.Employee");
            DropTable("dbo.Account");
        }
    }
}

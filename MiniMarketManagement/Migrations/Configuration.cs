﻿namespace MiniMarketManagement.Migrations
{
    using MiniMarketManagement.DAL.Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolTip;

    internal sealed class Configuration : DbMigrationsConfiguration<MiniMarketManagement.DAL.Entities.MiniMarketDB>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MiniMarketManagement.DAL.Entities.MiniMarketDB context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.
        }
    }
}

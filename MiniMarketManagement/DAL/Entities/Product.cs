namespace MiniMarketManagement.DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Product
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Product()
        {
            DetailImportProductCards = new HashSet<DetailImportProductCard>();
            DetailInvoices = new HashSet<DetailInvoice>();
            DetailReturnProductCards = new HashSet<DetailReturnProductCard>();
            Repositories = new HashSet<Repository>();
        }

        [StringLength(50)]
        public string ProductID { get; set; }

        [Required]
        [StringLength(255)]
        public string ProductName { get; set; }

        [Required]
        [StringLength(50)]
        public string ProductTypeID { get; set; }

        public double BuyPrice { get; set; }

        [StringLength(255)]
        public string ProductImage { get; set; }

        [Required]
        [StringLength(50)]
        public string SupplierID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetailImportProductCard> DetailImportProductCards { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetailInvoice> DetailInvoices { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetailReturnProductCard> DetailReturnProductCards { get; set; }

        public virtual ProductType ProductType { get; set; }

        public virtual Supplier Supplier { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Repository> Repositories { get; set; }
    }
}

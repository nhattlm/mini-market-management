namespace MiniMarketManagement.DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DetailReturnProductCard")]
    public partial class DetailReturnProductCard
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string ProductID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string ReturnProductID { get; set; }

        public double Quantity { get; set; }

        public double price { get; set; }

        [Required]
        [StringLength(255)]
        public string ReturnReason { get; set; }

        public virtual Product Product { get; set; }

        public virtual ReturnProductsCard ReturnProductsCard { get; set; }
    }
}

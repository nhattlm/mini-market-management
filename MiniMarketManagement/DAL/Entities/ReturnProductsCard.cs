namespace MiniMarketManagement.DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ReturnProductsCard")]
    public partial class ReturnProductsCard
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ReturnProductsCard()
        {
            DetailReturnProductCards = new HashSet<DetailReturnProductCard>();
        }

        [Key]
        [StringLength(50)]
        public string ReturnProductID { get; set; }

        [Column(TypeName = "date")]
        public DateTime ReturnProductDate { get; set; }

        [Required]
        [StringLength(50)]
        public string EmployeeID { get; set; }

        public double PriceTotal { get; set; }

        [Required]
        [StringLength(50)]
        public string ImportProductID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetailReturnProductCard> DetailReturnProductCards { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual ImportProductCard ImportProductCard { get; set; }
    }
}

namespace MiniMarketManagement.DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ImportProductCard")]
    public partial class ImportProductCard
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ImportProductCard()
        {
            DetailImportProductCards = new HashSet<DetailImportProductCard>();
            ReturnProductsCards = new HashSet<ReturnProductsCard>();
        }

        [Key]
        [StringLength(50)]
        public string ImportProductID { get; set; }

        [Required]
        [StringLength(50)]
        public string SupplierID { get; set; }

        [Column(TypeName = "date")]
        public DateTime ImportProductDate { get; set; }

        [Required]
        [StringLength(50)]
        public string EmployeeID { get; set; }

        public double PriceTotal { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetailImportProductCard> DetailImportProductCards { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual Supplier Supplier { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ReturnProductsCard> ReturnProductsCards { get; set; }
    }
}

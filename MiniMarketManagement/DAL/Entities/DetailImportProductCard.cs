namespace MiniMarketManagement.DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DetailImportProductCard")]
    public partial class DetailImportProductCard
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string ImportProductID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string ProductID { get; set; }

        public double Quantity { get; set; }

        public double Price { get; set; }

        public bool Returned { get; set; }

        public virtual ImportProductCard ImportProductCard { get; set; }

        public virtual Product Product { get; set; }
    }
}

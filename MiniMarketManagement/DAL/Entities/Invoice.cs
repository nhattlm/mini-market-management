namespace MiniMarketManagement.DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Invoice")]
    public partial class Invoice
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Invoice()
        {
            DetailInvoices = new HashSet<DetailInvoice>();
        }

        [StringLength(50)]
        public string InvoiceID { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        public double PriceTotal { get; set; }

        public double? MinusPrice { get; set; }

        [Required]
        [StringLength(50)]
        public string EmployeeID { get; set; }

        [StringLength(50)]
        public string CustomerID { get; set; }

        public virtual Customer Customer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DetailInvoice> DetailInvoices { get; set; }

        public virtual Employee Employee { get; set; }
    }
}

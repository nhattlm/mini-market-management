namespace MiniMarketManagement.DAL.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DetailInvoice")]
    public partial class DetailInvoice
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string InvoiceID { get; set; }

        public double Price { get; set; }

        public double Quantity { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(50)]
        public string ProductID { get; set; }

        public double? UseDIscount { get; set; }

        public virtual Invoice Invoice { get; set; }

        public virtual Product Product { get; set; }
    }
}

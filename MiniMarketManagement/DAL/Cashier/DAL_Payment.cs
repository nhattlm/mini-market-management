﻿using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Entities;
using MiniMarketManagement.BLL;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.Entity.Migrations;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ProgressBar;

namespace MiniMarketManagement.DAL.Cashier
{
    public class DAL_Payment
    {
        public List<Product> GetAllProduct()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Products.ToList();
        }


        public Product GetProduct(string txt_ProductID)
        {
            MiniMarketDB db = new MiniMarketDB();

            return db.Products.SingleOrDefault(x => x.ProductID == txt_ProductID);
        }
        public List<Product> GetProductSearch(string txt_search)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Products.Where(s =>
                s.ProductID.Contains(txt_search) ||
                s.ProductName.Contains(txt_search)
            ).ToList();
        }
        public void ProductUpdate(Product product)
        {
            MiniMarketDB db = new MiniMarketDB();
            db.Products.AddOrUpdate(product);
            db.SaveChanges();
        }
        public Invoice GetInvoice()
        {
            return new Invoice();
        }
        public void InvoiceUpdate(Invoice invoice)
        {
            MiniMarketDB db = new MiniMarketDB();
            db.Invoices.AddOrUpdate(invoice);
            db.SaveChanges();
        }
        public List<Invoice> GetAllInvoice()
        {
            MiniMarketDB dB = new MiniMarketDB();
            return dB.Invoices.ToList();
        }
        public DetailInvoice GetDetailInvoice()
        {
            return new DetailInvoice();
        }
        public List<DetailInvoice> GetAllDetailInvoice()
        {
            MiniMarketDB dB = new MiniMarketDB();
            return dB.DetailInvoices.ToList();
        }
        public int GetLastIndexOfInvoice()
        {
            MiniMarketDB db = new MiniMarketDB();
            bool check = !db.Invoices.Any();
            if (check)
            {
                return 0;
            }
            else
            {
                var lastRow = db.Invoices
                    .Select(item => new
                    {
                        NumberPart = item.InvoiceID.Substring(1)
                    })
                    .AsEnumerable()
                    .OrderByDescending(item => int.Parse(item.NumberPart))
                    .FirstOrDefault();
                return int.Parse(lastRow.NumberPart);
            }
        }
        public void DetailInvoiceUpdate(DetailInvoice detailInvoice)
        {
            MiniMarketDB db = new MiniMarketDB();
            db.DetailInvoices.Add(detailInvoice);
            db.SaveChanges();
        }
        public Customer GetCustomer()
        {
            return new Customer();
        }
        public List<Customer> GetAllCustomer()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Customers.ToList();
        }
        public List<Customer> GetCustomerSearch(string txt_CustomerID)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Customers.Where(s =>
                s.CustomerID == txt_CustomerID ||
                s.PhoneNumber == txt_CustomerID
            ).ToList();
        }
        public void CustomerUpdate(Customer customer)
        {
            MiniMarketDB db = new MiniMarketDB();
            db.Customers.AddOrUpdate(customer);
            db.SaveChanges();
        }
        public List<Repository> GetRepositoryFindProductID(string PriductID)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Repositories.Where(s =>
                s.ProductID.Contains(PriductID)
            ).ToList();
        }
        public List<Repository> GetRepositorySearch(string txt_Search)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Repositories.Where(s =>
                s.ProductID.Contains(txt_Search) ||
                s.Product.ProductName.Contains(txt_Search) ||
                s.Product.ProductType.ProductTypeName.Contains(txt_Search)
            ).ToList();
        }
        public List<Repository> GetAllRepository()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Repositories.ToList();
        }
        public void RepositoryUpdate(Repository repository)
        {
            MiniMarketDB db = new MiniMarketDB();
            db.Repositories.AddOrUpdate(repository);
            db.SaveChanges();
        }
        public Invoice GetInvoice_From_InvoiceID(string txt_InvoiceID)
        {
            MiniMarketDB db = new MiniMarketDB();

            return db.Invoices.SingleOrDefault(x => x.InvoiceID == txt_InvoiceID);
        }
        public List<DetailInvoice> GetDetailInvoiceSearch(string txt_InvoiceID)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.DetailInvoices.Where(s =>
                s.InvoiceID == txt_InvoiceID
            ).ToList();
        }
        public Customer GetCustomerByCustommerID_Or_PhoneNumber(string txt_CustomerID)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Customers.SingleOrDefault(s =>
                s.CustomerID == txt_CustomerID ||
                s.PhoneNumber == txt_CustomerID
            );
        }
    }
}

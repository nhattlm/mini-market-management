﻿using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMarketManagement.DAL.Cashier
{
    public class DAL_Register
    {
        public List<Invoice> GetInvoiceSearch(string txt_InvoiceID)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Invoices.Where(s =>
                s.InvoiceID.Contains(txt_InvoiceID)
            ).ToList();
        }
        public int GetLastIndexOfCustomer()
        {
            MiniMarketDB db = new MiniMarketDB();
            bool check = !db.Customers.Any();
            if (check)
            {
                return 0;
            }
            else
            {
                var lastRow = db.Customers
                    .Select(item => new
                    {
                        NumberPart = item.CustomerID.Substring(1)
                    })
                    .AsEnumerable()
                    .OrderByDescending(item => int.Parse(item.NumberPart))
                    .FirstOrDefault();
                return int.Parse(lastRow.NumberPart);
            }
        }
        public int GetLastIndexOfInvoice()
        {
            MiniMarketDB db = new MiniMarketDB();
            bool check = !db.Invoices.Any();
            if (check)
            {
                return 0;
            }
            else
            {
                var lastRow = db.Invoices
                    .Select(item => new
                    {
                        NumberPart = item.InvoiceID.Substring(1)
                    })
                    .AsEnumerable()
                    .OrderByDescending(item => int.Parse(item.NumberPart))
                    .FirstOrDefault();
                return int.Parse(lastRow.NumberPart);
            }
        }
    }
}

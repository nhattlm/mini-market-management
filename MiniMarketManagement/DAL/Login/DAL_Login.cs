﻿using MiniMarketManagement.DAL.Entities;
using System;
using System.Linq;
using System.Windows.Forms;
using MiniMarketManagement.GUI.Administrator;
using MiniMarketManagement.GUI.Storekeeper;
using MiniMarketManagement.GUI.Cashier;
using System.Data.Entity;
using System.Collections.Generic;
using Guna.UI2.WinForms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ProgressBar;
using System.Security.Cryptography;
using MiniMarketManagement.GUI;

namespace MiniMarketManagement.DAL.Login
{
    public class DAL_Login
    {
        public List<Account> GetAccountList()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Accounts.ToList();
        }
    }
}

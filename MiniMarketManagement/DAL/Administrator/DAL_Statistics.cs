﻿using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace MiniMarketManagement.DAL.Administrator
{
    internal class DAL_Statistics
    {

        public double GetTotalIncome()
        {
            MiniMarketDB db = new MiniMarketDB();
            return new DAL_Invoices().GetInvoices()?.Sum(item => item.PriceTotal) ?? 0;
        }
        public double GetTotalExpense()
        {
            MiniMarketDB db = new MiniMarketDB();
            double totalExpense = db.ImportProductCards.Sum(item => item.PriceTotal);
            var returnedItems = db.DetailImportProductCards.Where(detail => detail.Returned);
            foreach (var returnedItem in returnedItems)
            {
                bool isItemInReturnProductsCart = db.ReturnProductsCards.Any(returnedCard => returnedCard.ImportProductID == returnedItem.ImportProductID);
                if (isItemInReturnProductsCart)
                {
                    totalExpense -= returnedItem.Quantity * returnedItem.Price;
                }
            }
            return totalExpense;
        }
        public double GetTotalProfit()
        {
            return GetTotalIncome() - GetTotalExpense();
        }
        public double GetTotalIncomeByMonth(DateTime firstDate, DateTime lastDate)
        {
            MiniMarketDB db = new MiniMarketDB();
            var totalIncomeByMonth = db.Invoices
                .Where(invoice => invoice.Date >= firstDate && invoice.Date <= lastDate && invoice.Date.Year == DateTime.Now.Year).ToList();
            if (totalIncomeByMonth == null)
            {
                return 0;       
            }              
            return totalIncomeByMonth.Sum(invoice => invoice.PriceTotal);
        }
        public double GetTotalExpenseByMonth(DateTime firstDate, DateTime lastDate)
        {
            MiniMarketDB db = new MiniMarketDB();
            double totalExpenseByMonth = db.ImportProductCards.Where(ImportProductCard => ImportProductCard.ImportProductDate >= firstDate
                                         && ImportProductCard.ImportProductDate <= lastDate && ImportProductCard.ImportProductDate.Year == DateTime.Now.Year).ToList()
                                        .Sum(item => item.PriceTotal);
            var returnedItemsByMonth = db.DetailImportProductCards.Where(detail => detail.Returned && detail.ImportProductCard.ImportProductDate >= firstDate
                                        && detail.ImportProductCard.ImportProductDate <= lastDate && detail.ImportProductCard.ImportProductDate.Year == DateTime.Now.Year);
            foreach (var returnedItem in returnedItemsByMonth)
            {
                bool isItemInReturnProductsCartThatMonth = db.ReturnProductsCards.Any(returnedCard => returnedCard.ImportProductID == returnedItem.ImportProductID
                                                && returnedCard.ReturnProductDate >= firstDate && returnedCard.ReturnProductDate <= lastDate
                                                && returnedCard.ReturnProductDate.Year == DateTime.Now.Year);
                if (isItemInReturnProductsCartThatMonth)
                {
                    totalExpenseByMonth -= returnedItem.Quantity * returnedItem.Price;
                }
            }
            return totalExpenseByMonth;
        }
        public double GetTotalProfitByMonth(DateTime firstDate, DateTime lastDate)
        {
            return GetTotalIncomeByMonth(firstDate, lastDate) - GetTotalExpenseByMonth(firstDate, lastDate);
        }
        public double CompareTotalIncomeWithPreviousMonth(DateTime currentFirstDate, DateTime currentLastDate, DateTime previousFirstDate, DateTime previousLastDate)
        {
            return GetTotalIncomeByMonth(currentFirstDate, currentLastDate) - GetTotalIncomeByMonth(previousFirstDate, previousLastDate);
        }
        public double CompareTotalExpenseWithPreviousMonth(DateTime currentFirstDate, DateTime currentLastDate, DateTime previousFirstDate, DateTime previousLastDate)
        {
            return GetTotalExpenseByMonth(currentFirstDate, currentLastDate) - GetTotalExpenseByMonth(previousFirstDate, previousLastDate);
        }
        public double CompareTotalProfitWithPreviousMonth(DateTime currentFirstDate, DateTime currentLastDate, DateTime previousFirstDate, DateTime previousLastDate)
        {
            if (GetTotalProfitByMonth(previousFirstDate, previousLastDate) == 0)
            {
                return 0;
            }
            return ((GetTotalProfitByMonth(currentFirstDate, currentLastDate) - GetTotalProfitByMonth(previousFirstDate, previousLastDate)) / GetTotalProfitByMonth(previousFirstDate, previousLastDate)) * 100;
        }
    }
}

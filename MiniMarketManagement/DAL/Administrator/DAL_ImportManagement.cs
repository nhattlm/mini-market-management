﻿using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMarketManagement.DAL.Administrator
{
    internal class DAL_ImportManagement
    {
        public List<DetailImportProductCard> GetDetailImportProductCard()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.DetailImportProductCards.ToList();
        }
        public List<DetailImportProductCard> SearchBy(string ImportProductCartID, Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate)
        {
            MiniMarketDB db = new MiniMarketDB();
            var query = db.DetailImportProductCards.AsQueryable();
            if (fromDate.Value >= toDate.Value)
            {
                if (!string.IsNullOrEmpty(ImportProductCartID))
                {
                    query = query.Where(item => item.ImportProductID == ImportProductCartID);
                }
                if (fromDate.Value != null)
                {
                    query = query.Where(item => item.ImportProductCard.ImportProductDate <= fromDate.Value);
                }
                if (toDate.Value != null)
                {
                    query = query.Where(item => item.ImportProductCard.ImportProductDate >= toDate.Value);
                }
                return query.ToList();
            }
            else
            {
                if (!string.IsNullOrEmpty(ImportProductCartID))
                {
                    query = query.Where(item => item.ImportProductID == ImportProductCartID);
                }
                if (fromDate.Value != null)
                {
                    query = query.Where(item => item.ImportProductCard.ImportProductDate >= fromDate.Value);
                }
                if (toDate.Value != null)
                {
                    query = query.Where(item => item.ImportProductCard.ImportProductDate <= toDate.Value);
                }
                return query.ToList();
            }
        }
        public double LoadDefaultImportQuantity()
        {
            return GetDetailImportProductCard()?.Sum(item => item.Quantity) ?? 0;
        }
        public double LoadDeafaultImportProductMoney()
        {
            return GetDetailImportProductCard()?.Sum(item => item.Price * item.Quantity) ?? 0;
        }
        public double ImportQuantity(string ImportProductCartID, Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate)
        {
            if (fromDate.Value >= toDate.Value)
            {
                if (!string.IsNullOrEmpty(ImportProductCartID))
                {
                    // Trường hợp có điều kiện tìm kiếm
                    var filteredItems = GetDetailImportProductCard().Where(item => item.ImportProductID.ToLower() == ImportProductCartID.ToLower()
                                        && item.ImportProductCard.ImportProductDate <= fromDate.Value && item.ImportProductCard.ImportProductDate >= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.Quantity);
                }
                if (string.IsNullOrEmpty(ImportProductCartID))
                {
                    var filteredItems = GetDetailImportProductCard().Where(item => item.ImportProductCard.ImportProductDate <= fromDate.Value && item.ImportProductCard.ImportProductDate >= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.Quantity);
                }
                else
                {
                    // Trường hợp không có điều kiện tìm kiếm, trả về tổng quantity của tất cả mục
                    return GetDetailImportProductCard()?.Sum(item => item.Quantity) ?? 0;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ImportProductCartID))
                {
                    // Trường hợp có điều kiện tìm kiếm
                    var filteredItems = GetDetailImportProductCard().Where(item => item.ImportProductID.ToLower() == ImportProductCartID.ToLower()
                                        && item.ImportProductCard.ImportProductDate >= fromDate.Value && item.ImportProductCard.ImportProductDate <= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.Quantity);
                }
                if (string.IsNullOrEmpty(ImportProductCartID))
                {
                    var filteredItems = GetDetailImportProductCard().Where(item => item.ImportProductCard.ImportProductDate >= fromDate.Value && item.ImportProductCard.ImportProductDate <= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.Quantity);
                }
                else
                {
                    // Trường hợp không có điều kiện tìm kiếm, trả về tổng quantity của tất cả mục
                    return GetDetailImportProductCard()?.Sum(item => item.Quantity) ?? 0;
                }
            }
        }

        public double ImportProductMoney(string ImportProductCartID, Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate)
        {
            if (fromDate.Value >= toDate.Value)
            {
                if (!string.IsNullOrEmpty(ImportProductCartID))
                {
                    // Trường hợp có điều kiện tìm kiếm
                    var filteredItems = GetDetailImportProductCard().Where(item => item.ImportProductID.ToLower() == ImportProductCartID.ToLower()
                                        && item.ImportProductCard.ImportProductDate <= fromDate.Value && item.ImportProductCard.ImportProductDate >= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.Price * item.Quantity);
                }
                if (string.IsNullOrEmpty(ImportProductCartID))
                {
                    var filteredItems = GetDetailImportProductCard().Where(item => item.ImportProductCard.ImportProductDate <= fromDate.Value && item.ImportProductCard.ImportProductDate >= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.Price * item.Quantity);
                }
                else
                {
                    // Trường hợp không có điều kiện tìm kiếm, trả về tổng tiền của tất cả mục
                    return GetDetailImportProductCard()?.Sum(item => item.Price * item.Quantity) ?? 0;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ImportProductCartID))
                {
                    // Trường hợp có điều kiện tìm kiếm
                    var filteredItems = GetDetailImportProductCard().Where(item => item.ImportProductID.ToLower() == ImportProductCartID.ToLower()
                                        && item.ImportProductCard.ImportProductDate >= fromDate.Value && item.ImportProductCard.ImportProductDate <= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.Price * item.Quantity);
                }
                if (string.IsNullOrEmpty(ImportProductCartID))
                {
                    var filteredItems = GetDetailImportProductCard().Where(item => item.ImportProductCard.ImportProductDate >= fromDate.Value && item.ImportProductCard.ImportProductDate <= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.Price * item.Quantity);
                }
                else
                {
                    // Trường hợp không có điều kiện tìm kiếm, trả về tổng tiền của tất cả mục
                    return GetDetailImportProductCard()?.Sum(item => item.Price * item.Quantity) ?? 0;
                }
            }
        }
    }
}

﻿using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Data.Entity.Infrastructure.Design.Executor;

namespace MiniMarketManagement.DAL.Administrator
{
    internal class DAL_ReturnManagement
    {
        public List<DetailReturnProductCard> GetDetailReturnProductCard()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.DetailReturnProductCards.ToList();
        }
        public List<DetailReturnProductCard> SearchBy(string ReturnProductCartID, Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate)
        {
            MiniMarketDB db = new MiniMarketDB();
            var query = db.DetailReturnProductCards.AsQueryable();
            if (fromDate.Value >= toDate.Value)
            {
                if (!string.IsNullOrEmpty(ReturnProductCartID))
                {
                    query = query.Where(item => item.ReturnProductID == ReturnProductCartID);
                }
                if (fromDate.Value != null)
                {
                    query = query.Where(item => item.ReturnProductsCard.ReturnProductDate <= fromDate.Value);
                }
                if (toDate.Value != null)
                {
                    query = query.Where(item => item.ReturnProductsCard.ReturnProductDate >= toDate.Value);
                }
                return query.ToList();
            }
            else
            {
                if (!string.IsNullOrEmpty(ReturnProductCartID))
                {
                    query = query.Where(item => item.ReturnProductID == ReturnProductCartID);
                }
                if (fromDate.Value != null)
                {
                    query = query.Where(item => item.ReturnProductsCard.ReturnProductDate >= fromDate.Value);
                }
                if (toDate.Value != null)
                {
                    query = query.Where(item => item.ReturnProductsCard.ReturnProductDate <= toDate.Value);
                }
                return query.ToList();
            }
        }
        public double LoadDefaultReturnQuantity()
        {
            return GetDetailReturnProductCard()?.Sum(item => item.Quantity) ?? 0;
        }
        public double LoadDeafaultReturnProductMoney()
        {
            return GetDetailReturnProductCard()?.Sum(item => item.price * item.Quantity) ?? 0;
        }
        public double ReturnQuantity(string ReturnProductCartID, Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate)
        {
            if (fromDate.Value >= toDate.Value)
            {
                if (!string.IsNullOrEmpty(ReturnProductCartID))
                {
                    // Trường hợp có điều kiện tìm kiếm
                    var filteredItems = GetDetailReturnProductCard().Where(item => item.ReturnProductID.ToLower() == ReturnProductCartID.ToLower()
                                        && item.ReturnProductsCard.ReturnProductDate <= fromDate.Value && item.ReturnProductsCard.ReturnProductDate >= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.Quantity);
                }
                if (string.IsNullOrEmpty(ReturnProductCartID))
                {
                    var filteredItems = GetDetailReturnProductCard().Where(item => item.ReturnProductsCard.ReturnProductDate <= fromDate.Value && item.ReturnProductsCard.ReturnProductDate >= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.Quantity);
                }
                else
                {
                    // Trường hợp không có điều kiện tìm kiếm, trả về tổng quantity của tất cả mục
                    return GetDetailReturnProductCard()?.Sum(item => item.Quantity) ?? 0;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ReturnProductCartID))
                {
                    // Trường hợp có điều kiện tìm kiếm
                    var filteredItems = GetDetailReturnProductCard().Where(item => item.ReturnProductID.ToLower() == ReturnProductCartID.ToLower()
                                        && item.ReturnProductsCard.ReturnProductDate >= fromDate.Value && item.ReturnProductsCard.ReturnProductDate <= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.Quantity);
                }
                if (string.IsNullOrEmpty(ReturnProductCartID))
                {
                    var filteredItems = GetDetailReturnProductCard().Where(item => item.ReturnProductsCard.ReturnProductDate >= fromDate.Value && item.ReturnProductsCard.ReturnProductDate <= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.Quantity);
                }
                else
                {
                    // Trường hợp không có điều kiện tìm kiếm, trả về tổng quantity của tất cả mục
                    return GetDetailReturnProductCard()?.Sum(item => item.Quantity) ?? 0;
                }
            }
        }

        public double ReturnProductMoney(string ReturnProductCartID, Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate)
        {
            if (fromDate.Value >= toDate.Value)
            {
                if (!string.IsNullOrEmpty(ReturnProductCartID))
                {
                    // Trường hợp có điều kiện tìm kiếm
                    var filteredItems = GetDetailReturnProductCard().Where(item => item.ReturnProductID.ToLower() == ReturnProductCartID.ToLower()
                                        && item.ReturnProductsCard.ReturnProductDate <= fromDate.Value && item.ReturnProductsCard.ReturnProductDate >= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.price * item.Quantity);
                }
                if (string.IsNullOrEmpty(ReturnProductCartID))
                {
                    var filteredItems = GetDetailReturnProductCard().Where(item => item.ReturnProductsCard.ReturnProductDate <= fromDate.Value && item.ReturnProductsCard.ReturnProductDate >= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.price * item.Quantity);
                }
                else
                {
                    // Trường hợp không có điều kiện tìm kiếm, trả về tổng tiền của tất cả mục
                    return GetDetailReturnProductCard()?.Sum(item => item.price * item.Quantity) ?? 0;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(ReturnProductCartID))
                {
                    // Trường hợp có điều kiện tìm kiếm
                    var filteredItems = GetDetailReturnProductCard().Where(item => item.ReturnProductID.ToLower() == ReturnProductCartID.ToLower()
                                        && item.ReturnProductsCard.ReturnProductDate >= fromDate.Value && item.ReturnProductsCard.ReturnProductDate <= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.price * item.Quantity);
                }
                if (string.IsNullOrEmpty(ReturnProductCartID))
                {
                    var filteredItems = GetDetailReturnProductCard().Where(item => item.ReturnProductsCard.ReturnProductDate >= fromDate.Value && item.ReturnProductsCard.ReturnProductDate <= toDate.Value).ToList();
                    return filteredItems.Sum(item => item.price * item.Quantity);
                }
                else
                {
                    // Trường hợp không có điều kiện tìm kiếm, trả về tổng tiền của tất cả mục
                    return GetDetailReturnProductCard()?.Sum(item => item.price * item.Quantity) ?? 0;
                }
            }
        }
    }
}

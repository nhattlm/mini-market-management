﻿using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMarketManagement.DAL.Administrator
{
    public class DAL_CreateAccount
    {
        public List<Account> GetAllAccount()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Accounts.ToList();
        }
        public void NewAccount(Account account)
        {
            MiniMarketDB db = new MiniMarketDB();
            db.Accounts.Add(account);
            db.SaveChanges();
        }
        public void AccountUpdate(Account account)
        {
            MiniMarketDB db = new MiniMarketDB();
            db.Accounts.AddOrUpdate(account);
            db.SaveChanges();
        }
        public Account GetAccount() 
        {
            return new Account();
        }
        public List<Account> GetAccountSearch(string txt_search)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Accounts.Where(s =>
                s.Username.Contains(txt_search) ||
                s.EmployeeID.Contains(txt_search)
            ).ToList();
        }
        public void DeleteAccount(string username)
        {
            MiniMarketDB db = new MiniMarketDB();
            var account = db.Accounts.SingleOrDefault(s => s.Username == username);
            db.Accounts.Remove(account);
            db.SaveChanges();
        }
        public Employee GetEmployee_Have_EmployeeID(string txt_EmployeeID)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Employees.SingleOrDefault(s => s.EmployeeID == txt_EmployeeID);
        }
        public Account GetAccount_Have_Username(string txt_username)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Accounts.SingleOrDefault(S => S.Username.Contains(txt_username)
            );
        }
        public List<Employee> GetAllEmployee()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Employees.ToList();
        }
    }
}

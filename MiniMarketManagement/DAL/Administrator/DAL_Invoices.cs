﻿using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace MiniMarketManagement.DAL.Administrator
{
    internal class DAL_Invoices
    {
        public int CountLoyalCustomerInvoices()
        {
            MiniMarketDB db = new MiniMarketDB();
            return GetLoyalCustomerInvoices().Count();
        }
        public int CountCustomerInvoices()
        {
            MiniMarketDB db = new MiniMarketDB();
            return GetCustomerInvoices().Count();
        }
        public List<Invoice> GetInvoices()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Invoices.ToList();
        }
        public List<Invoice> GetLoyalCustomerInvoices()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Invoices.Where(invoice => invoice.CustomerID != null)
            .Join(db.Customers, invoice => invoice.CustomerID, customer => customer.CustomerID, (invoice, customer) => invoice).ToList();
        }
        public List<Invoice> GetCustomerInvoices()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Invoices.Where(invoice => invoice.CustomerID == null || !db.Customers
            .Any(customer => customer.CustomerID == invoice.CustomerID)).ToList();
        }
        public List<Invoice> Filter(Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate, string invoiceID, string customerID)
        {
            MiniMarketDB db = new MiniMarketDB();
            IQueryable<Invoice> query = db.Invoices;

            if (fromDate.Value != null && toDate.Value != null)
            {
                query = query.Where(invoice => invoice.Date >= fromDate.Value && invoice.Date <= toDate.Value);
            }

            if (!string.IsNullOrEmpty(invoiceID))
            {
                query = query.Where(invoice => invoice.InvoiceID == invoiceID);
            }

            if (!string.IsNullOrEmpty(customerID))
            {
                query = query.Where(invoice => invoice.CustomerID == customerID);
            }

            return query.ToList();
        }

    }
}

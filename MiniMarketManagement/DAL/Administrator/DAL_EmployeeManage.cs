﻿using Guna.UI2.WinForms;
using MiniMarketManagement.BLL.Administrator;
using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace MiniMarketManagement.DAL.Administrator
{
    public class DAL_EmployeeManage
    {
        public Employee GetEmployee(string txt_EmployeeID)
        {
            MiniMarketDB db = new MiniMarketDB();

            return db.Employees.SingleOrDefault(x => x.EmployeeID == txt_EmployeeID);
        }
        public Employee NewEmployee()
        {
            return new Employee();
        }
        public List<Employee> GetAllEmployee()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Employees.Where(employee => employee.Deleted != true).ToList();
        }
        public List<Position> GetAllPosition()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Positions.ToList();
        }
        public List<Employee> GetEmployeeSearch(string txt_Search)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Employees.Where(s => s.EmployeeID.Contains(txt_Search) ||
            s.EmployeeName.Contains(txt_Search) || 
            s.EmployeePhone.Contains(txt_Search)
            ).ToList();
        }
        public void EmployeeAddOrUpdate(Employee employee)
        {
            MiniMarketDB db = new MiniMarketDB();
            db.Employees.AddOrUpdate(employee);
            db.SaveChanges();
        }
        public int GetLastIndexOfEmployee()
        {
            MiniMarketDB db = new MiniMarketDB();
            bool check = !db.Employees.Any();
            if (check)
            {
                return 0;
            }
            else
            {
                var lastRow = db.Employees
                    .Select(item => new
                    {
                        NumberPart = item.EmployeeID.Substring(2)
                    })
                    .AsEnumerable()
                    .OrderByDescending(item => int.Parse(item.NumberPart))
                    .FirstOrDefault();
                return int.Parse(lastRow.NumberPart);
            }
        }
        public void DeleteEmployee(string txt_employeeID)
        {
            MiniMarketDB db = new MiniMarketDB();
            var employee = db.Employees.SingleOrDefault(s => s.EmployeeID == txt_employeeID);
            var isEmployeeWorking = db.Invoices.Any(invoice => invoice.EmployeeID == txt_employeeID)
                                   || db.ImportProductCards.Any(ImportProductCard => ImportProductCard.EmployeeID == txt_employeeID)
                                   || db.ReturnProductsCards.Any(ReturnProductsCards => ReturnProductsCards.EmployeeID == txt_employeeID);
            var isEmployeeHaveAccount = db.Accounts.Any(account => account.EmployeeID == txt_employeeID);
            var isEmployeeIsAdministrator = db.Employees.SingleOrDefault(s => s.EmployeeID == txt_employeeID).PositionID == "CV01";
            if (isEmployeeWorking)
            {
                employee.Deleted = true;
                db.SaveChanges();
                return;
            }
            else if (isEmployeeIsAdministrator)
            {
                if (isEmployeeHaveAccount)
                {
                    MessageBox.Show("Vui lòng xóa tài khoản nhân viên trước khi xóa nhân viên!");
                    return;
                }
            }
            db.Employees.Remove(employee);
            db.SaveChanges();
        }
    }
}

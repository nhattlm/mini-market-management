﻿using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.DAL.Administrator
{
    internal class DAL_LoyalCustomer
    {
        public List<Customer> GetCustomerList()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Customers.ToList();
        }
        public void UpdateCustomer(string CustomerID, string CustomerName, string CustomerPhoneNumber, float CustomerPoint)
        {
            MiniMarketDB db = new MiniMarketDB();
            var customer = db.Customers.FirstOrDefault(c => c.CustomerID == CustomerID);
            if (customer != null)
            {
                customer.CustomerName = CustomerName;
                customer.PhoneNumber = CustomerPhoneNumber;
                customer.Point = CustomerPoint;
                db.SaveChanges();
            }
        }
        public void DeleteCustomer(string CustomerID)
        {
            MiniMarketDB db = new MiniMarketDB();
            var customer = db.Customers.FirstOrDefault(c => c.CustomerID == CustomerID);
            if (customer != null)
            {
                db.Customers.Remove(customer);
                db.SaveChanges();
            }
        }
        public int TotalLoyalCustomer()
        {
            MiniMarketDB db = new MiniMarketDB();
            int total = db.Customers.Count();
            return total;
        }
        public int TotalCustomer()
        {
            MiniMarketDB db = new MiniMarketDB();
            int total = db.Invoices.Where(i => !db.Customers.Any(c => c.CustomerID == i.CustomerID)).Count();
            return total;
        }
        public List<Customer> SearchCustomersByIDOrPhoneNumber(string data)
        {
            MiniMarketDB db = new MiniMarketDB();
            if (!string.IsNullOrEmpty(data))
            {
                return db.Customers.Where(customer => customer.CustomerID.ToLower().Contains(data.ToLower()) || customer.PhoneNumber.Trim().Contains(data.ToLower())).ToList();
            }
            return GetCustomerList();
        }
    }
}

﻿using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace MiniMarketManagement.DAL.Storekeeper
{
    internal class DAL_AddSupplierAndProducts_Storekeeper
    {
        public List<ProductType> GetProductTypeslist()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.ProductTypes.ToList();
        } 
        public List<Product> GetProductslist()
        {
            MiniMarketDB dB = new MiniMarketDB();
            return dB.Products.ToList();
        }
        public Supplier GetSupplierBySupplierID(string supplierID)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Suppliers.SingleOrDefault(x => x.SupplierID == supplierID);
        }
        public List<Supplier> GetSupplierslist()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Suppliers.ToList();
        }
        public void AddSupplier(Supplier supplier)
        {
            MiniMarketDB db = new MiniMarketDB();
            db.Suppliers.Add(supplier);
            db.SaveChanges();
        }
        public Product GetProductbyProductID(string productID)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Products.SingleOrDefault(x=>x.ProductID == productID);
        }
        public void AddProduct(Product product)
        {
            MiniMarketDB db = new MiniMarketDB();
            db.Products.Add(product);
            db.SaveChanges();
        }
        public void UpdateSupplier(string supplierID, string SupplierName, string SupplierPhoneNumber)
        {
            MiniMarketDB db = new MiniMarketDB();
            var supplier = db.Suppliers.SingleOrDefault(x => x.SupplierID == supplierID);
            supplier.SupplierName = SupplierName;
            supplier.PhoneNumber = SupplierPhoneNumber;
            db.SaveChanges();
        }
        public void UpdateProduct(string productID, string productName,string productTypeID,double BuyPrice,string imgName)
        {
            MiniMarketDB db = new MiniMarketDB();
            var product = db.Products.SingleOrDefault( x => x.ProductID == productID);
            product.ProductName = productName;
            product.ProductTypeID = productTypeID;
            product.BuyPrice = BuyPrice;
            product.ProductImage = imgName;
            db.SaveChanges();
        }
    }
}

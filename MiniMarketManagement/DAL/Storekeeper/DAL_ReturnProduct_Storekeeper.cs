﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniMarketManagement.DAL.Entities;
namespace MiniMarketManagement.DAL.Storekeeper
{
    internal class DAL_ReturnProduct_Storekeeper
    {
        public List<DetailImportProductCard> GetListDetailImportProductCards()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.DetailImportProductCards.Where(x=> x.Returned == false).ToList();
        }
        public Repository GetRepositoryByProductIDandSupplierID(string productID, string SupplierID)
        {
            MiniMarketDB miniMarketDB = new MiniMarketDB();
            return miniMarketDB.Repositories.SingleOrDefault(x => x.ProductID == productID && x.SupplierID == SupplierID);
        }
        public ImportProductCard GetImportProductCardByImportProductID(string IProductID)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.ImportProductCards.SingleOrDefault(x => x.ImportProductID == IProductID);
        }
        public DetailImportProductCard GetDetailImportProductCardByIPCIDAndPID(string IPCID, string PID)
        {
            MiniMarketDB miniMarketDB = new MiniMarketDB();
            return miniMarketDB.DetailImportProductCards.SingleOrDefault(x => x.ProductID == PID && x.ImportProductID == IPCID);
        }
        public int GetLastIndexOfReturnProductCard()
        {
            MiniMarketDB context = new MiniMarketDB();
            bool check = !context.ReturnProductsCards.Any();
            if (check)
            {
                return 0;
            }
            else
            {
                var lastRow = context.ReturnProductsCards
                    .Select(item => new
                    {
                        NumberPart = item.ReturnProductID.Substring(2)
                    })
                    .AsEnumerable()
                    .OrderByDescending(item => int.Parse(item.NumberPart))
                    .FirstOrDefault();
                return int.Parse(lastRow.NumberPart);
            }
        }
        public void AddReturnProductCardAndDetailReturnProductCard(List<ReturnProductsCard> RPC, List<DetailReturnProductCard> DRPC)
        {
            MiniMarketDB context = new MiniMarketDB();

            foreach (ReturnProductsCard returnProductsCard in RPC)
            {
                context.ReturnProductsCards.Add(returnProductsCard);
                context.SaveChanges();
            }
            foreach (DetailReturnProductCard detail in DRPC)
            {
                context.DetailReturnProductCards.Add(detail);
                context.SaveChanges();
                var RIPC = context.DetailReturnProductCards.SingleOrDefault(x=> x.ReturnProductID == detail.ReturnProductID && x.ProductID == detail.ProductID);
                var DIPC = context.DetailImportProductCards.SingleOrDefault(x => x.ImportProductID == RIPC.ReturnProductsCard.ImportProductID 
                                                                            && x.ProductID == RIPC.ProductID);
                DIPC.Returned = true;
                var product = context.Repositories.SingleOrDefault(x => x.ProductID == detail.ProductID);
                if (product == null)
                {
                    continue;
                }
                else
                {
                    product.ProductQuantity -= detail.Quantity;
                    if (product.ProductQuantity <= 0)
                    {
                        context.Repositories.Remove(product);
                    }
                }
                context.SaveChanges();
            }
        }
        public List<DetailReturnProductCard> GetDetailReturnProductByID(string ReturnProductCardID)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.DetailReturnProductCards.Where(s =>
                    s.ReturnProductID == ReturnProductCardID
                    ).ToList();
        }
        public ReturnProductsCard GetReturnProductCardByReturnID(string ReturnProductID)
        {
            MiniMarketDB context = new MiniMarketDB();
            return context.ReturnProductsCards.SingleOrDefault(x => x.ReturnProductID == ReturnProductID);
        }
    }
}

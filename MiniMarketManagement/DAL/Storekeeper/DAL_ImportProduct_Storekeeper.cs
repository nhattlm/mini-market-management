﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Entities;
namespace MiniMarketManagement.DAL.Storekeeper
{
    internal class DAL_ImportProduct_Storekeeper
    {
        public List<Repository> GetProductListinRepository()
        {
            MiniMarketDB context = new MiniMarketDB();
            return context.Repositories.ToList();
        }
        public List<Product> GetProductListinProducts()
        {
            MiniMarketDB context = new MiniMarketDB();
            return context.Products.ToList();
        }
        public Product GetProductByProductID(string ProductID)
        {
            MiniMarketDB context = new MiniMarketDB();
            return context.Products.SingleOrDefault(x => x.ProductID == ProductID);
        }
        public Repository GetProductByProductIDinRepository(string productID)
        {
            MiniMarketDB context = new MiniMarketDB();
            return context.Repositories.SingleOrDefault(x => x.ProductID == productID);
        }
        public void AddImportProductCardAndDetailImportProductCard(List<ImportProductCard> IPC, List<DetailImportProductCard> DIPC)
        {
            MiniMarketDB context = new MiniMarketDB();
            foreach (ImportProductCard importProductCard in IPC)
            {
                context.ImportProductCards.Add(importProductCard);
                context.SaveChanges();
            }
            foreach (DetailImportProductCard detail in DIPC)
            {
                context.DetailImportProductCards.Add(detail);
                var product = context.Repositories.SingleOrDefault(x => x.ProductID == detail.ProductID);
                if (product == null)
                {
                    Repository repository = new Repository();
                    repository.ProductID = detail.ProductID;
                    repository.SellPrice = (detail.Price + 1000);
                    repository.ProductQuantity = detail.Quantity;
                    repository.SupplierID = detail.ImportProductCard.SupplierID;
                    context.Repositories.Add(repository);
                }
                else
                {
                    product.ProductQuantity += detail.Quantity;
                }
                context.SaveChanges();
            }
        }
        public int GetLastIndexOfImportProductCard()
        {
            MiniMarketDB context = new MiniMarketDB();
            bool check = !context.ImportProductCards.Any();
            if (check)
            {
                return 0;
            }
            else
            {
                var lastRow = context.ImportProductCards
                    .Select(item => new
                    {
                        NumberPart = item.ImportProductID.Substring(2)
                    })
                    .AsEnumerable()
                    .OrderByDescending(item => int.Parse(item.NumberPart))
                    .FirstOrDefault();
                return int.Parse(lastRow.NumberPart);
            }
        }
        public List<DetailImportProductCard> GetDetailImportProductByID(string ImportProductCardID)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.DetailImportProductCards.Where(s =>
                    s.ImportProductID == ImportProductCardID
                    ).ToList();
        }
        public ImportProductCard GetImportByImportID(string importID)
        {
            MiniMarketDB context = new MiniMarketDB();
            return context.ImportProductCards.SingleOrDefault(x => x.ImportProductID == importID);
        }
    }
}

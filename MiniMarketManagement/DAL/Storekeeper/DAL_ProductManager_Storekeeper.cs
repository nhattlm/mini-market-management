﻿using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMarketManagement.DAL.Storekeeper
{
    public class DAL_ProductManager_Storekeeper
    {
        public List<Product> GetAllProduct()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Products.ToList();
        }
        public List<Repository> GetProductID_Form_Repository(string PriductID)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Repositories.Where(s =>
                s.ProductID.Contains(PriductID)
            ).ToList();
        }
        public void RepositoryDelete(string PriductID)
        {
            MiniMarketDB db = new MiniMarketDB();
            var product = db.Repositories.SingleOrDefault(s =>
                s.ProductID == PriductID
            );
            db.Repositories.Remove( product );
            db.SaveChanges();
        }
        public List<Repository> GetAllRepository()
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Repositories.ToList();
        }
        public void RepositoryUpdate(Repository repository)
        {
            MiniMarketDB db = new MiniMarketDB();
            db.Repositories.AddOrUpdate(repository);
            db.SaveChanges();
        }

        public Product GetProduct(string txt_ProductID)
        {
            MiniMarketDB db = new MiniMarketDB();

            return db.Products.SingleOrDefault(x => x.ProductID == txt_ProductID);
        }
        public List<Repository> RepositorySearch(string txt_search)
        {
            MiniMarketDB db = new MiniMarketDB();
            return db.Repositories.Where(s =>
                s.ProductID.Contains(txt_search) ||
                s.Product.ProductName.Contains(txt_search) ||
                s.Product.ProductType.ProductTypeName.Contains(txt_search)
            ).ToList();
        }
    }
}

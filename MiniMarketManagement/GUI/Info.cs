﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.GUI
{
    public partial class frm_Info : Form
    {
        public frm_Info()
        {
            InitializeComponent();
        }
        private void VisitLink(LinkLabel link, string url)
        {
            link.LinkVisited = true;
            System.Diagnostics.Process.Start(url);
        }

        private void ll_Gitlab_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                VisitLink(ll_Gitlab, "https://gitlab.com/nhattlm/mini-market-management");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to open link that was clicked.");
            }
        }
    }
}

﻿namespace MiniMarketManagement.GUI.Storekeeper
{
    partial class frm_ReturnProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ReturnProduct));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_Search = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_ImportProductID = new Guna.UI2.WinForms.Guna2TextBox();
            this.pb_ProductImage = new Guna.UI2.WinForms.Guna2PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_IPDate = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_Supplier = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_TotalPrice = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_ProductName = new Guna.UI2.WinForms.Guna2TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txt_ProductID = new Guna.UI2.WinForms.Guna2TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txt_ImportProductAmount = new Guna.UI2.WinForms.Guna2TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cb_DisplayNearImportProductCard = new Guna.UI2.WinForms.Guna2ToggleSwitch();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.LargeIcon = new System.Windows.Forms.ImageList(this.components);
            this.dgv_ListImportProductCard = new Guna.UI2.WinForms.Guna2DataGridView();
            this.ImportProductID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product__Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductQuantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ImportProductDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_ListSelectedImportProductCard = new Guna.UI2.WinForms.Guna2DataGridView();
            this.Import_Product_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Buy_Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Import_Product_Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Return_Reason = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label13 = new System.Windows.Forms.Label();
            this.txt_Reason = new Guna.UI2.WinForms.Guna2TextBox();
            this.btn_DeleteSelected = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_Return = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_AddProduct = new Guna.UI2.WinForms.Guna2GradientButton();
            this.printReturnProductCardDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.printReturnProductCard = new System.Drawing.Printing.PrintDocument();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ProductImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListImportProductCard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListSelectedImportProductCard)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_Search);
            this.groupBox1.Controls.Add(this.txt_ImportProductID);
            this.groupBox1.Controls.Add(this.pb_ProductImage);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txt_IPDate);
            this.groupBox1.Controls.Add(this.txt_Supplier);
            this.groupBox1.Controls.Add(this.txt_TotalPrice);
            this.groupBox1.Controls.Add(this.txt_ProductName);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.txt_ProductID);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(7, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(503, 559);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin sản phẩm";
            // 
            // txt_Search
            // 
            this.txt_Search.Animated = true;
            this.txt_Search.BackColor = System.Drawing.Color.Transparent;
            this.txt_Search.BorderColor = System.Drawing.Color.Silver;
            this.txt_Search.BorderRadius = 15;
            this.txt_Search.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Search.DefaultText = "";
            this.txt_Search.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Search.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Search.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Search.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_Search.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.IconLeft = global::MiniMarketManagement.Properties.Resources.search;
            this.txt_Search.Location = new System.Drawing.Point(173, 32);
            this.txt_Search.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PasswordChar = '\0';
            this.txt_Search.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Search.PlaceholderText = "Mã phiếu/Mã/Tên sản phẩm";
            this.txt_Search.SelectedText = "";
            this.txt_Search.ShadowDecoration.BorderRadius = 15;
            this.txt_Search.ShadowDecoration.Enabled = true;
            this.txt_Search.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Search.Size = new System.Drawing.Size(287, 35);
            this.txt_Search.TabIndex = 10;
            this.txt_Search.TextChanged += new System.EventHandler(this.txt_Search_TextChanged);
            // 
            // txt_ImportProductID
            // 
            this.txt_ImportProductID.Animated = true;
            this.txt_ImportProductID.BackColor = System.Drawing.Color.Transparent;
            this.txt_ImportProductID.BorderColor = System.Drawing.Color.Silver;
            this.txt_ImportProductID.BorderRadius = 15;
            this.txt_ImportProductID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ImportProductID.DefaultText = "";
            this.txt_ImportProductID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ImportProductID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ImportProductID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ImportProductID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ImportProductID.Enabled = false;
            this.txt_ImportProductID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ImportProductID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ImportProductID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ImportProductID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ImportProductID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ImportProductID.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ImportProductID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ImportProductID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ImportProductID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ImportProductID.IconLeft = global::MiniMarketManagement.Properties.Resources.id__1_;
            this.txt_ImportProductID.Location = new System.Drawing.Point(173, 90);
            this.txt_ImportProductID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ImportProductID.Name = "txt_ImportProductID";
            this.txt_ImportProductID.PasswordChar = '\0';
            this.txt_ImportProductID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ImportProductID.PlaceholderText = "Mã phiếu nhập";
            this.txt_ImportProductID.SelectedText = "";
            this.txt_ImportProductID.ShadowDecoration.BorderRadius = 15;
            this.txt_ImportProductID.ShadowDecoration.Enabled = true;
            this.txt_ImportProductID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ImportProductID.Size = new System.Drawing.Size(287, 35);
            this.txt_ImportProductID.TabIndex = 10;
            // 
            // pb_ProductImage
            // 
            this.pb_ProductImage.BorderRadius = 5;
            this.pb_ProductImage.Image = global::MiniMarketManagement.Properties.Resources.image__2_;
            this.pb_ProductImage.ImageRotate = 0F;
            this.pb_ProductImage.InitialImage = global::MiniMarketManagement.Properties.Resources.image;
            this.pb_ProductImage.Location = new System.Drawing.Point(173, 429);
            this.pb_ProductImage.Name = "pb_ProductImage";
            this.pb_ProductImage.Size = new System.Drawing.Size(195, 123);
            this.pb_ProductImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_ProductImage.TabIndex = 12;
            this.pb_ProductImage.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(15, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 23);
            this.label9.TabIndex = 4;
            this.label9.Text = "Tìm kiếm";
            // 
            // txt_IPDate
            // 
            this.txt_IPDate.Animated = true;
            this.txt_IPDate.BackColor = System.Drawing.Color.Transparent;
            this.txt_IPDate.BorderColor = System.Drawing.Color.Silver;
            this.txt_IPDate.BorderRadius = 15;
            this.txt_IPDate.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_IPDate.DefaultText = "";
            this.txt_IPDate.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_IPDate.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_IPDate.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_IPDate.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_IPDate.Enabled = false;
            this.txt_IPDate.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_IPDate.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_IPDate.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_IPDate.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_IPDate.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_IPDate.ForeColor = System.Drawing.Color.DimGray;
            this.txt_IPDate.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_IPDate.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_IPDate.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_IPDate.IconLeft = global::MiniMarketManagement.Properties.Resources.calendar;
            this.txt_IPDate.Location = new System.Drawing.Point(173, 384);
            this.txt_IPDate.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_IPDate.Name = "txt_IPDate";
            this.txt_IPDate.PasswordChar = '\0';
            this.txt_IPDate.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_IPDate.PlaceholderText = "Ngày nhập";
            this.txt_IPDate.SelectedText = "";
            this.txt_IPDate.ShadowDecoration.BorderRadius = 15;
            this.txt_IPDate.ShadowDecoration.Enabled = true;
            this.txt_IPDate.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_IPDate.Size = new System.Drawing.Size(287, 35);
            this.txt_IPDate.TabIndex = 11;
            // 
            // txt_Supplier
            // 
            this.txt_Supplier.Animated = true;
            this.txt_Supplier.BackColor = System.Drawing.Color.Transparent;
            this.txt_Supplier.BorderColor = System.Drawing.Color.Silver;
            this.txt_Supplier.BorderRadius = 15;
            this.txt_Supplier.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Supplier.DefaultText = "";
            this.txt_Supplier.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Supplier.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Supplier.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Supplier.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Supplier.Enabled = false;
            this.txt_Supplier.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Supplier.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Supplier.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Supplier.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Supplier.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_Supplier.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Supplier.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Supplier.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Supplier.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Supplier.IconLeft = global::MiniMarketManagement.Properties.Resources.agreement;
            this.txt_Supplier.Location = new System.Drawing.Point(173, 322);
            this.txt_Supplier.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_Supplier.Name = "txt_Supplier";
            this.txt_Supplier.PasswordChar = '\0';
            this.txt_Supplier.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Supplier.PlaceholderText = "Nhà cung cấp";
            this.txt_Supplier.SelectedText = "";
            this.txt_Supplier.ShadowDecoration.BorderRadius = 15;
            this.txt_Supplier.ShadowDecoration.Enabled = true;
            this.txt_Supplier.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Supplier.Size = new System.Drawing.Size(287, 35);
            this.txt_Supplier.TabIndex = 11;
            // 
            // txt_TotalPrice
            // 
            this.txt_TotalPrice.Animated = true;
            this.txt_TotalPrice.BackColor = System.Drawing.Color.Transparent;
            this.txt_TotalPrice.BorderColor = System.Drawing.Color.Silver;
            this.txt_TotalPrice.BorderRadius = 15;
            this.txt_TotalPrice.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_TotalPrice.DefaultText = "";
            this.txt_TotalPrice.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_TotalPrice.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_TotalPrice.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_TotalPrice.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_TotalPrice.Enabled = false;
            this.txt_TotalPrice.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_TotalPrice.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_TotalPrice.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_TotalPrice.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_TotalPrice.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_TotalPrice.ForeColor = System.Drawing.Color.DimGray;
            this.txt_TotalPrice.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_TotalPrice.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_TotalPrice.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_TotalPrice.IconLeft = global::MiniMarketManagement.Properties.Resources.price_tag1;
            this.txt_TotalPrice.Location = new System.Drawing.Point(173, 264);
            this.txt_TotalPrice.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_TotalPrice.Name = "txt_TotalPrice";
            this.txt_TotalPrice.PasswordChar = '\0';
            this.txt_TotalPrice.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_TotalPrice.PlaceholderText = "Tổng tiền";
            this.txt_TotalPrice.SelectedText = "";
            this.txt_TotalPrice.ShadowDecoration.BorderRadius = 15;
            this.txt_TotalPrice.ShadowDecoration.Enabled = true;
            this.txt_TotalPrice.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_TotalPrice.Size = new System.Drawing.Size(287, 35);
            this.txt_TotalPrice.TabIndex = 11;
            // 
            // txt_ProductName
            // 
            this.txt_ProductName.Animated = true;
            this.txt_ProductName.BackColor = System.Drawing.Color.Transparent;
            this.txt_ProductName.BorderColor = System.Drawing.Color.Silver;
            this.txt_ProductName.BorderRadius = 15;
            this.txt_ProductName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ProductName.DefaultText = "";
            this.txt_ProductName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ProductName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ProductName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductName.Enabled = false;
            this.txt_ProductName.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ProductName.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductName.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductName.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ProductName.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductName.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductName.IconLeft = global::MiniMarketManagement.Properties.Resources.features2;
            this.txt_ProductName.Location = new System.Drawing.Point(173, 206);
            this.txt_ProductName.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ProductName.Name = "txt_ProductName";
            this.txt_ProductName.PasswordChar = '\0';
            this.txt_ProductName.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ProductName.PlaceholderText = "Tên sản phẩm";
            this.txt_ProductName.SelectedText = "";
            this.txt_ProductName.ShadowDecoration.BorderRadius = 15;
            this.txt_ProductName.ShadowDecoration.Enabled = true;
            this.txt_ProductName.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ProductName.Size = new System.Drawing.Size(287, 35);
            this.txt_ProductName.TabIndex = 11;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(15, 384);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 23);
            this.label12.TabIndex = 4;
            this.label12.Text = "Ngày nhập";
            // 
            // txt_ProductID
            // 
            this.txt_ProductID.Animated = true;
            this.txt_ProductID.BackColor = System.Drawing.Color.Transparent;
            this.txt_ProductID.BorderColor = System.Drawing.Color.Silver;
            this.txt_ProductID.BorderRadius = 15;
            this.txt_ProductID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ProductID.DefaultText = "";
            this.txt_ProductID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ProductID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ProductID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductID.Enabled = false;
            this.txt_ProductID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ProductID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ProductID.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductID.IconLeft = global::MiniMarketManagement.Properties.Resources.id__1_1;
            this.txt_ProductID.Location = new System.Drawing.Point(173, 148);
            this.txt_ProductID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ProductID.Name = "txt_ProductID";
            this.txt_ProductID.PasswordChar = '\0';
            this.txt_ProductID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ProductID.PlaceholderText = "Mã sản phẩm";
            this.txt_ProductID.SelectedText = "";
            this.txt_ProductID.ShadowDecoration.BorderRadius = 15;
            this.txt_ProductID.ShadowDecoration.Enabled = true;
            this.txt_ProductID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ProductID.Size = new System.Drawing.Size(287, 35);
            this.txt_ProductID.TabIndex = 11;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(15, 322);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(139, 23);
            this.label10.TabIndex = 4;
            this.label10.Text = "Nhà cung cấp";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = "Mã phiếu nhập";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 264);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(101, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "Tổng tiền";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(15, 206);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(143, 23);
            this.label11.TabIndex = 4;
            this.label11.Text = "Tên sản phẩm";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(15, 429);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 23);
            this.label8.TabIndex = 4;
            this.label8.Text = "Ảnh sản phẩm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(136, 23);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mã sản phẩm";
            // 
            // txt_ImportProductAmount
            // 
            this.txt_ImportProductAmount.Animated = true;
            this.txt_ImportProductAmount.BackColor = System.Drawing.Color.Transparent;
            this.txt_ImportProductAmount.BorderColor = System.Drawing.Color.Silver;
            this.txt_ImportProductAmount.BorderRadius = 15;
            this.txt_ImportProductAmount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ImportProductAmount.DefaultText = "";
            this.txt_ImportProductAmount.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ImportProductAmount.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ImportProductAmount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ImportProductAmount.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ImportProductAmount.Enabled = false;
            this.txt_ImportProductAmount.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ImportProductAmount.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ImportProductAmount.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ImportProductAmount.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ImportProductAmount.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ImportProductAmount.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ImportProductAmount.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ImportProductAmount.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ImportProductAmount.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ImportProductAmount.Location = new System.Drawing.Point(985, 651);
            this.txt_ImportProductAmount.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ImportProductAmount.Name = "txt_ImportProductAmount";
            this.txt_ImportProductAmount.PasswordChar = '\0';
            this.txt_ImportProductAmount.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ImportProductAmount.PlaceholderText = "";
            this.txt_ImportProductAmount.ReadOnly = true;
            this.txt_ImportProductAmount.SelectedText = "";
            this.txt_ImportProductAmount.ShadowDecoration.BorderRadius = 15;
            this.txt_ImportProductAmount.ShadowDecoration.Enabled = true;
            this.txt_ImportProductAmount.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ImportProductAmount.Size = new System.Drawing.Size(131, 33);
            this.txt_ImportProductAmount.TabIndex = 26;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(865, 656);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 23);
            this.label2.TabIndex = 17;
            this.label2.Text = "Thành tiền";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(864, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(252, 18);
            this.label1.TabIndex = 18;
            this.label1.Text = "Hiển thị hóa đơn nhập hàng gần nhất";
            // 
            // cb_DisplayNearImportProductCard
            // 
            this.cb_DisplayNearImportProductCard.Animated = true;
            this.cb_DisplayNearImportProductCard.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cb_DisplayNearImportProductCard.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cb_DisplayNearImportProductCard.CheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_DisplayNearImportProductCard.CheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_DisplayNearImportProductCard.Location = new System.Drawing.Point(820, 10);
            this.cb_DisplayNearImportProductCard.Name = "cb_DisplayNearImportProductCard";
            this.cb_DisplayNearImportProductCard.Size = new System.Drawing.Size(38, 20);
            this.cb_DisplayNearImportProductCard.TabIndex = 16;
            this.cb_DisplayNearImportProductCard.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_DisplayNearImportProductCard.UncheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_DisplayNearImportProductCard.UncheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_DisplayNearImportProductCard.UncheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_DisplayNearImportProductCard.CheckedChanged += new System.EventHandler(this.cb_DisplayNearestImportProductCard_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(690, 335);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(287, 23);
            this.label7.TabIndex = 19;
            this.label7.Text = "Danh sách sản phẩm đã chọn";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(572, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(206, 23);
            this.label6.TabIndex = 20;
            this.label6.Text = "Danh sách sản phẩm";
            // 
            // LargeIcon
            // 
            this.LargeIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("LargeIcon.ImageStream")));
            this.LargeIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.LargeIcon.Images.SetKeyName(0, "OIP.jpg");
            // 
            // dgv_ListImportProductCard
            // 
            this.dgv_ListImportProductCard.AllowUserToAddRows = false;
            this.dgv_ListImportProductCard.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dgv_ListImportProductCard.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_ListImportProductCard.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ListImportProductCard.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_ListImportProductCard.ColumnHeadersHeight = 18;
            this.dgv_ListImportProductCard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ListImportProductCard.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ImportProductID,
            this.ProductID,
            this.Product__Name,
            this.BuyPrice,
            this.ProductQuantity,
            this.SupplierName,
            this.ImportProductDate});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ListImportProductCard.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_ListImportProductCard.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListImportProductCard.Location = new System.Drawing.Point(516, 36);
            this.dgv_ListImportProductCard.Name = "dgv_ListImportProductCard";
            this.dgv_ListImportProductCard.ReadOnly = true;
            this.dgv_ListImportProductCard.RowHeadersVisible = false;
            this.dgv_ListImportProductCard.RowHeadersWidth = 51;
            this.dgv_ListImportProductCard.RowTemplate.Height = 24;
            this.dgv_ListImportProductCard.Size = new System.Drawing.Size(611, 283);
            this.dgv_ListImportProductCard.TabIndex = 30;
            this.dgv_ListImportProductCard.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ListImportProductCard.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_ListImportProductCard.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_ListImportProductCard.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_ListImportProductCard.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_ListImportProductCard.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_ListImportProductCard.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListImportProductCard.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_ListImportProductCard.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_ListImportProductCard.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ListImportProductCard.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_ListImportProductCard.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ListImportProductCard.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_ListImportProductCard.ThemeStyle.ReadOnly = true;
            this.dgv_ListImportProductCard.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ListImportProductCard.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_ListImportProductCard.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ListImportProductCard.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ListImportProductCard.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_ListImportProductCard.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListImportProductCard.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ListImportProductCard.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListImportProductCard_CellClick);
            this.dgv_ListImportProductCard.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListImportProductCard_CellDoubleClick);
            // 
            // ImportProductID
            // 
            this.ImportProductID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ImportProductID.FillWeight = 120F;
            this.ImportProductID.HeaderText = "Mã phiếu nhập";
            this.ImportProductID.MinimumWidth = 6;
            this.ImportProductID.Name = "ImportProductID";
            this.ImportProductID.ReadOnly = true;
            // 
            // ProductID
            // 
            this.ProductID.FillWeight = 110F;
            this.ProductID.HeaderText = "Mã sản phẩm";
            this.ProductID.MinimumWidth = 6;
            this.ProductID.Name = "ProductID";
            this.ProductID.ReadOnly = true;
            // 
            // Product__Name
            // 
            this.Product__Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Product__Name.FillWeight = 120F;
            this.Product__Name.HeaderText = "Tên sản phẩm";
            this.Product__Name.MinimumWidth = 6;
            this.Product__Name.Name = "Product__Name";
            this.Product__Name.ReadOnly = true;
            // 
            // BuyPrice
            // 
            this.BuyPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BuyPrice.FillWeight = 80F;
            this.BuyPrice.HeaderText = "Giá mua";
            this.BuyPrice.MinimumWidth = 6;
            this.BuyPrice.Name = "BuyPrice";
            this.BuyPrice.ReadOnly = true;
            // 
            // ProductQuantity
            // 
            this.ProductQuantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProductQuantity.FillWeight = 80F;
            this.ProductQuantity.HeaderText = "Số lượng";
            this.ProductQuantity.MinimumWidth = 6;
            this.ProductQuantity.Name = "ProductQuantity";
            this.ProductQuantity.ReadOnly = true;
            // 
            // SupplierName
            // 
            this.SupplierName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SupplierName.FillWeight = 120F;
            this.SupplierName.HeaderText = "Nhà cung cấp";
            this.SupplierName.MinimumWidth = 6;
            this.SupplierName.Name = "SupplierName";
            this.SupplierName.ReadOnly = true;
            // 
            // ImportProductDate
            // 
            this.ImportProductDate.HeaderText = "Ngày nhập";
            this.ImportProductDate.MinimumWidth = 6;
            this.ImportProductDate.Name = "ImportProductDate";
            this.ImportProductDate.ReadOnly = true;
            // 
            // dgv_ListSelectedImportProductCard
            // 
            this.dgv_ListSelectedImportProductCard.AllowUserToAddRows = false;
            this.dgv_ListSelectedImportProductCard.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.dgv_ListSelectedImportProductCard.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_ListSelectedImportProductCard.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ListSelectedImportProductCard.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_ListSelectedImportProductCard.ColumnHeadersHeight = 18;
            this.dgv_ListSelectedImportProductCard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ListSelectedImportProductCard.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Import_Product_ID,
            this.Product_ID,
            this.Product_Name,
            this.Buy_Price,
            this.Product_Quantity,
            this.Supplier_Name,
            this.Import_Product_Date,
            this.Return_Reason});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ListSelectedImportProductCard.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_ListSelectedImportProductCard.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListSelectedImportProductCard.Location = new System.Drawing.Point(516, 361);
            this.dgv_ListSelectedImportProductCard.Name = "dgv_ListSelectedImportProductCard";
            this.dgv_ListSelectedImportProductCard.ReadOnly = true;
            this.dgv_ListSelectedImportProductCard.RowHeadersVisible = false;
            this.dgv_ListSelectedImportProductCard.RowHeadersWidth = 51;
            this.dgv_ListSelectedImportProductCard.RowTemplate.Height = 24;
            this.dgv_ListSelectedImportProductCard.Size = new System.Drawing.Size(611, 283);
            this.dgv_ListSelectedImportProductCard.TabIndex = 30;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListSelectedImportProductCard.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ListSelectedImportProductCard.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.ReadOnly = true;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ListSelectedImportProductCard.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ListSelectedImportProductCard.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_ListSelectedImportProductCard.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListSelectedImportProductCard.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ListSelectedImportProductCard.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListSelectedImportProductCard_CellClick);
            this.dgv_ListSelectedImportProductCard.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListImportProductCard_CellDoubleClick);
            // 
            // Import_Product_ID
            // 
            this.Import_Product_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Import_Product_ID.FillWeight = 120F;
            this.Import_Product_ID.HeaderText = "Mã phiếu nhập";
            this.Import_Product_ID.MinimumWidth = 6;
            this.Import_Product_ID.Name = "Import_Product_ID";
            this.Import_Product_ID.ReadOnly = true;
            // 
            // Product_ID
            // 
            this.Product_ID.FillWeight = 120F;
            this.Product_ID.HeaderText = "Mã sản phẩm";
            this.Product_ID.MinimumWidth = 6;
            this.Product_ID.Name = "Product_ID";
            this.Product_ID.ReadOnly = true;
            // 
            // Product_Name
            // 
            this.Product_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Product_Name.FillWeight = 120F;
            this.Product_Name.HeaderText = "Tên sản phẩm";
            this.Product_Name.MinimumWidth = 6;
            this.Product_Name.Name = "Product_Name";
            this.Product_Name.ReadOnly = true;
            // 
            // Buy_Price
            // 
            this.Buy_Price.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Buy_Price.FillWeight = 70F;
            this.Buy_Price.HeaderText = "Giá mua";
            this.Buy_Price.MinimumWidth = 6;
            this.Buy_Price.Name = "Buy_Price";
            this.Buy_Price.ReadOnly = true;
            // 
            // Product_Quantity
            // 
            this.Product_Quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Product_Quantity.FillWeight = 70F;
            this.Product_Quantity.HeaderText = "Số lượng";
            this.Product_Quantity.MinimumWidth = 6;
            this.Product_Quantity.Name = "Product_Quantity";
            this.Product_Quantity.ReadOnly = true;
            // 
            // Supplier_Name
            // 
            this.Supplier_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Supplier_Name.FillWeight = 120F;
            this.Supplier_Name.HeaderText = "Nhà cung cấp";
            this.Supplier_Name.MinimumWidth = 6;
            this.Supplier_Name.Name = "Supplier_Name";
            this.Supplier_Name.ReadOnly = true;
            // 
            // Import_Product_Date
            // 
            this.Import_Product_Date.FillWeight = 90F;
            this.Import_Product_Date.HeaderText = "Ngày nhập";
            this.Import_Product_Date.MinimumWidth = 6;
            this.Import_Product_Date.Name = "Import_Product_Date";
            this.Import_Product_Date.ReadOnly = true;
            // 
            // Return_Reason
            // 
            this.Return_Reason.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Return_Reason.FillWeight = 70F;
            this.Return_Reason.HeaderText = "Lý do";
            this.Return_Reason.MinimumWidth = 6;
            this.Return_Reason.Name = "Return_Reason";
            this.Return_Reason.ReadOnly = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(96, 582);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 23);
            this.label13.TabIndex = 13;
            this.label13.Text = "Lý do";
            // 
            // txt_Reason
            // 
            this.txt_Reason.Animated = true;
            this.txt_Reason.BackColor = System.Drawing.Color.Transparent;
            this.txt_Reason.BorderColor = System.Drawing.Color.Silver;
            this.txt_Reason.BorderRadius = 15;
            this.txt_Reason.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Reason.DefaultText = "";
            this.txt_Reason.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Reason.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Reason.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Reason.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Reason.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Reason.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Reason.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Reason.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Reason.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_Reason.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Reason.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Reason.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Reason.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Reason.IconLeft = global::MiniMarketManagement.Properties.Resources.question_mark;
            this.txt_Reason.Location = new System.Drawing.Point(180, 582);
            this.txt_Reason.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_Reason.Name = "txt_Reason";
            this.txt_Reason.PasswordChar = '\0';
            this.txt_Reason.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Reason.PlaceholderText = "Lý do";
            this.txt_Reason.SelectedText = "";
            this.txt_Reason.ShadowDecoration.BorderRadius = 15;
            this.txt_Reason.ShadowDecoration.Enabled = true;
            this.txt_Reason.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Reason.Size = new System.Drawing.Size(287, 35);
            this.txt_Reason.TabIndex = 14;
            // 
            // btn_DeleteSelected
            // 
            this.btn_DeleteSelected.BackColor = System.Drawing.Color.Transparent;
            this.btn_DeleteSelected.BorderRadius = 10;
            this.btn_DeleteSelected.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_DeleteSelected.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_DeleteSelected.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_DeleteSelected.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_DeleteSelected.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_DeleteSelected.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_DeleteSelected.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_DeleteSelected.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_DeleteSelected.ForeColor = System.Drawing.Color.White;
            this.btn_DeleteSelected.Image = global::MiniMarketManagement.Properties.Resources.empty_cart;
            this.btn_DeleteSelected.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_DeleteSelected.Location = new System.Drawing.Point(414, 642);
            this.btn_DeleteSelected.Name = "btn_DeleteSelected";
            this.btn_DeleteSelected.ShadowDecoration.BorderRadius = 10;
            this.btn_DeleteSelected.ShadowDecoration.Enabled = true;
            this.btn_DeleteSelected.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_DeleteSelected.Size = new System.Drawing.Size(85, 37);
            this.btn_DeleteSelected.TabIndex = 22;
            this.btn_DeleteSelected.Text = "Xóa";
            this.btn_DeleteSelected.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_DeleteSelected.Click += new System.EventHandler(this.btn_DeleteSelected_Click);
            // 
            // btn_Return
            // 
            this.btn_Return.BackColor = System.Drawing.Color.Transparent;
            this.btn_Return.BorderRadius = 10;
            this.btn_Return.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Return.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Return.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Return.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Return.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Return.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Return.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Return.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Return.ForeColor = System.Drawing.Color.White;
            this.btn_Return.Image = global::MiniMarketManagement.Properties.Resources.package;
            this.btn_Return.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Return.Location = new System.Drawing.Point(23, 642);
            this.btn_Return.Name = "btn_Return";
            this.btn_Return.ShadowDecoration.BorderRadius = 10;
            this.btn_Return.ShadowDecoration.Enabled = true;
            this.btn_Return.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Return.Size = new System.Drawing.Size(123, 37);
            this.btn_Return.TabIndex = 23;
            this.btn_Return.Text = "Trả hàng";
            this.btn_Return.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_Return.Click += new System.EventHandler(this.btn_Return_Click);
            // 
            // btn_AddProduct
            // 
            this.btn_AddProduct.BackColor = System.Drawing.Color.Transparent;
            this.btn_AddProduct.BorderRadius = 10;
            this.btn_AddProduct.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_AddProduct.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_AddProduct.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_AddProduct.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_AddProduct.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_AddProduct.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_AddProduct.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_AddProduct.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_AddProduct.ForeColor = System.Drawing.Color.White;
            this.btn_AddProduct.Image = global::MiniMarketManagement.Properties.Resources.add_to_cart__1_;
            this.btn_AddProduct.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_AddProduct.Location = new System.Drawing.Point(232, 642);
            this.btn_AddProduct.Name = "btn_AddProduct";
            this.btn_AddProduct.ShadowDecoration.BorderRadius = 10;
            this.btn_AddProduct.ShadowDecoration.Enabled = true;
            this.btn_AddProduct.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_AddProduct.Size = new System.Drawing.Size(96, 37);
            this.btn_AddProduct.TabIndex = 24;
            this.btn_AddProduct.Text = "Thêm";
            this.btn_AddProduct.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_AddProduct.Click += new System.EventHandler(this.btn_AddProduct_Click);
            // 
            // printReturnProductCardDialog
            // 
            this.printReturnProductCardDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printReturnProductCardDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printReturnProductCardDialog.ClientSize = new System.Drawing.Size(400, 300);
            this.printReturnProductCardDialog.Enabled = true;
            this.printReturnProductCardDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("printReturnProductCardDialog.Icon")));
            this.printReturnProductCardDialog.Name = "printReturnProductCardDialog";
            this.printReturnProductCardDialog.Visible = false;
            // 
            // frm_ReturnProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.txt_Reason);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.dgv_ListSelectedImportProductCard);
            this.Controls.Add(this.dgv_ListImportProductCard);
            this.Controls.Add(this.btn_DeleteSelected);
            this.Controls.Add(this.btn_Return);
            this.Controls.Add(this.btn_AddProduct);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txt_ImportProductAmount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cb_DisplayNearImportProductCard);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_ReturnProduct";
            this.Text = "ReturnProduct";
            this.Load += new System.EventHandler(this.frm_ReturnProduct_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ProductImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListImportProductCard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListSelectedImportProductCard)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2GradientButton btn_DeleteSelected;
        private Guna.UI2.WinForms.Guna2TextBox txt_Search;
        private Guna.UI2.WinForms.Guna2TextBox txt_ImportProductID;
        private Guna.UI2.WinForms.Guna2PictureBox pb_ProductImage;
        private Guna.UI2.WinForms.Guna2TextBox txt_ProductID;
        private Guna.UI2.WinForms.Guna2GradientButton btn_Return;
        private Guna.UI2.WinForms.Guna2GradientButton btn_AddProduct;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private Guna.UI2.WinForms.Guna2TextBox txt_ImportProductAmount;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2ToggleSwitch cb_DisplayNearImportProductCard;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ImageList LargeIcon;
        private Guna.UI2.WinForms.Guna2TextBox txt_Supplier;
        private Guna.UI2.WinForms.Guna2TextBox txt_TotalPrice;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_ListImportProductCard;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_ListSelectedImportProductCard;
        private Guna.UI2.WinForms.Guna2TextBox txt_ProductName;
        private System.Windows.Forms.Label label11;
        private Guna.UI2.WinForms.Guna2TextBox txt_IPDate;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImportProductID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product__Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuyPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductQuantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImportProductDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn Import_Product_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Buy_Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Import_Product_Date;
        private System.Windows.Forms.DataGridViewTextBoxColumn Return_Reason;
        private Guna.UI2.WinForms.Guna2TextBox txt_Reason;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PrintPreviewDialog printReturnProductCardDialog;
        private System.Drawing.Printing.PrintDocument printReturnProductCard;
    }
}
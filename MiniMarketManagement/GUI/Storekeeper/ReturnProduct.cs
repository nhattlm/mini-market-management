﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniMarketManagement.BLL.Storekeeper;
using MiniMarketManagement.DAL.Entities;
namespace MiniMarketManagement.GUI.Storekeeper
{
    public partial class frm_ReturnProduct : Form
    {
        public frm_ReturnProduct()
        {
            InitializeComponent();
        }

        private void frm_ReturnProduct_Load(object sender, EventArgs e)
        {
            BLL_ReturnProduct bLL_ReturnProduct = new BLL_ReturnProduct();
            bLL_ReturnProduct.Filldgv_ListImportProductCard(dgv_ListImportProductCard);
        }

        private void dgv_ListImportProductCard_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BLL_ReturnProduct bLL_ReturnProduct = new BLL_ReturnProduct();
            var row = e.RowIndex;
            if (row < 0)
            {
                return;
            }
            else
            {
                Image image;
                string ImportProductID;
                string product_ID;
                string product_Name;
                string totalPrice;
                string SupplierName;
                string IPDate;
                bLL_ReturnProduct.FillAllTxtBoxExceptTxtSearch(row, dgv_ListImportProductCard, out image, out ImportProductID, out product_ID,
                                                               out product_Name, out totalPrice, out SupplierName,out IPDate, "ImportProductID",
                                                               "ProductID", "Product__Name", "BuyPrice", "ProductQuantity", "SupplierName", "ImportProductDate");
                txt_ImportProductID.Text = ImportProductID;
                txt_ProductID.Text = product_ID;
                txt_ProductName.Text = product_Name;
                txt_TotalPrice.Text = totalPrice;
                txt_Supplier.Text = SupplierName;
                txt_IPDate.Text = IPDate;
                pb_ProductImage.Image = image;
            }
        }

        private void dgv_ListImportProductCard_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BLL_ReturnProduct bLL_ReturnProduct = new BLL_ReturnProduct();
            dgv_ListImportProductCard.ClearSelection();
            string ImportProductID;
            string ProductID;
            string ProductName;
            string totalPrice;
            string SupplierName;
            string IPDate;
            string reason;
            Image image;
            bLL_ReturnProduct.ClearInputFields(out image, out ImportProductID, out ProductID, out ProductName,out totalPrice, out SupplierName,out IPDate, out reason);
            pb_ProductImage.Image = image;
            txt_ImportProductID.Text = ImportProductID;
            txt_ProductID.Text = ProductID;
            txt_ProductName.Text = ProductName;
            txt_TotalPrice.Text = totalPrice;
            txt_Supplier.Text = SupplierName;
            txt_IPDate.Text = IPDate;
            txt_Reason.Text = reason; 
        }

        private void dgv_ListSelectedImportProductCard_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BLL_ReturnProduct bLL_ReturnProduct = new BLL_ReturnProduct();
            var row = e.RowIndex;
            if (row < 0)
            {
                return;
            }
            else
            {
                Image image;
                string ImportProductID;
                string product_ID;
                string product_Name;
                string totalPrice;
                string SupplierName;
                string IPDate;
                bLL_ReturnProduct.FillAllTxtBoxExceptTxtSearch(row, dgv_ListSelectedImportProductCard, out image, out ImportProductID, out product_ID,
                                                               out product_Name, out totalPrice, out SupplierName,out IPDate, "Import_Product_ID",
                                                               "Product_ID", "Product_Name", "Buy_Price", "Product_Quantity", "Supplier_Name", "Import_Product_Date");
                txt_ImportProductID.Text = ImportProductID;
                txt_ProductID.Text = product_ID;
                txt_ProductName.Text = product_Name;
                txt_TotalPrice.Text = totalPrice;
                txt_Supplier.Text = SupplierName;
                txt_IPDate.Text = IPDate;
                pb_ProductImage.Image = image;
                txt_Reason.Text = dgv_ListSelectedImportProductCard.Rows[row].Cells["Return_Reason"].Value.ToString();
            }
        }

        private void txt_Search_TextChanged(object sender, EventArgs e)
        {
            BLL_ReturnProduct bLL_ReturnProduct = new BLL_ReturnProduct();
            var txt = txt_Search.Text;
            bLL_ReturnProduct.SearchImportProductCard(txt, dgv_ListImportProductCard, "ImportProductID", "ProductID", "Product__Name");
            bLL_ReturnProduct.SearchImportProductCard(txt, dgv_ListSelectedImportProductCard, "Import_Product_ID", "Product_ID", "Product_Name");
        }
        bool cb = false;
        private void cb_DisplayNearestImportProductCard_CheckedChanged(object sender, EventArgs e)
        {
            BLL_ReturnProduct bLL_ReturnProduct = new BLL_ReturnProduct();
            if (!cb)
            {
                bLL_ReturnProduct.DisplayNearestIPC(dgv_ListImportProductCard, "ImportProductID");
                cb = true;
            }
            else
            {
                bLL_ReturnProduct.Filldgv_ListImportProductCard(dgv_ListImportProductCard);
                cb = false;
            }
        }

        private void btn_AddProduct_Click(object sender, EventArgs e)
        {
            BLL_ReturnProduct bLL_ReturnProduct = new BLL_ReturnProduct();
            string IPCID = txt_ImportProductID.Text.Trim();
            string PID = txt_ProductID.Text.Trim();
            Image image;
            string product_Name;
            string totalPrice;
            string SupplierName;
            string IPDate;
            string reason=txt_Reason.Text;
            if (bLL_ReturnProduct.importDIPCToSelectedDIPCByIPCID(dgv_ListSelectedImportProductCard, IPCID, PID, reason) == 1)
            {
                bLL_ReturnProduct.ClearInputFields(out image, out IPCID, out PID, out product_Name, out totalPrice, out SupplierName,out IPDate, out reason);
                pb_ProductImage.Image = image;
                txt_ImportProductID.Text = IPCID;
                txt_ProductID.Text = PID;
                txt_ProductName.Text = ProductName;
                txt_TotalPrice.Text = totalPrice;
                txt_Supplier.Text = SupplierName;
                txt_IPDate.Text = IPDate;
                txt_Reason.Text = reason;
                txt_ImportProductAmount.Text = bLL_ReturnProduct.sumBuyPriceOfSelectedIPCList(dgv_ListSelectedImportProductCard, "Buy_Price", "Product_Quantity").ToString();
            }
            else
            {
                return;
            }
        }

        private void btn_DeleteSelected_Click(object sender, EventArgs e)
        {
            BLL_ReturnProduct bLL_ReturnProduct = new BLL_ReturnProduct();
            string IPCID = txt_ImportProductID.Text.Trim();
            string PID = txt_ProductID.Text.Trim();
            Image image;
            string product_Name;
            string totalPrice;
            string SupplierName;
            string IPDate;
            string reason;
            if (bLL_ReturnProduct.DeleteDIPC(dgv_ListSelectedImportProductCard, IPCID, PID) == 1)
            {
                bLL_ReturnProduct.ClearInputFields(out image, out IPCID, out PID, out product_Name, out totalPrice, out SupplierName, out IPDate,out reason);
                pb_ProductImage.Image = image;
                txt_ImportProductID.Text = IPCID;
                txt_ProductID.Text = PID;
                txt_ProductName.Text = ProductName;
                txt_TotalPrice.Text = totalPrice;
                txt_Supplier.Text = SupplierName;
                txt_IPDate.Text = IPDate;
                txt_Reason.Text = reason;
                txt_ImportProductAmount.Text = bLL_ReturnProduct.sumBuyPriceOfSelectedIPCList(dgv_ListSelectedImportProductCard, "Buy_Price", "Product_Quantity").ToString();
            }
            else
            {
                return;
            }
        }
        ReturnProductsCard returnProductsCard = new ReturnProductsCard();
        private void btn_Return_Click(object sender, EventArgs e)
        {
            BLL_ReturnProduct bLL_ReturnProduct = new BLL_ReturnProduct();
            List<ReturnProductsCard> listRPC = new List<ReturnProductsCard>();
            if (bLL_ReturnProduct.AddSelectedDIPCToListRPCAndDRPC(dgv_ListSelectedImportProductCard, dgv_ListImportProductCard) == 1)
            {
                bLL_ReturnProduct.GetListReturnProductCards(out listRPC);
                foreach (var i in listRPC)
                {
                    returnProductsCard = i;
                    PrintDocument printDocument = new PrintDocument();
                    printDocument.PrintPage += new PrintPageEventHandler(CreatePrintReturnProductCard);

                    // Hiển thị hộp thoại xem trước và in
                    PrintPreviewDialog printPreviewDialog = new PrintPreviewDialog();
                    printPreviewDialog.Document = printDocument;
                    printPreviewDialog.ShowDialog();

                }
                txt_ImportProductAmount.Text = "0";
                txt_Search.Text = "";
            }
            else
            {
                return;
            }
        }
        private void CreatePrintReturnProductCard(Object sender, PrintPageEventArgs e)
        {
            BLL_ReturnProduct bLL_ReturnProduct = new BLL_ReturnProduct();
            bLL_ReturnProduct.CreateImport(sender, e, returnProductsCard, printReturnProductCard, printReturnProductCardDialog);

        }
    }
}

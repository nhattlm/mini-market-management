﻿using Guna.UI2.WinForms;
using MiniMarketManagement.BLL.Administrator;
using MiniMarketManagement.DAL.Administrator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.GUI.Storekeeper
{
    public partial class frm_WarehouseManagement : Form
    {
        ShowChildrenForm showChildrenForm = new ShowChildrenForm();
        BLL_EmployeeManage BLL_EmployeeManage = new BLL_EmployeeManage();
        public frm_WarehouseManagement()
        {
            InitializeComponent();
        }
        private void frm_WarehouseManagement_Load(object sender, EventArgs e)
        {
            btn_AddSupplierAndProducts_Click(sender, e);
            lb_EmployeeName.Text = showEmployeeName();
        }
        private string showEmployeeName()
        {
            return "Xin chào " + BLL_EmployeeManage.GetNameEmployeesByUsername();
        }
        private void btn_AddSupplierAndProducts_Click(object sender, EventArgs e)
        {
            frm_AddSupplierAndProducts formAddSupplierAndProducts = new frm_AddSupplierAndProducts();
            showChildrenForm.ShowForm(formAddSupplierAndProducts, btn_AddSupplierAndProducts, panel_Main);
            panel_Exit.Visible = false;
        }

        private void btn_ImportProduct_Click(object sender, EventArgs e)
        {
            frm_ImportProduct formImportProduct = new frm_ImportProduct();
            showChildrenForm.ShowForm(formImportProduct, btn_ImportProduct, panel_Main);
            panel_Exit.Visible = false;
        }

        private void btn_ProductManagement_Click(object sender, EventArgs e)
        {
            frm_ProductManager formProductManagement = new frm_ProductManager();
            showChildrenForm.ShowForm(formProductManagement, btn_ProductManagement, panel_Main);
            panel_Exit.Visible = false;
        }
        bool isPanelVisible = false;
        Guna2Button currentButton;
        private void btn_Exit_Click(object sender, EventArgs e)
        {
            currentButton = btn_Exit;
            if (isPanelVisible)
            {
                // Nếu panel đang hiển thị, ẩn nó
                panel_Exit.Visible = false;
                isPanelVisible = false;
                currentButton.Checked = false;
            }
            else
            {
                // Nếu panel không hiển thị, hiển thị nó
                panel_Exit.Visible = true;
                isPanelVisible = true;
            }
        }

        private void btn_ReturnProduct_Click(object sender, EventArgs e)
        {
            frm_ReturnProduct formReturnProduct = new frm_ReturnProduct();
            showChildrenForm.ShowForm(formReturnProduct, btn_ReturnProduct, panel_Main);
            panel_Exit.Visible = false;
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            try
            {
                this.Dispose();
                this.Close();
            }
            catch (Exception) { }
        }

        private void btn_SignOut_Click(object sender, EventArgs e)
        {
            try
            {
                frm_Login loginForm = new frm_Login();
                loginForm.Show();
                this.Hide();
            }
            catch (Exception) { }
        }

        private void btn_Minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void ibtn_Logo_DoubleClick(object sender, EventArgs e)
        {
            frm_Info info = new frm_Info();
            info.Show();
        }
    }
}

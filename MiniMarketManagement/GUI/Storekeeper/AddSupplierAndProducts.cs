﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Guna.UI2.WinForms;
using MiniMarketManagement.BLL.Storekeeper;
using MiniMarketManagement.DAL.Entities;

namespace MiniMarketManagement.GUI.Storekeeper
{
    public partial class frm_AddSupplierAndProducts : Form
    {
        bool cbS = false;
        bool cbP = false;
        public frm_AddSupplierAndProducts()
        {
            InitializeComponent();
        }

        private void frm_DisplayProduct_Load(object sender, EventArgs e)
        {
            BLL_AddSupplierAndProducts bLL_DisplayProduct = new BLL_AddSupplierAndProducts();
            bLL_DisplayProduct.FillDGVProduct(dgv_ListProduct);
            bLL_DisplayProduct.FillCBProductType(cmb_ProductType);
            bLL_DisplayProduct.FillDGVSupplier(dgv_ListSupplier);
            cmb_ProductType.SelectedIndex = 0;
        }
        private void dgv_ListProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var row = e.RowIndex;
            BLL_AddSupplierAndProducts bLL_AddSupplierAndProducts = new BLL_AddSupplierAndProducts();
            Image image;
            string SupplierID;
            string SupplierName;
            string SupplierPhoneNumber;
            string ProductID;
            string ProductName;
            string ProductTypeName;
            string BuyPrice;
            if (row < 0)
            {
                return;
            }
            else
            {
                pb_ProductImage.Image.Dispose();
                if (cbP == true)
                {
                    bLL_AddSupplierAndProducts.FillAllTxtAndPicboxOfProductExceptTxtSearchAndTxtSupplier(row, dgv_ListProduct, out image, out SupplierID, out SupplierName, out SupplierPhoneNumber,
                                                                        out ProductID, out ProductName, out ProductTypeName, out BuyPrice);
                    txt_SupplierID.Text = SupplierID;
                    txt_SupplierName.Text = SupplierName;
                    txt_SupplierPhoneNumber.Text = SupplierPhoneNumber;
                    txt_ProductID.Text = ProductID;
                    txt_ProductName.Text = ProductName;
                    cmb_ProductType.Text = ProductTypeName;
                    txt_BuyPrice.Text = BuyPrice;
                    pb_ProductImage.Image = image;
                }
                if (cbS == true)
                {
                    bLL_AddSupplierAndProducts.ClearInputFields(out image, out SupplierID, out SupplierName, out SupplierPhoneNumber, out ProductID,
                                                out ProductName, out ProductTypeName, out BuyPrice);
                    txt_SupplierID.Text = SupplierID;
                    txt_SupplierName.Text = SupplierName;
                    txt_SupplierPhoneNumber.Text = SupplierPhoneNumber;
                    txt_ProductID.Text = ProductID;
                    txt_ProductName.Text = ProductName;
                    cmb_ProductType.Text = ProductTypeName;
                    txt_BuyPrice.Text = BuyPrice;
                    pb_ProductImage.Image = image;
                    bLL_AddSupplierAndProducts.FillAllTxtBoxOfSupplierExcepttxtProductAndTxtSearch(row, dgv_ListProduct, out image, out SupplierID, out SupplierName, out SupplierPhoneNumber);
                    txt_SupplierID.Text = SupplierID;
                    txt_SupplierName.Text = SupplierName;
                    txt_SupplierPhoneNumber.Text = SupplierPhoneNumber;
                    pb_ProductImage.Image = image;
                }
                if (cbS == false && cbP == false)
                {
                    bLL_AddSupplierAndProducts.FillAllTxtAndPicboxOfProductExceptTxtSearchAndTxtSupplier(row, dgv_ListProduct, out image, out SupplierID, out SupplierName, out SupplierPhoneNumber,
                                                                        out ProductID, out ProductName, out ProductTypeName, out BuyPrice);
                    txt_SupplierID.Text = SupplierID;
                    txt_SupplierName.Text = SupplierName;
                    txt_SupplierPhoneNumber.Text = SupplierPhoneNumber;
                    txt_ProductID.Text = ProductID;
                    txt_ProductName.Text = ProductName;
                    cmb_ProductType.Text = ProductTypeName;
                    txt_BuyPrice.Text = BuyPrice;
                    pb_ProductImage.Image = image;
                }
            }
        }
        private void dgv_ListSupplier_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var row = e.RowIndex;
            BLL_AddSupplierAndProducts bLL_AddSupplierAndProducts = new BLL_AddSupplierAndProducts();
            Image image;
            string SupplierID;
            string SupplierName;
            string SupplierPhoneNumber;
            string ProductID;
            string ProductName;
            string ProductTypeName;
            string BuyPrice;
            if (row < 0)
            {
                return;
            }
            else
            {
                pb_ProductImage.Image.Dispose();
                if (cbS == true || (cbS == false && cbP == false))
                {

                    bLL_AddSupplierAndProducts.ClearInputFields(out image, out SupplierID, out SupplierName, out SupplierPhoneNumber, out ProductID,
                                                out ProductName, out ProductTypeName, out BuyPrice);
                    txt_SupplierID.Text = SupplierID;
                    txt_SupplierName.Text = SupplierName;
                    txt_SupplierPhoneNumber.Text = SupplierPhoneNumber;
                    txt_ProductID.Text = ProductID;
                    txt_ProductName.Text = ProductName;
                    cmb_ProductType.Text = ProductTypeName;
                    txt_BuyPrice.Text = BuyPrice;
                    pb_ProductImage.Image = image;
                    bLL_AddSupplierAndProducts.FillAllTxtBoxOfSupplierExcepttxtProductAndTxtSearchfordgvSupplier(row, dgv_ListSupplier, out image, out SupplierID, out SupplierName, out SupplierPhoneNumber);
                    txt_SupplierID.Text = SupplierID;
                    txt_SupplierName.Text = SupplierName;
                    txt_SupplierPhoneNumber.Text = SupplierPhoneNumber;
                    pb_ProductImage.Image = image;
                }
                if (cbP == true)
                {
                    bLL_AddSupplierAndProducts.FillAllTxtBoxOfSupplierExcepttxtProductAndTxtSearchfordgvSupplier(row, dgv_ListSupplier, out image, out SupplierID, out SupplierName, out SupplierPhoneNumber);
                    txt_SupplierID.Text = SupplierID;
                    txt_SupplierName.Text = SupplierName;
                    txt_SupplierPhoneNumber.Text = SupplierPhoneNumber;
                    if (txt_ProductID.Text == "")
                    {
                        pb_ProductImage.Image = image;
                    }
                }
            }
        }
        private void txt_Search_TextChanged(object sender, EventArgs e)
        {
            BLL_AddSupplierAndProducts bLL_DisplayProduct = new BLL_AddSupplierAndProducts();
            var txt = txt_Search.Text;
            bLL_DisplayProduct.SearchProductOrSupplierByIDAOrNameOfThem(txt, dgv_ListProduct);
        }

        private void dgv_ListProduct_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BLL_AddSupplierAndProducts bLL_AddSupplierAndProducts = new BLL_AddSupplierAndProducts();
            dgv_ListProduct.ClearSelection();
            Image image;
            string SupplierID;
            string SupplierName;
            string SupplierPhoneNumber;
            string ProductID;
            string ProductName;
            string ProductTypeName;
            string BuyPrice;
            bLL_AddSupplierAndProducts.ClearInputFields(out image, out SupplierID, out SupplierName, out SupplierPhoneNumber, out ProductID,
                                                out ProductName, out ProductTypeName, out BuyPrice);
            txt_SupplierID.Text = SupplierID;
            txt_SupplierName.Text = SupplierName;
            txt_SupplierPhoneNumber.Text = SupplierPhoneNumber;
            txt_ProductID.Text = ProductID;
            txt_ProductName.Text = ProductName;
            cmb_ProductType.Text = ProductTypeName;
            txt_BuyPrice.Text = BuyPrice;
            pb_ProductImage.Image = image;
        }

        private void btn_Location_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    BLL_AddSupplierAndProducts bLL_AddSupplierAndProducts = new BLL_AddSupplierAndProducts();
            //    pb_ProductImage.Image.Dispose();
            //    var location = bLL_AddSupplierAndProducts.AddImg();
            //    pb_ProductImage.ImageLocation = location;
            //    pb_ProductImage.Image = Image.FromFile(location);
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Có lỗi trong quá trình mở ảnh là: " + ex.Message);
            //}
            BLL_AddSupplierAndProducts bLL_AddSupplierAndProducts = new BLL_AddSupplierAndProducts();
            pb_ProductImage.Image.Dispose();
            bLL_AddSupplierAndProducts.openLocationImage(pb_ProductImage);
        }

        private void btn_addProductOrSupplier_Click(object sender, EventArgs e)
        {
            BLL_AddSupplierAndProducts bLL_AddSupplierAndProducts = new BLL_AddSupplierAndProducts();
            string SupplierID = txt_SupplierID.Text;
            string SupplierName = txt_SupplierName.Text;
            string SupplierPhoneNumber = txt_SupplierPhoneNumber.Text;
            string ProductID = txt_ProductID.Text;
            string ProductName = txt_ProductName.Text;
            string ProductTypeName = cmb_ProductType.SelectedValue.ToString();
            string BuyPrice = txt_BuyPrice.Text;
            Image image;
            if (bLL_AddSupplierAndProducts.AddNewSupplierOrNewProduct(cbS, cbP, SupplierID, SupplierName, SupplierPhoneNumber, ProductID, ProductName, ProductTypeName, BuyPrice
                                                                 , pb_ProductImage.Image) == 1)
            {
                bLL_AddSupplierAndProducts.ClearInputFields(out image, out SupplierID, out SupplierName, out SupplierPhoneNumber, out ProductID,
                                                                out ProductName, out ProductTypeName, out BuyPrice);
                txt_SupplierID.Text = SupplierID;
                txt_SupplierName.Text = SupplierName;
                txt_SupplierPhoneNumber.Text = SupplierPhoneNumber;
                txt_ProductID.Text = ProductID;
                txt_ProductName.Text = ProductName;
                cmb_ProductType.Text = ProductTypeName;
                txt_BuyPrice.Text = BuyPrice;
                pb_ProductImage.Image = image;
                bLL_AddSupplierAndProducts.FillDGVSupplier(dgv_ListSupplier);
                bLL_AddSupplierAndProducts.FillDGVProduct(dgv_ListProduct);
            }
            else
            {
                return;
            }
        }

        private void cb_AddOrUpdateSupplier_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_AddOrUpdateSupplier.Checked == true)
            {
                cbS = true;
                if (cb_AddOrUpdateOrDeleteProduct.Checked == true)
                {
                    cb_AddOrUpdateOrDeleteProduct.Checked = false;
                    cbP = false;
                }
                txt_SupplierID.Enabled = true;
                txt_SupplierName.Enabled = true;
                txt_SupplierPhoneNumber.Enabled = true;
                txt_ProductID.Enabled = false;
                txt_ProductName.Enabled = false;
                txt_BuyPrice.Enabled = false;
                cmb_ProductType.Enabled = false;
                cmb_ProductType.SelectedIndex = 0;
                btn_Location.Enabled = false;
            }
            else
            {
                cbS = false;
                txt_SupplierID.Enabled = false;
                txt_SupplierName.Enabled = false;
                txt_SupplierPhoneNumber.Enabled = false;
                txt_ProductID.Enabled = false;
                txt_ProductName.Enabled = false;
                txt_BuyPrice.Enabled = false;
                cmb_ProductType.Enabled = false;
                cmb_ProductType.SelectedIndex = 0;
                btn_Location.Enabled = false;
            }
        }

        private void cb_AddOrUpdateOrDeleteProduct_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_AddOrUpdateOrDeleteProduct.Checked == true)
            {
                cbP = true;
                if (cb_AddOrUpdateSupplier.Checked == true)
                {
                    cb_AddOrUpdateSupplier.Checked = false;
                    cbS = false;
                }
                txt_SupplierID.Enabled = false;
                txt_SupplierName.Enabled = false;
                txt_SupplierPhoneNumber.Enabled = false;
                txt_ProductID.Enabled = true;
                txt_ProductName.Enabled = true;
                txt_BuyPrice.Enabled = true;
                cmb_ProductType.Enabled = true;
                cmb_ProductType.SelectedIndex = 0;
                btn_Location.Enabled = true;
            }
            else
            {
                cbP = false;
                txt_SupplierID.Enabled = false;
                txt_SupplierName.Enabled = false;
                txt_SupplierPhoneNumber.Enabled = false;
                txt_ProductID.Enabled = false;
                txt_ProductName.Enabled = false;
                txt_BuyPrice.Enabled = false;
                cmb_ProductType.Enabled = false;
                cmb_ProductType.SelectedIndex = 0;
                btn_Location.Enabled = false;
            }
        }

        private void btn_UpdateProductOrSupplier_Click(object sender, EventArgs e)
        {
            BLL_AddSupplierAndProducts bLL_AddSupplierAndProducts = new BLL_AddSupplierAndProducts();
            string SupplierID = txt_SupplierID.Text;
            string SupplierName = txt_SupplierName.Text;
            string SupplierPhoneNumber = txt_SupplierPhoneNumber.Text;
            string ProductID = txt_ProductID.Text;
            string ProductName = txt_ProductName.Text;
            string ProductTypeName = cmb_ProductType.SelectedValue.ToString();
            string BuyPrice = txt_BuyPrice.Text;
            if (bLL_AddSupplierAndProducts.UpdateSupplierOrProduct(cbS, cbP, SupplierID, SupplierName, SupplierPhoneNumber,
                                                                      ProductID, ProductName, ProductTypeName, BuyPrice, pb_ProductImage.Image) == 1)
            {
                bLL_AddSupplierAndProducts.ClearInputFields(out Image image, out SupplierID, out SupplierName, out SupplierPhoneNumber, out ProductID,
                                                                out ProductName, out ProductTypeName, out BuyPrice);
                txt_SupplierID.Text = SupplierID;
                txt_SupplierName.Text = SupplierName;
                txt_SupplierPhoneNumber.Text = SupplierPhoneNumber;
                txt_ProductID.Text = ProductID;
                txt_ProductName.Text = ProductName;
                cmb_ProductType.Text = ProductTypeName;
                txt_BuyPrice.Text = BuyPrice;
                pb_ProductImage.Image = image;
                bLL_AddSupplierAndProducts.FillDGVSupplier(dgv_ListSupplier);
                bLL_AddSupplierAndProducts.FillDGVProduct(dgv_ListProduct);
            }
            else
            {
                return;
            }
        }
    }
}

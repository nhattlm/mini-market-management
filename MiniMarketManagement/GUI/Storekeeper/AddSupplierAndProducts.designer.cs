﻿namespace MiniMarketManagement.GUI.Storekeeper
{
    partial class frm_AddSupplierAndProducts
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.dgv_ListProduct = new Guna.UI2.WinForms.Guna2DataGridView();
            this.Product_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product_Type_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Buy_Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cb_AddOrUpdateOrDeleteProduct = new Guna.UI2.WinForms.Guna2ToggleSwitch();
            this.label11 = new System.Windows.Forms.Label();
            this.cb_AddOrUpdateSupplier = new Guna.UI2.WinForms.Guna2ToggleSwitch();
            this.btn_Location = new Guna.UI2.WinForms.Guna2GradientButton();
            this.cmb_ProductType = new Guna.UI2.WinForms.Guna2ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_BuyPrice = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_Search = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_SupplierID = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_ProductID = new Guna.UI2.WinForms.Guna2TextBox();
            this.pb_ProductImage = new Guna.UI2.WinForms.Guna2PictureBox();
            this.txt_SupplierPhoneNumber = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_SupplierName = new Guna.UI2.WinForms.Guna2TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_ProductName = new Guna.UI2.WinForms.Guna2TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_UpdateProductOrSupplier = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_addProductOrSupplier = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2CirclePictureBox1 = new Guna.UI2.WinForms.Guna2CirclePictureBox();
            this.dgv_ListSupplier = new Guna.UI2.WinForms.Guna2DataGridView();
            this.SupplierID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierPhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListProduct)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ProductImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.guna2CirclePictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListSupplier)).BeginInit();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // dgv_ListProduct
            // 
            this.dgv_ListProduct.AllowUserToAddRows = false;
            this.dgv_ListProduct.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.dgv_ListProduct.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_ListProduct.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ListProduct.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_ListProduct.ColumnHeadersHeight = 18;
            this.dgv_ListProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ListProduct.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Product_ID,
            this.Product_Name,
            this.Product_Type_Name,
            this.Buy_Price,
            this.Supplier});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ListProduct.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_ListProduct.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListProduct.Location = new System.Drawing.Point(521, 35);
            this.dgv_ListProduct.Name = "dgv_ListProduct";
            this.dgv_ListProduct.ReadOnly = true;
            this.dgv_ListProduct.RowHeadersVisible = false;
            this.dgv_ListProduct.RowHeadersWidth = 51;
            this.dgv_ListProduct.RowTemplate.Height = 24;
            this.dgv_ListProduct.Size = new System.Drawing.Size(611, 309);
            this.dgv_ListProduct.TabIndex = 0;
            this.dgv_ListProduct.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ListProduct.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_ListProduct.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_ListProduct.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_ListProduct.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_ListProduct.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_ListProduct.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListProduct.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_ListProduct.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_ListProduct.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ListProduct.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_ListProduct.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ListProduct.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_ListProduct.ThemeStyle.ReadOnly = true;
            this.dgv_ListProduct.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ListProduct.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_ListProduct.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ListProduct.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ListProduct.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_ListProduct.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListProduct.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ListProduct.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListProduct_CellClick);
            this.dgv_ListProduct.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListProduct_CellDoubleClick);
            // 
            // Product_ID
            // 
            this.Product_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Product_ID.FillWeight = 110F;
            this.Product_ID.HeaderText = "Mã sản phẩm";
            this.Product_ID.MinimumWidth = 6;
            this.Product_ID.Name = "Product_ID";
            this.Product_ID.ReadOnly = true;
            // 
            // Product_Name
            // 
            this.Product_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Product_Name.FillWeight = 120F;
            this.Product_Name.HeaderText = "Tên sản phẩm";
            this.Product_Name.MinimumWidth = 6;
            this.Product_Name.Name = "Product_Name";
            this.Product_Name.ReadOnly = true;
            // 
            // Product_Type_Name
            // 
            this.Product_Type_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Product_Type_Name.FillWeight = 70F;
            this.Product_Type_Name.HeaderText = "Loại";
            this.Product_Type_Name.MinimumWidth = 6;
            this.Product_Type_Name.Name = "Product_Type_Name";
            this.Product_Type_Name.ReadOnly = true;
            // 
            // Buy_Price
            // 
            this.Buy_Price.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Buy_Price.FillWeight = 80F;
            this.Buy_Price.HeaderText = "Giá mua";
            this.Buy_Price.MinimumWidth = 6;
            this.Buy_Price.Name = "Buy_Price";
            this.Buy_Price.ReadOnly = true;
            // 
            // Supplier
            // 
            this.Supplier.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Supplier.FillWeight = 120F;
            this.Supplier.HeaderText = "Nhà cung cấp";
            this.Supplier.MinimumWidth = 6;
            this.Supplier.Name = "Supplier";
            this.Supplier.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.cb_AddOrUpdateOrDeleteProduct);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.cb_AddOrUpdateSupplier);
            this.groupBox1.Controls.Add(this.btn_Location);
            this.groupBox1.Controls.Add(this.cmb_ProductType);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.txt_BuyPrice);
            this.groupBox1.Controls.Add(this.txt_Search);
            this.groupBox1.Controls.Add(this.txt_SupplierID);
            this.groupBox1.Controls.Add(this.txt_ProductID);
            this.groupBox1.Controls.Add(this.pb_ProductImage);
            this.groupBox1.Controls.Add(this.txt_SupplierPhoneNumber);
            this.groupBox1.Controls.Add(this.txt_SupplierName);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txt_ProductName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(503, 624);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Nhà cung cấp và thông tin sản phẩm";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(305, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(177, 18);
            this.label12.TabIndex = 19;
            this.label12.Text = "Thêm/sửa/xóa sản phẩm";
            // 
            // cb_AddOrUpdateOrDeleteProduct
            // 
            this.cb_AddOrUpdateOrDeleteProduct.Animated = true;
            this.cb_AddOrUpdateOrDeleteProduct.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cb_AddOrUpdateOrDeleteProduct.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cb_AddOrUpdateOrDeleteProduct.CheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_AddOrUpdateOrDeleteProduct.CheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_AddOrUpdateOrDeleteProduct.Location = new System.Drawing.Point(261, 26);
            this.cb_AddOrUpdateOrDeleteProduct.Name = "cb_AddOrUpdateOrDeleteProduct";
            this.cb_AddOrUpdateOrDeleteProduct.Size = new System.Drawing.Size(38, 20);
            this.cb_AddOrUpdateOrDeleteProduct.TabIndex = 18;
            this.cb_AddOrUpdateOrDeleteProduct.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_AddOrUpdateOrDeleteProduct.UncheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_AddOrUpdateOrDeleteProduct.UncheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_AddOrUpdateOrDeleteProduct.UncheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_AddOrUpdateOrDeleteProduct.CheckedChanged += new System.EventHandler(this.cb_AddOrUpdateOrDeleteProduct_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(57, 26);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(170, 18);
            this.label11.TabIndex = 19;
            this.label11.Text = "Thêm/sửa nhà cung cấp";
            // 
            // cb_AddOrUpdateSupplier
            // 
            this.cb_AddOrUpdateSupplier.Animated = true;
            this.cb_AddOrUpdateSupplier.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cb_AddOrUpdateSupplier.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cb_AddOrUpdateSupplier.CheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_AddOrUpdateSupplier.CheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_AddOrUpdateSupplier.Location = new System.Drawing.Point(13, 26);
            this.cb_AddOrUpdateSupplier.Name = "cb_AddOrUpdateSupplier";
            this.cb_AddOrUpdateSupplier.Size = new System.Drawing.Size(38, 20);
            this.cb_AddOrUpdateSupplier.TabIndex = 18;
            this.cb_AddOrUpdateSupplier.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_AddOrUpdateSupplier.UncheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_AddOrUpdateSupplier.UncheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_AddOrUpdateSupplier.UncheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_AddOrUpdateSupplier.CheckedChanged += new System.EventHandler(this.cb_AddOrUpdateSupplier_CheckedChanged);
            // 
            // btn_Location
            // 
            this.btn_Location.BackColor = System.Drawing.Color.Transparent;
            this.btn_Location.BorderRadius = 10;
            this.btn_Location.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Location.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Location.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Location.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Location.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Location.Enabled = false;
            this.btn_Location.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Location.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Location.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Location.ForeColor = System.Drawing.Color.White;
            this.btn_Location.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Location.Location = new System.Drawing.Point(416, 522);
            this.btn_Location.Name = "btn_Location";
            this.btn_Location.ShadowDecoration.BorderRadius = 10;
            this.btn_Location.ShadowDecoration.Enabled = true;
            this.btn_Location.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Location.Size = new System.Drawing.Size(49, 31);
            this.btn_Location.TabIndex = 17;
            this.btn_Location.Text = "...";
            this.btn_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_Location.TextOffset = new System.Drawing.Point(0, -7);
            this.btn_Location.Click += new System.EventHandler(this.btn_Location_Click);
            // 
            // cmb_ProductType
            // 
            this.cmb_ProductType.BackColor = System.Drawing.Color.Transparent;
            this.cmb_ProductType.BorderColor = System.Drawing.Color.Silver;
            this.cmb_ProductType.BorderRadius = 15;
            this.cmb_ProductType.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_ProductType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ProductType.Enabled = false;
            this.cmb_ProductType.FillColor = System.Drawing.Color.WhiteSmoke;
            this.cmb_ProductType.FocusedColor = System.Drawing.Color.Tomato;
            this.cmb_ProductType.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.cmb_ProductType.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.cmb_ProductType.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmb_ProductType.ForeColor = System.Drawing.Color.DimGray;
            this.cmb_ProductType.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.cmb_ProductType.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.cmb_ProductType.ItemHeight = 30;
            this.cmb_ProductType.Location = new System.Drawing.Point(189, 364);
            this.cmb_ProductType.Name = "cmb_ProductType";
            this.cmb_ProductType.ShadowDecoration.BorderRadius = 15;
            this.cmb_ProductType.ShadowDecoration.Enabled = true;
            this.cmb_ProductType.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.cmb_ProductType.Size = new System.Drawing.Size(271, 36);
            this.cmb_ProductType.TabIndex = 16;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 364);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(155, 24);
            this.label10.TabIndex = 15;
            this.label10.Text = "Loại sản phẩm";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 417);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 24);
            this.label7.TabIndex = 14;
            this.label7.Text = "Giá mua";
            // 
            // txt_BuyPrice
            // 
            this.txt_BuyPrice.Animated = true;
            this.txt_BuyPrice.BackColor = System.Drawing.Color.Transparent;
            this.txt_BuyPrice.BorderColor = System.Drawing.Color.Silver;
            this.txt_BuyPrice.BorderRadius = 15;
            this.txt_BuyPrice.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_BuyPrice.DefaultText = "";
            this.txt_BuyPrice.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_BuyPrice.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_BuyPrice.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_BuyPrice.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_BuyPrice.Enabled = false;
            this.txt_BuyPrice.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_BuyPrice.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_BuyPrice.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_BuyPrice.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_BuyPrice.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt_BuyPrice.ForeColor = System.Drawing.Color.DimGray;
            this.txt_BuyPrice.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_BuyPrice.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_BuyPrice.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_BuyPrice.IconLeft = global::MiniMarketManagement.Properties.Resources.price_tag;
            this.txt_BuyPrice.Location = new System.Drawing.Point(189, 417);
            this.txt_BuyPrice.Margin = new System.Windows.Forms.Padding(6, 8, 6, 8);
            this.txt_BuyPrice.Name = "txt_BuyPrice";
            this.txt_BuyPrice.PasswordChar = '\0';
            this.txt_BuyPrice.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_BuyPrice.PlaceholderText = "Giá mua";
            this.txt_BuyPrice.SelectedText = "";
            this.txt_BuyPrice.ShadowDecoration.BorderRadius = 15;
            this.txt_BuyPrice.ShadowDecoration.Enabled = true;
            this.txt_BuyPrice.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_BuyPrice.Size = new System.Drawing.Size(198, 35);
            this.txt_BuyPrice.TabIndex = 13;
            // 
            // txt_Search
            // 
            this.txt_Search.Animated = true;
            this.txt_Search.BackColor = System.Drawing.Color.Transparent;
            this.txt_Search.BorderColor = System.Drawing.Color.Silver;
            this.txt_Search.BorderRadius = 15;
            this.txt_Search.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Search.DefaultText = "";
            this.txt_Search.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Search.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Search.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Search.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_Search.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.IconLeft = global::MiniMarketManagement.Properties.Resources.search;
            this.txt_Search.Location = new System.Drawing.Point(160, 52);
            this.txt_Search.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PasswordChar = '\0';
            this.txt_Search.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Search.PlaceholderText = "Mã/Tên sản phẩm/Tên nhà cung cấp";
            this.txt_Search.SelectedText = "";
            this.txt_Search.ShadowDecoration.BorderRadius = 15;
            this.txt_Search.ShadowDecoration.Enabled = true;
            this.txt_Search.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Search.Size = new System.Drawing.Size(300, 35);
            this.txt_Search.TabIndex = 10;
            this.txt_Search.TextChanged += new System.EventHandler(this.txt_Search_TextChanged);
            // 
            // txt_SupplierID
            // 
            this.txt_SupplierID.Animated = true;
            this.txt_SupplierID.BackColor = System.Drawing.Color.Transparent;
            this.txt_SupplierID.BorderColor = System.Drawing.Color.Silver;
            this.txt_SupplierID.BorderRadius = 15;
            this.txt_SupplierID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_SupplierID.DefaultText = "";
            this.txt_SupplierID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_SupplierID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_SupplierID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SupplierID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SupplierID.Enabled = false;
            this.txt_SupplierID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_SupplierID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SupplierID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SupplierID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SupplierID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_SupplierID.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SupplierID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SupplierID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SupplierID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SupplierID.IconLeft = global::MiniMarketManagement.Properties.Resources.id__1_;
            this.txt_SupplierID.Location = new System.Drawing.Point(189, 104);
            this.txt_SupplierID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_SupplierID.Name = "txt_SupplierID";
            this.txt_SupplierID.PasswordChar = '\0';
            this.txt_SupplierID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_SupplierID.PlaceholderText = "Mã nhà sản xuất";
            this.txt_SupplierID.SelectedText = "";
            this.txt_SupplierID.ShadowDecoration.BorderRadius = 15;
            this.txt_SupplierID.ShadowDecoration.Enabled = true;
            this.txt_SupplierID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_SupplierID.Size = new System.Drawing.Size(271, 35);
            this.txt_SupplierID.TabIndex = 10;
            // 
            // txt_ProductID
            // 
            this.txt_ProductID.Animated = true;
            this.txt_ProductID.BackColor = System.Drawing.Color.Transparent;
            this.txt_ProductID.BorderColor = System.Drawing.Color.Silver;
            this.txt_ProductID.BorderRadius = 15;
            this.txt_ProductID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ProductID.DefaultText = "";
            this.txt_ProductID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ProductID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ProductID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductID.Enabled = false;
            this.txt_ProductID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ProductID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ProductID.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductID.IconLeft = global::MiniMarketManagement.Properties.Resources.id__1_;
            this.txt_ProductID.Location = new System.Drawing.Point(160, 260);
            this.txt_ProductID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ProductID.Name = "txt_ProductID";
            this.txt_ProductID.PasswordChar = '\0';
            this.txt_ProductID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ProductID.PlaceholderText = "Mã sản phẩm";
            this.txt_ProductID.SelectedText = "";
            this.txt_ProductID.ShadowDecoration.BorderRadius = 15;
            this.txt_ProductID.ShadowDecoration.Enabled = true;
            this.txt_ProductID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ProductID.Size = new System.Drawing.Size(300, 35);
            this.txt_ProductID.TabIndex = 10;
            // 
            // pb_ProductImage
            // 
            this.pb_ProductImage.BorderRadius = 5;
            this.pb_ProductImage.Image = global::MiniMarketManagement.Properties.Resources.image__2_;
            this.pb_ProductImage.ImageRotate = 0F;
            this.pb_ProductImage.InitialImage = global::MiniMarketManagement.Properties.Resources.image;
            this.pb_ProductImage.Location = new System.Drawing.Point(160, 463);
            this.pb_ProductImage.Name = "pb_ProductImage";
            this.pb_ProductImage.Size = new System.Drawing.Size(250, 155);
            this.pb_ProductImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_ProductImage.TabIndex = 12;
            this.pb_ProductImage.TabStop = false;
            // 
            // txt_SupplierPhoneNumber
            // 
            this.txt_SupplierPhoneNumber.Animated = true;
            this.txt_SupplierPhoneNumber.BackColor = System.Drawing.Color.Transparent;
            this.txt_SupplierPhoneNumber.BorderColor = System.Drawing.Color.Silver;
            this.txt_SupplierPhoneNumber.BorderRadius = 15;
            this.txt_SupplierPhoneNumber.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_SupplierPhoneNumber.DefaultText = "";
            this.txt_SupplierPhoneNumber.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_SupplierPhoneNumber.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_SupplierPhoneNumber.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SupplierPhoneNumber.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SupplierPhoneNumber.Enabled = false;
            this.txt_SupplierPhoneNumber.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_SupplierPhoneNumber.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SupplierPhoneNumber.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SupplierPhoneNumber.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SupplierPhoneNumber.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_SupplierPhoneNumber.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SupplierPhoneNumber.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SupplierPhoneNumber.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SupplierPhoneNumber.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SupplierPhoneNumber.IconLeft = global::MiniMarketManagement.Properties.Resources.call1;
            this.txt_SupplierPhoneNumber.Location = new System.Drawing.Point(189, 208);
            this.txt_SupplierPhoneNumber.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_SupplierPhoneNumber.Name = "txt_SupplierPhoneNumber";
            this.txt_SupplierPhoneNumber.PasswordChar = '\0';
            this.txt_SupplierPhoneNumber.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_SupplierPhoneNumber.PlaceholderText = "Số điện thoại nhà sản xuất";
            this.txt_SupplierPhoneNumber.SelectedText = "";
            this.txt_SupplierPhoneNumber.ShadowDecoration.BorderRadius = 15;
            this.txt_SupplierPhoneNumber.ShadowDecoration.Enabled = true;
            this.txt_SupplierPhoneNumber.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_SupplierPhoneNumber.Size = new System.Drawing.Size(271, 35);
            this.txt_SupplierPhoneNumber.TabIndex = 11;
            // 
            // txt_SupplierName
            // 
            this.txt_SupplierName.Animated = true;
            this.txt_SupplierName.BackColor = System.Drawing.Color.Transparent;
            this.txt_SupplierName.BorderColor = System.Drawing.Color.Silver;
            this.txt_SupplierName.BorderRadius = 15;
            this.txt_SupplierName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_SupplierName.DefaultText = "";
            this.txt_SupplierName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_SupplierName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_SupplierName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SupplierName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SupplierName.Enabled = false;
            this.txt_SupplierName.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_SupplierName.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SupplierName.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SupplierName.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SupplierName.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_SupplierName.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SupplierName.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SupplierName.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SupplierName.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SupplierName.IconLeft = global::MiniMarketManagement.Properties.Resources.user__1_2;
            this.txt_SupplierName.Location = new System.Drawing.Point(189, 156);
            this.txt_SupplierName.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_SupplierName.Name = "txt_SupplierName";
            this.txt_SupplierName.PasswordChar = '\0';
            this.txt_SupplierName.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_SupplierName.PlaceholderText = "Tên nhà sản xuất";
            this.txt_SupplierName.SelectedText = "";
            this.txt_SupplierName.ShadowDecoration.BorderRadius = 15;
            this.txt_SupplierName.ShadowDecoration.Enabled = true;
            this.txt_SupplierName.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_SupplierName.Size = new System.Drawing.Size(271, 35);
            this.txt_SupplierName.TabIndex = 11;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(9, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 23);
            this.label9.TabIndex = 4;
            this.label9.Text = "Tìm kiếm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(166, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Mã nhà sản xuất";
            // 
            // txt_ProductName
            // 
            this.txt_ProductName.Animated = true;
            this.txt_ProductName.BackColor = System.Drawing.Color.Transparent;
            this.txt_ProductName.BorderColor = System.Drawing.Color.Silver;
            this.txt_ProductName.BorderRadius = 15;
            this.txt_ProductName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ProductName.DefaultText = "";
            this.txt_ProductName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ProductName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ProductName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductName.Enabled = false;
            this.txt_ProductName.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ProductName.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductName.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductName.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ProductName.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductName.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductName.IconLeft = global::MiniMarketManagement.Properties.Resources.features1;
            this.txt_ProductName.Location = new System.Drawing.Point(160, 312);
            this.txt_ProductName.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ProductName.Name = "txt_ProductName";
            this.txt_ProductName.PasswordChar = '\0';
            this.txt_ProductName.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ProductName.PlaceholderText = "Tên sản phẩm";
            this.txt_ProductName.SelectedText = "";
            this.txt_ProductName.ShadowDecoration.BorderRadius = 15;
            this.txt_ProductName.ShadowDecoration.Enabled = true;
            this.txt_ProductName.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ProductName.Size = new System.Drawing.Size(300, 35);
            this.txt_ProductName.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(177, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "SĐT nhà sản xuất";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(9, 260);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = "Mã sản phẩm";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 156);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 23);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tên nhà sản xuất";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(5, 478);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 23);
            this.label8.TabIndex = 4;
            this.label8.Text = "Ảnh sản phẩm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(9, 312);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 23);
            this.label4.TabIndex = 4;
            this.label4.Text = "Tên sản phẩm";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(744, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(206, 23);
            this.label6.TabIndex = 17;
            this.label6.Text = "Danh sách sản phẩm";
            // 
            // btn_UpdateProductOrSupplier
            // 
            this.btn_UpdateProductOrSupplier.BackColor = System.Drawing.Color.Transparent;
            this.btn_UpdateProductOrSupplier.BorderRadius = 10;
            this.btn_UpdateProductOrSupplier.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_UpdateProductOrSupplier.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_UpdateProductOrSupplier.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_UpdateProductOrSupplier.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_UpdateProductOrSupplier.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_UpdateProductOrSupplier.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_UpdateProductOrSupplier.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_UpdateProductOrSupplier.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_UpdateProductOrSupplier.ForeColor = System.Drawing.Color.White;
            this.btn_UpdateProductOrSupplier.Image = global::MiniMarketManagement.Properties.Resources.refresh1;
            this.btn_UpdateProductOrSupplier.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_UpdateProductOrSupplier.Location = new System.Drawing.Point(388, 639);
            this.btn_UpdateProductOrSupplier.Name = "btn_UpdateProductOrSupplier";
            this.btn_UpdateProductOrSupplier.ShadowDecoration.BorderRadius = 10;
            this.btn_UpdateProductOrSupplier.ShadowDecoration.Enabled = true;
            this.btn_UpdateProductOrSupplier.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_UpdateProductOrSupplier.Size = new System.Drawing.Size(84, 45);
            this.btn_UpdateProductOrSupplier.TabIndex = 19;
            this.btn_UpdateProductOrSupplier.Text = "Sửa";
            this.btn_UpdateProductOrSupplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_UpdateProductOrSupplier.Click += new System.EventHandler(this.btn_UpdateProductOrSupplier_Click);
            // 
            // btn_addProductOrSupplier
            // 
            this.btn_addProductOrSupplier.BackColor = System.Drawing.Color.Transparent;
            this.btn_addProductOrSupplier.BorderRadius = 10;
            this.btn_addProductOrSupplier.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_addProductOrSupplier.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_addProductOrSupplier.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_addProductOrSupplier.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_addProductOrSupplier.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_addProductOrSupplier.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_addProductOrSupplier.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_addProductOrSupplier.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_addProductOrSupplier.ForeColor = System.Drawing.Color.White;
            this.btn_addProductOrSupplier.Image = global::MiniMarketManagement.Properties.Resources.add_friend;
            this.btn_addProductOrSupplier.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_addProductOrSupplier.Location = new System.Drawing.Point(260, 639);
            this.btn_addProductOrSupplier.Name = "btn_addProductOrSupplier";
            this.btn_addProductOrSupplier.ShadowDecoration.BorderRadius = 10;
            this.btn_addProductOrSupplier.ShadowDecoration.Enabled = true;
            this.btn_addProductOrSupplier.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_addProductOrSupplier.Size = new System.Drawing.Size(96, 45);
            this.btn_addProductOrSupplier.TabIndex = 19;
            this.btn_addProductOrSupplier.Text = "Thêm";
            this.btn_addProductOrSupplier.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_addProductOrSupplier.Click += new System.EventHandler(this.btn_addProductOrSupplier_Click);
            // 
            // guna2CirclePictureBox1
            // 
            this.guna2CirclePictureBox1.ImageRotate = 0F;
            this.guna2CirclePictureBox1.Location = new System.Drawing.Point(559, 7);
            this.guna2CirclePictureBox1.Name = "guna2CirclePictureBox1";
            this.guna2CirclePictureBox1.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2CirclePictureBox1.Size = new System.Drawing.Size(8, 8);
            this.guna2CirclePictureBox1.TabIndex = 12;
            this.guna2CirclePictureBox1.TabStop = false;
            // 
            // dgv_ListSupplier
            // 
            this.dgv_ListSupplier.AllowUserToAddRows = false;
            this.dgv_ListSupplier.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dgv_ListSupplier.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_ListSupplier.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ListSupplier.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_ListSupplier.ColumnHeadersHeight = 18;
            this.dgv_ListSupplier.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ListSupplier.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SupplierID,
            this.SupplierName,
            this.SupplierPhoneNumber});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ListSupplier.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_ListSupplier.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListSupplier.Location = new System.Drawing.Point(521, 373);
            this.dgv_ListSupplier.Name = "dgv_ListSupplier";
            this.dgv_ListSupplier.ReadOnly = true;
            this.dgv_ListSupplier.RowHeadersVisible = false;
            this.dgv_ListSupplier.RowHeadersWidth = 51;
            this.dgv_ListSupplier.RowTemplate.Height = 24;
            this.dgv_ListSupplier.Size = new System.Drawing.Size(611, 306);
            this.dgv_ListSupplier.TabIndex = 0;
            this.dgv_ListSupplier.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ListSupplier.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_ListSupplier.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_ListSupplier.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_ListSupplier.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_ListSupplier.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_ListSupplier.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListSupplier.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_ListSupplier.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_ListSupplier.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ListSupplier.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_ListSupplier.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ListSupplier.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_ListSupplier.ThemeStyle.ReadOnly = true;
            this.dgv_ListSupplier.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ListSupplier.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_ListSupplier.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ListSupplier.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ListSupplier.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_ListSupplier.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListSupplier.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ListSupplier.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListSupplier_CellClick);
            this.dgv_ListSupplier.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListProduct_CellDoubleClick);
            // 
            // SupplierID
            // 
            this.SupplierID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SupplierID.FillWeight = 110F;
            this.SupplierID.HeaderText = "Mã nhà cung cấp";
            this.SupplierID.MinimumWidth = 6;
            this.SupplierID.Name = "SupplierID";
            this.SupplierID.ReadOnly = true;
            // 
            // SupplierName
            // 
            this.SupplierName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SupplierName.FillWeight = 120F;
            this.SupplierName.HeaderText = "Tên nhà cung cấp";
            this.SupplierName.MinimumWidth = 6;
            this.SupplierName.Name = "SupplierName";
            this.SupplierName.ReadOnly = true;
            // 
            // SupplierPhoneNumber
            // 
            this.SupplierPhoneNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SupplierPhoneNumber.FillWeight = 150F;
            this.SupplierPhoneNumber.HeaderText = "Số điện thoại nhà cung cấp";
            this.SupplierPhoneNumber.MinimumWidth = 6;
            this.SupplierPhoneNumber.Name = "SupplierPhoneNumber";
            this.SupplierPhoneNumber.ReadOnly = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(727, 347);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(240, 23);
            this.label13.TabIndex = 17;
            this.label13.Text = "Danh sách nhà cung cấp";
            // 
            // frm_AddSupplierAndProducts
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.btn_UpdateProductOrSupplier);
            this.Controls.Add(this.btn_addProductOrSupplier);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.guna2CirclePictureBox1);
            this.Controls.Add(this.dgv_ListSupplier);
            this.Controls.Add(this.dgv_ListProduct);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_AddSupplierAndProducts";
            this.Load += new System.EventHandler(this.frm_DisplayProduct_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListProduct)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ProductImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.guna2CirclePictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListSupplier)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_ListProduct;
        private Guna.UI2.WinForms.Guna2CirclePictureBox guna2CirclePictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private Guna.UI2.WinForms.Guna2TextBox txt_Search;
        private Guna.UI2.WinForms.Guna2TextBox txt_ProductID;
        private Guna.UI2.WinForms.Guna2PictureBox pb_ProductImage;
        private System.Windows.Forms.Label label9;
        private Guna.UI2.WinForms.Guna2TextBox txt_ProductName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private Guna.UI2.WinForms.Guna2TextBox txt_SupplierID;
        private Guna.UI2.WinForms.Guna2TextBox txt_SupplierName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2TextBox txt_SupplierPhoneNumber;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label7;
        private Guna.UI2.WinForms.Guna2TextBox txt_BuyPrice;
        private System.Windows.Forms.Label label10;
        private Guna.UI2.WinForms.Guna2ComboBox cmb_ProductType;
        private Guna.UI2.WinForms.Guna2GradientButton btn_Location;
        private Guna.UI2.WinForms.Guna2GradientButton btn_addProductOrSupplier;
        private Guna.UI2.WinForms.Guna2GradientButton btn_UpdateProductOrSupplier;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_Type_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Buy_Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier;
        private System.Windows.Forms.Label label12;
        private Guna.UI2.WinForms.Guna2ToggleSwitch cb_AddOrUpdateOrDeleteProduct;
        private System.Windows.Forms.Label label11;
        private Guna.UI2.WinForms.Guna2ToggleSwitch cb_AddOrUpdateSupplier;
        private System.Windows.Forms.Label label13;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_ListSupplier;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierID;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierPhoneNumber;
    }
}


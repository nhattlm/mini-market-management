﻿namespace MiniMarketManagement.GUI.Storekeeper
{
    partial class frm_ImportProduct
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ImportProduct));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.cb_DisplayLowProduct = new Guna.UI2.WinForms.Guna2ToggleSwitch();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_ImportProductAmount = new Guna.UI2.WinForms.Guna2TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_Search = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_ProductID = new Guna.UI2.WinForms.Guna2TextBox();
            this.pb_ProductImage = new Guna.UI2.WinForms.Guna2PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_ProductName = new Guna.UI2.WinForms.Guna2TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LargeIcon = new System.Windows.Forms.ImageList(this.components);
            this.dgv_ListSelectedProduct = new Guna.UI2.WinForms.Guna2DataGridView();
            this.Product_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product_Type_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Buy_Price = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Calculation_Unit_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_ProductsList = new Guna.UI2.WinForms.Guna2DataGridView();
            this.ProductID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Product__Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuyPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CalculationUnitName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SupplierName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txt_ProductQuantity = new Guna.UI2.WinForms.Guna2TextBox();
            this.btn_Order = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_MinusQuantityOrDeleteProduct = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_AddProduct = new Guna.UI2.WinForms.Guna2GradientButton();
            this.printImportProductCard = new System.Drawing.Printing.PrintDocument();
            this.printImportProductCardDialog = new System.Windows.Forms.PrintPreviewDialog();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ProductImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListSelectedProduct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ProductsList)).BeginInit();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // cb_DisplayLowProduct
            // 
            this.cb_DisplayLowProduct.Animated = true;
            this.cb_DisplayLowProduct.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cb_DisplayLowProduct.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cb_DisplayLowProduct.CheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_DisplayLowProduct.CheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_DisplayLowProduct.Location = new System.Drawing.Point(876, 9);
            this.cb_DisplayLowProduct.Name = "cb_DisplayLowProduct";
            this.cb_DisplayLowProduct.Size = new System.Drawing.Size(38, 20);
            this.cb_DisplayLowProduct.TabIndex = 3;
            this.cb_DisplayLowProduct.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_DisplayLowProduct.UncheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_DisplayLowProduct.UncheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_DisplayLowProduct.UncheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_DisplayLowProduct.CheckedChanged += new System.EventHandler(this.cb_DisplayLowProduct_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(920, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 18);
            this.label1.TabIndex = 4;
            this.label1.Text = "Hiển thị sản phẩm sắp hết";
            // 
            // txt_ImportProductAmount
            // 
            this.txt_ImportProductAmount.Animated = true;
            this.txt_ImportProductAmount.BackColor = System.Drawing.Color.Transparent;
            this.txt_ImportProductAmount.BorderColor = System.Drawing.Color.Silver;
            this.txt_ImportProductAmount.BorderRadius = 15;
            this.txt_ImportProductAmount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ImportProductAmount.DefaultText = "";
            this.txt_ImportProductAmount.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ImportProductAmount.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ImportProductAmount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ImportProductAmount.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ImportProductAmount.Enabled = false;
            this.txt_ImportProductAmount.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ImportProductAmount.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ImportProductAmount.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ImportProductAmount.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ImportProductAmount.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ImportProductAmount.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ImportProductAmount.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ImportProductAmount.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ImportProductAmount.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ImportProductAmount.Location = new System.Drawing.Point(990, 650);
            this.txt_ImportProductAmount.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ImportProductAmount.Name = "txt_ImportProductAmount";
            this.txt_ImportProductAmount.PasswordChar = '\0';
            this.txt_ImportProductAmount.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ImportProductAmount.PlaceholderText = "";
            this.txt_ImportProductAmount.ReadOnly = true;
            this.txt_ImportProductAmount.SelectedText = "";
            this.txt_ImportProductAmount.ShadowDecoration.BorderRadius = 15;
            this.txt_ImportProductAmount.ShadowDecoration.Enabled = true;
            this.txt_ImportProductAmount.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ImportProductAmount.Size = new System.Drawing.Size(131, 33);
            this.txt_ImportProductAmount.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(870, 655);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 23);
            this.label2.TabIndex = 4;
            this.label2.Text = "Thành tiền";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_Search);
            this.groupBox1.Controls.Add(this.txt_ProductID);
            this.groupBox1.Controls.Add(this.pb_ProductImage);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.txt_ProductName);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(503, 406);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin sản phẩm";
            // 
            // txt_Search
            // 
            this.txt_Search.Animated = true;
            this.txt_Search.BackColor = System.Drawing.Color.Transparent;
            this.txt_Search.BorderColor = System.Drawing.Color.Silver;
            this.txt_Search.BorderRadius = 15;
            this.txt_Search.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Search.DefaultText = "";
            this.txt_Search.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Search.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Search.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Search.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_Search.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.IconLeft = global::MiniMarketManagement.Properties.Resources.search;
            this.txt_Search.Location = new System.Drawing.Point(160, 52);
            this.txt_Search.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PasswordChar = '\0';
            this.txt_Search.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Search.PlaceholderText = "Mã/Tên sản phẩm";
            this.txt_Search.SelectedText = "";
            this.txt_Search.ShadowDecoration.BorderRadius = 15;
            this.txt_Search.ShadowDecoration.Enabled = true;
            this.txt_Search.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Search.Size = new System.Drawing.Size(300, 35);
            this.txt_Search.TabIndex = 10;
            this.txt_Search.TextChanged += new System.EventHandler(this.txt_Search_TextChanged);
            // 
            // txt_ProductID
            // 
            this.txt_ProductID.Animated = true;
            this.txt_ProductID.BackColor = System.Drawing.Color.Transparent;
            this.txt_ProductID.BorderColor = System.Drawing.Color.Silver;
            this.txt_ProductID.BorderRadius = 15;
            this.txt_ProductID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ProductID.DefaultText = "";
            this.txt_ProductID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ProductID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ProductID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductID.Enabled = false;
            this.txt_ProductID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ProductID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ProductID.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductID.IconLeft = global::MiniMarketManagement.Properties.Resources.id__1_;
            this.txt_ProductID.Location = new System.Drawing.Point(160, 101);
            this.txt_ProductID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ProductID.Name = "txt_ProductID";
            this.txt_ProductID.PasswordChar = '\0';
            this.txt_ProductID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ProductID.PlaceholderText = "Mã sản phẩm";
            this.txt_ProductID.SelectedText = "";
            this.txt_ProductID.ShadowDecoration.BorderRadius = 15;
            this.txt_ProductID.ShadowDecoration.Enabled = true;
            this.txt_ProductID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ProductID.Size = new System.Drawing.Size(300, 35);
            this.txt_ProductID.TabIndex = 10;
            // 
            // pb_ProductImage
            // 
            this.pb_ProductImage.BorderRadius = 5;
            this.pb_ProductImage.Image = global::MiniMarketManagement.Properties.Resources.image__2_;
            this.pb_ProductImage.ImageRotate = 0F;
            this.pb_ProductImage.InitialImage = global::MiniMarketManagement.Properties.Resources.image;
            this.pb_ProductImage.Location = new System.Drawing.Point(170, 225);
            this.pb_ProductImage.Name = "pb_ProductImage";
            this.pb_ProductImage.Size = new System.Drawing.Size(250, 155);
            this.pb_ProductImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pb_ProductImage.TabIndex = 12;
            this.pb_ProductImage.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(15, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(98, 23);
            this.label9.TabIndex = 4;
            this.label9.Text = "Tìm kiếm";
            // 
            // txt_ProductName
            // 
            this.txt_ProductName.Animated = true;
            this.txt_ProductName.BackColor = System.Drawing.Color.Transparent;
            this.txt_ProductName.BorderColor = System.Drawing.Color.Silver;
            this.txt_ProductName.BorderRadius = 15;
            this.txt_ProductName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ProductName.DefaultText = "";
            this.txt_ProductName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ProductName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ProductName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductName.Enabled = false;
            this.txt_ProductName.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ProductName.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductName.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductName.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ProductName.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductName.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductName.IconLeft = global::MiniMarketManagement.Properties.Resources.features1;
            this.txt_ProductName.Location = new System.Drawing.Point(160, 150);
            this.txt_ProductName.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ProductName.Name = "txt_ProductName";
            this.txt_ProductName.PasswordChar = '\0';
            this.txt_ProductName.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ProductName.PlaceholderText = "Tên sản phẩm";
            this.txt_ProductName.SelectedText = "";
            this.txt_ProductName.ShadowDecoration.BorderRadius = 15;
            this.txt_ProductName.ShadowDecoration.Enabled = true;
            this.txt_ProductName.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ProductName.Size = new System.Drawing.Size(300, 35);
            this.txt_ProductName.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(15, 101);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(136, 23);
            this.label5.TabIndex = 4;
            this.label5.Text = "Mã sản phẩm";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(15, 240);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(145, 23);
            this.label8.TabIndex = 4;
            this.label8.Text = "Ảnh sản phẩm";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(15, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(143, 23);
            this.label4.TabIndex = 4;
            this.label4.Text = "Tên sản phẩm";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(24, 479);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(345, 23);
            this.label3.TabIndex = 4;
            this.label3.Text = "Số lượng của sản phẩm muốn nhập";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(642, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(206, 23);
            this.label6.TabIndex = 4;
            this.label6.Text = "Danh sách sản phẩm";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(642, 334);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(287, 23);
            this.label7.TabIndex = 4;
            this.label7.Text = "Danh sách sản phẩm đã chọn";
            // 
            // LargeIcon
            // 
            this.LargeIcon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("LargeIcon.ImageStream")));
            this.LargeIcon.TransparentColor = System.Drawing.Color.Transparent;
            this.LargeIcon.Images.SetKeyName(0, "OIP.jpg");
            // 
            // dgv_ListSelectedProduct
            // 
            this.dgv_ListSelectedProduct.AllowUserToAddRows = false;
            this.dgv_ListSelectedProduct.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.dgv_ListSelectedProduct.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_ListSelectedProduct.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ListSelectedProduct.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_ListSelectedProduct.ColumnHeadersHeight = 18;
            this.dgv_ListSelectedProduct.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ListSelectedProduct.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Product_ID,
            this.Product_Name,
            this.Product_Type_Name,
            this.Buy_Price,
            this.Calculation_Unit_Name,
            this.Product_Quantity,
            this.Supplier});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ListSelectedProduct.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_ListSelectedProduct.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListSelectedProduct.Location = new System.Drawing.Point(521, 364);
            this.dgv_ListSelectedProduct.Name = "dgv_ListSelectedProduct";
            this.dgv_ListSelectedProduct.ReadOnly = true;
            this.dgv_ListSelectedProduct.RowHeadersVisible = false;
            this.dgv_ListSelectedProduct.RowHeadersWidth = 51;
            this.dgv_ListSelectedProduct.RowTemplate.Height = 24;
            this.dgv_ListSelectedProduct.Size = new System.Drawing.Size(611, 283);
            this.dgv_ListSelectedProduct.TabIndex = 15;
            this.dgv_ListSelectedProduct.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ListSelectedProduct.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_ListSelectedProduct.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_ListSelectedProduct.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_ListSelectedProduct.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_ListSelectedProduct.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_ListSelectedProduct.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListSelectedProduct.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_ListSelectedProduct.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_ListSelectedProduct.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ListSelectedProduct.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_ListSelectedProduct.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ListSelectedProduct.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_ListSelectedProduct.ThemeStyle.ReadOnly = true;
            this.dgv_ListSelectedProduct.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ListSelectedProduct.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_ListSelectedProduct.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ListSelectedProduct.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ListSelectedProduct.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_ListSelectedProduct.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ListSelectedProduct.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ListSelectedProduct.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ListSelectedProduct_CellClick);
            this.dgv_ListSelectedProduct.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ProductsList_CellDoubleClick);
            // 
            // Product_ID
            // 
            this.Product_ID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Product_ID.FillWeight = 110F;
            this.Product_ID.HeaderText = "Mã sản phẩm";
            this.Product_ID.MinimumWidth = 6;
            this.Product_ID.Name = "Product_ID";
            this.Product_ID.ReadOnly = true;
            // 
            // Product_Name
            // 
            this.Product_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Product_Name.FillWeight = 120F;
            this.Product_Name.HeaderText = "Tên sản phẩm";
            this.Product_Name.MinimumWidth = 6;
            this.Product_Name.Name = "Product_Name";
            this.Product_Name.ReadOnly = true;
            // 
            // Product_Type_Name
            // 
            this.Product_Type_Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Product_Type_Name.FillWeight = 80F;
            this.Product_Type_Name.HeaderText = "Loại";
            this.Product_Type_Name.MinimumWidth = 6;
            this.Product_Type_Name.Name = "Product_Type_Name";
            this.Product_Type_Name.ReadOnly = true;
            // 
            // Buy_Price
            // 
            this.Buy_Price.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Buy_Price.FillWeight = 80F;
            this.Buy_Price.HeaderText = "Giá mua";
            this.Buy_Price.MinimumWidth = 6;
            this.Buy_Price.Name = "Buy_Price";
            this.Buy_Price.ReadOnly = true;
            // 
            // Calculation_Unit_Name
            // 
            this.Calculation_Unit_Name.FillWeight = 90F;
            this.Calculation_Unit_Name.HeaderText = "Đơn vị tính";
            this.Calculation_Unit_Name.MinimumWidth = 6;
            this.Calculation_Unit_Name.Name = "Calculation_Unit_Name";
            this.Calculation_Unit_Name.ReadOnly = true;
            // 
            // Product_Quantity
            // 
            this.Product_Quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Product_Quantity.FillWeight = 80F;
            this.Product_Quantity.HeaderText = "Số lượng";
            this.Product_Quantity.MinimumWidth = 6;
            this.Product_Quantity.Name = "Product_Quantity";
            this.Product_Quantity.ReadOnly = true;
            // 
            // Supplier
            // 
            this.Supplier.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Supplier.FillWeight = 110F;
            this.Supplier.HeaderText = "Nhà cung cấp";
            this.Supplier.MinimumWidth = 6;
            this.Supplier.Name = "Supplier";
            this.Supplier.ReadOnly = true;
            // 
            // dgv_ProductsList
            // 
            this.dgv_ProductsList.AllowUserToAddRows = false;
            this.dgv_ProductsList.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dgv_ProductsList.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_ProductsList.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ProductsList.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_ProductsList.ColumnHeadersHeight = 18;
            this.dgv_ProductsList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ProductsList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ProductID,
            this.Product__Name,
            this.ProductTypeName,
            this.BuyPrice,
            this.CalculationUnitName,
            this.SupplierName});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ProductsList.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_ProductsList.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ProductsList.Location = new System.Drawing.Point(521, 35);
            this.dgv_ProductsList.Name = "dgv_ProductsList";
            this.dgv_ProductsList.ReadOnly = true;
            this.dgv_ProductsList.RowHeadersVisible = false;
            this.dgv_ProductsList.RowHeadersWidth = 51;
            this.dgv_ProductsList.RowTemplate.Height = 24;
            this.dgv_ProductsList.Size = new System.Drawing.Size(611, 296);
            this.dgv_ProductsList.TabIndex = 15;
            this.dgv_ProductsList.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ProductsList.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_ProductsList.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_ProductsList.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_ProductsList.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_ProductsList.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_ProductsList.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ProductsList.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_ProductsList.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_ProductsList.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ProductsList.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_ProductsList.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ProductsList.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_ProductsList.ThemeStyle.ReadOnly = true;
            this.dgv_ProductsList.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ProductsList.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_ProductsList.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ProductsList.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ProductsList.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_ProductsList.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ProductsList.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ProductsList.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ProductsList_CellClick);
            this.dgv_ProductsList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ProductsList_CellDoubleClick);
            // 
            // ProductID
            // 
            this.ProductID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProductID.FillWeight = 110F;
            this.ProductID.HeaderText = "Mã sản phẩm";
            this.ProductID.MinimumWidth = 6;
            this.ProductID.Name = "ProductID";
            this.ProductID.ReadOnly = true;
            // 
            // Product__Name
            // 
            this.Product__Name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Product__Name.FillWeight = 120F;
            this.Product__Name.HeaderText = "Tên sản phẩm";
            this.Product__Name.MinimumWidth = 6;
            this.Product__Name.Name = "Product__Name";
            this.Product__Name.ReadOnly = true;
            // 
            // ProductTypeName
            // 
            this.ProductTypeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProductTypeName.FillWeight = 80F;
            this.ProductTypeName.HeaderText = "Loại";
            this.ProductTypeName.MinimumWidth = 6;
            this.ProductTypeName.Name = "ProductTypeName";
            this.ProductTypeName.ReadOnly = true;
            // 
            // BuyPrice
            // 
            this.BuyPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.BuyPrice.FillWeight = 80F;
            this.BuyPrice.HeaderText = "Giá mua";
            this.BuyPrice.MinimumWidth = 6;
            this.BuyPrice.Name = "BuyPrice";
            this.BuyPrice.ReadOnly = true;
            // 
            // CalculationUnitName
            // 
            this.CalculationUnitName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CalculationUnitName.FillWeight = 90F;
            this.CalculationUnitName.HeaderText = "Đơn vị tính";
            this.CalculationUnitName.MinimumWidth = 6;
            this.CalculationUnitName.Name = "CalculationUnitName";
            this.CalculationUnitName.ReadOnly = true;
            // 
            // SupplierName
            // 
            this.SupplierName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.SupplierName.FillWeight = 110F;
            this.SupplierName.HeaderText = "Nhà cung cấp";
            this.SupplierName.MinimumWidth = 6;
            this.SupplierName.Name = "SupplierName";
            this.SupplierName.ReadOnly = true;
            // 
            // txt_ProductQuantity
            // 
            this.txt_ProductQuantity.Animated = true;
            this.txt_ProductQuantity.BackColor = System.Drawing.Color.Transparent;
            this.txt_ProductQuantity.BorderColor = System.Drawing.Color.Silver;
            this.txt_ProductQuantity.BorderRadius = 15;
            this.txt_ProductQuantity.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ProductQuantity.DefaultText = "";
            this.txt_ProductQuantity.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ProductQuantity.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ProductQuantity.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductQuantity.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductQuantity.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ProductQuantity.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductQuantity.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductQuantity.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductQuantity.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ProductQuantity.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductQuantity.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductQuantity.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductQuantity.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductQuantity.IconLeft = global::MiniMarketManagement.Properties.Resources.how_much;
            this.txt_ProductQuantity.Location = new System.Drawing.Point(28, 521);
            this.txt_ProductQuantity.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ProductQuantity.Name = "txt_ProductQuantity";
            this.txt_ProductQuantity.PasswordChar = '\0';
            this.txt_ProductQuantity.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ProductQuantity.PlaceholderText = "Số lượng";
            this.txt_ProductQuantity.SelectedText = "";
            this.txt_ProductQuantity.ShadowDecoration.BorderRadius = 15;
            this.txt_ProductQuantity.ShadowDecoration.Enabled = true;
            this.txt_ProductQuantity.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ProductQuantity.Size = new System.Drawing.Size(193, 35);
            this.txt_ProductQuantity.TabIndex = 11;
            // 
            // btn_Order
            // 
            this.btn_Order.BackColor = System.Drawing.Color.Transparent;
            this.btn_Order.BorderRadius = 10;
            this.btn_Order.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Order.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Order.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Order.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Order.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Order.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Order.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Order.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Order.ForeColor = System.Drawing.Color.White;
            this.btn_Order.Image = global::MiniMarketManagement.Properties.Resources.package;
            this.btn_Order.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Order.Location = new System.Drawing.Point(28, 610);
            this.btn_Order.Name = "btn_Order";
            this.btn_Order.ShadowDecoration.BorderRadius = 10;
            this.btn_Order.ShadowDecoration.Enabled = true;
            this.btn_Order.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Order.Size = new System.Drawing.Size(123, 37);
            this.btn_Order.TabIndex = 9;
            this.btn_Order.Text = "Đặt hàng";
            this.btn_Order.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_Order.Click += new System.EventHandler(this.btn_Order_Click);
            // 
            // btn_MinusQuantityOrDeleteProduct
            // 
            this.btn_MinusQuantityOrDeleteProduct.BackColor = System.Drawing.Color.Transparent;
            this.btn_MinusQuantityOrDeleteProduct.BorderRadius = 10;
            this.btn_MinusQuantityOrDeleteProduct.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_MinusQuantityOrDeleteProduct.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_MinusQuantityOrDeleteProduct.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_MinusQuantityOrDeleteProduct.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_MinusQuantityOrDeleteProduct.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_MinusQuantityOrDeleteProduct.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_MinusQuantityOrDeleteProduct.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_MinusQuantityOrDeleteProduct.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_MinusQuantityOrDeleteProduct.ForeColor = System.Drawing.Color.White;
            this.btn_MinusQuantityOrDeleteProduct.Image = global::MiniMarketManagement.Properties.Resources.empty_cart;
            this.btn_MinusQuantityOrDeleteProduct.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_MinusQuantityOrDeleteProduct.Location = new System.Drawing.Point(377, 610);
            this.btn_MinusQuantityOrDeleteProduct.Name = "btn_MinusQuantityOrDeleteProduct";
            this.btn_MinusQuantityOrDeleteProduct.ShadowDecoration.BorderRadius = 10;
            this.btn_MinusQuantityOrDeleteProduct.ShadowDecoration.Enabled = true;
            this.btn_MinusQuantityOrDeleteProduct.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_MinusQuantityOrDeleteProduct.Size = new System.Drawing.Size(116, 37);
            this.btn_MinusQuantityOrDeleteProduct.TabIndex = 9;
            this.btn_MinusQuantityOrDeleteProduct.Text = "Bớt/Xóa";
            this.btn_MinusQuantityOrDeleteProduct.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_MinusQuantityOrDeleteProduct.Click += new System.EventHandler(this.btn_MinusQuantityOrDeleteProduct_Click);
            // 
            // btn_AddProduct
            // 
            this.btn_AddProduct.BackColor = System.Drawing.Color.Transparent;
            this.btn_AddProduct.BorderRadius = 10;
            this.btn_AddProduct.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_AddProduct.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_AddProduct.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_AddProduct.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_AddProduct.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_AddProduct.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_AddProduct.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_AddProduct.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_AddProduct.ForeColor = System.Drawing.Color.White;
            this.btn_AddProduct.Image = global::MiniMarketManagement.Properties.Resources.add_to_cart__1_;
            this.btn_AddProduct.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_AddProduct.Location = new System.Drawing.Point(216, 610);
            this.btn_AddProduct.Name = "btn_AddProduct";
            this.btn_AddProduct.ShadowDecoration.BorderRadius = 10;
            this.btn_AddProduct.ShadowDecoration.Enabled = true;
            this.btn_AddProduct.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_AddProduct.Size = new System.Drawing.Size(96, 37);
            this.btn_AddProduct.TabIndex = 9;
            this.btn_AddProduct.Text = "Thêm";
            this.btn_AddProduct.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_AddProduct.Click += new System.EventHandler(this.btn_AddProduct_Click);
            // 
            // printImportProductCardDialog
            // 
            this.printImportProductCardDialog.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printImportProductCardDialog.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printImportProductCardDialog.ClientSize = new System.Drawing.Size(400, 300);
            this.printImportProductCardDialog.Enabled = true;
            this.printImportProductCardDialog.Icon = ((System.Drawing.Icon)(resources.GetObject("printImportProductCardDialog.Icon")));
            this.printImportProductCardDialog.Name = "printImportProductCardDialog";
            this.printImportProductCardDialog.Visible = false;
            // 
            // frm_ImportProduct
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.dgv_ProductsList);
            this.Controls.Add(this.dgv_ListSelectedProduct);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.txt_ImportProductAmount);
            this.Controls.Add(this.txt_ProductQuantity);
            this.Controls.Add(this.btn_Order);
            this.Controls.Add(this.btn_MinusQuantityOrDeleteProduct);
            this.Controls.Add(this.btn_AddProduct);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cb_DisplayLowProduct);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_ImportProduct";
            this.Text = "ImportProduct";
            this.Load += new System.EventHandler(this.frm_ImportProduct_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pb_ProductImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ListSelectedProduct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ProductsList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2ToggleSwitch cb_DisplayLowProduct;
        private Guna.UI2.WinForms.Guna2GradientButton btn_AddProduct;
        private Guna.UI2.WinForms.Guna2TextBox txt_ProductID;
        private Guna.UI2.WinForms.Guna2TextBox txt_ProductName;
        private Guna.UI2.WinForms.Guna2PictureBox pb_ProductImage;
        private Guna.UI2.WinForms.Guna2TextBox txt_ImportProductAmount;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2TextBox txt_ProductQuantity;
        private Guna.UI2.WinForms.Guna2GradientButton btn_Order;
        private Guna.UI2.WinForms.Guna2GradientButton btn_MinusQuantityOrDeleteProduct;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ImageList LargeIcon;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_ListSelectedProduct;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_ProductsList;
        private Guna.UI2.WinForms.Guna2TextBox txt_Search;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_Type_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Buy_Price;
        private System.Windows.Forms.DataGridViewTextBoxColumn Calculation_Unit_Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Product__Name;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuyPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn CalculationUnitName;
        private System.Windows.Forms.DataGridViewTextBoxColumn SupplierName;
        private System.Drawing.Printing.PrintDocument printImportProductCard;
        private System.Windows.Forms.PrintPreviewDialog printImportProductCardDialog;
    }
}
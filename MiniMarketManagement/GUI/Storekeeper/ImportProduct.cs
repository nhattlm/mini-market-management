﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniMarketManagement.BLL.Storekeeper;
using MiniMarketManagement.DAL.Entities;
namespace MiniMarketManagement.GUI.Storekeeper
{
    public partial class frm_ImportProduct : Form
    {
        public frm_ImportProduct()
        {
            InitializeComponent();
        }

        private void frm_ImportProduct_Load(object sender, EventArgs e)
        {
            BLL_ImportProduct bLL_ImportProduct = new BLL_ImportProduct();
            bLL_ImportProduct.FillDGV(dgv_ProductsList);

        }
        private void txt_Search_TextChanged(object sender, EventArgs e)
        {
            BLL_ImportProduct bLL_ImportProduct = new BLL_ImportProduct();
            var txt = txt_Search.Text;
            bLL_ImportProduct.SearchProduct(txt, dgv_ProductsList, "ProductID", "Product__Name");
            bLL_ImportProduct.SearchProduct(txt, dgv_ListSelectedProduct, "Product_ID", "Product_Name");
        }
        private void dgv_ProductsList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BLL_ImportProduct bLL_ImportProduct = new BLL_ImportProduct();
            var row = e.RowIndex;
            if (row < 0)
            {
                return;
            }
            else
            {
                Image image;
                string product_ID;
                string product_Name;
                bLL_ImportProduct.FillPicboxAndTxtProductIDAndTxtName(row, dgv_ProductsList, out image, out product_ID, out product_Name, "ProductID", "Product__Name");
                pb_ProductImage.Image = image;
                txt_ProductID.Text = product_ID.Trim();
                txt_ProductName.Text = product_Name.Trim();
            }
        }
        ImportProductCard importProductCard = new ImportProductCard();
        private void btn_Order_Click(object sender, EventArgs e)
        {
            List<ImportProductCard> listIPC = new List<ImportProductCard>();
            BLL_ImportProduct bLL_ImportProduct = new BLL_ImportProduct();

            if (bLL_ImportProduct.AddSelectedProductsToListIPCAndDIPC(dgv_ListSelectedProduct, dgv_ProductsList) == 1)
            {
                bLL_ImportProduct.GetListImportProductCards(out listIPC);
                foreach (var i in listIPC)
                {
                    importProductCard = i;
                    PrintDocument printDocument = new PrintDocument();
                    printDocument.PrintPage += new PrintPageEventHandler(CreatePrintImportProductCard);

                    // Hiển thị hộp thoại xem trước và in
                    PrintPreviewDialog printPreviewDialog = new PrintPreviewDialog();
                    printPreviewDialog.Document = printDocument;
                    printPreviewDialog.ShowDialog();

                }
                txt_ImportProductAmount.Text = "0";
                txt_Search.Text = "";
            }
            else
            {
                return;
            }
        }

        private void btn_AddProduct_Click(object sender, EventArgs e)
        {
            BLL_ImportProduct bLL_ImportProduct = new BLL_ImportProduct();
            string ID = txt_ProductID.Text.Trim();
            string Quantity = txt_ProductQuantity.Text.Trim();
            string Name;
            Image image;
            if (bLL_ImportProduct.importProductToSelectedListByProductID(dgv_ListSelectedProduct, ID, Quantity) == 1)
            {
                bLL_ImportProduct.ClearInputFields(out ID, out image, out Quantity, out Name);
                pb_ProductImage.Image = image;
                txt_ProductName.Text = Name;
                txt_ProductID.Text = ID;
                txt_ProductQuantity.Text = Quantity;
                txt_ImportProductAmount.Text = bLL_ImportProduct.sumBuyPriceOfSelectedProductsList(dgv_ListSelectedProduct, "Buy_Price", "Product_Quantity").ToString();
            }
            else
            {
                return;
            }

        }

        private void btn_MinusQuantityOrDeleteProduct_Click(object sender, EventArgs e)
        {
            BLL_ImportProduct bLL_ImportProduct = new BLL_ImportProduct();
            string ID = txt_ProductID.Text.Trim();
            string Quantity = txt_ProductQuantity.Text.Trim();
            string Name;
            Image image;
            if (bLL_ImportProduct.MinusQuantityOrDeleteProduct(dgv_ListSelectedProduct, ID, Quantity) == 1)
            {
                bLL_ImportProduct.ClearInputFields(out ID, out image, out Quantity, out Name);
                pb_ProductImage.Image = image;
                txt_ProductName.Text = Name;
                txt_ProductID.Text = ID;
                txt_ProductQuantity.Text = Quantity;
                txt_ImportProductAmount.Text = bLL_ImportProduct.sumBuyPriceOfSelectedProductsList(dgv_ListSelectedProduct, "Buy_Price", "Product_Quantity").ToString();
            }
            else
            {
                return;
            }

        }
        private void dgv_ListSelectedProduct_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BLL_ImportProduct bLL_ImportProduct = new BLL_ImportProduct();
            var row = e.RowIndex;
            if (row < 0)
            {
                return;
            }
            else
            {
                Image image;
                string product_ID;
                string product_Name;
                bLL_ImportProduct.FillPicboxAndTxtProductIDAndTxtName(row, dgv_ListSelectedProduct, out image, out product_ID, out product_Name, "Product_ID", "Product_Name");
                pb_ProductImage.Image = image;
                txt_ProductID.Text = product_ID.Trim();
                txt_ProductName.Text = product_Name.Trim();
            }
        }
        bool cb = false;
        private void cb_DisplayLowProduct_CheckedChanged(object sender, EventArgs e)
        {
            BLL_ImportProduct bLL_ImportProduct = new BLL_ImportProduct();
            if (!cb)
            {
                bLL_ImportProduct.DisplayLowQuantityProduct(dgv_ProductsList, "ProductID");
                cb = true;
            }
            else
            {
                bLL_ImportProduct.FillDGV(dgv_ProductsList);
                cb = false;
            }
        }

        private void dgv_ProductsList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BLL_ImportProduct bLL_ImportProduct = new BLL_ImportProduct();
            dgv_ProductsList.ClearSelection();
            string ID;
            string Quantity;
            string Name;
            Image image;
            bLL_ImportProduct.ClearInputFields(out ID, out image, out Quantity, out Name);
            pb_ProductImage.Image = image;
            txt_ProductName.Text = Name;
            txt_ProductID.Text = ID;
            txt_ProductQuantity.Text = Quantity;
        }
        private void CreatePrintImportProductCard(Object sender, PrintPageEventArgs e)
        {
            BLL_ImportProduct bLL_ImportProduct = new BLL_ImportProduct();
            bLL_ImportProduct.CreateImport(sender, e, importProductCard, printImportProductCard, printImportProductCardDialog);

        }
    }
}

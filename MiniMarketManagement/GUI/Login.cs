﻿using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniMarketManagement.BLL.Cashier;
using MiniMarketManagement.DAL.Cashier;
using System.Data.Entity;
using System.Net.NetworkInformation;
using MiniMarketManagement.GUI.Administrator;

namespace MiniMarketManagement.GUI
{
    public partial class frm_Login : System.Windows.Forms.Form
    {
        public frm_Login()
        {
            InitializeComponent();
        }

        private void btn_Login_Click(object sender, EventArgs e)
        {
            BLL_Login login = new BLL_Login();
            login.saveUser(txt_UserName.Text);
            login.Login(txt_UserName.Text, txt_Password.Text, this);
        }

        
        private void cb_ShowPassword_CheckedChanged(object sender, EventArgs e)
        {
            BLL_Login bll_showpass = new BLL_Login();
            bll_showpass.ShowPassword(cb_ShowPassword, txt_Password);
        }

        private void btn_Exit_Click(object sender, EventArgs e)
        {
            try
            {
                this.Dispose();
                this.Close();
            }
            catch (Exception) { }
        }
        private void frm_Login_Load(object sender, EventArgs e)
        {
            txt_UserName.Select();
        }

        private void txt_Password_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter)
            {
                btn_Login_Click(sender, e);
            }
            if(e.KeyCode == Keys.Escape)
            {
                btn_Exit_Click(sender,e);
            }
        }

        private void txt_UserName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btn_Login_Click(sender, e);
            }
            if (e.KeyCode == Keys.Escape)
            {
                btn_Exit_Click(sender, e);
            }
        }
    }
}

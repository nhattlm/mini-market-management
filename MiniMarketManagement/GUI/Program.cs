﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniMarketManagement.GUI;
using MiniMarketManagement.GUI.Administrator;
using MiniMarketManagement.GUI.Cashier;
using MiniMarketManagement.GUI.Storekeeper;

namespace MiniMarketManagement
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frm_Login());
        }
    }
}

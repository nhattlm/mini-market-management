﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniMarketManagement.BLL.Cashier;
namespace MiniMarketManagement.GUI.Cashier
{
    public partial class frm_Register : System.Windows.Forms.Form
    {
        BLL_Register bll_register = new BLL_Register();
        public frm_Register()
        {
            InitializeComponent();
        }

        private void frm_Register_Load(object sender, EventArgs e)
        {
            bll_register.LoadDgvCustomer_And_textboxInvoiceID(dgv_Customer,txt_InvoiceID);
        }

        private void btn_Register_Click(object sender, EventArgs e)
        {
            bll_register.RegisterCustomer(txt_InvoiceID, txt_CustomerName, txt_CustomerPhone);
            bll_register.LoadDgvCustomer_And_textboxInvoiceID(dgv_Customer,txt_InvoiceID);
        }

        private void btn_CheckInvoiceID_Click(object sender, EventArgs e)
        {
            bll_register.btn_checkInvoiceID(txt_InvoiceID,txt_CustomerName,txt_CustomerPhone);

        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            txt_CustomerName.Clear();
            txt_CustomerPhone.Clear();
            txt_CustomerName.Enabled = false;
            txt_CustomerPhone.Enabled = false;
        }
    }
}

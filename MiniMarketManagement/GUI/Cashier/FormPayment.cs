﻿using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniMarketManagement.BLL.Cashier;
using MiniMarketManagement.DAL.Cashier;
using Guna.UI2.WinForms;
using MiniMarketManagement.BLL.Storekeeper;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Drawing.Printing;
using System.IO;

namespace MiniMarketManagement.GUI.Cashier
{
    public partial class frm_FormPayment : System.Windows.Forms.Form
    {
        BLL_Payment bll_payment = new BLL_Payment();
        const string EmptyImPath = "Resources\\image (2).png";

        public frm_FormPayment()
        {
            InitializeComponent();
        }


        private void frm_FormPayment_Load(object sender, EventArgs e)
        {
            bll_payment.LoadDgvFindProduct( dgvFind_Product);
        }


        private void dgvFind_Product_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            MiniMarketDB dB = new MiniMarketDB();
            var listProduct = dB.Products.ToList();
            Image image;
            bll_payment.LoadCellDgvFindProduct(sender, e, dgvFind_Product, txt_ProductID, txt_ProductName ,out image);
            pic_Product.Image = image;
            txt_Search.Text = null;
            txt_Quantity.Text = null;
            bll_payment.LoadDgvFindProduct(dgvFind_Product);
        }


        private void btn_addBill_Click(object sender, EventArgs e)
        {
            bool check = bll_payment.btn_AddOrUpdate_DgvPayProduct(dgvPayment_Product, txt_ProductID, txt_ProductName, txt_Quantity);
            if (check == false)
            {
                return;
            }
            var t = bll_payment.CalculateTotalAmount(dgvPayment_Product, txt_InputPoint.Text);
            txt_Amount.Text = t;
            string imagePath;
            string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            imagePath = Path.Combine(projectDirectory, EmptyImPath);
            pic_Product.Image = Image.FromFile(imagePath);
        }


        private void dgvPayment_Product_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if(e.RowIndex < 0)
            {
                return;
            }
            bll_payment.LoadCellDgvPayProduct(sender, e, dgvPayment_Product, txt_ProductID, txt_ProductName, txt_Quantity);
        }


        private void btn_Delete_Click(object sender, EventArgs e)
        {
            bll_payment.deleteDgvPayment(dgvPayment_Product, txt_ProductID, txt_ProductName, txt_Quantity);
        }
        private void txt_Search_TextChanged(object sender, EventArgs e)
        {
            bll_payment.SearchByProductId_Or_Name(txt_Search.Text, dgvFind_Product);
        }
        private void btn_Payment_Click(object sender, EventArgs e)
        {
            bool check = bll_payment.clickPayment(dgvPayment_Product, dgvFind_Product, txt_InputPoint.Text, txt_CustomerID.Text, txt_ShowPoint.Text, cb_AccumulatePoints, cb_UsePoints);
            if (check == false)
            {
                return;
            }
            var t = bll_payment.CalculateTotalAmount(dgvPayment_Product, txt_InputPoint.Text);
            txt_Amount.Text = t;
            PrintDocument printDocument = new PrintDocument();
            printDocument.PrintPage += new PrintPageEventHandler(CreatePrintInvoice);

            // Hiển thị hộp thoại xem trước và in
            PrintPreviewDialog printPreviewDialog = new PrintPreviewDialog();
            printPreviewDialog.Document = printDocument;
            printPreviewDialog.ShowDialog();
        }

        private void cb_AccumulatePoints_CheckedChanged(object sender, EventArgs e)
        {
            bll_payment.AccumulatePoints(cb_AccumulatePoints, txt_CustomerID,txt_ShowPoint,btn_SearchCustomer);
        }

        private void cb_UsePoints_CheckedChanged(object sender, EventArgs e)
        {
            bll_payment.UsePoints(cb_UsePoints, txt_InputPoint);
        }
        private void btn_SearchCustomer_Click(object sender, EventArgs e)
        {
            bll_payment.FindCustomerID_And_ShowPoint(cb_AccumulatePoints, txt_CustomerID, txt_ShowPoint);
        }

        private void txt_InputPoint_TextChanged(object sender, EventArgs e)
        {
            var t = bll_payment.CalculateTotalAmount(dgvPayment_Product, txt_InputPoint.Text);
            txt_Amount.Text = t;
        }
        private void CreatePrintInvoice(Object sender, PrintPageEventArgs e)
        {
            bll_payment.CreateInvoice(sender, e, printInvoice, printdiaInvoice);
        }

    }
}

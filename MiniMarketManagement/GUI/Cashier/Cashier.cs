﻿using Guna.UI2.WinForms;
using MiniMarketManagement.BLL.Administrator;
using MiniMarketManagement.DAL.Administrator;
using MiniMarketManagement.GUI.Storekeeper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.GUI.Cashier
{
    public partial class frm_Cashier : Form
    {
        ShowChildrenForm showChildrenForm = new ShowChildrenForm();
        BLL_EmployeeManage BLL_EmployeeManage = new BLL_EmployeeManage();
        public frm_Cashier()
        {
            InitializeComponent();
        }
        private void frm_Cashier_Load(object sender, EventArgs e)
        {
            btn_Payment_Click(sender, e);
            lb_EmployeeName.Text = showEmployeeName();
        }
        private string showEmployeeName()
        {
            return "Xin chào " + BLL_EmployeeManage.GetNameEmployeesByUsername();
        }
        private void btn_Payment_Click(object sender, EventArgs e)
        {
            frm_FormPayment formPayment = new frm_FormPayment();
            showChildrenForm.ShowForm(formPayment, btn_Payment, panel_Main);
            panel_Exit.Visible = false;
        }

        private void btn_Register_Click(object sender, EventArgs e)
        {         
            frm_Register formRegister = new frm_Register();
            showChildrenForm.ShowForm(formRegister, btn_Register, panel_Main);
            panel_Exit.Visible = false;
        }
        bool isPanelVisible = false;
        Guna2Button currentButton;
        private void btn_Exit_Click(object sender, EventArgs e)
        {
            currentButton = btn_Exit;
            if (isPanelVisible)
            {
                // Nếu panel đang hiển thị, ẩn nó
                panel_Exit.Visible = false;
                isPanelVisible = false;
                currentButton.Checked = false;
            }
            else
            {
                // Nếu panel không hiển thị, hiển thị nó
                panel_Exit.Visible = true;
                isPanelVisible = true;
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            try
            {
                this.Dispose();
                this.Close();
            }
            catch (Exception) { }
        }

        private void btn_SignOut_Click(object sender, EventArgs e)
        {
            try
            {
                frm_Login loginForm = new frm_Login();
                loginForm.Show();
                this.Hide();
            }
            catch (Exception) { }
        }

        private void btn_Minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void ibtn_Logo_DoubleClick(object sender, EventArgs e)
        {
            frm_Info info = new frm_Info();
            info.Show();
        }
    }
}

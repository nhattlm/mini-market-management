﻿namespace MiniMarketManagement.GUI.Cashier
{
    partial class frm_Cashier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Cashier));
            this.panel_Main = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.panel_Exit = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.btn_Close = new Guna.UI2.WinForms.Guna2Button();
            this.btn_SignOut = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.guna2DragControl2 = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.panel_CashierSideBoard = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.lb_EmployeeName = new System.Windows.Forms.Label();
            this.guna2Separator2 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2Separator1 = new Guna.UI2.WinForms.Guna2Separator();
            this.btn_Minimize = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Exit = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Register = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Payment = new Guna.UI2.WinForms.Guna2Button();
            this.ibtn_Logo = new Guna.UI2.WinForms.Guna2ImageButton();
            this.panel_Main.SuspendLayout();
            this.panel_Exit.SuspendLayout();
            this.panel_CashierSideBoard.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Main
            // 
            this.panel_Main.BackColor = System.Drawing.Color.Transparent;
            this.panel_Main.Controls.Add(this.panel_Exit);
            this.panel_Main.FillColor = System.Drawing.Color.White;
            this.panel_Main.Location = new System.Drawing.Point(244, -1);
            this.panel_Main.Name = "panel_Main";
            this.panel_Main.Radius = 5;
            this.panel_Main.ShadowColor = System.Drawing.Color.Black;
            this.panel_Main.ShadowDepth = 95;
            this.panel_Main.ShadowShift = 8;
            this.panel_Main.Size = new System.Drawing.Size(1139, 696);
            this.panel_Main.TabIndex = 4;
            // 
            // panel_Exit
            // 
            this.panel_Exit.BackColor = System.Drawing.Color.Transparent;
            this.panel_Exit.BorderRadius = 10;
            this.panel_Exit.Controls.Add(this.btn_Close);
            this.panel_Exit.Controls.Add(this.btn_SignOut);
            this.panel_Exit.FillColor = System.Drawing.Color.Crimson;
            this.panel_Exit.FillColor2 = System.Drawing.Color.Red;
            this.panel_Exit.FillColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panel_Exit.FillColor4 = System.Drawing.Color.Crimson;
            this.panel_Exit.ForeColor = System.Drawing.Color.Transparent;
            this.panel_Exit.Location = new System.Drawing.Point(0, 570);
            this.panel_Exit.Name = "panel_Exit";
            this.panel_Exit.ShadowDecoration.Depth = 10;
            this.panel_Exit.ShadowDecoration.Enabled = true;
            this.panel_Exit.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.panel_Exit.Size = new System.Drawing.Size(225, 123);
            this.panel_Exit.TabIndex = 2;
            this.panel_Exit.Visible = false;
            // 
            // btn_Close
            // 
            this.btn_Close.Animated = true;
            this.btn_Close.BackColor = System.Drawing.Color.Transparent;
            this.btn_Close.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Close.BorderRadius = 15;
            this.btn_Close.BorderThickness = 2;
            this.btn_Close.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Close.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Close.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Close.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Close.FillColor = System.Drawing.Color.Transparent;
            this.btn_Close.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.btn_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Close.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Close.Image = global::MiniMarketManagement.Properties.Resources.exit__1_;
            this.btn_Close.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Close.ImageOffset = new System.Drawing.Point(2, 0);
            this.btn_Close.ImageSize = new System.Drawing.Size(25, 25);
            this.btn_Close.IndicateFocus = true;
            this.btn_Close.Location = new System.Drawing.Point(3, 3);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.ShadowDecoration.Depth = 1;
            this.btn_Close.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Close.Size = new System.Drawing.Size(219, 55);
            this.btn_Close.TabIndex = 2;
            this.btn_Close.Text = "Thoát";
            this.btn_Close.UseTransparentBackground = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_SignOut
            // 
            this.btn_SignOut.Animated = true;
            this.btn_SignOut.BackColor = System.Drawing.Color.Transparent;
            this.btn_SignOut.BorderColor = System.Drawing.Color.Transparent;
            this.btn_SignOut.BorderRadius = 15;
            this.btn_SignOut.BorderThickness = 2;
            this.btn_SignOut.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_SignOut.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_SignOut.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_SignOut.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_SignOut.FillColor = System.Drawing.Color.Transparent;
            this.btn_SignOut.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.btn_SignOut.ForeColor = System.Drawing.Color.White;
            this.btn_SignOut.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_SignOut.Image = global::MiniMarketManagement.Properties.Resources.logout;
            this.btn_SignOut.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_SignOut.ImageOffset = new System.Drawing.Point(2, 0);
            this.btn_SignOut.ImageSize = new System.Drawing.Size(26, 26);
            this.btn_SignOut.IndicateFocus = true;
            this.btn_SignOut.Location = new System.Drawing.Point(3, 64);
            this.btn_SignOut.Name = "btn_SignOut";
            this.btn_SignOut.ShadowDecoration.Depth = 1;
            this.btn_SignOut.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_SignOut.Size = new System.Drawing.Size(219, 55);
            this.btn_SignOut.TabIndex = 2;
            this.btn_SignOut.Text = "Đăng xuất";
            this.btn_SignOut.UseTransparentBackground = true;
            this.btn_SignOut.Click += new System.EventHandler(this.btn_SignOut_Click);
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // guna2DragControl2
            // 
            this.guna2DragControl2.DockIndicatorTransparencyValue = 0.6D;
            this.guna2DragControl2.TargetControl = this.panel_CashierSideBoard;
            this.guna2DragControl2.TransparentWhileDrag = false;
            // 
            // panel_CashierSideBoard
            // 
            this.panel_CashierSideBoard.BackColor = System.Drawing.Color.Transparent;
            this.panel_CashierSideBoard.BorderRadius = 10;
            this.panel_CashierSideBoard.Controls.Add(this.lb_EmployeeName);
            this.panel_CashierSideBoard.Controls.Add(this.guna2Separator2);
            this.panel_CashierSideBoard.Controls.Add(this.guna2Separator1);
            this.panel_CashierSideBoard.Controls.Add(this.btn_Minimize);
            this.panel_CashierSideBoard.Controls.Add(this.btn_Exit);
            this.panel_CashierSideBoard.Controls.Add(this.btn_Register);
            this.panel_CashierSideBoard.Controls.Add(this.btn_Payment);
            this.panel_CashierSideBoard.Controls.Add(this.ibtn_Logo);
            this.panel_CashierSideBoard.FillColor = System.Drawing.Color.HotPink;
            this.panel_CashierSideBoard.FillColor2 = System.Drawing.Color.Red;
            this.panel_CashierSideBoard.FillColor3 = System.Drawing.Color.LightCyan;
            this.panel_CashierSideBoard.FillColor4 = System.Drawing.Color.Crimson;
            this.panel_CashierSideBoard.Location = new System.Drawing.Point(-19, -1);
            this.panel_CashierSideBoard.Name = "panel_CashierSideBoard";
            this.panel_CashierSideBoard.ShadowDecoration.Depth = 10;
            this.panel_CashierSideBoard.ShadowDecoration.Enabled = true;
            this.panel_CashierSideBoard.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.panel_CashierSideBoard.Size = new System.Drawing.Size(258, 696);
            this.panel_CashierSideBoard.TabIndex = 1;
            // 
            // lb_EmployeeName
            // 
            this.lb_EmployeeName.AutoSize = true;
            this.lb_EmployeeName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_EmployeeName.ForeColor = System.Drawing.Color.MistyRose;
            this.lb_EmployeeName.Location = new System.Drawing.Point(31, 92);
            this.lb_EmployeeName.Name = "lb_EmployeeName";
            this.lb_EmployeeName.Size = new System.Drawing.Size(55, 18);
            this.lb_EmployeeName.TabIndex = 9;
            this.lb_EmployeeName.Text = "label1";
            // 
            // guna2Separator2
            // 
            this.guna2Separator2.FillColor = System.Drawing.Color.LightGray;
            this.guna2Separator2.Location = new System.Drawing.Point(29, 556);
            this.guna2Separator2.Name = "guna2Separator2";
            this.guna2Separator2.Size = new System.Drawing.Size(216, 10);
            this.guna2Separator2.TabIndex = 3;
            // 
            // guna2Separator1
            // 
            this.guna2Separator1.FillColor = System.Drawing.Color.LightGray;
            this.guna2Separator1.Location = new System.Drawing.Point(31, 113);
            this.guna2Separator1.Name = "guna2Separator1";
            this.guna2Separator1.Size = new System.Drawing.Size(216, 10);
            this.guna2Separator1.TabIndex = 2;
            // 
            // btn_Minimize
            // 
            this.btn_Minimize.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar;
            this.btn_Minimize.Animated = true;
            this.btn_Minimize.BackColor = System.Drawing.Color.Transparent;
            this.btn_Minimize.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Minimize.BorderRadius = 20;
            this.btn_Minimize.BorderThickness = 2;
            this.btn_Minimize.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Minimize.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Minimize.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Minimize.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Minimize.FillColor = System.Drawing.Color.Transparent;
            this.btn_Minimize.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.btn_Minimize.ForeColor = System.Drawing.Color.White;
            this.btn_Minimize.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Minimize.HoverState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.btn_Minimize.Image = global::MiniMarketManagement.Properties.Resources.minimize1;
            this.btn_Minimize.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Minimize.ImageOffset = new System.Drawing.Point(2, 0);
            this.btn_Minimize.ImageSize = new System.Drawing.Size(26, 26);
            this.btn_Minimize.IndicateFocus = true;
            this.btn_Minimize.Location = new System.Drawing.Point(28, 572);
            this.btn_Minimize.Name = "btn_Minimize";
            this.btn_Minimize.ShadowDecoration.Depth = 1;
            this.btn_Minimize.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Minimize.Size = new System.Drawing.Size(219, 55);
            this.btn_Minimize.TabIndex = 1;
            this.btn_Minimize.Text = "Thu Nhỏ";
            this.btn_Minimize.UseTransparentBackground = true;
            this.btn_Minimize.Click += new System.EventHandler(this.btn_Minimize_Click);
            // 
            // btn_Exit
            // 
            this.btn_Exit.Animated = true;
            this.btn_Exit.BackColor = System.Drawing.Color.Transparent;
            this.btn_Exit.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Exit.BorderRadius = 20;
            this.btn_Exit.BorderThickness = 2;
            this.btn_Exit.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Exit.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Exit.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Exit.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Exit.FillColor = System.Drawing.Color.Transparent;
            this.btn_Exit.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.btn_Exit.ForeColor = System.Drawing.Color.White;
            this.btn_Exit.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Exit.Image = global::MiniMarketManagement.Properties.Resources.menu__3_1;
            this.btn_Exit.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Exit.ImageOffset = new System.Drawing.Point(4, 0);
            this.btn_Exit.ImageSize = new System.Drawing.Size(23, 23);
            this.btn_Exit.IndicateFocus = true;
            this.btn_Exit.Location = new System.Drawing.Point(28, 633);
            this.btn_Exit.Name = "btn_Exit";
            this.btn_Exit.ShadowDecoration.Depth = 1;
            this.btn_Exit.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Exit.Size = new System.Drawing.Size(219, 55);
            this.btn_Exit.TabIndex = 1;
            this.btn_Exit.Text = "Tùy chọn";
            this.btn_Exit.UseTransparentBackground = true;
            this.btn_Exit.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // btn_Register
            // 
            this.btn_Register.Animated = true;
            this.btn_Register.BackColor = System.Drawing.Color.Transparent;
            this.btn_Register.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Register.BorderRadius = 20;
            this.btn_Register.BorderThickness = 2;
            this.btn_Register.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Register.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Register.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Register.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Register.FillColor = System.Drawing.Color.Transparent;
            this.btn_Register.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Register.ForeColor = System.Drawing.Color.White;
            this.btn_Register.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Register.IndicateFocus = true;
            this.btn_Register.Location = new System.Drawing.Point(28, 188);
            this.btn_Register.Name = "btn_Register";
            this.btn_Register.ShadowDecoration.Depth = 1;
            this.btn_Register.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Register.Size = new System.Drawing.Size(219, 55);
            this.btn_Register.TabIndex = 1;
            this.btn_Register.Text = "Đăng ký";
            this.btn_Register.UseTransparentBackground = true;
            this.btn_Register.Click += new System.EventHandler(this.btn_Register_Click);
            // 
            // btn_Payment
            // 
            this.btn_Payment.Animated = true;
            this.btn_Payment.BackColor = System.Drawing.Color.Transparent;
            this.btn_Payment.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Payment.BorderRadius = 20;
            this.btn_Payment.BorderThickness = 2;
            this.btn_Payment.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Payment.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Payment.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Payment.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Payment.FillColor = System.Drawing.Color.Transparent;
            this.btn_Payment.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Payment.ForeColor = System.Drawing.Color.White;
            this.btn_Payment.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Payment.IndicateFocus = true;
            this.btn_Payment.Location = new System.Drawing.Point(28, 127);
            this.btn_Payment.Name = "btn_Payment";
            this.btn_Payment.ShadowDecoration.Depth = 1;
            this.btn_Payment.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Payment.Size = new System.Drawing.Size(219, 55);
            this.btn_Payment.TabIndex = 1;
            this.btn_Payment.Text = "Thanh toán";
            this.btn_Payment.UseTransparentBackground = true;
            this.btn_Payment.Click += new System.EventHandler(this.btn_Payment_Click);
            // 
            // ibtn_Logo
            // 
            this.ibtn_Logo.AnimatedGIF = true;
            this.ibtn_Logo.BackColor = System.Drawing.Color.Transparent;
            this.ibtn_Logo.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.ibtn_Logo.HoverState.ImageSize = new System.Drawing.Size(100, 100);
            this.ibtn_Logo.Image = ((System.Drawing.Image)(resources.GetObject("ibtn_Logo.Image")));
            this.ibtn_Logo.ImageOffset = new System.Drawing.Point(0, 0);
            this.ibtn_Logo.ImageRotate = 0F;
            this.ibtn_Logo.ImageSize = new System.Drawing.Size(90, 90);
            this.ibtn_Logo.Location = new System.Drawing.Point(81, -11);
            this.ibtn_Logo.Name = "ibtn_Logo";
            this.ibtn_Logo.PressedState.ImageSize = new System.Drawing.Size(95, 95);
            this.ibtn_Logo.Size = new System.Drawing.Size(111, 118);
            this.ibtn_Logo.TabIndex = 1;
            this.ibtn_Logo.UseTransparentBackground = true;
            this.ibtn_Logo.DoubleClick += new System.EventHandler(this.ibtn_Logo_DoubleClick);
            // 
            // frm_Cashier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1384, 695);
            this.Controls.Add(this.panel_Main);
            this.Controls.Add(this.panel_CashierSideBoard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_Cashier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cashier";
            this.Load += new System.EventHandler(this.frm_Cashier_Load);
            this.panel_Main.ResumeLayout(false);
            this.panel_Exit.ResumeLayout(false);
            this.panel_CashierSideBoard.ResumeLayout(false);
            this.panel_CashierSideBoard.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2DragControl guna2DragControl2;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel panel_CashierSideBoard;
        private Guna.UI2.WinForms.Guna2Button btn_Minimize;
        private Guna.UI2.WinForms.Guna2Button btn_Exit;
        private Guna.UI2.WinForms.Guna2Button btn_Register;
        private Guna.UI2.WinForms.Guna2Button btn_Payment;
        private Guna.UI2.WinForms.Guna2ImageButton ibtn_Logo;
        private Guna.UI2.WinForms.Guna2ShadowPanel panel_Main;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator1;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator2;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel panel_Exit;
        private Guna.UI2.WinForms.Guna2Button btn_Close;
        private Guna.UI2.WinForms.Guna2Button btn_SignOut;
        private System.Windows.Forms.Label lb_EmployeeName;
    }
}
﻿namespace MiniMarketManagement.GUI.Cashier
{
    partial class frm_Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Register));
            this.dgv_Customer = new Guna.UI2.WinForms.Guna2DataGridView();
            this.dgv_CustomerID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_CustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_CustomerPhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_PointNow = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.btn_Register = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_Cancel = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_InvoiceID = new Guna.UI2.WinForms.Guna2TextBox();
            this.btn_CheckInvoiceID = new Guna.UI2.WinForms.Guna2GradientButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txt_CustomerPhone = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_CustomerName = new Guna.UI2.WinForms.Guna2TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Customer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgv_Customer
            // 
            this.dgv_Customer.AllowUserToAddRows = false;
            this.dgv_Customer.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dgv_Customer.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_Customer.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Customer.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_Customer.ColumnHeadersHeight = 18;
            this.dgv_Customer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_Customer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv_CustomerID,
            this.dgv_CustomerName,
            this.dgv_CustomerPhone,
            this.dgv_PointNow});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Customer.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_Customer.GridColor = System.Drawing.Color.Silver;
            this.dgv_Customer.Location = new System.Drawing.Point(12, 264);
            this.dgv_Customer.Name = "dgv_Customer";
            this.dgv_Customer.ReadOnly = true;
            this.dgv_Customer.RowHeadersVisible = false;
            this.dgv_Customer.RowHeadersWidth = 51;
            this.dgv_Customer.RowTemplate.Height = 24;
            this.dgv_Customer.Size = new System.Drawing.Size(1110, 415);
            this.dgv_Customer.TabIndex = 0;
            this.dgv_Customer.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_Customer.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_Customer.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_Customer.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_Customer.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_Customer.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_Customer.ThemeStyle.GridColor = System.Drawing.Color.Silver;
            this.dgv_Customer.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_Customer.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_Customer.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_Customer.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_Customer.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_Customer.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_Customer.ThemeStyle.ReadOnly = true;
            this.dgv_Customer.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_Customer.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_Customer.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_Customer.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_Customer.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_Customer.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_Customer.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            // 
            // dgv_CustomerID
            // 
            this.dgv_CustomerID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_CustomerID.HeaderText = "Mã khách hàng";
            this.dgv_CustomerID.MinimumWidth = 6;
            this.dgv_CustomerID.Name = "dgv_CustomerID";
            this.dgv_CustomerID.ReadOnly = true;
            // 
            // dgv_CustomerName
            // 
            this.dgv_CustomerName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_CustomerName.HeaderText = "Tên khách hàng";
            this.dgv_CustomerName.MinimumWidth = 6;
            this.dgv_CustomerName.Name = "dgv_CustomerName";
            this.dgv_CustomerName.ReadOnly = true;
            // 
            // dgv_CustomerPhone
            // 
            this.dgv_CustomerPhone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_CustomerPhone.HeaderText = "Số điện thoại";
            this.dgv_CustomerPhone.MinimumWidth = 6;
            this.dgv_CustomerPhone.Name = "dgv_CustomerPhone";
            this.dgv_CustomerPhone.ReadOnly = true;
            // 
            // dgv_PointNow
            // 
            this.dgv_PointNow.HeaderText = "Số điểm hiện tại";
            this.dgv_PointNow.MinimumWidth = 6;
            this.dgv_PointNow.Name = "dgv_PointNow";
            this.dgv_PointNow.ReadOnly = true;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // btn_Register
            // 
            this.btn_Register.BackColor = System.Drawing.Color.Transparent;
            this.btn_Register.BorderRadius = 10;
            this.btn_Register.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Register.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Register.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Register.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Register.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Register.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Register.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Register.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Register.ForeColor = System.Drawing.Color.White;
            this.btn_Register.Image = ((System.Drawing.Image)(resources.GetObject("btn_Register.Image")));
            this.btn_Register.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Register.Location = new System.Drawing.Point(755, 155);
            this.btn_Register.Name = "btn_Register";
            this.btn_Register.ShadowDecoration.BorderRadius = 10;
            this.btn_Register.ShadowDecoration.Color = System.Drawing.Color.DimGray;
            this.btn_Register.ShadowDecoration.Enabled = true;
            this.btn_Register.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Register.Size = new System.Drawing.Size(113, 39);
            this.btn_Register.TabIndex = 0;
            this.btn_Register.Text = "Đăng ký";
            this.btn_Register.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_Register.Click += new System.EventHandler(this.btn_Register_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.BackColor = System.Drawing.Color.Transparent;
            this.btn_Cancel.BorderRadius = 10;
            this.btn_Cancel.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Cancel.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Cancel.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Cancel.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Cancel.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Cancel.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Cancel.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Cancel.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Cancel.ForeColor = System.Drawing.Color.White;
            this.btn_Cancel.Image = ((System.Drawing.Image)(resources.GetObject("btn_Cancel.Image")));
            this.btn_Cancel.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Cancel.Location = new System.Drawing.Point(755, 208);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.ShadowDecoration.BorderRadius = 10;
            this.btn_Cancel.ShadowDecoration.Color = System.Drawing.Color.DimGray;
            this.btn_Cancel.ShadowDecoration.Enabled = true;
            this.btn_Cancel.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Cancel.Size = new System.Drawing.Size(82, 39);
            this.btn_Cancel.TabIndex = 0;
            this.btn_Cancel.Text = "Hủy";
            this.btn_Cancel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.guna2PictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("guna2PictureBox1.Image")));
            this.guna2PictureBox1.ImageRotate = 0F;
            this.guna2PictureBox1.Location = new System.Drawing.Point(12, 12);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.Size = new System.Drawing.Size(253, 246);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.guna2PictureBox1.TabIndex = 13;
            this.guna2PictureBox1.TabStop = false;
            this.guna2PictureBox1.UseTransparentBackground = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_InvoiceID);
            this.groupBox1.Controls.Add(this.btn_CheckInvoiceID);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(433, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(316, 94);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Kiểm tra hóa đơn";
            // 
            // txt_InvoiceID
            // 
            this.txt_InvoiceID.Animated = true;
            this.txt_InvoiceID.BackColor = System.Drawing.Color.Transparent;
            this.txt_InvoiceID.BorderColor = System.Drawing.Color.LightGray;
            this.txt_InvoiceID.BorderRadius = 15;
            this.txt_InvoiceID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_InvoiceID.DefaultText = "";
            this.txt_InvoiceID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_InvoiceID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_InvoiceID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_InvoiceID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_InvoiceID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_InvoiceID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_InvoiceID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_InvoiceID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_InvoiceID.Font = new System.Drawing.Font("Tahoma", 10.2F);
            this.txt_InvoiceID.ForeColor = System.Drawing.Color.Black;
            this.txt_InvoiceID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_InvoiceID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_InvoiceID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_InvoiceID.Location = new System.Drawing.Point(11, 40);
            this.txt_InvoiceID.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.txt_InvoiceID.Name = "txt_InvoiceID";
            this.txt_InvoiceID.PasswordChar = '\0';
            this.txt_InvoiceID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_InvoiceID.PlaceholderText = "Mã hóa đơn";
            this.txt_InvoiceID.SelectedText = "";
            this.txt_InvoiceID.ShadowDecoration.BorderRadius = 15;
            this.txt_InvoiceID.ShadowDecoration.Enabled = true;
            this.txt_InvoiceID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_InvoiceID.Size = new System.Drawing.Size(212, 39);
            this.txt_InvoiceID.TabIndex = 1;
            // 
            // btn_CheckInvoiceID
            // 
            this.btn_CheckInvoiceID.BackColor = System.Drawing.Color.Transparent;
            this.btn_CheckInvoiceID.BorderRadius = 10;
            this.btn_CheckInvoiceID.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_CheckInvoiceID.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_CheckInvoiceID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_CheckInvoiceID.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_CheckInvoiceID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_CheckInvoiceID.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_CheckInvoiceID.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_CheckInvoiceID.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_CheckInvoiceID.ForeColor = System.Drawing.Color.White;
            this.btn_CheckInvoiceID.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_CheckInvoiceID.Location = new System.Drawing.Point(231, 40);
            this.btn_CheckInvoiceID.Name = "btn_CheckInvoiceID";
            this.btn_CheckInvoiceID.ShadowDecoration.BorderRadius = 10;
            this.btn_CheckInvoiceID.ShadowDecoration.Color = System.Drawing.Color.DimGray;
            this.btn_CheckInvoiceID.ShadowDecoration.Enabled = true;
            this.btn_CheckInvoiceID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_CheckInvoiceID.Size = new System.Drawing.Size(74, 39);
            this.btn_CheckInvoiceID.TabIndex = 0;
            this.btn_CheckInvoiceID.Text = "Check";
            this.btn_CheckInvoiceID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_CheckInvoiceID.Click += new System.EventHandler(this.btn_CheckInvoiceID_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txt_CustomerPhone);
            this.groupBox2.Controls.Add(this.txt_CustomerName);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(433, 113);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(316, 145);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin khách hàng";
            // 
            // txt_CustomerPhone
            // 
            this.txt_CustomerPhone.Animated = true;
            this.txt_CustomerPhone.BackColor = System.Drawing.Color.Transparent;
            this.txt_CustomerPhone.BorderColor = System.Drawing.Color.LightGray;
            this.txt_CustomerPhone.BorderRadius = 15;
            this.txt_CustomerPhone.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_CustomerPhone.DefaultText = "";
            this.txt_CustomerPhone.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_CustomerPhone.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_CustomerPhone.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerPhone.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerPhone.Enabled = false;
            this.txt_CustomerPhone.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_CustomerPhone.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerPhone.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerPhone.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerPhone.Font = new System.Drawing.Font("Tahoma", 10.2F);
            this.txt_CustomerPhone.ForeColor = System.Drawing.Color.Black;
            this.txt_CustomerPhone.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerPhone.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerPhone.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerPhone.Location = new System.Drawing.Point(8, 95);
            this.txt_CustomerPhone.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.txt_CustomerPhone.Name = "txt_CustomerPhone";
            this.txt_CustomerPhone.PasswordChar = '\0';
            this.txt_CustomerPhone.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_CustomerPhone.PlaceholderText = "Số điện thoại";
            this.txt_CustomerPhone.SelectedText = "";
            this.txt_CustomerPhone.ShadowDecoration.BorderRadius = 15;
            this.txt_CustomerPhone.ShadowDecoration.Enabled = true;
            this.txt_CustomerPhone.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_CustomerPhone.Size = new System.Drawing.Size(297, 39);
            this.txt_CustomerPhone.TabIndex = 3;
            // 
            // txt_CustomerName
            // 
            this.txt_CustomerName.Animated = true;
            this.txt_CustomerName.BackColor = System.Drawing.Color.Transparent;
            this.txt_CustomerName.BorderColor = System.Drawing.Color.LightGray;
            this.txt_CustomerName.BorderRadius = 15;
            this.txt_CustomerName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_CustomerName.DefaultText = "";
            this.txt_CustomerName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_CustomerName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_CustomerName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerName.Enabled = false;
            this.txt_CustomerName.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_CustomerName.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerName.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerName.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerName.Font = new System.Drawing.Font("Tahoma", 10.2F);
            this.txt_CustomerName.ForeColor = System.Drawing.Color.Black;
            this.txt_CustomerName.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerName.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerName.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerName.Location = new System.Drawing.Point(8, 42);
            this.txt_CustomerName.Margin = new System.Windows.Forms.Padding(5, 8, 5, 8);
            this.txt_CustomerName.Name = "txt_CustomerName";
            this.txt_CustomerName.PasswordChar = '\0';
            this.txt_CustomerName.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_CustomerName.PlaceholderText = "Tên khách hàng";
            this.txt_CustomerName.SelectedText = "";
            this.txt_CustomerName.ShadowDecoration.BorderRadius = 15;
            this.txt_CustomerName.ShadowDecoration.Enabled = true;
            this.txt_CustomerName.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_CustomerName.Size = new System.Drawing.Size(297, 39);
            this.txt_CustomerName.TabIndex = 2;
            // 
            // frm_Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.guna2PictureBox1);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_Register);
            this.Controls.Add(this.dgv_Customer);
            this.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_Register";
            this.Text = "FormManagerEmployee";
            this.Load += new System.EventHandler(this.frm_Register_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Customer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private Guna.UI2.WinForms.Guna2DataGridView dgv_Customer;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2GradientButton btn_Register;
        private Guna.UI2.WinForms.Guna2GradientButton btn_Cancel;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_CustomerID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_CustomerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_CustomerPhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_PointNow;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private Guna.UI2.WinForms.Guna2TextBox txt_InvoiceID;
        private Guna.UI2.WinForms.Guna2GradientButton btn_CheckInvoiceID;
        private Guna.UI2.WinForms.Guna2TextBox txt_CustomerPhone;
        private Guna.UI2.WinForms.Guna2TextBox txt_CustomerName;
    }
}
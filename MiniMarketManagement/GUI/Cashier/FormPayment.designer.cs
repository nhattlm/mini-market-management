﻿namespace MiniMarketManagement.GUI.Cashier
{
    partial class frm_FormPayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_FormPayment));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.txt_Amount = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dgvPayment_Product = new Guna.UI2.WinForms.Guna2DataGridView();
            this.dgvPay_ProductID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvPay_ProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvPay_TypeProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvPay_TimeNow = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvPay_CalculationUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvPay_SellPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvPay_Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvPay_ShowDiscount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.cb_AccumulatePoints = new Guna.UI2.WinForms.Guna2ToggleSwitch();
            this.cb_UsePoints = new Guna.UI2.WinForms.Guna2ToggleSwitch();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.pic_Product = new Guna.UI2.WinForms.Guna2PictureBox();
            this.txt_ProductName = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_Search = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_Quantity = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_ProductID = new Guna.UI2.WinForms.Guna2TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_SearchCustomer = new Guna.UI2.WinForms.Guna2GradientButton();
            this.txt_CustomerID = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_ShowPoint = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_InputPoint = new Guna.UI2.WinForms.Guna2TextBox();
            this.btn_Delete = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_addBill = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_Payment = new Guna.UI2.WinForms.Guna2GradientButton();
            this.dgvFind_ShowDiscount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFind_QuantityProduct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFind_SellPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFind_CalculationUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFind_TypeProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFind_ProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFind_ProductID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvFind_Product = new Guna.UI2.WinForms.Guna2DataGridView();
            this.printInvoice = new System.Drawing.Printing.PrintDocument();
            this.printdiaInvoice = new System.Windows.Forms.PrintPreviewDialog();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPayment_Product)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Product)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFind_Product)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_Amount
            // 
            this.txt_Amount.Animated = true;
            this.txt_Amount.BorderColor = System.Drawing.Color.LightGray;
            this.txt_Amount.BorderRadius = 10;
            this.txt_Amount.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Amount.DefaultText = "";
            this.txt_Amount.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Amount.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Amount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Amount.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Amount.Enabled = false;
            this.txt_Amount.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Amount.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.txt_Amount.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_Amount.ForeColor = System.Drawing.Color.Black;
            this.txt_Amount.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.txt_Amount.Location = new System.Drawing.Point(1003, 658);
            this.txt_Amount.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_Amount.Name = "txt_Amount";
            this.txt_Amount.PasswordChar = '\0';
            this.txt_Amount.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Amount.PlaceholderText = "";
            this.txt_Amount.SelectedText = "";
            this.txt_Amount.Size = new System.Drawing.Size(117, 23);
            this.txt_Amount.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(894, 658);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(101, 21);
            this.label4.TabIndex = 1;
            this.label4.Text = "Thành tiền";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(676, 356);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(231, 24);
            this.label6.TabIndex = 1;
            this.label6.Text = "Danh sách thanh toán";
            // 
            // dgvPayment_Product
            // 
            this.dgvPayment_Product.AllowUserToAddRows = false;
            this.dgvPayment_Product.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dgvPayment_Product.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvPayment_Product.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvPayment_Product.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPayment_Product.ColumnHeadersHeight = 18;
            this.dgvPayment_Product.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgvPayment_Product.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvPay_ProductID,
            this.dgvPay_ProductName,
            this.dgvPay_TypeProductName,
            this.dgvPay_TimeNow,
            this.dgvPay_CalculationUnit,
            this.dgvPay_SellPrice,
            this.dgvPay_Quantity,
            this.dgvPay_ShowDiscount});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvPayment_Product.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvPayment_Product.GridColor = System.Drawing.Color.Silver;
            this.dgvPayment_Product.Location = new System.Drawing.Point(475, 383);
            this.dgvPayment_Product.Name = "dgvPayment_Product";
            this.dgvPayment_Product.ReadOnly = true;
            this.dgvPayment_Product.RowHeadersVisible = false;
            this.dgvPayment_Product.RowHeadersWidth = 51;
            this.dgvPayment_Product.RowTemplate.Height = 24;
            this.dgvPayment_Product.Size = new System.Drawing.Size(647, 272);
            this.dgvPayment_Product.TabIndex = 4;
            this.dgvPayment_Product.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgvPayment_Product.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgvPayment_Product.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgvPayment_Product.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgvPayment_Product.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgvPayment_Product.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvPayment_Product.ThemeStyle.GridColor = System.Drawing.Color.Silver;
            this.dgvPayment_Product.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgvPayment_Product.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvPayment_Product.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvPayment_Product.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgvPayment_Product.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgvPayment_Product.ThemeStyle.HeaderStyle.Height = 18;
            this.dgvPayment_Product.ThemeStyle.ReadOnly = true;
            this.dgvPayment_Product.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgvPayment_Product.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvPayment_Product.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvPayment_Product.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgvPayment_Product.ThemeStyle.RowsStyle.Height = 24;
            this.dgvPayment_Product.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgvPayment_Product.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgvPayment_Product.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPayment_Product_CellClick);
            // 
            // dgvPay_ProductID
            // 
            this.dgvPay_ProductID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvPay_ProductID.HeaderText = "Mã sản phẩm";
            this.dgvPay_ProductID.MinimumWidth = 6;
            this.dgvPay_ProductID.Name = "dgvPay_ProductID";
            this.dgvPay_ProductID.ReadOnly = true;
            // 
            // dgvPay_ProductName
            // 
            this.dgvPay_ProductName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvPay_ProductName.HeaderText = "Tên sản phẩm";
            this.dgvPay_ProductName.MinimumWidth = 6;
            this.dgvPay_ProductName.Name = "dgvPay_ProductName";
            this.dgvPay_ProductName.ReadOnly = true;
            // 
            // dgvPay_TypeProductName
            // 
            this.dgvPay_TypeProductName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvPay_TypeProductName.HeaderText = "Loại sản phẩm";
            this.dgvPay_TypeProductName.MinimumWidth = 6;
            this.dgvPay_TypeProductName.Name = "dgvPay_TypeProductName";
            this.dgvPay_TypeProductName.ReadOnly = true;
            // 
            // dgvPay_TimeNow
            // 
            this.dgvPay_TimeNow.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvPay_TimeNow.HeaderText = "Ngày mua hàng";
            this.dgvPay_TimeNow.MinimumWidth = 6;
            this.dgvPay_TimeNow.Name = "dgvPay_TimeNow";
            this.dgvPay_TimeNow.ReadOnly = true;
            // 
            // dgvPay_CalculationUnit
            // 
            this.dgvPay_CalculationUnit.FillWeight = 90F;
            this.dgvPay_CalculationUnit.HeaderText = "Đơn vị tính";
            this.dgvPay_CalculationUnit.MinimumWidth = 6;
            this.dgvPay_CalculationUnit.Name = "dgvPay_CalculationUnit";
            this.dgvPay_CalculationUnit.ReadOnly = true;
            // 
            // dgvPay_SellPrice
            // 
            this.dgvPay_SellPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvPay_SellPrice.FillWeight = 80F;
            this.dgvPay_SellPrice.HeaderText = "Giá bán";
            this.dgvPay_SellPrice.MinimumWidth = 6;
            this.dgvPay_SellPrice.Name = "dgvPay_SellPrice";
            this.dgvPay_SellPrice.ReadOnly = true;
            // 
            // dgvPay_Quantity
            // 
            this.dgvPay_Quantity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvPay_Quantity.FillWeight = 80F;
            this.dgvPay_Quantity.HeaderText = "Số lượng";
            this.dgvPay_Quantity.MinimumWidth = 6;
            this.dgvPay_Quantity.Name = "dgvPay_Quantity";
            this.dgvPay_Quantity.ReadOnly = true;
            // 
            // dgvPay_ShowDiscount
            // 
            this.dgvPay_ShowDiscount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvPay_ShowDiscount.FillWeight = 110F;
            this.dgvPay_ShowDiscount.HeaderText = "Giảm giá (%)";
            this.dgvPay_ShowDiscount.MinimumWidth = 6;
            this.dgvPay_ShowDiscount.Name = "dgvPay_ShowDiscount";
            this.dgvPay_ShowDiscount.ReadOnly = true;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // cb_AccumulatePoints
            // 
            this.cb_AccumulatePoints.Animated = true;
            this.cb_AccumulatePoints.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cb_AccumulatePoints.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cb_AccumulatePoints.CheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_AccumulatePoints.CheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_AccumulatePoints.Location = new System.Drawing.Point(34, 35);
            this.cb_AccumulatePoints.Name = "cb_AccumulatePoints";
            this.cb_AccumulatePoints.Size = new System.Drawing.Size(35, 20);
            this.cb_AccumulatePoints.TabIndex = 7;
            this.cb_AccumulatePoints.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_AccumulatePoints.UncheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_AccumulatePoints.UncheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_AccumulatePoints.UncheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_AccumulatePoints.CheckedChanged += new System.EventHandler(this.cb_AccumulatePoints_CheckedChanged);
            // 
            // cb_UsePoints
            // 
            this.cb_UsePoints.Animated = true;
            this.cb_UsePoints.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cb_UsePoints.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.cb_UsePoints.CheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_UsePoints.CheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_UsePoints.Location = new System.Drawing.Point(276, 35);
            this.cb_UsePoints.Name = "cb_UsePoints";
            this.cb_UsePoints.Size = new System.Drawing.Size(35, 20);
            this.cb_UsePoints.TabIndex = 7;
            this.cb_UsePoints.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_UsePoints.UncheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_UsePoints.UncheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_UsePoints.UncheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_UsePoints.CheckedChanged += new System.EventHandler(this.cb_UsePoints_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(76, 35);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 21);
            this.label7.TabIndex = 8;
            this.label7.Text = "Tích điểm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(20, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 21);
            this.label9.TabIndex = 1;
            this.label9.Text = "Mã khách hàng";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(317, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 21);
            this.label8.TabIndex = 8;
            this.label8.Text = "Sử dụng điểm";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(20, 123);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 21);
            this.label10.TabIndex = 1;
            this.label10.Text = "Số điểm";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(20, 172);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 21);
            this.label11.TabIndex = 1;
            this.label11.Text = "Nhập điểm";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(73, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 21);
            this.label1.TabIndex = 11;
            this.label1.Text = "Mã sản phẩm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(66, 145);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 21);
            this.label2.TabIndex = 12;
            this.label2.Text = "Tên sản phẩm";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(108, 194);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(87, 21);
            this.label3.TabIndex = 13;
            this.label3.Text = "Số lượng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(676, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(219, 24);
            this.label5.TabIndex = 1;
            this.label5.Text = "Danh sách sản phẩm";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.pic_Product);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txt_ProductName);
            this.groupBox1.Controls.Add(this.txt_Search);
            this.groupBox1.Controls.Add(this.txt_Quantity);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txt_ProductID);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(3, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(457, 349);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin sản phẩm";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(21, 47);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(175, 21);
            this.label13.TabIndex = 11;
            this.label13.Text = "Tìm kiếm sản phẩm";
            // 
            // pic_Product
            // 
            this.pic_Product.BackColor = System.Drawing.Color.Transparent;
            this.pic_Product.Image = ((System.Drawing.Image)(resources.GetObject("pic_Product.Image")));
            this.pic_Product.ImageRotate = 0F;
            this.pic_Product.Location = new System.Drawing.Point(204, 239);
            this.pic_Product.Name = "pic_Product";
            this.pic_Product.Size = new System.Drawing.Size(240, 106);
            this.pic_Product.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic_Product.TabIndex = 16;
            this.pic_Product.TabStop = false;
            this.pic_Product.UseTransparentBackground = true;
            // 
            // txt_ProductName
            // 
            this.txt_ProductName.Animated = true;
            this.txt_ProductName.BackColor = System.Drawing.Color.Transparent;
            this.txt_ProductName.BorderColor = System.Drawing.Color.LightGray;
            this.txt_ProductName.BorderRadius = 15;
            this.txt_ProductName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ProductName.DefaultText = "";
            this.txt_ProductName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ProductName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ProductName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductName.Enabled = false;
            this.txt_ProductName.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ProductName.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductName.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductName.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ProductName.ForeColor = System.Drawing.Color.Black;
            this.txt_ProductName.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductName.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductName.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductName.IconLeft = global::MiniMarketManagement.Properties.Resources.features1;
            this.txt_ProductName.Location = new System.Drawing.Point(203, 145);
            this.txt_ProductName.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ProductName.Name = "txt_ProductName";
            this.txt_ProductName.PasswordChar = '\0';
            this.txt_ProductName.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ProductName.PlaceholderText = "Tên sản phẩm";
            this.txt_ProductName.SelectedText = "";
            this.txt_ProductName.ShadowDecoration.BorderRadius = 15;
            this.txt_ProductName.ShadowDecoration.Enabled = true;
            this.txt_ProductName.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ProductName.Size = new System.Drawing.Size(241, 35);
            this.txt_ProductName.TabIndex = 14;
            // 
            // txt_Search
            // 
            this.txt_Search.Animated = true;
            this.txt_Search.BackColor = System.Drawing.Color.Transparent;
            this.txt_Search.BorderColor = System.Drawing.Color.LightGray;
            this.txt_Search.BorderRadius = 15;
            this.txt_Search.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Search.DefaultText = "";
            this.txt_Search.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Search.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Search.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Search.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_Search.ForeColor = System.Drawing.Color.Black;
            this.txt_Search.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.IconLeft = global::MiniMarketManagement.Properties.Resources.search1;
            this.txt_Search.Location = new System.Drawing.Point(204, 47);
            this.txt_Search.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PasswordChar = '\0';
            this.txt_Search.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Search.PlaceholderText = "Tìm kiếm sản phẩm";
            this.txt_Search.SelectedText = "";
            this.txt_Search.ShadowDecoration.BorderRadius = 15;
            this.txt_Search.ShadowDecoration.Enabled = true;
            this.txt_Search.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Search.Size = new System.Drawing.Size(240, 35);
            this.txt_Search.TabIndex = 15;
            this.txt_Search.TextChanged += new System.EventHandler(this.txt_Search_TextChanged);
            // 
            // txt_Quantity
            // 
            this.txt_Quantity.Animated = true;
            this.txt_Quantity.BackColor = System.Drawing.Color.Transparent;
            this.txt_Quantity.BorderColor = System.Drawing.Color.LightGray;
            this.txt_Quantity.BorderRadius = 15;
            this.txt_Quantity.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Quantity.DefaultText = "";
            this.txt_Quantity.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Quantity.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Quantity.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Quantity.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Quantity.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Quantity.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Quantity.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Quantity.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Quantity.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_Quantity.ForeColor = System.Drawing.Color.Black;
            this.txt_Quantity.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Quantity.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Quantity.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Quantity.IconLeft = global::MiniMarketManagement.Properties.Resources.how_much2;
            this.txt_Quantity.Location = new System.Drawing.Point(204, 194);
            this.txt_Quantity.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_Quantity.Name = "txt_Quantity";
            this.txt_Quantity.PasswordChar = '\0';
            this.txt_Quantity.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Quantity.PlaceholderText = "Số lượng";
            this.txt_Quantity.SelectedText = "";
            this.txt_Quantity.ShadowDecoration.BorderRadius = 15;
            this.txt_Quantity.ShadowDecoration.Enabled = true;
            this.txt_Quantity.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Quantity.Size = new System.Drawing.Size(240, 35);
            this.txt_Quantity.TabIndex = 15;
            // 
            // txt_ProductID
            // 
            this.txt_ProductID.Animated = true;
            this.txt_ProductID.BackColor = System.Drawing.Color.Transparent;
            this.txt_ProductID.BorderColor = System.Drawing.Color.Silver;
            this.txt_ProductID.BorderRadius = 15;
            this.txt_ProductID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ProductID.DefaultText = "";
            this.txt_ProductID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ProductID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ProductID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ProductID.Enabled = false;
            this.txt_ProductID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ProductID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ProductID.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ProductID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ProductID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ProductID.IconLeft = global::MiniMarketManagement.Properties.Resources.id__1_2;
            this.txt_ProductID.Location = new System.Drawing.Point(204, 96);
            this.txt_ProductID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ProductID.Name = "txt_ProductID";
            this.txt_ProductID.PasswordChar = '\0';
            this.txt_ProductID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ProductID.PlaceholderText = "Mã sản phẩm";
            this.txt_ProductID.SelectedText = "";
            this.txt_ProductID.ShadowDecoration.BorderRadius = 15;
            this.txt_ProductID.ShadowDecoration.Enabled = true;
            this.txt_ProductID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ProductID.Size = new System.Drawing.Size(240, 35);
            this.txt_ProductID.TabIndex = 10;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cb_AccumulatePoints);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.btn_SearchCustomer);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.cb_UsePoints);
            this.groupBox2.Controls.Add(this.txt_CustomerID);
            this.groupBox2.Controls.Add(this.txt_ShowPoint);
            this.groupBox2.Controls.Add(this.txt_InputPoint);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(4, 383);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(456, 226);
            this.groupBox2.TabIndex = 18;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin khách hàng";
            // 
            // btn_SearchCustomer
            // 
            this.btn_SearchCustomer.BackColor = System.Drawing.Color.Transparent;
            this.btn_SearchCustomer.BorderRadius = 10;
            this.btn_SearchCustomer.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_SearchCustomer.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_SearchCustomer.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_SearchCustomer.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_SearchCustomer.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_SearchCustomer.Enabled = false;
            this.btn_SearchCustomer.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_SearchCustomer.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_SearchCustomer.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_SearchCustomer.ForeColor = System.Drawing.Color.White;
            this.btn_SearchCustomer.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_SearchCustomer.Location = new System.Drawing.Point(350, 74);
            this.btn_SearchCustomer.Name = "btn_SearchCustomer";
            this.btn_SearchCustomer.ShadowDecoration.BorderRadius = 10;
            this.btn_SearchCustomer.ShadowDecoration.Enabled = true;
            this.btn_SearchCustomer.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_SearchCustomer.Size = new System.Drawing.Size(100, 35);
            this.btn_SearchCustomer.TabIndex = 0;
            this.btn_SearchCustomer.Text = "Tìm kiếm";
            this.btn_SearchCustomer.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_SearchCustomer.Click += new System.EventHandler(this.btn_SearchCustomer_Click);
            // 
            // txt_CustomerID
            // 
            this.txt_CustomerID.Animated = true;
            this.txt_CustomerID.BackColor = System.Drawing.Color.Transparent;
            this.txt_CustomerID.BorderColor = System.Drawing.Color.LightGray;
            this.txt_CustomerID.BorderRadius = 15;
            this.txt_CustomerID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_CustomerID.DefaultText = "";
            this.txt_CustomerID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_CustomerID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_CustomerID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerID.Enabled = false;
            this.txt_CustomerID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_CustomerID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_CustomerID.ForeColor = System.Drawing.Color.Black;
            this.txt_CustomerID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerID.IconLeft = global::MiniMarketManagement.Properties.Resources.id__1_3;
            this.txt_CustomerID.Location = new System.Drawing.Point(175, 74);
            this.txt_CustomerID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_CustomerID.Name = "txt_CustomerID";
            this.txt_CustomerID.PasswordChar = '\0';
            this.txt_CustomerID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_CustomerID.PlaceholderText = "Mã khách hàng";
            this.txt_CustomerID.SelectedText = "";
            this.txt_CustomerID.ShadowDecoration.BorderRadius = 15;
            this.txt_CustomerID.ShadowDecoration.Enabled = true;
            this.txt_CustomerID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_CustomerID.Size = new System.Drawing.Size(167, 35);
            this.txt_CustomerID.TabIndex = 3;
            // 
            // txt_ShowPoint
            // 
            this.txt_ShowPoint.Animated = true;
            this.txt_ShowPoint.BackColor = System.Drawing.Color.Transparent;
            this.txt_ShowPoint.BorderColor = System.Drawing.Color.LightGray;
            this.txt_ShowPoint.BorderRadius = 15;
            this.txt_ShowPoint.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_ShowPoint.DefaultText = "";
            this.txt_ShowPoint.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_ShowPoint.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_ShowPoint.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ShowPoint.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_ShowPoint.Enabled = false;
            this.txt_ShowPoint.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_ShowPoint.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ShowPoint.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ShowPoint.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ShowPoint.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_ShowPoint.ForeColor = System.Drawing.Color.Black;
            this.txt_ShowPoint.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_ShowPoint.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_ShowPoint.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_ShowPoint.IconLeft = global::MiniMarketManagement.Properties.Resources.checklist1;
            this.txt_ShowPoint.Location = new System.Drawing.Point(175, 123);
            this.txt_ShowPoint.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_ShowPoint.Name = "txt_ShowPoint";
            this.txt_ShowPoint.PasswordChar = '\0';
            this.txt_ShowPoint.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_ShowPoint.PlaceholderText = "Số điểm";
            this.txt_ShowPoint.SelectedText = "";
            this.txt_ShowPoint.ShadowDecoration.BorderRadius = 15;
            this.txt_ShowPoint.ShadowDecoration.Enabled = true;
            this.txt_ShowPoint.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_ShowPoint.Size = new System.Drawing.Size(167, 35);
            this.txt_ShowPoint.TabIndex = 3;
            // 
            // txt_InputPoint
            // 
            this.txt_InputPoint.Animated = true;
            this.txt_InputPoint.BackColor = System.Drawing.Color.Transparent;
            this.txt_InputPoint.BorderColor = System.Drawing.Color.LightGray;
            this.txt_InputPoint.BorderRadius = 15;
            this.txt_InputPoint.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_InputPoint.DefaultText = "";
            this.txt_InputPoint.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_InputPoint.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_InputPoint.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_InputPoint.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_InputPoint.Enabled = false;
            this.txt_InputPoint.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_InputPoint.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_InputPoint.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_InputPoint.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_InputPoint.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_InputPoint.ForeColor = System.Drawing.Color.Black;
            this.txt_InputPoint.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_InputPoint.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_InputPoint.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_InputPoint.IconLeft = global::MiniMarketManagement.Properties.Resources.inputPoint;
            this.txt_InputPoint.Location = new System.Drawing.Point(175, 172);
            this.txt_InputPoint.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_InputPoint.Name = "txt_InputPoint";
            this.txt_InputPoint.PasswordChar = '\0';
            this.txt_InputPoint.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_InputPoint.PlaceholderText = "Nhập điểm";
            this.txt_InputPoint.SelectedText = "";
            this.txt_InputPoint.ShadowDecoration.BorderRadius = 15;
            this.txt_InputPoint.ShadowDecoration.Enabled = true;
            this.txt_InputPoint.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_InputPoint.Size = new System.Drawing.Size(167, 35);
            this.txt_InputPoint.TabIndex = 3;
            this.txt_InputPoint.TextChanged += new System.EventHandler(this.txt_InputPoint_TextChanged);
            // 
            // btn_Delete
            // 
            this.btn_Delete.BackColor = System.Drawing.Color.Transparent;
            this.btn_Delete.BorderRadius = 10;
            this.btn_Delete.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Delete.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Delete.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Delete.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Delete.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Delete.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Delete.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Delete.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Delete.ForeColor = System.Drawing.Color.White;
            this.btn_Delete.Image = ((System.Drawing.Image)(resources.GetObject("btn_Delete.Image")));
            this.btn_Delete.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Delete.Location = new System.Drawing.Point(376, 622);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.ShadowDecoration.BorderRadius = 10;
            this.btn_Delete.ShadowDecoration.Enabled = true;
            this.btn_Delete.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Delete.Size = new System.Drawing.Size(83, 45);
            this.btn_Delete.TabIndex = 6;
            this.btn_Delete.Text = "Xóa";
            this.btn_Delete.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // btn_addBill
            // 
            this.btn_addBill.BackColor = System.Drawing.Color.Transparent;
            this.btn_addBill.BorderRadius = 10;
            this.btn_addBill.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_addBill.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_addBill.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_addBill.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_addBill.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_addBill.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_addBill.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_addBill.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_addBill.ForeColor = System.Drawing.Color.White;
            this.btn_addBill.Image = ((System.Drawing.Image)(resources.GetObject("btn_addBill.Image")));
            this.btn_addBill.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_addBill.Location = new System.Drawing.Point(206, 622);
            this.btn_addBill.Name = "btn_addBill";
            this.btn_addBill.ShadowDecoration.BorderRadius = 10;
            this.btn_addBill.ShadowDecoration.Enabled = true;
            this.btn_addBill.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_addBill.Size = new System.Drawing.Size(129, 45);
            this.btn_addBill.TabIndex = 6;
            this.btn_addBill.Text = "Thêm/Sửa";
            this.btn_addBill.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_addBill.Click += new System.EventHandler(this.btn_addBill_Click);
            // 
            // btn_Payment
            // 
            this.btn_Payment.BackColor = System.Drawing.Color.Transparent;
            this.btn_Payment.BorderRadius = 10;
            this.btn_Payment.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Payment.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Payment.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Payment.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Payment.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Payment.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Payment.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Payment.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Payment.ForeColor = System.Drawing.Color.White;
            this.btn_Payment.Image = ((System.Drawing.Image)(resources.GetObject("btn_Payment.Image")));
            this.btn_Payment.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Payment.Location = new System.Drawing.Point(28, 622);
            this.btn_Payment.Name = "btn_Payment";
            this.btn_Payment.ShadowDecoration.BorderRadius = 10;
            this.btn_Payment.ShadowDecoration.Enabled = true;
            this.btn_Payment.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Payment.Size = new System.Drawing.Size(137, 45);
            this.btn_Payment.TabIndex = 6;
            this.btn_Payment.Text = "Thanh toán";
            this.btn_Payment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_Payment.Click += new System.EventHandler(this.btn_Payment_Click);
            // 
            // dgvFind_ShowDiscount
            // 
            this.dgvFind_ShowDiscount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvFind_ShowDiscount.HeaderText = "Giảm giá (%)";
            this.dgvFind_ShowDiscount.MinimumWidth = 6;
            this.dgvFind_ShowDiscount.Name = "dgvFind_ShowDiscount";
            this.dgvFind_ShowDiscount.ReadOnly = true;
            // 
            // dgvFind_QuantityProduct
            // 
            this.dgvFind_QuantityProduct.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvFind_QuantityProduct.HeaderText = "Số lượng còn lại";
            this.dgvFind_QuantityProduct.MinimumWidth = 6;
            this.dgvFind_QuantityProduct.Name = "dgvFind_QuantityProduct";
            this.dgvFind_QuantityProduct.ReadOnly = true;
            // 
            // dgvFind_SellPrice
            // 
            this.dgvFind_SellPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvFind_SellPrice.FillWeight = 80F;
            this.dgvFind_SellPrice.HeaderText = "Giá bán";
            this.dgvFind_SellPrice.MinimumWidth = 6;
            this.dgvFind_SellPrice.Name = "dgvFind_SellPrice";
            this.dgvFind_SellPrice.ReadOnly = true;
            // 
            // dgvFind_CalculationUnit
            // 
            this.dgvFind_CalculationUnit.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvFind_CalculationUnit.FillWeight = 80F;
            this.dgvFind_CalculationUnit.HeaderText = "Đơn vị tính";
            this.dgvFind_CalculationUnit.MinimumWidth = 6;
            this.dgvFind_CalculationUnit.Name = "dgvFind_CalculationUnit";
            this.dgvFind_CalculationUnit.ReadOnly = true;
            // 
            // dgvFind_TypeProductName
            // 
            this.dgvFind_TypeProductName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvFind_TypeProductName.FillWeight = 110F;
            this.dgvFind_TypeProductName.HeaderText = "Loại sản phẩm";
            this.dgvFind_TypeProductName.MinimumWidth = 6;
            this.dgvFind_TypeProductName.Name = "dgvFind_TypeProductName";
            this.dgvFind_TypeProductName.ReadOnly = true;
            // 
            // dgvFind_ProductName
            // 
            this.dgvFind_ProductName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvFind_ProductName.FillWeight = 110F;
            this.dgvFind_ProductName.HeaderText = "Tên sản phẩm";
            this.dgvFind_ProductName.MinimumWidth = 6;
            this.dgvFind_ProductName.Name = "dgvFind_ProductName";
            this.dgvFind_ProductName.ReadOnly = true;
            // 
            // dgvFind_ProductID
            // 
            this.dgvFind_ProductID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgvFind_ProductID.FillWeight = 110F;
            this.dgvFind_ProductID.HeaderText = "Mã sản phẩm";
            this.dgvFind_ProductID.MinimumWidth = 6;
            this.dgvFind_ProductID.Name = "dgvFind_ProductID";
            this.dgvFind_ProductID.ReadOnly = true;
            // 
            // dgvFind_Product
            // 
            this.dgvFind_Product.AllowUserToAddRows = false;
            this.dgvFind_Product.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.dgvFind_Product.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvFind_Product.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvFind_Product.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgvFind_Product.ColumnHeadersHeight = 18;
            this.dgvFind_Product.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgvFind_Product.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvFind_ProductID,
            this.dgvFind_ProductName,
            this.dgvFind_TypeProductName,
            this.dgvFind_CalculationUnit,
            this.dgvFind_SellPrice,
            this.dgvFind_QuantityProduct,
            this.dgvFind_ShowDiscount});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvFind_Product.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgvFind_Product.GridColor = System.Drawing.Color.SeaShell;
            this.dgvFind_Product.Location = new System.Drawing.Point(475, 39);
            this.dgvFind_Product.Name = "dgvFind_Product";
            this.dgvFind_Product.ReadOnly = true;
            this.dgvFind_Product.RowHeadersVisible = false;
            this.dgvFind_Product.RowHeadersWidth = 51;
            this.dgvFind_Product.RowTemplate.Height = 24;
            this.dgvFind_Product.Size = new System.Drawing.Size(647, 292);
            this.dgvFind_Product.TabIndex = 4;
            this.dgvFind_Product.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgvFind_Product.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgvFind_Product.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgvFind_Product.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgvFind_Product.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgvFind_Product.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgvFind_Product.ThemeStyle.GridColor = System.Drawing.Color.SeaShell;
            this.dgvFind_Product.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgvFind_Product.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvFind_Product.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvFind_Product.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgvFind_Product.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgvFind_Product.ThemeStyle.HeaderStyle.Height = 18;
            this.dgvFind_Product.ThemeStyle.ReadOnly = true;
            this.dgvFind_Product.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgvFind_Product.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgvFind_Product.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvFind_Product.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgvFind_Product.ThemeStyle.RowsStyle.Height = 24;
            this.dgvFind_Product.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgvFind_Product.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgvFind_Product.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvFind_Product_CellClick);
            // 
            // printdiaInvoice
            // 
            this.printdiaInvoice.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.printdiaInvoice.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.printdiaInvoice.ClientSize = new System.Drawing.Size(400, 300);
            this.printdiaInvoice.Enabled = true;
            this.printdiaInvoice.Icon = ((System.Drawing.Icon)(resources.GetObject("printdiaInvoice.Icon")));
            this.printdiaInvoice.Name = "printdiaInvoice";
            this.printdiaInvoice.Visible = false;
            // 
            // frm_FormPayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgvFind_Product);
            this.Controls.Add(this.btn_Delete);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btn_addBill);
            this.Controls.Add(this.btn_Payment);
            this.Controls.Add(this.dgvPayment_Product);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txt_Amount);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_FormPayment";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.frm_FormPayment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPayment_Product)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Product)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFind_Product)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private Guna.UI2.WinForms.Guna2GradientButton btn_Delete;
        private Guna.UI2.WinForms.Guna2GradientButton btn_Payment;
        private System.Windows.Forms.Label label4;
        private Guna.UI2.WinForms.Guna2TextBox txt_Amount;
        private Guna.UI2.WinForms.Guna2GradientButton btn_addBill;
        private System.Windows.Forms.Label label6;
        private Guna.UI2.WinForms.Guna2DataGridView dgvPayment_Product;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2TextBox txt_CustomerID;
        private System.Windows.Forms.Label label7;
        private Guna.UI2.WinForms.Guna2ToggleSwitch cb_UsePoints;
        private Guna.UI2.WinForms.Guna2ToggleSwitch cb_AccumulatePoints;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private Guna.UI2.WinForms.Guna2TextBox txt_ShowPoint;
        private Guna.UI2.WinForms.Guna2TextBox txt_InputPoint;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private Guna.UI2.WinForms.Guna2PictureBox pic_Product;
        private Guna.UI2.WinForms.Guna2TextBox txt_ProductName;
        private Guna.UI2.WinForms.Guna2TextBox txt_ProductID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2TextBox txt_Quantity;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private Guna.UI2.WinForms.Guna2TextBox txt_Search;
        private System.Windows.Forms.Label label13;
        private Guna.UI2.WinForms.Guna2GradientButton btn_SearchCustomer;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvPay_ProductID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvPay_ProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvPay_TypeProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvPay_TimeNow;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvPay_CalculationUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvPay_SellPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvPay_Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvPay_ShowDiscount;
        private Guna.UI2.WinForms.Guna2DataGridView dgvFind_Product;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFind_ProductID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFind_ProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFind_TypeProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFind_CalculationUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFind_SellPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFind_QuantityProduct;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvFind_ShowDiscount;
        private System.Drawing.Printing.PrintDocument printInvoice;
        private System.Windows.Forms.PrintPreviewDialog printdiaInvoice;
    }
}
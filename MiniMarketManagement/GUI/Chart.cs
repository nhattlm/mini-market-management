﻿using Guna.Charts.WinForms;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniMarketManagement.GUI.Administrator;
using System.Security.Cryptography.X509Certificates;
using MiniMarketManagement.BLL.Administrator;
using MiniMarketManagement.DAL.Administrator;

namespace MiniMarketManagement.GUI
{
    internal class Chart
    {
        public static void SplineArea(Guna.Charts.WinForms.GunaChart chart)
        {
            string[] months = { "Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6"
                               , "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12" };
            chart.YAxes.GridLines.Display = true;
            var dataset1 = new GunaSplineDataset();
            var dataset2 = new GunaSplineDataset();
            var dataset3 = new GunaSplineDataset();

            //Cài thuộc tính cho dataset1
            dataset1.PointRadius = 4;
            dataset1.PointStyle = PointStyle.Circle;
            dataset1.FillColor = Color.LightGreen;
            dataset1.BorderColor = Color.LightGreen;
            dataset1.Label = "Tổng Thu";

            //Cài thuộc tính cho dataset2
            dataset2.PointRadius = 4;
            dataset2.PointStyle = PointStyle.Circle;
            dataset2.FillColor = Color.Aqua;
            dataset2.BorderColor = Color.Aqua;
            dataset2.Label = "Tổng Chi";

            //Cài thuộc tính cho dataset3
            dataset3.PointRadius = 4;
            dataset3.PointStyle = PointStyle.Circle;
            dataset3.FillColor = Color.HotPink;
            dataset3.BorderColor = Color.HotPink;
            dataset3.Label = "Lợi Nhuận";

            //Vẽ biểu đồ thống kê
            BLL_Statistics bLL_Statistics = new BLL_Statistics();
            for (int i = 0; i < 12; i++)
            {
                dataset1.DataPoints.Add(months[i], bLL_Statistics.GetTotalIncomeByMonthForChart(months, i + 1));
                dataset2.DataPoints.Add(months[i], bLL_Statistics.GetTotalExpenseByMonthForChart(months, i + 1));
                dataset3.DataPoints.Add(months[i], bLL_Statistics.GetTotalProfitByMonthForChart(months, i + 1));
            }
            chart.Datasets.Add(dataset1);
            chart.Datasets.Add(dataset2);
            chart.Datasets.Add(dataset3);
            chart.Update();
        }
        public static void Pie(Guna.Charts.WinForms.GunaChart chart)
        {
            string[] types = { "Khách Hàng Thân Thiết", "Khách Vãng Lai" };
            chart.Legend.Position = LegendPosition.Top;
            chart.XAxes.Display = false;
            chart.YAxes.Display = false;

            //Cài thuộc tính cho dataset1
            var dataset1 = new GunaPieDataset();
            dataset1.Label = types[0];
            dataset1.LegendBoxBorderColor = Color.Salmon;
            dataset1.LegendBoxFillColor = Color.Salmon;
            dataset1.FillColors.Add(Color.MediumPurple);
            dataset1.FillColors.Add(Color.Salmon);

            //Cài thuộc tính cho dataset2
            var dataset2 = new GunaPieDataset();
            dataset2.Label = types[1];
            dataset2.LegendBoxBorderColor = Color.MediumPurple;
            dataset2.LegendBoxFillColor = Color.MediumPurple;

            BLL_LoyalCustomer bLL_LoyalCustomer = new BLL_LoyalCustomer();
            dataset1.DataPoints.Add(types[0], bLL_LoyalCustomer.TotalLoyalCustomer());
            dataset1.DataPoints.Add(types[1], bLL_LoyalCustomer.TotalCustomer());
            chart.Datasets.Add(dataset1);
            chart.Datasets.Add(dataset2);
            chart.Update();
        }
    }
}

﻿namespace MiniMarketManagement.GUI.Administrator
{
    partial class frm_Admin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Admin));
            this.guna2DragControl1 = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.panel_AdminSideBoard = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.lb_EmployeeName = new System.Windows.Forms.Label();
            this.btn_Minimize = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Option = new Guna.UI2.WinForms.Guna2Button();
            this.btn_EmployeeManagement = new Guna.UI2.WinForms.Guna2Button();
            this.btn_CreateAccount = new Guna.UI2.WinForms.Guna2Button();
            this.btn_ReturnManagement = new Guna.UI2.WinForms.Guna2Button();
            this.btn_ImportManagement = new Guna.UI2.WinForms.Guna2Button();
            this.btn_LoyalCustomers = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Invoices = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Statistics = new Guna.UI2.WinForms.Guna2Button();
            this.ibnt_Logo = new Guna.UI2.WinForms.Guna2ImageButton();
            this.guna2Separator2 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2Separator1 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.guna2DragControl2 = new Guna.UI2.WinForms.Guna2DragControl(this.components);
            this.panel_Main = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.panel_Exit = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.btn_Close = new Guna.UI2.WinForms.Guna2Button();
            this.btn_SignOut = new Guna.UI2.WinForms.Guna2Button();
            this.guna2AnimateWindow1 = new Guna.UI2.WinForms.Guna2AnimateWindow(this.components);
            this.panel_AdminSideBoard.SuspendLayout();
            this.panel_Main.SuspendLayout();
            this.panel_Exit.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2DragControl1
            // 
            this.guna2DragControl1.DockIndicatorTransparencyValue = 0.6D;
            this.guna2DragControl1.TargetControl = this.panel_AdminSideBoard;
            this.guna2DragControl1.TransparentWhileDrag = false;
            // 
            // panel_AdminSideBoard
            // 
            this.panel_AdminSideBoard.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_AdminSideBoard.BackColor = System.Drawing.Color.Transparent;
            this.panel_AdminSideBoard.BorderRadius = 10;
            this.panel_AdminSideBoard.Controls.Add(this.lb_EmployeeName);
            this.panel_AdminSideBoard.Controls.Add(this.btn_Minimize);
            this.panel_AdminSideBoard.Controls.Add(this.btn_Option);
            this.panel_AdminSideBoard.Controls.Add(this.btn_EmployeeManagement);
            this.panel_AdminSideBoard.Controls.Add(this.btn_CreateAccount);
            this.panel_AdminSideBoard.Controls.Add(this.btn_ReturnManagement);
            this.panel_AdminSideBoard.Controls.Add(this.btn_ImportManagement);
            this.panel_AdminSideBoard.Controls.Add(this.btn_LoyalCustomers);
            this.panel_AdminSideBoard.Controls.Add(this.btn_Invoices);
            this.panel_AdminSideBoard.Controls.Add(this.btn_Statistics);
            this.panel_AdminSideBoard.Controls.Add(this.ibnt_Logo);
            this.panel_AdminSideBoard.Controls.Add(this.guna2Separator2);
            this.panel_AdminSideBoard.Controls.Add(this.guna2Separator1);
            this.panel_AdminSideBoard.FillColor = System.Drawing.Color.HotPink;
            this.panel_AdminSideBoard.FillColor2 = System.Drawing.Color.Red;
            this.panel_AdminSideBoard.FillColor3 = System.Drawing.Color.LightCyan;
            this.panel_AdminSideBoard.FillColor4 = System.Drawing.Color.Crimson;
            this.panel_AdminSideBoard.Location = new System.Drawing.Point(-19, -1);
            this.panel_AdminSideBoard.Name = "panel_AdminSideBoard";
            this.panel_AdminSideBoard.ShadowDecoration.Depth = 10;
            this.panel_AdminSideBoard.ShadowDecoration.Enabled = true;
            this.panel_AdminSideBoard.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.panel_AdminSideBoard.Size = new System.Drawing.Size(258, 696);
            this.panel_AdminSideBoard.TabIndex = 0;
            // 
            // lb_EmployeeName
            // 
            this.lb_EmployeeName.AutoSize = true;
            this.lb_EmployeeName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_EmployeeName.ForeColor = System.Drawing.Color.MistyRose;
            this.lb_EmployeeName.Location = new System.Drawing.Point(31, 92);
            this.lb_EmployeeName.Name = "lb_EmployeeName";
            this.lb_EmployeeName.Size = new System.Drawing.Size(55, 18);
            this.lb_EmployeeName.TabIndex = 9;
            this.lb_EmployeeName.Text = "label1";
            // 
            // btn_Minimize
            // 
            this.btn_Minimize.Animated = true;
            this.btn_Minimize.BackColor = System.Drawing.Color.Transparent;
            this.btn_Minimize.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Minimize.BorderRadius = 20;
            this.btn_Minimize.BorderThickness = 2;
            this.btn_Minimize.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Minimize.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Minimize.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Minimize.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Minimize.FillColor = System.Drawing.Color.Transparent;
            this.btn_Minimize.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.btn_Minimize.ForeColor = System.Drawing.Color.White;
            this.btn_Minimize.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Minimize.Image = global::MiniMarketManagement.Properties.Resources.minimize;
            this.btn_Minimize.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Minimize.ImageOffset = new System.Drawing.Point(2, 0);
            this.btn_Minimize.ImageSize = new System.Drawing.Size(26, 26);
            this.btn_Minimize.IndicateFocus = true;
            this.btn_Minimize.Location = new System.Drawing.Point(28, 572);
            this.btn_Minimize.Name = "btn_Minimize";
            this.btn_Minimize.ShadowDecoration.Depth = 1;
            this.btn_Minimize.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Minimize.Size = new System.Drawing.Size(219, 55);
            this.btn_Minimize.TabIndex = 2;
            this.btn_Minimize.Text = "Thu nhỏ";
            this.btn_Minimize.UseTransparentBackground = true;
            this.btn_Minimize.Click += new System.EventHandler(this.btn_Minimize_Click);
            // 
            // btn_Option
            // 
            this.btn_Option.Animated = true;
            this.btn_Option.BackColor = System.Drawing.Color.Transparent;
            this.btn_Option.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Option.BorderRadius = 20;
            this.btn_Option.BorderThickness = 2;
            this.btn_Option.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Option.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Option.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Option.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Option.FillColor = System.Drawing.Color.Transparent;
            this.btn_Option.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.btn_Option.ForeColor = System.Drawing.Color.White;
            this.btn_Option.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Option.Image = global::MiniMarketManagement.Properties.Resources.menu__3_;
            this.btn_Option.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Option.ImageOffset = new System.Drawing.Point(4, 0);
            this.btn_Option.ImageSize = new System.Drawing.Size(23, 23);
            this.btn_Option.IndicateFocus = true;
            this.btn_Option.Location = new System.Drawing.Point(28, 633);
            this.btn_Option.Name = "btn_Option";
            this.btn_Option.ShadowDecoration.Depth = 1;
            this.btn_Option.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Option.Size = new System.Drawing.Size(219, 55);
            this.btn_Option.TabIndex = 2;
            this.btn_Option.Text = "Tùy chọn";
            this.btn_Option.UseTransparentBackground = true;
            this.btn_Option.Click += new System.EventHandler(this.btn_Exit_Click);
            // 
            // btn_EmployeeManagement
            // 
            this.btn_EmployeeManagement.Animated = true;
            this.btn_EmployeeManagement.BackColor = System.Drawing.Color.Transparent;
            this.btn_EmployeeManagement.BorderColor = System.Drawing.Color.Transparent;
            this.btn_EmployeeManagement.BorderRadius = 20;
            this.btn_EmployeeManagement.BorderThickness = 2;
            this.btn_EmployeeManagement.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_EmployeeManagement.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_EmployeeManagement.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_EmployeeManagement.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_EmployeeManagement.FillColor = System.Drawing.Color.Transparent;
            this.btn_EmployeeManagement.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EmployeeManagement.ForeColor = System.Drawing.Color.White;
            this.btn_EmployeeManagement.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_EmployeeManagement.IndicateFocus = true;
            this.btn_EmployeeManagement.Location = new System.Drawing.Point(28, 493);
            this.btn_EmployeeManagement.Name = "btn_EmployeeManagement";
            this.btn_EmployeeManagement.ShadowDecoration.Depth = 1;
            this.btn_EmployeeManagement.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_EmployeeManagement.Size = new System.Drawing.Size(219, 55);
            this.btn_EmployeeManagement.TabIndex = 2;
            this.btn_EmployeeManagement.Text = "Quản lý nhân viên";
            this.btn_EmployeeManagement.UseTransparentBackground = true;
            this.btn_EmployeeManagement.Click += new System.EventHandler(this.btn_EmployeeManagement_Click);
            // 
            // btn_CreateAccount
            // 
            this.btn_CreateAccount.Animated = true;
            this.btn_CreateAccount.BackColor = System.Drawing.Color.Transparent;
            this.btn_CreateAccount.BorderColor = System.Drawing.Color.Transparent;
            this.btn_CreateAccount.BorderRadius = 20;
            this.btn_CreateAccount.BorderThickness = 2;
            this.btn_CreateAccount.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_CreateAccount.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_CreateAccount.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_CreateAccount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_CreateAccount.FillColor = System.Drawing.Color.Transparent;
            this.btn_CreateAccount.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_CreateAccount.ForeColor = System.Drawing.Color.White;
            this.btn_CreateAccount.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_CreateAccount.IndicateFocus = true;
            this.btn_CreateAccount.Location = new System.Drawing.Point(28, 432);
            this.btn_CreateAccount.Name = "btn_CreateAccount";
            this.btn_CreateAccount.ShadowDecoration.Depth = 1;
            this.btn_CreateAccount.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_CreateAccount.Size = new System.Drawing.Size(219, 55);
            this.btn_CreateAccount.TabIndex = 2;
            this.btn_CreateAccount.Text = "Tạo tài khoản";
            this.btn_CreateAccount.UseTransparentBackground = true;
            this.btn_CreateAccount.Click += new System.EventHandler(this.btn_CreateAccount_Click);
            // 
            // btn_ReturnManagement
            // 
            this.btn_ReturnManagement.Animated = true;
            this.btn_ReturnManagement.BackColor = System.Drawing.Color.Transparent;
            this.btn_ReturnManagement.BorderColor = System.Drawing.Color.Transparent;
            this.btn_ReturnManagement.BorderRadius = 20;
            this.btn_ReturnManagement.BorderThickness = 2;
            this.btn_ReturnManagement.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_ReturnManagement.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_ReturnManagement.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_ReturnManagement.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_ReturnManagement.FillColor = System.Drawing.Color.Transparent;
            this.btn_ReturnManagement.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ReturnManagement.ForeColor = System.Drawing.Color.White;
            this.btn_ReturnManagement.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_ReturnManagement.IndicateFocus = true;
            this.btn_ReturnManagement.Location = new System.Drawing.Point(28, 371);
            this.btn_ReturnManagement.Name = "btn_ReturnManagement";
            this.btn_ReturnManagement.ShadowDecoration.Depth = 1;
            this.btn_ReturnManagement.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_ReturnManagement.Size = new System.Drawing.Size(219, 55);
            this.btn_ReturnManagement.TabIndex = 2;
            this.btn_ReturnManagement.Text = "Trả hàng";
            this.btn_ReturnManagement.UseTransparentBackground = true;
            this.btn_ReturnManagement.Click += new System.EventHandler(this.btn_ReturnManagement_Click);
            // 
            // btn_ImportManagement
            // 
            this.btn_ImportManagement.Animated = true;
            this.btn_ImportManagement.BackColor = System.Drawing.Color.Transparent;
            this.btn_ImportManagement.BorderColor = System.Drawing.Color.Transparent;
            this.btn_ImportManagement.BorderRadius = 20;
            this.btn_ImportManagement.BorderThickness = 2;
            this.btn_ImportManagement.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_ImportManagement.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_ImportManagement.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_ImportManagement.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_ImportManagement.FillColor = System.Drawing.Color.Transparent;
            this.btn_ImportManagement.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ImportManagement.ForeColor = System.Drawing.Color.White;
            this.btn_ImportManagement.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_ImportManagement.IndicateFocus = true;
            this.btn_ImportManagement.Location = new System.Drawing.Point(28, 310);
            this.btn_ImportManagement.Name = "btn_ImportManagement";
            this.btn_ImportManagement.ShadowDecoration.Depth = 1;
            this.btn_ImportManagement.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_ImportManagement.Size = new System.Drawing.Size(219, 55);
            this.btn_ImportManagement.TabIndex = 2;
            this.btn_ImportManagement.Text = "Nhập hàng";
            this.btn_ImportManagement.UseTransparentBackground = true;
            this.btn_ImportManagement.Click += new System.EventHandler(this.btn_ImportManagement_Click);
            // 
            // btn_LoyalCustomers
            // 
            this.btn_LoyalCustomers.Animated = true;
            this.btn_LoyalCustomers.BackColor = System.Drawing.Color.Transparent;
            this.btn_LoyalCustomers.BorderColor = System.Drawing.Color.Transparent;
            this.btn_LoyalCustomers.BorderRadius = 20;
            this.btn_LoyalCustomers.BorderThickness = 2;
            this.btn_LoyalCustomers.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_LoyalCustomers.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_LoyalCustomers.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_LoyalCustomers.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_LoyalCustomers.FillColor = System.Drawing.Color.Transparent;
            this.btn_LoyalCustomers.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_LoyalCustomers.ForeColor = System.Drawing.Color.White;
            this.btn_LoyalCustomers.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_LoyalCustomers.IndicateFocus = true;
            this.btn_LoyalCustomers.Location = new System.Drawing.Point(28, 249);
            this.btn_LoyalCustomers.Name = "btn_LoyalCustomers";
            this.btn_LoyalCustomers.ShadowDecoration.Depth = 1;
            this.btn_LoyalCustomers.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_LoyalCustomers.Size = new System.Drawing.Size(219, 55);
            this.btn_LoyalCustomers.TabIndex = 2;
            this.btn_LoyalCustomers.Text = "Khách hàng";
            this.btn_LoyalCustomers.UseTransparentBackground = true;
            this.btn_LoyalCustomers.Click += new System.EventHandler(this.btn_LoyalCustomers_Click);
            // 
            // btn_Invoices
            // 
            this.btn_Invoices.Animated = true;
            this.btn_Invoices.BackColor = System.Drawing.Color.Transparent;
            this.btn_Invoices.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Invoices.BorderRadius = 20;
            this.btn_Invoices.BorderThickness = 2;
            this.btn_Invoices.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Invoices.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Invoices.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Invoices.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Invoices.FillColor = System.Drawing.Color.Transparent;
            this.btn_Invoices.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Invoices.ForeColor = System.Drawing.Color.White;
            this.btn_Invoices.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Invoices.IndicateFocus = true;
            this.btn_Invoices.Location = new System.Drawing.Point(28, 188);
            this.btn_Invoices.Name = "btn_Invoices";
            this.btn_Invoices.ShadowDecoration.Depth = 1;
            this.btn_Invoices.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Invoices.Size = new System.Drawing.Size(219, 55);
            this.btn_Invoices.TabIndex = 2;
            this.btn_Invoices.Text = "Hóa đơn";
            this.btn_Invoices.UseTransparentBackground = true;
            this.btn_Invoices.Click += new System.EventHandler(this.btn_Invoices_Click);
            // 
            // btn_Statistics
            // 
            this.btn_Statistics.Animated = true;
            this.btn_Statistics.BackColor = System.Drawing.Color.Transparent;
            this.btn_Statistics.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Statistics.BorderRadius = 20;
            this.btn_Statistics.BorderThickness = 2;
            this.btn_Statistics.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Statistics.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Statistics.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Statistics.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Statistics.FillColor = System.Drawing.Color.Transparent;
            this.btn_Statistics.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Statistics.ForeColor = System.Drawing.Color.White;
            this.btn_Statistics.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Statistics.IndicateFocus = true;
            this.btn_Statistics.Location = new System.Drawing.Point(28, 127);
            this.btn_Statistics.Name = "btn_Statistics";
            this.btn_Statistics.ShadowDecoration.Depth = 1;
            this.btn_Statistics.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Statistics.Size = new System.Drawing.Size(219, 55);
            this.btn_Statistics.TabIndex = 2;
            this.btn_Statistics.Text = "Thống kê";
            this.btn_Statistics.UseTransparentBackground = true;
            this.btn_Statistics.Click += new System.EventHandler(this.btn_Statistics_Click);
            // 
            // ibnt_Logo
            // 
            this.ibnt_Logo.AnimatedGIF = true;
            this.ibnt_Logo.BackColor = System.Drawing.Color.Transparent;
            this.ibnt_Logo.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.ibnt_Logo.HoverState.ImageSize = new System.Drawing.Size(100, 100);
            this.ibnt_Logo.Image = ((System.Drawing.Image)(resources.GetObject("ibnt_Logo.Image")));
            this.ibnt_Logo.ImageOffset = new System.Drawing.Point(0, 0);
            this.ibnt_Logo.ImageRotate = 0F;
            this.ibnt_Logo.ImageSize = new System.Drawing.Size(90, 90);
            this.ibnt_Logo.Location = new System.Drawing.Point(81, -11);
            this.ibnt_Logo.Name = "ibnt_Logo";
            this.ibnt_Logo.PressedState.ImageSize = new System.Drawing.Size(95, 95);
            this.ibnt_Logo.Size = new System.Drawing.Size(111, 118);
            this.ibnt_Logo.TabIndex = 6;
            this.ibnt_Logo.UseTransparentBackground = true;
            this.ibnt_Logo.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ibtn_Logo_MouseDoubleClick);
            // 
            // guna2Separator2
            // 
            this.guna2Separator2.FillColor = System.Drawing.Color.LightGray;
            this.guna2Separator2.Location = new System.Drawing.Point(29, 556);
            this.guna2Separator2.Name = "guna2Separator2";
            this.guna2Separator2.Size = new System.Drawing.Size(216, 10);
            this.guna2Separator2.TabIndex = 5;
            // 
            // guna2Separator1
            // 
            this.guna2Separator1.FillColor = System.Drawing.Color.LightGray;
            this.guna2Separator1.Location = new System.Drawing.Point(31, 113);
            this.guna2Separator1.Name = "guna2Separator1";
            this.guna2Separator1.Size = new System.Drawing.Size(216, 10);
            this.guna2Separator1.TabIndex = 1;
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // guna2DragControl2
            // 
            this.guna2DragControl2.DockIndicatorTransparencyValue = 0.6D;
            this.guna2DragControl2.TargetControl = this.panel_Main;
            this.guna2DragControl2.TransparentWhileDrag = false;
            // 
            // panel_Main
            // 
            this.panel_Main.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Main.BackColor = System.Drawing.Color.Transparent;
            this.panel_Main.Controls.Add(this.panel_Exit);
            this.panel_Main.FillColor = System.Drawing.Color.White;
            this.panel_Main.Location = new System.Drawing.Point(244, -1);
            this.panel_Main.Name = "panel_Main";
            this.panel_Main.Radius = 5;
            this.panel_Main.ShadowColor = System.Drawing.Color.Black;
            this.panel_Main.ShadowDepth = 95;
            this.panel_Main.ShadowShift = 8;
            this.panel_Main.Size = new System.Drawing.Size(1139, 696);
            this.panel_Main.TabIndex = 3;
            // 
            // panel_Exit
            // 
            this.panel_Exit.BackColor = System.Drawing.Color.Transparent;
            this.panel_Exit.BorderRadius = 10;
            this.panel_Exit.Controls.Add(this.btn_Close);
            this.panel_Exit.Controls.Add(this.btn_SignOut);
            this.panel_Exit.FillColor = System.Drawing.Color.Crimson;
            this.panel_Exit.FillColor2 = System.Drawing.Color.Red;
            this.panel_Exit.FillColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panel_Exit.FillColor4 = System.Drawing.Color.Crimson;
            this.panel_Exit.ForeColor = System.Drawing.Color.Transparent;
            this.panel_Exit.Location = new System.Drawing.Point(0, 570);
            this.panel_Exit.Name = "panel_Exit";
            this.panel_Exit.ShadowDecoration.Depth = 10;
            this.panel_Exit.ShadowDecoration.Enabled = true;
            this.panel_Exit.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.panel_Exit.Size = new System.Drawing.Size(225, 123);
            this.panel_Exit.TabIndex = 1;
            this.panel_Exit.Visible = false;
            // 
            // btn_Close
            // 
            this.btn_Close.Animated = true;
            this.btn_Close.BackColor = System.Drawing.Color.Transparent;
            this.btn_Close.BorderColor = System.Drawing.Color.Transparent;
            this.btn_Close.BorderRadius = 15;
            this.btn_Close.BorderThickness = 2;
            this.btn_Close.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Close.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Close.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Close.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Close.FillColor = System.Drawing.Color.Transparent;
            this.btn_Close.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.btn_Close.ForeColor = System.Drawing.Color.White;
            this.btn_Close.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_Close.Image = global::MiniMarketManagement.Properties.Resources.exit__1_;
            this.btn_Close.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Close.ImageOffset = new System.Drawing.Point(2, 0);
            this.btn_Close.ImageSize = new System.Drawing.Size(25, 25);
            this.btn_Close.IndicateFocus = true;
            this.btn_Close.Location = new System.Drawing.Point(3, 3);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.ShadowDecoration.Depth = 1;
            this.btn_Close.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_Close.Size = new System.Drawing.Size(219, 55);
            this.btn_Close.TabIndex = 2;
            this.btn_Close.Text = "Thoát";
            this.btn_Close.UseTransparentBackground = true;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // btn_SignOut
            // 
            this.btn_SignOut.Animated = true;
            this.btn_SignOut.BackColor = System.Drawing.Color.Transparent;
            this.btn_SignOut.BorderColor = System.Drawing.Color.Transparent;
            this.btn_SignOut.BorderRadius = 15;
            this.btn_SignOut.BorderThickness = 2;
            this.btn_SignOut.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_SignOut.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_SignOut.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_SignOut.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_SignOut.FillColor = System.Drawing.Color.Transparent;
            this.btn_SignOut.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.btn_SignOut.ForeColor = System.Drawing.Color.White;
            this.btn_SignOut.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btn_SignOut.Image = global::MiniMarketManagement.Properties.Resources.logout__1_1;
            this.btn_SignOut.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_SignOut.ImageOffset = new System.Drawing.Point(2, 0);
            this.btn_SignOut.ImageSize = new System.Drawing.Size(26, 26);
            this.btn_SignOut.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_SignOut.IndicateFocus = true;
            this.btn_SignOut.Location = new System.Drawing.Point(3, 64);
            this.btn_SignOut.Name = "btn_SignOut";
            this.btn_SignOut.ShadowDecoration.Depth = 1;
            this.btn_SignOut.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.btn_SignOut.Size = new System.Drawing.Size(219, 55);
            this.btn_SignOut.TabIndex = 2;
            this.btn_SignOut.Text = "Đăng xuất";
            this.btn_SignOut.UseTransparentBackground = true;
            this.btn_SignOut.Click += new System.EventHandler(this.btn_SignOut_Click);
            // 
            // frm_Admin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1384, 695);
            this.Controls.Add(this.panel_Main);
            this.Controls.Add(this.panel_AdminSideBoard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_Admin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Admin";
            this.Load += new System.EventHandler(this.frm_Admin_Load);
            this.panel_AdminSideBoard.ResumeLayout(false);
            this.panel_AdminSideBoard.PerformLayout();
            this.panel_Main.ResumeLayout(false);
            this.panel_Exit.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2DragControl guna2DragControl1;
        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2DragControl guna2DragControl2;
        private Guna.UI2.WinForms.Guna2ShadowPanel panel_Main;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel panel_Exit;
        private Guna.UI2.WinForms.Guna2Button btn_Close;
        private Guna.UI2.WinForms.Guna2Button btn_SignOut;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel panel_AdminSideBoard;
        private Guna.UI2.WinForms.Guna2Button btn_Minimize;
        private Guna.UI2.WinForms.Guna2Button btn_Option;
        private Guna.UI2.WinForms.Guna2Button btn_EmployeeManagement;
        private Guna.UI2.WinForms.Guna2Button btn_CreateAccount;
        private Guna.UI2.WinForms.Guna2Button btn_ReturnManagement;
        private Guna.UI2.WinForms.Guna2Button btn_ImportManagement;
        private Guna.UI2.WinForms.Guna2Button btn_LoyalCustomers;
        private Guna.UI2.WinForms.Guna2Button btn_Invoices;
        private Guna.UI2.WinForms.Guna2Button btn_Statistics;
        private Guna.UI2.WinForms.Guna2ImageButton ibnt_Logo;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator2;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator1;
        private Guna.UI2.WinForms.Guna2AnimateWindow guna2AnimateWindow1;
        private System.Windows.Forms.Label lb_EmployeeName;
    }
}


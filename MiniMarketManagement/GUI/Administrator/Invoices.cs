﻿using MiniMarketManagement.BLL.Administrator;
using MiniMarketManagement.DAL.Administrator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.GUI.Administrator
{
    public partial class frm_Invoices : Form
    {
        public frm_Invoices()
        {
            InitializeComponent();
        }
        private void Clear()
        {
            txt_SearchInvoiceByCustomerID.Clear();
            txt_SearchInvoiceByInvoiceID.Clear();
        }
        private void frm_Invoices_Load(object sender, EventArgs e)
        {
            BLL_Invoices bLL_Invoices = new BLL_Invoices();
            lb_LoyalCustomerInvoices.Text = new BLL_Invoices().GetLoyalCustomerInvoices().ToString();
            lb_CustomersInvoices.Text = new BLL_Invoices().GetCustomerInvoices().ToString();
            bLL_Invoices.ShowInvoice(dgv_Invoices);
        }
        private void txt_LoyalCustomers_TextChanged(object sender, EventArgs e)
        {
            lb_LoyalCustomerInvoices.Text = new BLL_Invoices().GetLoyalCustomerInvoices().ToString();
        }

        private void txt_Customers_TextChanged(object sender, EventArgs e)
        {
            lb_CustomersInvoices.Text = new BLL_Invoices().GetCustomerInvoices().ToString();
        }

        private void chb_FilterLoyalCustomerInvoice_CheckedChanged(object sender, EventArgs e)
        {
            BLL_Invoices bLL_Invoices = new BLL_Invoices();            
            bLL_Invoices.FilterLoyalCustomerInvoiceChecked(chb_FilterLoyalCustomerInvoice, chb_FillterCustomerInvoice, dgv_Invoices);  
        }

        private void chb_FillterCustomer_CheckedChanged(object sender, EventArgs e)
        {
            BLL_Invoices bLL_Invoices = new BLL_Invoices();
            bLL_Invoices.FilterCustomerInvoiceChecked( chb_FilterLoyalCustomerInvoice, chb_FillterCustomerInvoice, dgv_Invoices);
        }

        private void btn_Search_Click(object sender, EventArgs e)
        {
            BLL_Invoices bLL_Invoices = new BLL_Invoices();
            bLL_Invoices.Filter(dtp_fromDate, dtp_toDate, txt_SearchInvoiceByInvoiceID.Text, txt_SearchInvoiceByCustomerID.Text, dgv_Invoices);
            Clear();
        }
        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            frm_Invoices_Load(sender, e);
            chb_FillterCustomerInvoice.Checked = false;
            chb_FilterLoyalCustomerInvoice.Checked = false;
        }
    }
}

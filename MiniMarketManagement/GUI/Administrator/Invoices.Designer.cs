﻿namespace MiniMarketManagement.GUI.Administrator
{
    partial class frm_Invoices
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.dgv_Invoices = new Guna.UI2.WinForms.Guna2DataGridView();
            this.InvoiceID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InvoiceDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PriceTotal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MinusPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.chb_FilterLoyalCustomerInvoice = new System.Windows.Forms.CheckBox();
            this.chb_FillterCustomerInvoice = new System.Windows.Forms.CheckBox();
            this.guna2CustomGradientPanel1 = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.lb_LoyalCustomerInvoices = new System.Windows.Forms.Label();
            this.lb_CustomersInvoices = new System.Windows.Forms.Label();
            this.guna2TextBox4 = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp_toDate = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.dtp_fromDate = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.btn_Refresh = new Guna.UI2.WinForms.Guna2Button();
            this.btn_Search = new Guna.UI2.WinForms.Guna2Button();
            this.txt_SearchInvoiceByCustomerID = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_SearchInvoiceByInvoiceID = new Guna.UI2.WinForms.Guna2TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Invoices)).BeginInit();
            this.guna2CustomGradientPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // dgv_Invoices
            // 
            this.dgv_Invoices.AllowUserToAddRows = false;
            this.dgv_Invoices.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            this.dgv_Invoices.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_Invoices.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Invoices.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_Invoices.ColumnHeadersHeight = 18;
            this.dgv_Invoices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_Invoices.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.InvoiceID,
            this.CustomerID,
            this.EmployeeID,
            this.InvoiceDate,
            this.PriceTotal,
            this.MinusPrice});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Invoices.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_Invoices.GridColor = System.Drawing.Color.SeaShell;
            this.dgv_Invoices.Location = new System.Drawing.Point(0, 242);
            this.dgv_Invoices.Name = "dgv_Invoices";
            this.dgv_Invoices.ReadOnly = true;
            this.dgv_Invoices.RowHeadersVisible = false;
            this.dgv_Invoices.RowHeadersWidth = 51;
            this.dgv_Invoices.RowTemplate.Height = 24;
            this.dgv_Invoices.Size = new System.Drawing.Size(1136, 449);
            this.dgv_Invoices.TabIndex = 28;
            this.dgv_Invoices.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_Invoices.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_Invoices.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_Invoices.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_Invoices.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_Invoices.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_Invoices.ThemeStyle.GridColor = System.Drawing.Color.SeaShell;
            this.dgv_Invoices.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_Invoices.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_Invoices.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_Invoices.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_Invoices.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_Invoices.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_Invoices.ThemeStyle.ReadOnly = true;
            this.dgv_Invoices.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_Invoices.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_Invoices.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_Invoices.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_Invoices.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_Invoices.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_Invoices.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            // 
            // InvoiceID
            // 
            this.InvoiceID.HeaderText = "Mã hóa đơn";
            this.InvoiceID.MinimumWidth = 6;
            this.InvoiceID.Name = "InvoiceID";
            this.InvoiceID.ReadOnly = true;
            // 
            // CustomerID
            // 
            this.CustomerID.HeaderText = "Mã khách hàng";
            this.CustomerID.MinimumWidth = 6;
            this.CustomerID.Name = "CustomerID";
            this.CustomerID.ReadOnly = true;
            // 
            // EmployeeID
            // 
            this.EmployeeID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.EmployeeID.HeaderText = "Mã nhân viên";
            this.EmployeeID.MinimumWidth = 6;
            this.EmployeeID.Name = "EmployeeID";
            this.EmployeeID.ReadOnly = true;
            // 
            // InvoiceDate
            // 
            this.InvoiceDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.InvoiceDate.HeaderText = "Ngày xuất";
            this.InvoiceDate.MinimumWidth = 6;
            this.InvoiceDate.Name = "InvoiceDate";
            this.InvoiceDate.ReadOnly = true;
            // 
            // PriceTotal
            // 
            this.PriceTotal.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PriceTotal.HeaderText = "Tổng tiền";
            this.PriceTotal.MinimumWidth = 6;
            this.PriceTotal.Name = "PriceTotal";
            this.PriceTotal.ReadOnly = true;
            // 
            // MinusPrice
            // 
            this.MinusPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.MinusPrice.HeaderText = "Giảm giá";
            this.MinusPrice.MinimumWidth = 6;
            this.MinusPrice.Name = "MinusPrice";
            this.MinusPrice.ReadOnly = true;
            // 
            // chb_FilterLoyalCustomerInvoice
            // 
            this.chb_FilterLoyalCustomerInvoice.AutoSize = true;
            this.chb_FilterLoyalCustomerInvoice.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chb_FilterLoyalCustomerInvoice.Location = new System.Drawing.Point(21, 216);
            this.chb_FilterLoyalCustomerInvoice.Name = "chb_FilterLoyalCustomerInvoice";
            this.chb_FilterLoyalCustomerInvoice.Size = new System.Drawing.Size(229, 20);
            this.chb_FilterLoyalCustomerInvoice.TabIndex = 30;
            this.chb_FilterLoyalCustomerInvoice.Text = "Hóa đơn khách hàng thân thiết";
            this.chb_FilterLoyalCustomerInvoice.UseVisualStyleBackColor = true;
            this.chb_FilterLoyalCustomerInvoice.CheckedChanged += new System.EventHandler(this.chb_FilterLoyalCustomerInvoice_CheckedChanged);
            // 
            // chb_FillterCustomerInvoice
            // 
            this.chb_FillterCustomerInvoice.AutoSize = true;
            this.chb_FillterCustomerInvoice.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chb_FillterCustomerInvoice.Location = new System.Drawing.Point(283, 216);
            this.chb_FillterCustomerInvoice.Name = "chb_FillterCustomerInvoice";
            this.chb_FillterCustomerInvoice.Size = new System.Drawing.Size(178, 20);
            this.chb_FillterCustomerInvoice.TabIndex = 30;
            this.chb_FillterCustomerInvoice.Text = "Hóa đơn khách vãng lai";
            this.chb_FillterCustomerInvoice.UseVisualStyleBackColor = true;
            this.chb_FillterCustomerInvoice.CheckedChanged += new System.EventHandler(this.chb_FillterCustomer_CheckedChanged);
            // 
            // guna2CustomGradientPanel1
            // 
            this.guna2CustomGradientPanel1.BackColor = System.Drawing.Color.Snow;
            this.guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.LightPink;
            this.guna2CustomGradientPanel1.BorderRadius = 25;
            this.guna2CustomGradientPanel1.BorderThickness = 2;
            this.guna2CustomGradientPanel1.Controls.Add(this.lb_LoyalCustomerInvoices);
            this.guna2CustomGradientPanel1.Controls.Add(this.lb_CustomersInvoices);
            this.guna2CustomGradientPanel1.Controls.Add(this.guna2TextBox4);
            this.guna2CustomGradientPanel1.Controls.Add(this.label4);
            this.guna2CustomGradientPanel1.Controls.Add(this.label3);
            this.guna2CustomGradientPanel1.FillColor = System.Drawing.Color.Snow;
            this.guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.Snow;
            this.guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.Snow;
            this.guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.Snow;
            this.guna2CustomGradientPanel1.Location = new System.Drawing.Point(12, 12);
            this.guna2CustomGradientPanel1.Name = "guna2CustomGradientPanel1";
            this.guna2CustomGradientPanel1.Size = new System.Drawing.Size(471, 185);
            this.guna2CustomGradientPanel1.TabIndex = 31;
            // 
            // lb_LoyalCustomerInvoices
            // 
            this.lb_LoyalCustomerInvoices.AutoSize = true;
            this.lb_LoyalCustomerInvoices.Font = new System.Drawing.Font("Tahoma", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_LoyalCustomerInvoices.Location = new System.Drawing.Point(84, 48);
            this.lb_LoyalCustomerInvoices.Name = "lb_LoyalCustomerInvoices";
            this.lb_LoyalCustomerInvoices.Size = new System.Drawing.Size(55, 57);
            this.lb_LoyalCustomerInvoices.TabIndex = 37;
            this.lb_LoyalCustomerInvoices.Text = "0";
            // 
            // lb_CustomersInvoices
            // 
            this.lb_CustomersInvoices.AutoSize = true;
            this.lb_CustomersInvoices.Font = new System.Drawing.Font("Tahoma", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_CustomersInvoices.Location = new System.Drawing.Point(310, 48);
            this.lb_CustomersInvoices.Name = "lb_CustomersInvoices";
            this.lb_CustomersInvoices.Size = new System.Drawing.Size(55, 57);
            this.lb_CustomersInvoices.TabIndex = 37;
            this.lb_CustomersInvoices.Text = "0";
            // 
            // guna2TextBox4
            // 
            this.guna2TextBox4.Animated = true;
            this.guna2TextBox4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox4.DefaultText = "";
            this.guna2TextBox4.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.Enabled = false;
            this.guna2TextBox4.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.guna2TextBox4.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Location = new System.Drawing.Point(936, 65);
            this.guna2TextBox4.Margin = new System.Windows.Forms.Padding(10, 14, 10, 14);
            this.guna2TextBox4.Name = "guna2TextBox4";
            this.guna2TextBox4.PasswordChar = '\0';
            this.guna2TextBox4.PlaceholderForeColor = System.Drawing.Color.White;
            this.guna2TextBox4.PlaceholderText = "";
            this.guna2TextBox4.SelectedText = "";
            this.guna2TextBox4.Size = new System.Drawing.Size(515, 326);
            this.guna2TextBox4.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(253, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Hóa đơn khách vãng lai";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(35, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(166, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Hóa đơn KH thân thiết";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(792, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 18);
            this.label1.TabIndex = 35;
            this.label1.Text = "đến";
            // 
            // dtp_toDate
            // 
            this.dtp_toDate.Animated = true;
            this.dtp_toDate.BackColor = System.Drawing.Color.Transparent;
            this.dtp_toDate.BorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtp_toDate.BorderRadius = 15;
            this.dtp_toDate.BorderThickness = 1;
            this.dtp_toDate.Checked = true;
            this.dtp_toDate.FillColor = System.Drawing.Color.LightPink;
            this.dtp_toDate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.dtp_toDate.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.dtp_toDate.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dtp_toDate.Location = new System.Drawing.Point(833, 12);
            this.dtp_toDate.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dtp_toDate.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtp_toDate.Name = "dtp_toDate";
            this.dtp_toDate.Size = new System.Drawing.Size(289, 36);
            this.dtp_toDate.TabIndex = 33;
            this.dtp_toDate.Value = new System.DateTime(2023, 11, 30, 0, 0, 0, 0);
            // 
            // dtp_fromDate
            // 
            this.dtp_fromDate.Animated = true;
            this.dtp_fromDate.BackColor = System.Drawing.Color.Transparent;
            this.dtp_fromDate.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dtp_fromDate.BorderRadius = 15;
            this.dtp_fromDate.BorderThickness = 1;
            this.dtp_fromDate.Checked = true;
            this.dtp_fromDate.FillColor = System.Drawing.Color.LightPink;
            this.dtp_fromDate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.dtp_fromDate.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.dtp_fromDate.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dtp_fromDate.Location = new System.Drawing.Point(497, 12);
            this.dtp_fromDate.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dtp_fromDate.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtp_fromDate.Name = "dtp_fromDate";
            this.dtp_fromDate.Size = new System.Drawing.Size(289, 36);
            this.dtp_fromDate.TabIndex = 34;
            this.dtp_fromDate.Value = new System.DateTime(2023, 11, 1, 0, 0, 0, 0);
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.guna2PictureBox1.FillColor = System.Drawing.Color.Snow;
            this.guna2PictureBox1.Image = global::MiniMarketManagement.Properties.Resources.receipt;
            this.guna2PictureBox1.ImageRotate = 0F;
            this.guna2PictureBox1.Location = new System.Drawing.Point(610, 115);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.Size = new System.Drawing.Size(148, 115);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.guna2PictureBox1.TabIndex = 37;
            this.guna2PictureBox1.TabStop = false;
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.Animated = true;
            this.btn_Refresh.BackColor = System.Drawing.Color.Transparent;
            this.btn_Refresh.BorderColor = System.Drawing.Color.Silver;
            this.btn_Refresh.BorderRadius = 15;
            this.btn_Refresh.BorderThickness = 1;
            this.btn_Refresh.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Refresh.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Refresh.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Refresh.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Refresh.FillColor = System.Drawing.Color.LightPink;
            this.btn_Refresh.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Refresh.ForeColor = System.Drawing.Color.White;
            this.btn_Refresh.Image = global::MiniMarketManagement.Properties.Resources.refresh__1_;
            this.btn_Refresh.Location = new System.Drawing.Point(873, 115);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.Size = new System.Drawing.Size(111, 37);
            this.btn_Refresh.TabIndex = 36;
            this.btn_Refresh.Text = "Làm mới";
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // btn_Search
            // 
            this.btn_Search.Animated = true;
            this.btn_Search.BackColor = System.Drawing.Color.Transparent;
            this.btn_Search.BorderColor = System.Drawing.Color.Silver;
            this.btn_Search.BorderRadius = 15;
            this.btn_Search.BorderThickness = 1;
            this.btn_Search.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Search.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Search.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Search.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Search.FillColor = System.Drawing.Color.LightPink;
            this.btn_Search.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btn_Search.ForeColor = System.Drawing.Color.White;
            this.btn_Search.Image = global::MiniMarketManagement.Properties.Resources.find_my_friend;
            this.btn_Search.ImageSize = new System.Drawing.Size(17, 17);
            this.btn_Search.Location = new System.Drawing.Point(1013, 115);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.Size = new System.Drawing.Size(109, 37);
            this.btn_Search.TabIndex = 36;
            this.btn_Search.Text = "Tìm kiếm";
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // txt_SearchInvoiceByCustomerID
            // 
            this.txt_SearchInvoiceByCustomerID.Animated = true;
            this.txt_SearchInvoiceByCustomerID.BackColor = System.Drawing.Color.Transparent;
            this.txt_SearchInvoiceByCustomerID.BorderColor = System.Drawing.Color.Silver;
            this.txt_SearchInvoiceByCustomerID.BorderRadius = 15;
            this.txt_SearchInvoiceByCustomerID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_SearchInvoiceByCustomerID.DefaultText = "";
            this.txt_SearchInvoiceByCustomerID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_SearchInvoiceByCustomerID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_SearchInvoiceByCustomerID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SearchInvoiceByCustomerID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SearchInvoiceByCustomerID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_SearchInvoiceByCustomerID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SearchInvoiceByCustomerID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchInvoiceByCustomerID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SearchInvoiceByCustomerID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_SearchInvoiceByCustomerID.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchInvoiceByCustomerID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SearchInvoiceByCustomerID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchInvoiceByCustomerID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SearchInvoiceByCustomerID.IconLeft = global::MiniMarketManagement.Properties.Resources.search;
            this.txt_SearchInvoiceByCustomerID.Location = new System.Drawing.Point(833, 60);
            this.txt_SearchInvoiceByCustomerID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_SearchInvoiceByCustomerID.Name = "txt_SearchInvoiceByCustomerID";
            this.txt_SearchInvoiceByCustomerID.PasswordChar = '\0';
            this.txt_SearchInvoiceByCustomerID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_SearchInvoiceByCustomerID.PlaceholderText = "Mã khách hàng";
            this.txt_SearchInvoiceByCustomerID.SelectedText = "";
            this.txt_SearchInvoiceByCustomerID.ShadowDecoration.BorderRadius = 15;
            this.txt_SearchInvoiceByCustomerID.ShadowDecoration.Enabled = true;
            this.txt_SearchInvoiceByCustomerID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_SearchInvoiceByCustomerID.Size = new System.Drawing.Size(289, 35);
            this.txt_SearchInvoiceByCustomerID.TabIndex = 32;
            // 
            // txt_SearchInvoiceByInvoiceID
            // 
            this.txt_SearchInvoiceByInvoiceID.Animated = true;
            this.txt_SearchInvoiceByInvoiceID.BackColor = System.Drawing.Color.Transparent;
            this.txt_SearchInvoiceByInvoiceID.BorderColor = System.Drawing.Color.Silver;
            this.txt_SearchInvoiceByInvoiceID.BorderRadius = 15;
            this.txt_SearchInvoiceByInvoiceID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_SearchInvoiceByInvoiceID.DefaultText = "";
            this.txt_SearchInvoiceByInvoiceID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_SearchInvoiceByInvoiceID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_SearchInvoiceByInvoiceID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SearchInvoiceByInvoiceID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SearchInvoiceByInvoiceID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_SearchInvoiceByInvoiceID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SearchInvoiceByInvoiceID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchInvoiceByInvoiceID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SearchInvoiceByInvoiceID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_SearchInvoiceByInvoiceID.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchInvoiceByInvoiceID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SearchInvoiceByInvoiceID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchInvoiceByInvoiceID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SearchInvoiceByInvoiceID.IconLeft = global::MiniMarketManagement.Properties.Resources.search;
            this.txt_SearchInvoiceByInvoiceID.Location = new System.Drawing.Point(497, 60);
            this.txt_SearchInvoiceByInvoiceID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_SearchInvoiceByInvoiceID.Name = "txt_SearchInvoiceByInvoiceID";
            this.txt_SearchInvoiceByInvoiceID.PasswordChar = '\0';
            this.txt_SearchInvoiceByInvoiceID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_SearchInvoiceByInvoiceID.PlaceholderText = "Mã hóa đơn";
            this.txt_SearchInvoiceByInvoiceID.SelectedText = "";
            this.txt_SearchInvoiceByInvoiceID.ShadowDecoration.BorderRadius = 15;
            this.txt_SearchInvoiceByInvoiceID.ShadowDecoration.Enabled = true;
            this.txt_SearchInvoiceByInvoiceID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_SearchInvoiceByInvoiceID.Size = new System.Drawing.Size(289, 35);
            this.txt_SearchInvoiceByInvoiceID.TabIndex = 32;
            // 
            // frm_Invoices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.guna2PictureBox1);
            this.Controls.Add(this.btn_Refresh);
            this.Controls.Add(this.btn_Search);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtp_toDate);
            this.Controls.Add(this.dtp_fromDate);
            this.Controls.Add(this.txt_SearchInvoiceByCustomerID);
            this.Controls.Add(this.txt_SearchInvoiceByInvoiceID);
            this.Controls.Add(this.guna2CustomGradientPanel1);
            this.Controls.Add(this.chb_FillterCustomerInvoice);
            this.Controls.Add(this.chb_FilterLoyalCustomerInvoice);
            this.Controls.Add(this.dgv_Invoices);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_Invoices";
            this.Text = "Invoices";
            this.Load += new System.EventHandler(this.frm_Invoices_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Invoices)).EndInit();
            this.guna2CustomGradientPanel1.ResumeLayout(false);
            this.guna2CustomGradientPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_Invoices;
        private System.Windows.Forms.CheckBox chb_FillterCustomerInvoice;
        private System.Windows.Forms.CheckBox chb_FilterLoyalCustomerInvoice;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel guna2CustomGradientPanel1;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2Button btn_Search;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2DateTimePicker dtp_toDate;
        private Guna.UI2.WinForms.Guna2DateTimePicker dtp_fromDate;
        private Guna.UI2.WinForms.Guna2TextBox txt_SearchInvoiceByInvoiceID;
        private Guna.UI2.WinForms.Guna2TextBox txt_SearchInvoiceByCustomerID;
        private System.Windows.Forms.Label lb_LoyalCustomerInvoices;
        private System.Windows.Forms.Label lb_CustomersInvoices;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvoiceID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerID;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn InvoiceDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn PriceTotal;
        private System.Windows.Forms.DataGridViewTextBoxColumn MinusPrice;
        private Guna.UI2.WinForms.Guna2Button btn_Refresh;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
    }
}
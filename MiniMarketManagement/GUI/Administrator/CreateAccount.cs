﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniMarketManagement.BLL.Administrator;
namespace MiniMarketManagement.GUI.Administrator
{
    public partial class frm_CreateAccount : Form
    {
        BLL_CreateAccount bll_CreateAccount = new BLL_CreateAccount();
        public frm_CreateAccount()
        {
            InitializeComponent();
            bll_CreateAccount.FillDgv_Account(dgv_Account);
        }



        private void dgv_Account_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            bll_CreateAccount.LoadCellClick_Account(sender, e, dgv_Account, txt_Username,txt_Password,txt_EmployeeID);
        }
        private void btn_CreateAccount_Click(object sender, EventArgs e)
        {
            bll_CreateAccount.btn_createAccount(txt_Username, txt_Password, txt_EmployeeID);
            bll_CreateAccount.FillDgv_Account(dgv_Account);
        }

        private void txt_Search_TextChanged(object sender, EventArgs e)
        {
            bll_CreateAccount.SearchAccount(dgv_Account,txt_Search);
            txt_Username.Clear();
            txt_Password.Clear();
            txt_EmployeeID.Clear();
        }

        private void btn_ChangePassword_Click(object sender, EventArgs e)
        {
            var check = bll_CreateAccount.btn_changePassword(txt_Username,txt_Password);
            if (check == true)
            {
                bll_CreateAccount.FillDgv_Account(dgv_Account);
                txt_Username.Clear();
                txt_Password.Clear();
                txt_EmployeeID.Clear();
            }
        }

        private void btn_DeleteAccount_Click(object sender, EventArgs e)
        {
            var check = bll_CreateAccount.deleteAcount(txt_Username);
            if(check == true)
            {
                bll_CreateAccount.FillDgv_Account(dgv_Account);
                txt_Username.Clear();
                txt_Password.Clear();
                txt_EmployeeID.Clear();
            }
        }

        private void txt_Username_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

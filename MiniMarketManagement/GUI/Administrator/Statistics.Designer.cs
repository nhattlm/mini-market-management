﻿namespace MiniMarketManagement.GUI.Administrator
{
    partial class frm_Statistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Guna.Charts.WinForms.ChartFont chartFont9 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.ChartFont chartFont10 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.ChartFont chartFont11 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.ChartFont chartFont12 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.Grid grid4 = new Guna.Charts.WinForms.Grid();
            Guna.Charts.WinForms.Tick tick4 = new Guna.Charts.WinForms.Tick();
            Guna.Charts.WinForms.ChartFont chartFont13 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.Grid grid5 = new Guna.Charts.WinForms.Grid();
            Guna.Charts.WinForms.Tick tick5 = new Guna.Charts.WinForms.Tick();
            Guna.Charts.WinForms.ChartFont chartFont14 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.Grid grid6 = new Guna.Charts.WinForms.Grid();
            Guna.Charts.WinForms.PointLabel pointLabel2 = new Guna.Charts.WinForms.PointLabel();
            Guna.Charts.WinForms.ChartFont chartFont15 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.Tick tick6 = new Guna.Charts.WinForms.Tick();
            Guna.Charts.WinForms.ChartFont chartFont16 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.ChartFont chartFont1 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.ChartFont chartFont2 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.ChartFont chartFont3 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.ChartFont chartFont4 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.Grid grid1 = new Guna.Charts.WinForms.Grid();
            Guna.Charts.WinForms.Tick tick1 = new Guna.Charts.WinForms.Tick();
            Guna.Charts.WinForms.ChartFont chartFont5 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.Grid grid2 = new Guna.Charts.WinForms.Grid();
            Guna.Charts.WinForms.Tick tick2 = new Guna.Charts.WinForms.Tick();
            Guna.Charts.WinForms.ChartFont chartFont6 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.Grid grid3 = new Guna.Charts.WinForms.Grid();
            Guna.Charts.WinForms.PointLabel pointLabel1 = new Guna.Charts.WinForms.PointLabel();
            Guna.Charts.WinForms.ChartFont chartFont7 = new Guna.Charts.WinForms.ChartFont();
            Guna.Charts.WinForms.Tick tick3 = new Guna.Charts.WinForms.Tick();
            Guna.Charts.WinForms.ChartFont chartFont8 = new Guna.Charts.WinForms.ChartFont();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.guna2AnimateWindow1 = new Guna.UI2.WinForms.Guna2AnimateWindow(this.components);
            this.guna2Separator2 = new Guna.UI2.WinForms.Guna2Separator();
            this.cmb_ExpenseMonth = new Guna.UI2.WinForms.Guna2ComboBox();
            this.lb_TotalExpense = new System.Windows.Forms.Label();
            this.label_Expense = new System.Windows.Forms.Label();
            this.cpb_Expense = new Guna.UI2.WinForms.Guna2CircleProgressBar();
            this.lb_ExpenseComparedToThePreviousMonth = new System.Windows.Forms.Label();
            this.panel_Expense = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2PictureBox2 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.panel_Income = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.cpb_Income = new Guna.UI2.WinForms.Guna2CircleProgressBar();
            this.lb_IncomeComparedToThePreviousMonth = new System.Windows.Forms.Label();
            this.label_Income = new System.Windows.Forms.Label();
            this.lb_TotalIncome = new System.Windows.Forms.Label();
            this.cmb_IncomeMonth = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2Separator1 = new Guna.UI2.WinForms.Guna2Separator();
            this.panel_Profit = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2PictureBox3 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.cpb_Profit = new Guna.UI2.WinForms.Guna2CircleProgressBar();
            this.lb_ProfitComparedToThePreviousMonth = new System.Windows.Forms.Label();
            this.label_Profit = new System.Windows.Forms.Label();
            this.lb_TotalProfit = new System.Windows.Forms.Label();
            this.cmb_ProfitMonth = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2Separator3 = new Guna.UI2.WinForms.Guna2Separator();
            this.panel_SplineChart = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.chart_Spline = new Guna.Charts.WinForms.GunaChart();
            this.cb_ViewMode = new Guna.UI2.WinForms.Guna2ToggleSwitch();
            this.guna2AnimateWindow2 = new Guna.UI2.WinForms.Guna2AnimateWindow(this.components);
            this.label_ViewMode = new System.Windows.Forms.Label();
            this.panel_PieChart = new Guna.UI2.WinForms.Guna2ShadowPanel();
            this.Chart_Pie = new Guna.Charts.WinForms.GunaChart();
            this.gunaPieDataset1 = new Guna.Charts.WinForms.GunaPieDataset();
            this.cpb_Expense.SuspendLayout();
            this.panel_Expense.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).BeginInit();
            this.panel_Income.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            this.cpb_Income.SuspendLayout();
            this.panel_Profit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox3)).BeginInit();
            this.cpb_Profit.SuspendLayout();
            this.panel_SplineChart.SuspendLayout();
            this.panel_PieChart.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // guna2Separator2
            // 
            this.guna2Separator2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.guna2Separator2.Location = new System.Drawing.Point(3, 55);
            this.guna2Separator2.Name = "guna2Separator2";
            this.guna2Separator2.Size = new System.Drawing.Size(351, 10);
            this.guna2Separator2.TabIndex = 0;
            // 
            // cmb_ExpenseMonth
            // 
            this.cmb_ExpenseMonth.BackColor = System.Drawing.Color.Transparent;
            this.cmb_ExpenseMonth.BorderColor = System.Drawing.Color.Silver;
            this.cmb_ExpenseMonth.BorderRadius = 8;
            this.cmb_ExpenseMonth.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_ExpenseMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ExpenseMonth.FocusedColor = System.Drawing.Color.Tomato;
            this.cmb_ExpenseMonth.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.cmb_ExpenseMonth.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.cmb_ExpenseMonth.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmb_ExpenseMonth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.cmb_ExpenseMonth.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.cmb_ExpenseMonth.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.cmb_ExpenseMonth.ItemHeight = 30;
            this.cmb_ExpenseMonth.Location = new System.Drawing.Point(60, 13);
            this.cmb_ExpenseMonth.Name = "cmb_ExpenseMonth";
            this.cmb_ExpenseMonth.Size = new System.Drawing.Size(168, 36);
            this.cmb_ExpenseMonth.TabIndex = 1;
            this.cmb_ExpenseMonth.SelectedIndexChanged += new System.EventHandler(this.cmb_ExpenseMonth_SelectedIndexChanged);
            // 
            // lb_TotalExpense
            // 
            this.lb_TotalExpense.AutoSize = true;
            this.lb_TotalExpense.Font = new System.Drawing.Font("Tahoma", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_TotalExpense.Location = new System.Drawing.Point(32, 104);
            this.lb_TotalExpense.Name = "lb_TotalExpense";
            this.lb_TotalExpense.Size = new System.Drawing.Size(44, 45);
            this.lb_TotalExpense.TabIndex = 2;
            this.lb_TotalExpense.Text = "0";
            // 
            // label_Expense
            // 
            this.label_Expense.AutoSize = true;
            this.label_Expense.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Expense.ForeColor = System.Drawing.Color.DimGray;
            this.label_Expense.Location = new System.Drawing.Point(77, 176);
            this.label_Expense.Name = "label_Expense";
            this.label_Expense.Size = new System.Drawing.Size(70, 18);
            this.label_Expense.TabIndex = 2;
            this.label_Expense.Text = "Tổng chi";
            // 
            // cpb_Expense
            // 
            this.cpb_Expense.Animated = true;
            this.cpb_Expense.AnimationSpeed = 0.5F;
            this.cpb_Expense.Controls.Add(this.lb_ExpenseComparedToThePreviousMonth);
            this.cpb_Expense.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(213)))), ((int)(((byte)(218)))), ((int)(((byte)(223)))));
            this.cpb_Expense.FillThickness = 11;
            this.cpb_Expense.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cpb_Expense.ForeColor = System.Drawing.Color.White;
            this.cpb_Expense.Location = new System.Drawing.Point(211, 71);
            this.cpb_Expense.Minimum = 0;
            this.cpb_Expense.Name = "cpb_Expense";
            this.cpb_Expense.ProgressColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.cpb_Expense.ProgressColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.cpb_Expense.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round;
            this.cpb_Expense.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round;
            this.cpb_Expense.ProgressThickness = 11;
            this.cpb_Expense.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.cpb_Expense.Size = new System.Drawing.Size(130, 130);
            this.cpb_Expense.TabIndex = 3;
            this.cpb_Expense.Text = "guna2CircleProgressBar2";
            this.cpb_Expense.Value = 70;
            // 
            // lb_ExpenseComparedToThePreviousMonth
            // 
            this.lb_ExpenseComparedToThePreviousMonth.AutoSize = true;
            this.lb_ExpenseComparedToThePreviousMonth.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ExpenseComparedToThePreviousMonth.ForeColor = System.Drawing.Color.DimGray;
            this.lb_ExpenseComparedToThePreviousMonth.Location = new System.Drawing.Point(18, 57);
            this.lb_ExpenseComparedToThePreviousMonth.Name = "lb_ExpenseComparedToThePreviousMonth";
            this.lb_ExpenseComparedToThePreviousMonth.Size = new System.Drawing.Size(18, 18);
            this.lb_ExpenseComparedToThePreviousMonth.TabIndex = 2;
            this.lb_ExpenseComparedToThePreviousMonth.Text = "0";
            this.lb_ExpenseComparedToThePreviousMonth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lb_ExpenseComparedToThePreviousMonth.Visible = false;
            // 
            // panel_Expense
            // 
            this.panel_Expense.BackColor = System.Drawing.Color.White;
            this.panel_Expense.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel_Expense.BorderThickness = 1;
            this.panel_Expense.Controls.Add(this.guna2PictureBox2);
            this.panel_Expense.Controls.Add(this.cpb_Expense);
            this.panel_Expense.Controls.Add(this.label_Expense);
            this.panel_Expense.Controls.Add(this.lb_TotalExpense);
            this.panel_Expense.Controls.Add(this.cmb_ExpenseMonth);
            this.panel_Expense.Controls.Add(this.guna2Separator2);
            this.panel_Expense.Location = new System.Drawing.Point(765, 12);
            this.panel_Expense.Name = "panel_Expense";
            this.panel_Expense.ShadowDecoration.Color = System.Drawing.Color.Silver;
            this.panel_Expense.ShadowDecoration.Enabled = true;
            this.panel_Expense.Size = new System.Drawing.Size(357, 219);
            this.panel_Expense.TabIndex = 1;
            // 
            // guna2PictureBox2
            // 
            this.guna2PictureBox2.FillColor = System.Drawing.Color.Snow;
            this.guna2PictureBox2.Image = global::MiniMarketManagement.Properties.Resources.devaluation;
            this.guna2PictureBox2.ImageRotate = 0F;
            this.guna2PictureBox2.Location = new System.Drawing.Point(12, 10);
            this.guna2PictureBox2.Name = "guna2PictureBox2";
            this.guna2PictureBox2.Size = new System.Drawing.Size(42, 42);
            this.guna2PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.guna2PictureBox2.TabIndex = 5;
            this.guna2PictureBox2.TabStop = false;
            // 
            // panel_Income
            // 
            this.panel_Income.BackColor = System.Drawing.Color.White;
            this.panel_Income.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel_Income.BorderThickness = 1;
            this.panel_Income.Controls.Add(this.guna2PictureBox1);
            this.panel_Income.Controls.Add(this.cpb_Income);
            this.panel_Income.Controls.Add(this.label_Income);
            this.panel_Income.Controls.Add(this.lb_TotalIncome);
            this.panel_Income.Controls.Add(this.cmb_IncomeMonth);
            this.panel_Income.Controls.Add(this.guna2Separator1);
            this.panel_Income.Location = new System.Drawing.Point(12, 12);
            this.panel_Income.Name = "panel_Income";
            this.panel_Income.ShadowDecoration.Color = System.Drawing.Color.Silver;
            this.panel_Income.ShadowDecoration.Enabled = true;
            this.panel_Income.Size = new System.Drawing.Size(358, 219);
            this.panel_Income.TabIndex = 2;
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.FillColor = System.Drawing.Color.Snow;
            this.guna2PictureBox1.Image = global::MiniMarketManagement.Properties.Resources.profit;
            this.guna2PictureBox1.ImageRotate = 0F;
            this.guna2PictureBox1.Location = new System.Drawing.Point(12, 10);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.Size = new System.Drawing.Size(42, 42);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.guna2PictureBox1.TabIndex = 5;
            this.guna2PictureBox1.TabStop = false;
            // 
            // cpb_Income
            // 
            this.cpb_Income.Animated = true;
            this.cpb_Income.AnimationSpeed = 0.5F;
            this.cpb_Income.Controls.Add(this.lb_IncomeComparedToThePreviousMonth);
            this.cpb_Income.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(213)))), ((int)(((byte)(218)))), ((int)(((byte)(223)))));
            this.cpb_Income.FillThickness = 11;
            this.cpb_Income.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cpb_Income.ForeColor = System.Drawing.Color.White;
            this.cpb_Income.Location = new System.Drawing.Point(209, 71);
            this.cpb_Income.Minimum = 0;
            this.cpb_Income.Name = "cpb_Income";
            this.cpb_Income.ProgressColor = System.Drawing.Color.Aqua;
            this.cpb_Income.ProgressColor2 = System.Drawing.Color.PaleGreen;
            this.cpb_Income.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round;
            this.cpb_Income.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round;
            this.cpb_Income.ProgressThickness = 11;
            this.cpb_Income.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.cpb_Income.Size = new System.Drawing.Size(130, 130);
            this.cpb_Income.TabIndex = 3;
            this.cpb_Income.Text = "guna2CircleProgressBar1";
            this.cpb_Income.Value = 70;
            // 
            // lb_IncomeComparedToThePreviousMonth
            // 
            this.lb_IncomeComparedToThePreviousMonth.AutoSize = true;
            this.lb_IncomeComparedToThePreviousMonth.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_IncomeComparedToThePreviousMonth.ForeColor = System.Drawing.Color.DimGray;
            this.lb_IncomeComparedToThePreviousMonth.Location = new System.Drawing.Point(23, 55);
            this.lb_IncomeComparedToThePreviousMonth.Name = "lb_IncomeComparedToThePreviousMonth";
            this.lb_IncomeComparedToThePreviousMonth.Size = new System.Drawing.Size(18, 18);
            this.lb_IncomeComparedToThePreviousMonth.TabIndex = 2;
            this.lb_IncomeComparedToThePreviousMonth.Text = "0";
            this.lb_IncomeComparedToThePreviousMonth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lb_IncomeComparedToThePreviousMonth.Visible = false;
            // 
            // label_Income
            // 
            this.label_Income.AutoSize = true;
            this.label_Income.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Income.ForeColor = System.Drawing.Color.DimGray;
            this.label_Income.Location = new System.Drawing.Point(69, 176);
            this.label_Income.Name = "label_Income";
            this.label_Income.Size = new System.Drawing.Size(72, 18);
            this.label_Income.TabIndex = 2;
            this.label_Income.Text = "Tổng thu";
            // 
            // lb_TotalIncome
            // 
            this.lb_TotalIncome.AutoSize = true;
            this.lb_TotalIncome.Font = new System.Drawing.Font("Tahoma", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_TotalIncome.Location = new System.Drawing.Point(29, 104);
            this.lb_TotalIncome.Name = "lb_TotalIncome";
            this.lb_TotalIncome.Size = new System.Drawing.Size(44, 45);
            this.lb_TotalIncome.TabIndex = 2;
            this.lb_TotalIncome.Text = "0";
            // 
            // cmb_IncomeMonth
            // 
            this.cmb_IncomeMonth.BackColor = System.Drawing.Color.Transparent;
            this.cmb_IncomeMonth.BorderColor = System.Drawing.Color.Silver;
            this.cmb_IncomeMonth.BorderRadius = 8;
            this.cmb_IncomeMonth.DisplayMember = "Tháng 1";
            this.cmb_IncomeMonth.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_IncomeMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_IncomeMonth.FocusedColor = System.Drawing.Color.Tomato;
            this.cmb_IncomeMonth.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.cmb_IncomeMonth.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.cmb_IncomeMonth.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmb_IncomeMonth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.cmb_IncomeMonth.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.cmb_IncomeMonth.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.cmb_IncomeMonth.ItemHeight = 30;
            this.cmb_IncomeMonth.Location = new System.Drawing.Point(60, 13);
            this.cmb_IncomeMonth.Name = "cmb_IncomeMonth";
            this.cmb_IncomeMonth.Size = new System.Drawing.Size(168, 36);
            this.cmb_IncomeMonth.TabIndex = 1;
            this.cmb_IncomeMonth.SelectedIndexChanged += new System.EventHandler(this.cmb_IncomeMonth_SelectedIndexChanged);
            // 
            // guna2Separator1
            // 
            this.guna2Separator1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.guna2Separator1.Location = new System.Drawing.Point(3, 55);
            this.guna2Separator1.Name = "guna2Separator1";
            this.guna2Separator1.Size = new System.Drawing.Size(351, 10);
            this.guna2Separator1.TabIndex = 0;
            // 
            // panel_Profit
            // 
            this.panel_Profit.BackColor = System.Drawing.Color.White;
            this.panel_Profit.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel_Profit.BorderThickness = 1;
            this.panel_Profit.Controls.Add(this.guna2PictureBox3);
            this.panel_Profit.Controls.Add(this.cpb_Profit);
            this.panel_Profit.Controls.Add(this.label_Profit);
            this.panel_Profit.Controls.Add(this.lb_TotalProfit);
            this.panel_Profit.Controls.Add(this.cmb_ProfitMonth);
            this.panel_Profit.Controls.Add(this.guna2Separator3);
            this.panel_Profit.Location = new System.Drawing.Point(376, 12);
            this.panel_Profit.Name = "panel_Profit";
            this.panel_Profit.ShadowDecoration.Color = System.Drawing.Color.Silver;
            this.panel_Profit.ShadowDecoration.Enabled = true;
            this.panel_Profit.Size = new System.Drawing.Size(383, 219);
            this.panel_Profit.TabIndex = 4;
            // 
            // guna2PictureBox3
            // 
            this.guna2PictureBox3.FillColor = System.Drawing.Color.Snow;
            this.guna2PictureBox3.Image = global::MiniMarketManagement.Properties.Resources.profit__1_;
            this.guna2PictureBox3.ImageRotate = 0F;
            this.guna2PictureBox3.Location = new System.Drawing.Point(12, 12);
            this.guna2PictureBox3.Name = "guna2PictureBox3";
            this.guna2PictureBox3.Size = new System.Drawing.Size(39, 39);
            this.guna2PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.guna2PictureBox3.TabIndex = 5;
            this.guna2PictureBox3.TabStop = false;
            // 
            // cpb_Profit
            // 
            this.cpb_Profit.Animated = true;
            this.cpb_Profit.AnimationSpeed = 0.5F;
            this.cpb_Profit.Backwards = true;
            this.cpb_Profit.Controls.Add(this.lb_ProfitComparedToThePreviousMonth);
            this.cpb_Profit.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(200)))), ((int)(((byte)(213)))), ((int)(((byte)(218)))), ((int)(((byte)(223)))));
            this.cpb_Profit.FillThickness = 11;
            this.cpb_Profit.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cpb_Profit.ForeColor = System.Drawing.Color.White;
            this.cpb_Profit.Location = new System.Drawing.Point(224, 71);
            this.cpb_Profit.Minimum = 0;
            this.cpb_Profit.Name = "cpb_Profit";
            this.cpb_Profit.ProgressColor = System.Drawing.Color.LightSteelBlue;
            this.cpb_Profit.ProgressColor2 = System.Drawing.Color.DeepSkyBlue;
            this.cpb_Profit.ProgressEndCap = System.Drawing.Drawing2D.LineCap.Round;
            this.cpb_Profit.ProgressStartCap = System.Drawing.Drawing2D.LineCap.Round;
            this.cpb_Profit.ProgressThickness = 11;
            this.cpb_Profit.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.cpb_Profit.Size = new System.Drawing.Size(130, 130);
            this.cpb_Profit.TabIndex = 3;
            this.cpb_Profit.Text = "guna2CircleProgressBar3";
            this.cpb_Profit.Value = 70;
            // 
            // lb_ProfitComparedToThePreviousMonth
            // 
            this.lb_ProfitComparedToThePreviousMonth.AutoSize = true;
            this.lb_ProfitComparedToThePreviousMonth.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ProfitComparedToThePreviousMonth.ForeColor = System.Drawing.Color.DimGray;
            this.lb_ProfitComparedToThePreviousMonth.Location = new System.Drawing.Point(24, 55);
            this.lb_ProfitComparedToThePreviousMonth.Name = "lb_ProfitComparedToThePreviousMonth";
            this.lb_ProfitComparedToThePreviousMonth.Size = new System.Drawing.Size(18, 18);
            this.lb_ProfitComparedToThePreviousMonth.TabIndex = 2;
            this.lb_ProfitComparedToThePreviousMonth.Text = "0";
            this.lb_ProfitComparedToThePreviousMonth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lb_ProfitComparedToThePreviousMonth.Visible = false;
            // 
            // label_Profit
            // 
            this.label_Profit.AutoSize = true;
            this.label_Profit.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Profit.ForeColor = System.Drawing.Color.DimGray;
            this.label_Profit.Location = new System.Drawing.Point(72, 176);
            this.label_Profit.Name = "label_Profit";
            this.label_Profit.Size = new System.Drawing.Size(80, 18);
            this.label_Profit.TabIndex = 2;
            this.label_Profit.Text = "Lợi nhuận";
            // 
            // lb_TotalProfit
            // 
            this.lb_TotalProfit.AutoSize = true;
            this.lb_TotalProfit.Font = new System.Drawing.Font("Tahoma", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_TotalProfit.Location = new System.Drawing.Point(6, 104);
            this.lb_TotalProfit.Name = "lb_TotalProfit";
            this.lb_TotalProfit.Size = new System.Drawing.Size(44, 45);
            this.lb_TotalProfit.TabIndex = 2;
            this.lb_TotalProfit.Text = "0";
            // 
            // cmb_ProfitMonth
            // 
            this.cmb_ProfitMonth.BackColor = System.Drawing.Color.Transparent;
            this.cmb_ProfitMonth.BorderColor = System.Drawing.Color.Silver;
            this.cmb_ProfitMonth.BorderRadius = 8;
            this.cmb_ProfitMonth.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_ProfitMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_ProfitMonth.FocusedColor = System.Drawing.Color.Tomato;
            this.cmb_ProfitMonth.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.cmb_ProfitMonth.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.cmb_ProfitMonth.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.cmb_ProfitMonth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.cmb_ProfitMonth.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.cmb_ProfitMonth.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.cmb_ProfitMonth.ItemHeight = 30;
            this.cmb_ProfitMonth.Location = new System.Drawing.Point(60, 13);
            this.cmb_ProfitMonth.Name = "cmb_ProfitMonth";
            this.cmb_ProfitMonth.Size = new System.Drawing.Size(168, 36);
            this.cmb_ProfitMonth.TabIndex = 1;
            this.cmb_ProfitMonth.SelectedIndexChanged += new System.EventHandler(this.cmb_ProfitMonth_SelectedIndexChanged);
            // 
            // guna2Separator3
            // 
            this.guna2Separator3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.guna2Separator3.Location = new System.Drawing.Point(3, 55);
            this.guna2Separator3.Name = "guna2Separator3";
            this.guna2Separator3.Size = new System.Drawing.Size(377, 10);
            this.guna2Separator3.TabIndex = 0;
            // 
            // panel_SplineChart
            // 
            this.panel_SplineChart.BackColor = System.Drawing.Color.Transparent;
            this.panel_SplineChart.Controls.Add(this.chart_Spline);
            this.panel_SplineChart.FillColor = System.Drawing.Color.White;
            this.panel_SplineChart.Location = new System.Drawing.Point(9, 237);
            this.panel_SplineChart.Name = "panel_SplineChart";
            this.panel_SplineChart.ShadowColor = System.Drawing.Color.Black;
            this.panel_SplineChart.Size = new System.Drawing.Size(668, 447);
            this.panel_SplineChart.TabIndex = 5;
            // 
            // chart_Spline
            // 
            this.chart_Spline.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chart_Spline.Animation.Duration = 900;
            this.chart_Spline.Animation.Easing = Guna.Charts.WinForms.Easing.EaseOutQuint;
            chartFont9.FontName = "Arial";
            this.chart_Spline.Legend.LabelFont = chartFont9;
            this.chart_Spline.Location = new System.Drawing.Point(3, 5);
            this.chart_Spline.Margin = new System.Windows.Forms.Padding(4);
            this.chart_Spline.Name = "chart_Spline";
            this.chart_Spline.Size = new System.Drawing.Size(661, 437);
            this.chart_Spline.TabIndex = 1;
            chartFont10.FontName = "Arial";
            chartFont10.Size = 12;
            chartFont10.Style = Guna.Charts.WinForms.ChartFontStyle.Bold;
            this.chart_Spline.Title.Font = chartFont10;
            chartFont11.FontName = "Arial";
            this.chart_Spline.Tooltips.BodyFont = chartFont11;
            chartFont12.FontName = "Arial";
            chartFont12.Size = 9;
            chartFont12.Style = Guna.Charts.WinForms.ChartFontStyle.Bold;
            this.chart_Spline.Tooltips.TitleFont = chartFont12;
            this.chart_Spline.XAxes.GridLines = grid4;
            chartFont13.FontName = "Arial";
            tick4.Font = chartFont13;
            this.chart_Spline.XAxes.Ticks = tick4;
            this.chart_Spline.YAxes.GridLines = grid5;
            chartFont14.FontName = "Arial";
            tick5.Font = chartFont14;
            this.chart_Spline.YAxes.Ticks = tick5;
            this.chart_Spline.ZAxes.GridLines = grid6;
            chartFont15.FontName = "Arial";
            pointLabel2.Font = chartFont15;
            this.chart_Spline.ZAxes.PointLabels = pointLabel2;
            chartFont16.FontName = "Arial";
            tick6.Font = chartFont16;
            this.chart_Spline.ZAxes.Ticks = tick6;
            // 
            // cb_ViewMode
            // 
            this.cb_ViewMode.Animated = true;
            this.cb_ViewMode.CheckedState.BorderColor = System.Drawing.Color.Silver;
            this.cb_ViewMode.CheckedState.BorderThickness = 1;
            this.cb_ViewMode.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(74)))));
            this.cb_ViewMode.CheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_ViewMode.CheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_ViewMode.Location = new System.Drawing.Point(683, 242);
            this.cb_ViewMode.Name = "cb_ViewMode";
            this.cb_ViewMode.ShadowDecoration.BorderRadius = 30;
            this.cb_ViewMode.Size = new System.Drawing.Size(35, 20);
            this.cb_ViewMode.TabIndex = 6;
            this.cb_ViewMode.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_ViewMode.UncheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(125)))), ((int)(((byte)(137)))), ((int)(((byte)(149)))));
            this.cb_ViewMode.UncheckedState.InnerBorderColor = System.Drawing.Color.White;
            this.cb_ViewMode.UncheckedState.InnerColor = System.Drawing.Color.White;
            this.cb_ViewMode.CheckedChanged += new System.EventHandler(this.cb_ViewMode_CheckedChanged);
            // 
            // label_ViewMode
            // 
            this.label_ViewMode.AutoSize = true;
            this.label_ViewMode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_ViewMode.ForeColor = System.Drawing.Color.DimGray;
            this.label_ViewMode.Location = new System.Drawing.Point(724, 244);
            this.label_ViewMode.Name = "label_ViewMode";
            this.label_ViewMode.Size = new System.Drawing.Size(82, 18);
            this.label_ViewMode.TabIndex = 2;
            this.label_ViewMode.Text = "Chế độ tối";
            // 
            // panel_PieChart
            // 
            this.panel_PieChart.BackColor = System.Drawing.Color.Transparent;
            this.panel_PieChart.Controls.Add(this.Chart_Pie);
            this.panel_PieChart.FillColor = System.Drawing.Color.White;
            this.panel_PieChart.Location = new System.Drawing.Point(683, 268);
            this.panel_PieChart.Name = "panel_PieChart";
            this.panel_PieChart.ShadowColor = System.Drawing.Color.Black;
            this.panel_PieChart.Size = new System.Drawing.Size(442, 416);
            this.panel_PieChart.TabIndex = 7;
            // 
            // Chart_Pie
            // 
            this.Chart_Pie.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Chart_Pie.Animation.Duration = 900;
            this.Chart_Pie.Animation.Easing = Guna.Charts.WinForms.Easing.EaseInOutCirc;
            chartFont1.FontName = "Arial";
            this.Chart_Pie.Legend.LabelFont = chartFont1;
            this.Chart_Pie.Location = new System.Drawing.Point(4, 4);
            this.Chart_Pie.Margin = new System.Windows.Forms.Padding(4);
            this.Chart_Pie.Name = "Chart_Pie";
            this.Chart_Pie.Size = new System.Drawing.Size(434, 408);
            this.Chart_Pie.TabIndex = 1;
            chartFont2.FontName = "Arial";
            chartFont2.Size = 12;
            chartFont2.Style = Guna.Charts.WinForms.ChartFontStyle.Bold;
            this.Chart_Pie.Title.Font = chartFont2;
            chartFont3.FontName = "Arial";
            this.Chart_Pie.Tooltips.BodyFont = chartFont3;
            chartFont4.FontName = "Arial";
            chartFont4.Size = 9;
            chartFont4.Style = Guna.Charts.WinForms.ChartFontStyle.Bold;
            this.Chart_Pie.Tooltips.TitleFont = chartFont4;
            this.Chart_Pie.XAxes.GridLines = grid1;
            chartFont5.FontName = "Arial";
            tick1.Font = chartFont5;
            this.Chart_Pie.XAxes.Ticks = tick1;
            this.Chart_Pie.YAxes.GridLines = grid2;
            chartFont6.FontName = "Arial";
            tick2.Font = chartFont6;
            this.Chart_Pie.YAxes.Ticks = tick2;
            this.Chart_Pie.ZAxes.GridLines = grid3;
            chartFont7.FontName = "Arial";
            pointLabel1.Font = chartFont7;
            this.Chart_Pie.ZAxes.PointLabels = pointLabel1;
            chartFont8.FontName = "Arial";
            tick3.Font = chartFont8;
            this.Chart_Pie.ZAxes.Ticks = tick3;
            // 
            // gunaPieDataset1
            // 
            this.gunaPieDataset1.Label = "Pie1";
            // 
            // frm_Statistics
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.panel_PieChart);
            this.Controls.Add(this.cb_ViewMode);
            this.Controls.Add(this.panel_Expense);
            this.Controls.Add(this.label_ViewMode);
            this.Controls.Add(this.panel_SplineChart);
            this.Controls.Add(this.panel_Profit);
            this.Controls.Add(this.panel_Income);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_Statistics";
            this.Text = "Statistics";
            this.Load += new System.EventHandler(this.frm_Statistics_Load);
            this.cpb_Expense.ResumeLayout(false);
            this.cpb_Expense.PerformLayout();
            this.panel_Expense.ResumeLayout(false);
            this.panel_Expense.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).EndInit();
            this.panel_Income.ResumeLayout(false);
            this.panel_Income.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            this.cpb_Income.ResumeLayout(false);
            this.cpb_Income.PerformLayout();
            this.panel_Profit.ResumeLayout(false);
            this.panel_Profit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox3)).EndInit();
            this.cpb_Profit.ResumeLayout(false);
            this.cpb_Profit.PerformLayout();
            this.panel_SplineChart.ResumeLayout(false);
            this.panel_PieChart.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2AnimateWindow guna2AnimateWindow1;
        private Guna.UI2.WinForms.Guna2Panel panel_Expense;
        private Guna.UI2.WinForms.Guna2CircleProgressBar cpb_Expense;
        private System.Windows.Forms.Label lb_ExpenseComparedToThePreviousMonth;
        private System.Windows.Forms.Label label_Expense;
        private System.Windows.Forms.Label lb_TotalExpense;
        private Guna.UI2.WinForms.Guna2ComboBox cmb_ExpenseMonth;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator2;
        private Guna.UI2.WinForms.Guna2Panel panel_Income;
        private Guna.UI2.WinForms.Guna2CircleProgressBar cpb_Income;
        private System.Windows.Forms.Label lb_IncomeComparedToThePreviousMonth;
        private System.Windows.Forms.Label label_Income;
        private System.Windows.Forms.Label lb_TotalIncome;
        private Guna.UI2.WinForms.Guna2ComboBox cmb_IncomeMonth;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator1;
        private Guna.UI2.WinForms.Guna2Panel panel_Profit;
        private Guna.UI2.WinForms.Guna2CircleProgressBar cpb_Profit;
        private System.Windows.Forms.Label lb_ProfitComparedToThePreviousMonth;
        private System.Windows.Forms.Label label_Profit;
        private System.Windows.Forms.Label lb_TotalProfit;
        private Guna.UI2.WinForms.Guna2ComboBox cmb_ProfitMonth;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator3;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox2;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox3;
        private Guna.UI2.WinForms.Guna2ShadowPanel panel_SplineChart;
        private Guna.Charts.WinForms.GunaChart chart_Spline;
        private Guna.UI2.WinForms.Guna2AnimateWindow guna2AnimateWindow2;
        private Guna.UI2.WinForms.Guna2ToggleSwitch cb_ViewMode;
        private System.Windows.Forms.Label label_ViewMode;
        private Guna.UI2.WinForms.Guna2ShadowPanel panel_PieChart;
        private Guna.Charts.WinForms.GunaChart Chart_Pie;
        private Guna.Charts.WinForms.GunaPieDataset gunaPieDataset1;
    }
}
﻿using Guna.UI2.WinForms;
using MiniMarketManagement.BLL.Administrator;
using MiniMarketManagement.DAL.Administrator;
using MiniMarketManagement.DAL.Entities;
using MiniMarketManagement.DAL.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Guna.UI2.Native.WinApi;

namespace MiniMarketManagement.GUI.Administrator
{
    public partial class frm_Admin : Form
    {
        ShowChildrenForm showChildrenForm = new ShowChildrenForm();
        BLL_EmployeeManage bLL_EmployeeManage = new BLL_EmployeeManage();
        public frm_Admin()
        {
            InitializeComponent();
        }
        private void frm_Admin_Load(object sender, EventArgs e)
        {
            btn_Statistics_Click(sender, e);
            lb_EmployeeName.Text = showEmployeeName();
        }
        private string showEmployeeName()
        {
            return "Xin chào " + bLL_EmployeeManage.GetNameEmployeesByUsername();
        }
        private void btn_Statistics_Click(object sender, EventArgs e)
        {
            frm_Statistics formStatistic = new frm_Statistics();
            showChildrenForm.ShowForm(formStatistic, btn_Statistics, panel_Main);
            panel_Exit.Visible = false;
        }

        private void btn_Invoices_Click(object sender, EventArgs e)
        {
            frm_Invoices formInvoices = new frm_Invoices();
            showChildrenForm.ShowForm(formInvoices, btn_Invoices, panel_Main);
            panel_Exit.Visible = false;
        }

        private void btn_LoyalCustomers_Click(object sender, EventArgs e)
        {
            frm_LoyalCustomers formLoyalCustomer = new frm_LoyalCustomers();
            showChildrenForm.ShowForm(formLoyalCustomer, btn_LoyalCustomers, panel_Main);
            panel_Exit.Visible = false;
        }

        private void btn_ImportManagement_Click(object sender, EventArgs e)
        {
            frm_ImportManagement formImportManagement = new frm_ImportManagement();
            showChildrenForm.ShowForm(formImportManagement, btn_ImportManagement, panel_Main);
            panel_Exit.Visible = false;
        }

        private void btn_ReturnManagement_Click(object sender, EventArgs e)
        {
            frm_ReturnManagement formReturnManagement = new frm_ReturnManagement();
            showChildrenForm.ShowForm(formReturnManagement, btn_ReturnManagement, panel_Main);
            panel_Exit.Visible = false;
        }
        private void btn_CreateAccount_Click(object sender, EventArgs e)
        {
            frm_CreateAccount formCreateAccoun = new frm_CreateAccount();
            showChildrenForm.ShowForm(formCreateAccoun, btn_CreateAccount, panel_Main);
            panel_Exit.Visible = false;
        }
        private void btn_EmployeeManagement_Click(object sender, EventArgs e)
        {
            frm_EmployeeManage formEmployeeManage = new frm_EmployeeManage();
            showChildrenForm.ShowForm(formEmployeeManage, btn_EmployeeManagement, panel_Main);
            panel_Exit.Visible = false;
        }

        bool isPanelVisible = false;
        Guna2Button currentButton;
        private void btn_Exit_Click(object sender, EventArgs e)
        {
            currentButton = btn_Option;
            if (isPanelVisible)
            {
                // Nếu panel đang hiển thị, ẩn nó
                panel_Exit.Visible = false;
                isPanelVisible = false;
                currentButton.Checked = false;
            }
            else
            {
                // Nếu panel không hiển thị, hiển thị nó
                panel_Exit.Visible = true;
                isPanelVisible = true;
            }
        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            try
            {
                this.Dispose();
                this.Close();
            }
            catch (Exception) { }
        }

        public bool isSignOut = true;
        private void btn_SignOut_Click(object sender, EventArgs e)
        {
            try
            {
                frm_Login loginForm = new frm_Login();
                loginForm.Show();
                this.Hide();
            }
            catch (Exception) { }
        }

        private void btn_Minimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        private void ibtn_Logo_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            frm_Info info = new frm_Info();
            info.Show();
        }
    }
}

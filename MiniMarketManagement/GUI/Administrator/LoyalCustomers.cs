﻿using MiniMarketManagement.BLL.Administrator;
using MiniMarketManagement.DAL.Administrator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.GUI.Administrator
{
    public partial class frm_LoyalCustomers : Form
    {
        public frm_LoyalCustomers()
        {
            InitializeComponent();
        }
        private void frm_LoyalCustomers_Load(object sender, EventArgs e)
        {
            BLL_LoyalCustomer bLL_LoyalCustomer = new BLL_LoyalCustomer();
            bLL_LoyalCustomer.ShowCustomerList(dgv_LoyalCustomers);
            lb_LoyalCustomers.Text = new BLL_LoyalCustomer().TotalLoyalCustomer().ToString();
            lb_Customers.Text = new BLL_LoyalCustomer().TotalCustomer().ToString();
        }
        public void clear()
        {
            txt_CustomerID.Text = string.Empty;
            txt_CustomerName.Text = string.Empty;
            txt_CustomerPhoneNumber.Text = string.Empty;
            txt_CustomerPoint.Text = string.Empty;
            txt_SearchCustomerByName.Text = string.Empty;
        }
        private void dgv_LoyalCustomers_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgv_LoyalCustomers.SelectedRows.Count > 0)
            {
                DataGridViewRow selectedRow = dgv_LoyalCustomers.SelectedRows[0];
                txt_CustomerName.Text = selectedRow.Cells["CustomerName"].Value.ToString();
                txt_CustomerID.Text = selectedRow.Cells["CustomerID"].Value.ToString();
                txt_CustomerPhoneNumber.Text = selectedRow.Cells["CustomerPhoneNumber"].Value.ToString();
                txt_CustomerPoint.Text = selectedRow.Cells["CustomerPoint"].Value.ToString();
            }else
            {
                return;
            }
        }

        private void btn_Update_Click(object sender, EventArgs e)
        {
            BLL_LoyalCustomer bLL_LoyalCustomer = new BLL_LoyalCustomer();
            bLL_LoyalCustomer.UpdateCustomer(txt_CustomerID.Text.Trim(), txt_CustomerName.Text.Trim(), txt_CustomerPhoneNumber.Text.Trim(), txt_CustomerPoint.Text.Trim());
            bLL_LoyalCustomer.ShowCustomerList(dgv_LoyalCustomers);
            txt_LoyalCustomers_TextChanged(sender, e);
            txt_Customers_TextChanged(sender, e);
            clear();
        }
        private void btn_Delete_Click(object sender, EventArgs e)
        {
            BLL_LoyalCustomer bLL_LoyalCustomer = new BLL_LoyalCustomer();
            bLL_LoyalCustomer.DeleteCustomer(txt_CustomerID.Text.Trim());
            bLL_LoyalCustomer.ShowCustomerList(dgv_LoyalCustomers);
            lb_LoyalCustomers.Text = new BLL_LoyalCustomer().TotalLoyalCustomer().ToString();
            lb_Customers.Text = new BLL_LoyalCustomer().TotalCustomer().ToString();
            clear();
        }
        private void txt_LoyalCustomers_TextChanged(object sender, EventArgs e)
        {
            lb_LoyalCustomers.Text = new BLL_LoyalCustomer().TotalLoyalCustomer().ToString();
        }

        private void txt_Customers_TextChanged(object sender, EventArgs e)
        {
            lb_LoyalCustomers.Text = new BLL_LoyalCustomer().TotalCustomer().ToString();
        }
        private void btn_Search_Click(object sender, EventArgs e)
        {
            BLL_LoyalCustomer bLL_LoyalCustomer = new BLL_LoyalCustomer();
            bLL_LoyalCustomer.SearchCustomersByIDOrPhoneNumber(txt_SearchCustomerByName.Text, dgv_LoyalCustomers);
            clear();
        }
    }
}

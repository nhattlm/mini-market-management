﻿using Guna.UI2.WinForms;
using MiniMarketManagement.BLL.Administrator;
using MiniMarketManagement.DAL.Administrator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniMarketManagement.GUI;
using System.Windows.Forms.DataVisualization.Charting;
using TheArtOfDevHtmlRenderer.Adapters.Entities;

namespace MiniMarketManagement.GUI.Administrator
{
    public partial class frm_Statistics : Form
    {
        public frm_Statistics()
        {
            InitializeComponent();
        }
        private void frm_Statistics_Load(object sender, EventArgs e)
        {
            FillComboBox();
            BLL_Statistics bLL_Statistics = new BLL_Statistics();
            lb_TotalIncome.Text = BLL_Statistics.ConvertMoneyToString(bLL_Statistics.GetTotalIncome());
            lb_TotalExpense.Text = BLL_Statistics.ConvertMoneyToString(bLL_Statistics.GetTotalExpense());
            lb_TotalProfit.Text = BLL_Statistics.ConvertMoneyToString(bLL_Statistics.GetTotalProfit());
            Chart.SplineArea(chart_Spline);
            Chart.Pie(Chart_Pie);

        }
        string[] Collections = { "Tháng 1", "Tháng 2", "Tháng 3", "Tháng 4", "Tháng 5", "Tháng 6"
                               , "Tháng 7", "Tháng 8", "Tháng 9", "Tháng 10", "Tháng 11", "Tháng 12"};
        //int DefaultItem = (DateTime.Now.Month) - 1;
        private void FillComboBox()
        {
            cmb_ExpenseMonth.Items.AddRange(Collections);
            cmb_IncomeMonth.Items.AddRange(Collections);
            cmb_ProfitMonth.Items.AddRange(Collections);
            //cmb_ExpenseMonth.SelectedIndex = DefaultItem;
            //cmb_IncomeMonth.SelectedIndex = DefaultItem;
        }
        private void cmb_IncomeMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            BLL_Statistics bLL_Statistics = new BLL_Statistics();
            lb_TotalIncome.Text = BLL_Statistics.ConvertMoneyToString(bLL_Statistics.GetTotalIncomeByMonth(Collections, cmb_IncomeMonth));
            (lb_IncomeComparedToThePreviousMonth.Text, lb_IncomeComparedToThePreviousMonth.ForeColor) = Draw_CompareIncomeFunction(new BLL_Statistics()
                .CompareTotalIncomeWithPreviousMonth(Collections, cmb_IncomeMonth));
            lb_IncomeComparedToThePreviousMonth.Visible = true;
        }

        private void cmb_ExpenseMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            BLL_Statistics bLL_Statistics = new BLL_Statistics();
            lb_TotalExpense.Text = BLL_Statistics.ConvertMoneyToString(bLL_Statistics.GetTotalExpenseByMonth(Collections, cmb_ExpenseMonth));
            (lb_ExpenseComparedToThePreviousMonth.Text, lb_ExpenseComparedToThePreviousMonth.ForeColor) = Draw_CompareExpenseFunction(new BLL_Statistics()
                .CompareTotalExpenseWithPreviousMonth(Collections, cmb_ExpenseMonth));
            lb_ExpenseComparedToThePreviousMonth.Visible = true;
        }
        private void cmb_ProfitMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            BLL_Statistics bLL_Statistics = new BLL_Statistics();
            lb_TotalProfit.Text = BLL_Statistics.ConvertMoneyToString(bLL_Statistics.GetTotalProfitByMonth(Collections, cmb_ProfitMonth));
            (lb_ProfitComparedToThePreviousMonth.Text, lb_ProfitComparedToThePreviousMonth.ForeColor) = Draw_CompareProfitFunction(new BLL_Statistics()
                .CompareTotalProfitWithPreviousMonth(Collections, cmb_ProfitMonth));
            lb_ProfitComparedToThePreviousMonth.Visible = true;
        }
        private (string, Color) Draw_CompareProfitFunction(double item)
        {
            if (item >= 0)
            {
                return ("+" + BLL_Statistics.ConvertToRate(item) + "%", Color.LightGreen);
            }
            else
            {
                return (BLL_Statistics.ConvertToRate(item) + "%", Color.Red);
            }
        }
        private (string, Color) Draw_CompareIncomeFunction(double item)
        {
            if (item >= 0)
            {
                return ("+" + BLL_Statistics.ConvertMoneyToString(item), Color.LightGreen);
            } else
            {
                return (BLL_Statistics.ConvertMoneyToString(item), Color.Red);
            }          
        }
        private (string, Color) Draw_CompareExpenseFunction(double item)
        {
            if (item > 0)
            {
                return ("+" + BLL_Statistics.ConvertMoneyToString(item), Color.Red);
            }
            else
            {
                return (BLL_Statistics.ConvertMoneyToString(item), Color.LightGreen);
            }
        }

        private void cb_ViewMode_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_ViewMode.Checked)
            {
                chart_Spline.ApplyConfig(ConfigChart.Dark.Config(), Color.FromArgb(38, 41, 59));
                Chart_Pie.ApplyConfig(ConfigChart.Dark.Config(), Color.FromArgb(38, 41, 59));
                Chart_Pie.XAxes.Display = false;
                Chart_Pie.YAxes.Display = false;
                BackColor = Color.FromArgb(42, 45, 64);
                ForeColor = Color.LightSteelBlue;
                panel_Income.BackColor = Color.FromArgb(38, 41, 59);
                panel_Expense.BackColor = Color.FromArgb(38, 41, 59);
                panel_Profit.BackColor = Color.FromArgb(38, 41, 59);
                panel_Income.BorderColor = Color.Silver;
                panel_Expense.BorderColor = Color.Silver;
                panel_Profit.BorderColor = Color.Silver;
                label_Expense.ForeColor = Color.LightGray;
                label_Income.ForeColor = Color.LightGray;
                label_Profit.ForeColor = Color.LightGray;
                label_ViewMode.ForeColor = Color.LightGray;
                cmb_ExpenseMonth.FillColor = Color.FromArgb(38, 41, 59);
                cmb_IncomeMonth.FillColor = Color.FromArgb(38, 41, 59);
                cmb_ProfitMonth.FillColor = Color.FromArgb(38, 41, 59);
                cmb_ExpenseMonth.FocusedState.BorderColor = Color.White;
                cmb_IncomeMonth.FocusedState.BorderColor = Color.White;
                cmb_ProfitMonth.FocusedState.BorderColor = Color.White;
                cmb_ExpenseMonth.HoverState.BorderColor = Color.White;
                cmb_IncomeMonth.HoverState.BorderColor = Color.White;
                cmb_ProfitMonth.HoverState.BorderColor = Color.White;
                cmb_ExpenseMonth.ForeColor = Color.WhiteSmoke;
                cmb_IncomeMonth.ForeColor = Color.WhiteSmoke;
                cmb_ProfitMonth.ForeColor = Color.WhiteSmoke;
                cpb_Income.Backwards = true;
                cpb_Profit.Backwards = false;
                cpb_Expense.Backwards = true;
            }
            else
            {
                chart_Spline.ApplyConfig(ConfigChart.Light.Config(), Color.White);
                Chart_Pie.ApplyConfig(ConfigChart.Light.Config(), Color.White);
                Chart_Pie.XAxes.Display = false;
                Chart_Pie.YAxes.Display = false;
                BackColor = Color.White;
                ForeColor = Color.Black;
                panel_Income.BackColor = Color.White;
                panel_Expense.BackColor = Color.White;
                panel_Profit.BackColor = Color.White;
                label_Expense.ForeColor = Color.DimGray;
                label_Income.ForeColor = Color.DimGray;
                label_Profit.ForeColor = Color.DimGray;
                cmb_ExpenseMonth.FillColor = Color.White;
                cmb_IncomeMonth.FillColor = Color.White;
                cmb_ProfitMonth.FillColor = Color.White;
                cmb_ExpenseMonth.FocusedState.BorderColor = Color.Tomato;
                cmb_IncomeMonth.FocusedState.BorderColor = Color.Tomato;
                cmb_ProfitMonth.FocusedState.BorderColor = Color.Tomato;
                cmb_ExpenseMonth.HoverState.BorderColor = Color.Tomato;
                cmb_IncomeMonth.HoverState.BorderColor = Color.Tomato;
                cmb_ProfitMonth.HoverState.BorderColor = Color.Tomato;
                cmb_ExpenseMonth.ForeColor = Color.FromArgb(68, 88, 112);
                cmb_IncomeMonth.ForeColor = Color.FromArgb(68, 88, 112);
                cmb_ProfitMonth.ForeColor = Color.FromArgb(68, 88, 112);
                cpb_Income.Backwards = false;
                cpb_Profit.Backwards = true;
                cpb_Expense.Backwards = false;
            }

        }
    }
}

﻿using Microsoft.Reporting.WinForms;
using MiniMarketManagement.DAL.Entities;
using MiniMarketManagement.Report.RPM_RP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.GUI.Administrator
{
    public partial class frmRP_RPM : Form
    {
        public frmRP_RPM()
        {
            InitializeComponent();
        }

        private void frmRP_RPM_Load(object sender, EventArgs e)
        {
            MiniMarketDB db = new MiniMarketDB();
            var ListRPDto = db.DetailReturnProductCards.Select(c => new RP_RPMDto
            {
                ReturnProductID = c.ReturnProductID,
                ProductID = c.ProductID,
                ProductName = c.Product.ProductName,
                ProductTypeName = c.Product.ProductType.ProductTypeName,
                BuyPrice = c.price,
                CalculationUnitName = c.Product.ProductType.CalculationUnitName,
                Quantity = c.Quantity,
                SupplierName = c.ReturnProductsCard.ImportProductCard.Supplier.SupplierName
            }).ToList();
            this.reportViewer1.LocalReport.ReportPath = "./Report/RPM_RP/RP_RPM.rdlc";
            var reportDataSource = new ReportDataSource("RP_RPM", ListRPDto);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer1.RefreshReport();
        }
    }
}

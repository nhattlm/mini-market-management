﻿namespace MiniMarketManagement.GUI.Administrator
{
    partial class frm_CreateAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.dgv_Account = new Guna.UI2.WinForms.Guna2DataGridView();
            this.dgv_Username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_Password = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_EmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_EmployeeID = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_Password = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_Username = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_Search = new Guna.UI2.WinForms.Guna2TextBox();
            this.btn_ChangePassword = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_DeleteAccount = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_CreateAccount = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Account)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // dgv_Account
            // 
            this.dgv_Account.AllowUserToAddRows = false;
            this.dgv_Account.AllowUserToDeleteRows = false;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            this.dgv_Account.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgv_Account.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Account.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgv_Account.ColumnHeadersHeight = 18;
            this.dgv_Account.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_Account.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv_Username,
            this.dgv_Password,
            this.dgv_EmployeeID});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Account.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgv_Account.GridColor = System.Drawing.Color.Silver;
            this.dgv_Account.Location = new System.Drawing.Point(0, 292);
            this.dgv_Account.Name = "dgv_Account";
            this.dgv_Account.ReadOnly = true;
            this.dgv_Account.RowHeadersVisible = false;
            this.dgv_Account.RowHeadersWidth = 51;
            this.dgv_Account.RowTemplate.Height = 24;
            this.dgv_Account.Size = new System.Drawing.Size(1137, 387);
            this.dgv_Account.TabIndex = 0;
            this.dgv_Account.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_Account.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_Account.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_Account.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_Account.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_Account.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_Account.ThemeStyle.GridColor = System.Drawing.Color.Silver;
            this.dgv_Account.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_Account.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_Account.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_Account.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_Account.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_Account.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_Account.ThemeStyle.ReadOnly = true;
            this.dgv_Account.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_Account.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_Account.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_Account.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_Account.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_Account.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_Account.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_Account.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Account_CellClick);
            // 
            // dgv_Username
            // 
            this.dgv_Username.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_Username.HeaderText = "Tài khoản";
            this.dgv_Username.MinimumWidth = 6;
            this.dgv_Username.Name = "dgv_Username";
            this.dgv_Username.ReadOnly = true;
            // 
            // dgv_Password
            // 
            this.dgv_Password.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_Password.HeaderText = "Mật khẩu";
            this.dgv_Password.MinimumWidth = 6;
            this.dgv_Password.Name = "dgv_Password";
            this.dgv_Password.ReadOnly = true;
            // 
            // dgv_EmployeeID
            // 
            this.dgv_EmployeeID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_EmployeeID.HeaderText = "Mã nhân viên";
            this.dgv_EmployeeID.MinimumWidth = 6;
            this.dgv_EmployeeID.Name = "dgv_EmployeeID";
            this.dgv_EmployeeID.ReadOnly = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txt_EmployeeID);
            this.groupBox1.Controls.Add(this.txt_Password);
            this.groupBox1.Controls.Add(this.txt_Username);
            this.groupBox1.Controls.Add(this.txt_Search);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(378, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(436, 261);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin tài khoản";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(31, 216);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 24);
            this.label4.TabIndex = 0;
            this.label4.Text = "Mã nhân viên";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(31, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "Mật khẩu";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(31, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 24);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tìm kiếm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(31, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tài Khoản";
            // 
            // txt_EmployeeID
            // 
            this.txt_EmployeeID.Animated = true;
            this.txt_EmployeeID.BackColor = System.Drawing.Color.Transparent;
            this.txt_EmployeeID.BorderColor = System.Drawing.Color.LightGray;
            this.txt_EmployeeID.BorderRadius = 15;
            this.txt_EmployeeID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_EmployeeID.DefaultText = "";
            this.txt_EmployeeID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_EmployeeID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_EmployeeID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_EmployeeID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_EmployeeID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_EmployeeID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_EmployeeID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_EmployeeID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_EmployeeID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_EmployeeID.ForeColor = System.Drawing.Color.Black;
            this.txt_EmployeeID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_EmployeeID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_EmployeeID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_EmployeeID.IconLeft = global::MiniMarketManagement.Properties.Resources.id__1_4;
            this.txt_EmployeeID.Location = new System.Drawing.Point(194, 209);
            this.txt_EmployeeID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_EmployeeID.Name = "txt_EmployeeID";
            this.txt_EmployeeID.PasswordChar = '\0';
            this.txt_EmployeeID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_EmployeeID.PlaceholderText = "Mã nhân viên";
            this.txt_EmployeeID.SelectedText = "";
            this.txt_EmployeeID.ShadowDecoration.BorderRadius = 15;
            this.txt_EmployeeID.ShadowDecoration.Enabled = true;
            this.txt_EmployeeID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_EmployeeID.Size = new System.Drawing.Size(228, 37);
            this.txt_EmployeeID.TabIndex = 16;
            // 
            // txt_Password
            // 
            this.txt_Password.Animated = true;
            this.txt_Password.BackColor = System.Drawing.Color.Transparent;
            this.txt_Password.BorderColor = System.Drawing.Color.LightGray;
            this.txt_Password.BorderRadius = 15;
            this.txt_Password.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Password.DefaultText = "";
            this.txt_Password.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Password.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Password.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Password.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Password.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Password.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Password.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Password.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Password.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_Password.ForeColor = System.Drawing.Color.Black;
            this.txt_Password.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Password.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Password.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Password.IconLeft = global::MiniMarketManagement.Properties.Resources.padlock;
            this.txt_Password.Location = new System.Drawing.Point(194, 158);
            this.txt_Password.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_Password.Name = "txt_Password";
            this.txt_Password.PasswordChar = '\0';
            this.txt_Password.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Password.PlaceholderText = "Mật khẩu";
            this.txt_Password.SelectedText = "";
            this.txt_Password.ShadowDecoration.BorderRadius = 15;
            this.txt_Password.ShadowDecoration.Enabled = true;
            this.txt_Password.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Password.Size = new System.Drawing.Size(228, 37);
            this.txt_Password.TabIndex = 16;
            // 
            // txt_Username
            // 
            this.txt_Username.Animated = true;
            this.txt_Username.BackColor = System.Drawing.Color.Transparent;
            this.txt_Username.BorderColor = System.Drawing.Color.LightGray;
            this.txt_Username.BorderRadius = 15;
            this.txt_Username.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Username.DefaultText = "";
            this.txt_Username.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Username.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Username.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Username.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Username.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Username.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Username.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Username.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Username.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_Username.ForeColor = System.Drawing.Color.Black;
            this.txt_Username.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Username.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Username.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Username.IconLeft = global::MiniMarketManagement.Properties.Resources.user__1_;
            this.txt_Username.Location = new System.Drawing.Point(194, 107);
            this.txt_Username.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_Username.Name = "txt_Username";
            this.txt_Username.PasswordChar = '\0';
            this.txt_Username.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Username.PlaceholderText = "Tài khoản";
            this.txt_Username.SelectedText = "";
            this.txt_Username.ShadowDecoration.BorderRadius = 15;
            this.txt_Username.ShadowDecoration.Enabled = true;
            this.txt_Username.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Username.Size = new System.Drawing.Size(228, 37);
            this.txt_Username.TabIndex = 16;
            this.txt_Username.TextChanged += new System.EventHandler(this.txt_Username_TextChanged);
            // 
            // txt_Search
            // 
            this.txt_Search.Animated = true;
            this.txt_Search.BackColor = System.Drawing.Color.Transparent;
            this.txt_Search.BorderColor = System.Drawing.Color.LightGray;
            this.txt_Search.BorderRadius = 15;
            this.txt_Search.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Search.DefaultText = "";
            this.txt_Search.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Search.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Search.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Search.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_Search.ForeColor = System.Drawing.Color.Black;
            this.txt_Search.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.IconLeft = global::MiniMarketManagement.Properties.Resources.search1;
            this.txt_Search.Location = new System.Drawing.Point(194, 56);
            this.txt_Search.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PasswordChar = '\0';
            this.txt_Search.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Search.PlaceholderText = "Tìm kiếm";
            this.txt_Search.SelectedText = "";
            this.txt_Search.ShadowDecoration.BorderRadius = 15;
            this.txt_Search.ShadowDecoration.Enabled = true;
            this.txt_Search.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Search.Size = new System.Drawing.Size(228, 37);
            this.txt_Search.TabIndex = 1;
            this.txt_Search.TextChanged += new System.EventHandler(this.txt_Search_TextChanged);
            // 
            // btn_ChangePassword
            // 
            this.btn_ChangePassword.BackColor = System.Drawing.Color.Transparent;
            this.btn_ChangePassword.BorderRadius = 10;
            this.btn_ChangePassword.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_ChangePassword.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_ChangePassword.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_ChangePassword.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_ChangePassword.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_ChangePassword.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_ChangePassword.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_ChangePassword.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btn_ChangePassword.ForeColor = System.Drawing.Color.White;
            this.btn_ChangePassword.Image = global::MiniMarketManagement.Properties.Resources.password;
            this.btn_ChangePassword.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_ChangePassword.Location = new System.Drawing.Point(872, 124);
            this.btn_ChangePassword.Name = "btn_ChangePassword";
            this.btn_ChangePassword.ShadowDecoration.BorderRadius = 10;
            this.btn_ChangePassword.ShadowDecoration.Enabled = true;
            this.btn_ChangePassword.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_ChangePassword.Size = new System.Drawing.Size(179, 48);
            this.btn_ChangePassword.TabIndex = 0;
            this.btn_ChangePassword.Text = "Đổi mật khẩu";
            this.btn_ChangePassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_ChangePassword.Click += new System.EventHandler(this.btn_ChangePassword_Click);
            // 
            // btn_DeleteAccount
            // 
            this.btn_DeleteAccount.BackColor = System.Drawing.Color.Transparent;
            this.btn_DeleteAccount.BorderRadius = 10;
            this.btn_DeleteAccount.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_DeleteAccount.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_DeleteAccount.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_DeleteAccount.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_DeleteAccount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_DeleteAccount.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_DeleteAccount.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_DeleteAccount.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btn_DeleteAccount.ForeColor = System.Drawing.Color.White;
            this.btn_DeleteAccount.Image = global::MiniMarketManagement.Properties.Resources.user1;
            this.btn_DeleteAccount.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_DeleteAccount.Location = new System.Drawing.Point(872, 192);
            this.btn_DeleteAccount.Name = "btn_DeleteAccount";
            this.btn_DeleteAccount.ShadowDecoration.BorderRadius = 10;
            this.btn_DeleteAccount.ShadowDecoration.Enabled = true;
            this.btn_DeleteAccount.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_DeleteAccount.Size = new System.Drawing.Size(179, 48);
            this.btn_DeleteAccount.TabIndex = 0;
            this.btn_DeleteAccount.Text = "Xóa tài khoản";
            this.btn_DeleteAccount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_DeleteAccount.Click += new System.EventHandler(this.btn_DeleteAccount_Click);
            // 
            // btn_CreateAccount
            // 
            this.btn_CreateAccount.BackColor = System.Drawing.Color.Transparent;
            this.btn_CreateAccount.BorderRadius = 10;
            this.btn_CreateAccount.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_CreateAccount.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_CreateAccount.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_CreateAccount.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_CreateAccount.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_CreateAccount.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_CreateAccount.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_CreateAccount.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btn_CreateAccount.ForeColor = System.Drawing.Color.White;
            this.btn_CreateAccount.Image = global::MiniMarketManagement.Properties.Resources.add_friend;
            this.btn_CreateAccount.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_CreateAccount.Location = new System.Drawing.Point(872, 56);
            this.btn_CreateAccount.Name = "btn_CreateAccount";
            this.btn_CreateAccount.ShadowDecoration.BorderRadius = 10;
            this.btn_CreateAccount.ShadowDecoration.Enabled = true;
            this.btn_CreateAccount.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_CreateAccount.Size = new System.Drawing.Size(179, 48);
            this.btn_CreateAccount.TabIndex = 0;
            this.btn_CreateAccount.Text = "Đăng ký";
            this.btn_CreateAccount.Click += new System.EventHandler(this.btn_CreateAccount_Click);
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.Image = global::MiniMarketManagement.Properties.Resources.createAccount;
            this.guna2PictureBox1.ImageRotate = 0F;
            this.guna2PictureBox1.Location = new System.Drawing.Point(28, 12);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.Size = new System.Drawing.Size(315, 261);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.guna2PictureBox1.TabIndex = 20;
            this.guna2PictureBox1.TabStop = false;
            // 
            // frm_CreateAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.btn_ChangePassword);
            this.Controls.Add(this.btn_DeleteAccount);
            this.Controls.Add(this.btn_CreateAccount);
            this.Controls.Add(this.guna2PictureBox1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.dgv_Account);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_CreateAccount";
            this.Text = "CreateAccount";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Account)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_Account;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_Username;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_Password;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_EmployeeID;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2TextBox txt_EmployeeID;
        private Guna.UI2.WinForms.Guna2TextBox txt_Password;
        private Guna.UI2.WinForms.Guna2TextBox txt_Username;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2TextBox txt_Search;
        private Guna.UI2.WinForms.Guna2GradientButton btn_CreateAccount;
        private Guna.UI2.WinForms.Guna2GradientButton btn_DeleteAccount;
        private Guna.UI2.WinForms.Guna2GradientButton btn_ChangePassword;
    }
}
﻿using Microsoft.Reporting.WinForms;
using MiniMarketManagement.DAL.Entities;
using MiniMarketManagement.Report.IPM_RP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.GUI.Administrator
{
    public partial class frmRP_IPM : Form
    {
        public frmRP_IPM()
        {
            InitializeComponent();
        }

        private void frmRP_IPM_Load(object sender, EventArgs e)
        {
            MiniMarketDB db = new MiniMarketDB();
            var ListIPDto = db.DetailImportProductCards.Select(c => new RP_IPMDto
            {
                ImportProductID = c.ImportProductID,
                ProductID = c.ProductID,
                ProductName = c.Product.ProductName,
                ProductTypeName = c.Product.ProductType.ProductTypeName,
                BuyPrice = c.Price,
                CalculationUnitName = c.Product.ProductType.CalculationUnitName,
                Quantity = c.Quantity,
                SupplierName = c.ImportProductCard.Supplier.SupplierName
            }).ToList();
            this.reportViewer1.LocalReport.ReportPath = "./Report/IPM_RP/RP_IPM.rdlc";
            var reportDataSource = new ReportDataSource("RP_IPM", ListIPDto);
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer1.RefreshReport();
        }

    }
}

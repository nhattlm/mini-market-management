﻿using MiniMarketManagement.BLL.Administrator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniMarketManagement.GUI;
using MiniMarketManagement.DAL.Administrator;
using System.IO;

namespace MiniMarketManagement.GUI.Administrator
{
    public partial class frm_ImportManagement : Form
    {
        public frm_ImportManagement()
        {
            InitializeComponent();
        }

        private void frm_ImportManagement_Load(object sender, EventArgs e)
        {
            BLL_ImportManagement bLL_ImportManagement = new BLL_ImportManagement();
            bLL_ImportManagement.ShowDetailImportProduct(dgv_ImportProductManagement);
            lb_ImportQuantity.Text = new BLL_ImportManagement().LoadDefaultImportQuantity().ToString();
            lb_TotalImportCash.Text = BLL_ImportManagement.ConvertMoneyToString(new BLL_ImportManagement().LoadDeafaultImportProductMoney());
        }
        private void lb_ImportQuantity_TextChanged(object sender, EventArgs e)
        {
            lb_ImportQuantity.Text = new BLL_ImportManagement().ImportQuantity(txt_SearchProductByImportProductCartID.Text, dtp_fromDate, dtp_toDate).ToString();
        }

        private void lb_TotalImportCash_TextChanged(object sender, EventArgs e)
        {
            lb_TotalImportCash.Text = BLL_ImportManagement.ConvertMoneyToString(new BLL_ImportManagement().ImportProductMoney(txt_SearchProductByImportProductCartID.Text, dtp_fromDate, dtp_toDate));
        }

        private void btn_Filter_Click(object sender, EventArgs e)
        {
            BLL_ImportManagement bLLImportManagement = new BLL_ImportManagement();
            bLLImportManagement.SearchBy(txt_SearchProductByImportProductCartID.Text, dtp_fromDate, dtp_toDate, dgv_ImportProductManagement);
            lb_ImportQuantity_TextChanged(sender, e);
            lb_TotalImportCash_TextChanged(sender, e);
            txt_SearchProductByImportProductCartID.Text = null;
        }
        private void dgv_ImportProductManagement_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var row = e.RowIndex;
            if (row < 0)
            {
                return;
            }
            Image image;
            BLL_ImportManagement bLL_ImportManagement = new BLL_ImportManagement();
            bLL_ImportManagement.FillPicbox(row, dgv_ImportProductManagement, out image);
            pic_Product.Image = image;
        }      
        private void setEmptyImage()
        {
            const string EmptyImPath = "Resources\\image (2).png";
            pic_Product.Image = Image.FromFile(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, EmptyImPath));
        }
        private void btn_DeleteFilter_Click(object sender, EventArgs e)
        {
            BLL_ImportManagement bLL_ImportManagement = new BLL_ImportManagement();
            bLL_ImportManagement.ShowDetailImportProduct(dgv_ImportProductManagement);
            lb_ImportQuantity.Text = new BLL_ImportManagement().LoadDefaultImportQuantity().ToString();
            lb_TotalImportCash.Text = BLL_ImportManagement.ConvertMoneyToString(new BLL_ImportManagement().LoadDeafaultImportProductMoney());
            setEmptyImage();
        }
        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            frm_ImportManagement_Load(sender, e);
            setEmptyImage();
        }

        private void btn_ExportListImportProduct_Click(object sender, EventArgs e)
        {
            frmRP_IPM frmRP_IPM = new frmRP_IPM();
            frmRP_IPM.ShowDialog();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MiniMarketManagement.BLL.Administrator;
namespace MiniMarketManagement.GUI.Administrator
{
    public partial class frm_EmployeeManage : Form
    {
        const string EmptyImPath = "Resources\\image (2).png";
        BLL_EmployeeManage bll_EmployeeManage = new BLL_EmployeeManage();
        public frm_EmployeeManage()
        {
            InitializeComponent();
        }

        private void frm_EmployeeManage_Load(object sender, EventArgs e)
        {
            bll_EmployeeManage.FillDgv_Employee(dgv_Employee);
            bll_EmployeeManage.FillComboBoxPosition(cmb_Position);
        }

        private void dgv_Employee_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
            {
                return;
            }
            Image image;
            bll_EmployeeManage.LoadCellClick_Employee(sender, e,dgv_Employee,txt_EmployeeID,txt_EmployeeName,txt_EmployeePhone,
                txt_EmployeeBirth,cmb_Position, out image);
            Pic_Employee.Image = image;
        }

        private void txt_Search_TextChanged(object sender, EventArgs e)
        {
            bll_EmployeeManage.SearchEmployee(dgv_Employee,txt_Search);
            txt_EmployeeID.Text = "";
            txt_EmployeeName.Clear();
            txt_EmployeePhone.Clear();
            txt_EmployeeBirth.Clear();
            cmb_Position.SelectedIndex = 0;
            string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            var imagePath = Path.Combine(projectDirectory, EmptyImPath);
            Pic_Employee.Image = Image.FromFile(imagePath);
        }

        private void btn_Location_Click(object sender, EventArgs e)
        {
            Pic_Employee.Image.Dispose();
            bll_EmployeeManage.openLocationImage(sender, e, Pic_Employee);
        }

        private void btn_AddOrUpdateEmployee_Click(object sender, EventArgs e)
        {
            var t = bll_EmployeeManage.btn_AddOrUpdate_Employee(txt_EmployeeID, txt_EmployeeName, txt_EmployeePhone, txt_EmployeeBirth, cmb_Position, Pic_Employee);
            if (t == false)
            {
                return;
            }
            bll_EmployeeManage.FillDgv_Employee(dgv_Employee);
            string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            var imagePath = Path.Combine(projectDirectory, EmptyImPath);
            Pic_Employee.Image = Image.FromFile(imagePath);
        }

        private void btn_DeleteEmployee_Click(object sender, EventArgs e)
        {
            var check = bll_EmployeeManage.btn_DeleteEmployee(txt_EmployeeID);
            if(check == true)
            {
                bll_EmployeeManage.FillDgv_Employee(dgv_Employee);
                txt_EmployeeID.Text = "";
                txt_EmployeeName.Clear();
                txt_EmployeePhone.Clear();
                txt_EmployeeBirth.Clear();
                cmb_Position.SelectedIndex = 0;
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                var imagePath = Path.Combine(projectDirectory, EmptyImPath);
                Pic_Employee.Image = Image.FromFile(imagePath);
            }
        }
    }
}

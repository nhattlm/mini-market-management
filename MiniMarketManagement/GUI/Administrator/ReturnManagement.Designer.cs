﻿namespace MiniMarketManagement.GUI.Administrator
{
    partial class frm_ReturnManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ReturnManagement));
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pic_Product = new Guna.UI2.WinForms.Guna2PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp_toDate = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.dtp_fromDate = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.guna2AnimateWindow1 = new Guna.UI2.WinForms.Guna2AnimateWindow(this.components);
            this.guna2CustomGradientPanel3 = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.lb_ReturnQuantity = new System.Windows.Forms.Label();
            this.lb_TotalReturnMoney = new System.Windows.Forms.Label();
            this.guna2TextBox4 = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dgv_ReturnProductManagement = new Guna.UI2.WinForms.Guna2DataGridView();
            this.ReturnProductID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CalculationUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_Refresh = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_DeleteFilter = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_Filter = new Guna.UI2.WinForms.Guna2Button();
            this.txt_SearchProductByReturnProductCartID = new Guna.UI2.WinForms.Guna2TextBox();
            this.btn_ExportListImportProduct = new Guna.UI2.WinForms.Guna2GradientButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Product)).BeginInit();
            this.guna2CustomGradientPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ReturnProductManagement)).BeginInit();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pic_Product);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(599, 83);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 212);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mặt hàng";
            // 
            // pic_Product
            // 
            this.pic_Product.BackColor = System.Drawing.Color.Transparent;
            this.pic_Product.Image = ((System.Drawing.Image)(resources.GetObject("pic_Product.Image")));
            this.pic_Product.ImageRotate = 0F;
            this.pic_Product.Location = new System.Drawing.Point(6, 27);
            this.pic_Product.Name = "pic_Product";
            this.pic_Product.Size = new System.Drawing.Size(293, 179);
            this.pic_Product.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic_Product.TabIndex = 17;
            this.pic_Product.TabStop = false;
            this.pic_Product.UseTransparentBackground = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(647, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 18);
            this.label1.TabIndex = 17;
            this.label1.Text = "đến";
            // 
            // dtp_toDate
            // 
            this.dtp_toDate.Animated = true;
            this.dtp_toDate.BackColor = System.Drawing.Color.Transparent;
            this.dtp_toDate.BorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtp_toDate.BorderRadius = 15;
            this.dtp_toDate.BorderThickness = 1;
            this.dtp_toDate.Checked = true;
            this.dtp_toDate.FillColor = System.Drawing.Color.LightPink;
            this.dtp_toDate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.dtp_toDate.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.dtp_toDate.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dtp_toDate.Location = new System.Drawing.Point(688, 17);
            this.dtp_toDate.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dtp_toDate.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtp_toDate.Name = "dtp_toDate";
            this.dtp_toDate.Size = new System.Drawing.Size(289, 36);
            this.dtp_toDate.TabIndex = 15;
            this.dtp_toDate.Value = new System.DateTime(2023, 11, 30, 0, 0, 0, 0);
            // 
            // dtp_fromDate
            // 
            this.dtp_fromDate.Animated = true;
            this.dtp_fromDate.BackColor = System.Drawing.Color.Transparent;
            this.dtp_fromDate.BorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtp_fromDate.BorderRadius = 15;
            this.dtp_fromDate.BorderThickness = 1;
            this.dtp_fromDate.Checked = true;
            this.dtp_fromDate.FillColor = System.Drawing.Color.LightPink;
            this.dtp_fromDate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.dtp_fromDate.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.dtp_fromDate.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dtp_fromDate.Location = new System.Drawing.Point(352, 17);
            this.dtp_fromDate.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dtp_fromDate.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtp_fromDate.Name = "dtp_fromDate";
            this.dtp_fromDate.Size = new System.Drawing.Size(289, 36);
            this.dtp_fromDate.TabIndex = 16;
            this.dtp_fromDate.Value = new System.DateTime(2023, 11, 1, 0, 0, 0, 0);
            // 
            // guna2CustomGradientPanel3
            // 
            this.guna2CustomGradientPanel3.BackColor = System.Drawing.Color.Snow;
            this.guna2CustomGradientPanel3.BorderColor = System.Drawing.Color.LightPink;
            this.guna2CustomGradientPanel3.BorderRadius = 25;
            this.guna2CustomGradientPanel3.BorderThickness = 2;
            this.guna2CustomGradientPanel3.Controls.Add(this.lb_ReturnQuantity);
            this.guna2CustomGradientPanel3.Controls.Add(this.lb_TotalReturnMoney);
            this.guna2CustomGradientPanel3.Controls.Add(this.guna2TextBox4);
            this.guna2CustomGradientPanel3.Controls.Add(this.label4);
            this.guna2CustomGradientPanel3.Controls.Add(this.label5);
            this.guna2CustomGradientPanel3.Location = new System.Drawing.Point(50, 83);
            this.guna2CustomGradientPanel3.Name = "guna2CustomGradientPanel3";
            this.guna2CustomGradientPanel3.Size = new System.Drawing.Size(503, 212);
            this.guna2CustomGradientPanel3.TabIndex = 26;
            // 
            // lb_ReturnQuantity
            // 
            this.lb_ReturnQuantity.AutoSize = true;
            this.lb_ReturnQuantity.Font = new System.Drawing.Font("Tahoma", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ReturnQuantity.Location = new System.Drawing.Point(78, 65);
            this.lb_ReturnQuantity.Name = "lb_ReturnQuantity";
            this.lb_ReturnQuantity.Size = new System.Drawing.Size(55, 57);
            this.lb_ReturnQuantity.TabIndex = 29;
            this.lb_ReturnQuantity.Text = "0";
            this.lb_ReturnQuantity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_TotalReturnMoney
            // 
            this.lb_TotalReturnMoney.AutoSize = true;
            this.lb_TotalReturnMoney.Font = new System.Drawing.Font("Tahoma", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_TotalReturnMoney.Location = new System.Drawing.Point(292, 65);
            this.lb_TotalReturnMoney.Name = "lb_TotalReturnMoney";
            this.lb_TotalReturnMoney.Size = new System.Drawing.Size(55, 57);
            this.lb_TotalReturnMoney.TabIndex = 29;
            this.lb_TotalReturnMoney.Text = "0";
            this.lb_TotalReturnMoney.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // guna2TextBox4
            // 
            this.guna2TextBox4.Animated = true;
            this.guna2TextBox4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox4.DefaultText = "";
            this.guna2TextBox4.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.Enabled = false;
            this.guna2TextBox4.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.guna2TextBox4.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Location = new System.Drawing.Point(936, 65);
            this.guna2TextBox4.Margin = new System.Windows.Forms.Padding(10, 14, 10, 14);
            this.guna2TextBox4.Name = "guna2TextBox4";
            this.guna2TextBox4.PasswordChar = '\0';
            this.guna2TextBox4.PlaceholderForeColor = System.Drawing.Color.White;
            this.guna2TextBox4.PlaceholderText = "";
            this.guna2TextBox4.SelectedText = "";
            this.guna2TextBox4.Size = new System.Drawing.Size(515, 326);
            this.guna2TextBox4.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(281, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tổng tiền hàng nhận lại";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(49, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(175, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Số lượng hàng hoàn trả";
            // 
            // dgv_ReturnProductManagement
            // 
            this.dgv_ReturnProductManagement.AllowUserToAddRows = false;
            this.dgv_ReturnProductManagement.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dgv_ReturnProductManagement.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_ReturnProductManagement.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ReturnProductManagement.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_ReturnProductManagement.ColumnHeadersHeight = 18;
            this.dgv_ReturnProductManagement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ReturnProductManagement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ReturnProductID,
            this.ProductID,
            this.ProductName,
            this.TypeProductName,
            this.ProductPrice,
            this.CalculationUnit,
            this.Quantity,
            this.Supplier});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ReturnProductManagement.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_ReturnProductManagement.GridColor = System.Drawing.Color.SeaShell;
            this.dgv_ReturnProductManagement.Location = new System.Drawing.Point(-1, 316);
            this.dgv_ReturnProductManagement.Name = "dgv_ReturnProductManagement";
            this.dgv_ReturnProductManagement.ReadOnly = true;
            this.dgv_ReturnProductManagement.RowHeadersVisible = false;
            this.dgv_ReturnProductManagement.RowHeadersWidth = 51;
            this.dgv_ReturnProductManagement.RowTemplate.Height = 24;
            this.dgv_ReturnProductManagement.Size = new System.Drawing.Size(1134, 375);
            this.dgv_ReturnProductManagement.TabIndex = 27;
            this.dgv_ReturnProductManagement.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ReturnProductManagement.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_ReturnProductManagement.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_ReturnProductManagement.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_ReturnProductManagement.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_ReturnProductManagement.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_ReturnProductManagement.ThemeStyle.GridColor = System.Drawing.Color.SeaShell;
            this.dgv_ReturnProductManagement.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_ReturnProductManagement.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_ReturnProductManagement.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ReturnProductManagement.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_ReturnProductManagement.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ReturnProductManagement.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_ReturnProductManagement.ThemeStyle.ReadOnly = true;
            this.dgv_ReturnProductManagement.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ReturnProductManagement.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_ReturnProductManagement.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ReturnProductManagement.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ReturnProductManagement.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_ReturnProductManagement.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ReturnProductManagement.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ReturnProductManagement.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ReturnProductManagement_CellClick);
            // 
            // ReturnProductID
            // 
            this.ReturnProductID.HeaderText = "Mã phiếu trả hàng";
            this.ReturnProductID.MinimumWidth = 6;
            this.ReturnProductID.Name = "ReturnProductID";
            this.ReturnProductID.ReadOnly = true;
            // 
            // ProductID
            // 
            this.ProductID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProductID.HeaderText = "Mã sản phẩm";
            this.ProductID.MinimumWidth = 6;
            this.ProductID.Name = "ProductID";
            this.ProductID.ReadOnly = true;
            // 
            // ProductName
            // 
            this.ProductName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProductName.HeaderText = "Tên sản phẩm";
            this.ProductName.MinimumWidth = 6;
            this.ProductName.Name = "ProductName";
            this.ProductName.ReadOnly = true;
            // 
            // TypeProductName
            // 
            this.TypeProductName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TypeProductName.HeaderText = "Loại";
            this.TypeProductName.MinimumWidth = 6;
            this.TypeProductName.Name = "TypeProductName";
            this.TypeProductName.ReadOnly = true;
            // 
            // ProductPrice
            // 
            this.ProductPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProductPrice.HeaderText = "Giá mua";
            this.ProductPrice.MinimumWidth = 6;
            this.ProductPrice.Name = "ProductPrice";
            this.ProductPrice.ReadOnly = true;
            // 
            // CalculationUnit
            // 
            this.CalculationUnit.HeaderText = "Đơn vị tính";
            this.CalculationUnit.MinimumWidth = 6;
            this.CalculationUnit.Name = "CalculationUnit";
            this.CalculationUnit.ReadOnly = true;
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "Số lượng";
            this.Quantity.MinimumWidth = 6;
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            // 
            // Supplier
            // 
            this.Supplier.HeaderText = "Nhà cung cấp";
            this.Supplier.MinimumWidth = 6;
            this.Supplier.Name = "Supplier";
            this.Supplier.ReadOnly = true;
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.BackColor = System.Drawing.Color.Transparent;
            this.btn_Refresh.BorderRadius = 10;
            this.btn_Refresh.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Refresh.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Refresh.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Refresh.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Refresh.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Refresh.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Refresh.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Refresh.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Refresh.ForeColor = System.Drawing.Color.White;
            this.btn_Refresh.Image = global::MiniMarketManagement.Properties.Resources.refresh;
            this.btn_Refresh.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Refresh.ImageSize = new System.Drawing.Size(17, 17);
            this.btn_Refresh.Location = new System.Drawing.Point(936, 238);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.ShadowDecoration.BorderRadius = 10;
            this.btn_Refresh.ShadowDecoration.Enabled = true;
            this.btn_Refresh.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Refresh.Size = new System.Drawing.Size(177, 36);
            this.btn_Refresh.TabIndex = 29;
            this.btn_Refresh.Text = "Làm mới";
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // btn_DeleteFilter
            // 
            this.btn_DeleteFilter.BackColor = System.Drawing.Color.Transparent;
            this.btn_DeleteFilter.BorderRadius = 10;
            this.btn_DeleteFilter.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_DeleteFilter.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_DeleteFilter.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_DeleteFilter.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_DeleteFilter.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_DeleteFilter.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_DeleteFilter.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_DeleteFilter.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DeleteFilter.ForeColor = System.Drawing.Color.White;
            this.btn_DeleteFilter.Image = ((System.Drawing.Image)(resources.GetObject("btn_DeleteFilter.Image")));
            this.btn_DeleteFilter.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_DeleteFilter.Location = new System.Drawing.Point(936, 110);
            this.btn_DeleteFilter.Name = "btn_DeleteFilter";
            this.btn_DeleteFilter.ShadowDecoration.BorderRadius = 10;
            this.btn_DeleteFilter.ShadowDecoration.Enabled = true;
            this.btn_DeleteFilter.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_DeleteFilter.Size = new System.Drawing.Size(177, 36);
            this.btn_DeleteFilter.TabIndex = 28;
            this.btn_DeleteFilter.Text = "Xóa điều kiện lọc";
            this.btn_DeleteFilter.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_DeleteFilter.Click += new System.EventHandler(this.btn_DeleteFilter_Click);
            // 
            // btn_Filter
            // 
            this.btn_Filter.Animated = true;
            this.btn_Filter.BorderColor = System.Drawing.Color.Silver;
            this.btn_Filter.BorderRadius = 15;
            this.btn_Filter.BorderThickness = 1;
            this.btn_Filter.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Filter.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Filter.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Filter.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Filter.FillColor = System.Drawing.Color.LightPink;
            this.btn_Filter.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Filter.ForeColor = System.Drawing.Color.White;
            this.btn_Filter.Image = global::MiniMarketManagement.Properties.Resources.filter__1_;
            this.btn_Filter.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Filter.Location = new System.Drawing.Point(1000, 17);
            this.btn_Filter.Name = "btn_Filter";
            this.btn_Filter.Size = new System.Drawing.Size(103, 37);
            this.btn_Filter.TabIndex = 18;
            this.btn_Filter.Text = "   Lọc";
            this.btn_Filter.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // txt_SearchProductByReturnProductCartID
            // 
            this.txt_SearchProductByReturnProductCartID.Animated = true;
            this.txt_SearchProductByReturnProductCartID.BackColor = System.Drawing.Color.Transparent;
            this.txt_SearchProductByReturnProductCartID.BorderColor = System.Drawing.Color.Silver;
            this.txt_SearchProductByReturnProductCartID.BorderRadius = 15;
            this.txt_SearchProductByReturnProductCartID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_SearchProductByReturnProductCartID.DefaultText = "";
            this.txt_SearchProductByReturnProductCartID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_SearchProductByReturnProductCartID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_SearchProductByReturnProductCartID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SearchProductByReturnProductCartID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SearchProductByReturnProductCartID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_SearchProductByReturnProductCartID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SearchProductByReturnProductCartID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchProductByReturnProductCartID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SearchProductByReturnProductCartID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_SearchProductByReturnProductCartID.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchProductByReturnProductCartID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SearchProductByReturnProductCartID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchProductByReturnProductCartID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SearchProductByReturnProductCartID.IconLeft = global::MiniMarketManagement.Properties.Resources.search;
            this.txt_SearchProductByReturnProductCartID.Location = new System.Drawing.Point(30, 17);
            this.txt_SearchProductByReturnProductCartID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_SearchProductByReturnProductCartID.Name = "txt_SearchProductByReturnProductCartID";
            this.txt_SearchProductByReturnProductCartID.PasswordChar = '\0';
            this.txt_SearchProductByReturnProductCartID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_SearchProductByReturnProductCartID.PlaceholderText = "Mã phiếu trả hàng";
            this.txt_SearchProductByReturnProductCartID.SelectedText = "";
            this.txt_SearchProductByReturnProductCartID.ShadowDecoration.BorderRadius = 15;
            this.txt_SearchProductByReturnProductCartID.ShadowDecoration.Enabled = true;
            this.txt_SearchProductByReturnProductCartID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_SearchProductByReturnProductCartID.Size = new System.Drawing.Size(279, 35);
            this.txt_SearchProductByReturnProductCartID.TabIndex = 14;
            // 
            // btn_ExportListImportProduct
            // 
            this.btn_ExportListImportProduct.BackColor = System.Drawing.Color.Transparent;
            this.btn_ExportListImportProduct.BorderRadius = 10;
            this.btn_ExportListImportProduct.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_ExportListImportProduct.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_ExportListImportProduct.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_ExportListImportProduct.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_ExportListImportProduct.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_ExportListImportProduct.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_ExportListImportProduct.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_ExportListImportProduct.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ExportListImportProduct.ForeColor = System.Drawing.Color.White;
            this.btn_ExportListImportProduct.Image = global::MiniMarketManagement.Properties.Resources.export;
            this.btn_ExportListImportProduct.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_ExportListImportProduct.ImageSize = new System.Drawing.Size(17, 17);
            this.btn_ExportListImportProduct.Location = new System.Drawing.Point(936, 174);
            this.btn_ExportListImportProduct.Name = "btn_ExportListImportProduct";
            this.btn_ExportListImportProduct.ShadowDecoration.BorderRadius = 10;
            this.btn_ExportListImportProduct.ShadowDecoration.Enabled = true;
            this.btn_ExportListImportProduct.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_ExportListImportProduct.Size = new System.Drawing.Size(177, 36);
            this.btn_ExportListImportProduct.TabIndex = 30;
            this.btn_ExportListImportProduct.Text = "Xuất danh sách";
            this.btn_ExportListImportProduct.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_ExportListImportProduct.Click += new System.EventHandler(this.btn_ExportListImportProduct_Click);
            // 
            // frm_ReturnManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.btn_ExportListImportProduct);
            this.Controls.Add(this.btn_Refresh);
            this.Controls.Add(this.btn_DeleteFilter);
            this.Controls.Add(this.dgv_ReturnProductManagement);
            this.Controls.Add(this.guna2CustomGradientPanel3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Filter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtp_toDate);
            this.Controls.Add(this.dtp_fromDate);
            this.Controls.Add(this.txt_SearchProductByReturnProductCartID);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_ReturnManagement";
            this.Text = "ReturnManagement";
            this.Load += new System.EventHandler(this.frm_ReturnManagement_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_Product)).EndInit();
            this.guna2CustomGradientPanel3.ResumeLayout(false);
            this.guna2CustomGradientPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ReturnProductManagement)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private System.Windows.Forms.GroupBox groupBox1;
        private Guna.UI2.WinForms.Guna2PictureBox pic_Product;
        private Guna.UI2.WinForms.Guna2Button btn_Filter;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2DateTimePicker dtp_toDate;
        private Guna.UI2.WinForms.Guna2DateTimePicker dtp_fromDate;
        private Guna.UI2.WinForms.Guna2TextBox txt_SearchProductByReturnProductCartID;
        private Guna.UI2.WinForms.Guna2AnimateWindow guna2AnimateWindow1;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel guna2CustomGradientPanel3;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_ReturnProductManagement;
        private Guna.UI2.WinForms.Guna2GradientButton btn_DeleteFilter;
        private System.Windows.Forms.Label lb_ReturnQuantity;
        private System.Windows.Forms.Label lb_TotalReturnMoney;
        private Guna.UI2.WinForms.Guna2GradientButton btn_Refresh;
        private Guna.UI2.WinForms.Guna2GradientButton btn_ExportListImportProduct;
        private System.Windows.Forms.DataGridViewTextBoxColumn ReturnProductID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn CalculationUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier;
    }
}
﻿namespace MiniMarketManagement.GUI.Administrator
{
    partial class frm_ImportManagement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_ImportManagement));
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.dgv_ImportProductManagement = new Guna.UI2.WinForms.Guna2DataGridView();
            this.ImportProductID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TypeProductName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProductPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CalculationUnit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Quantity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Supplier = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtp_fromDate = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.dtp_toDate = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pic_Product = new Guna.UI2.WinForms.Guna2PictureBox();
            this.guna2CustomGradientPanel3 = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.lb_TotalImportCash = new System.Windows.Forms.Label();
            this.lb_ImportQuantity = new System.Windows.Forms.Label();
            this.guna2TextBox4 = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.btn_Refresh = new Guna.UI2.WinForms.Guna2GradientButton();
            this.guna2GradientButton1 = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_Filter = new Guna.UI2.WinForms.Guna2Button();
            this.txt_SearchProductByImportProductCartID = new Guna.UI2.WinForms.Guna2TextBox();
            this.btn_ExportListImportProduct = new Guna.UI2.WinForms.Guna2GradientButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ImportProductManagement)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Product)).BeginInit();
            this.guna2CustomGradientPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // dgv_ImportProductManagement
            // 
            this.dgv_ImportProductManagement.AllowUserToAddRows = false;
            this.dgv_ImportProductManagement.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dgv_ImportProductManagement.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_ImportProductManagement.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_ImportProductManagement.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_ImportProductManagement.ColumnHeadersHeight = 18;
            this.dgv_ImportProductManagement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ImportProductManagement.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ImportProductID,
            this.ProductID,
            this.ProductName,
            this.TypeProductName,
            this.ProductPrice,
            this.CalculationUnit,
            this.Quantity,
            this.Supplier});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_ImportProductManagement.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_ImportProductManagement.GridColor = System.Drawing.Color.SeaShell;
            this.dgv_ImportProductManagement.Location = new System.Drawing.Point(0, 317);
            this.dgv_ImportProductManagement.Name = "dgv_ImportProductManagement";
            this.dgv_ImportProductManagement.ReadOnly = true;
            this.dgv_ImportProductManagement.RowHeadersVisible = false;
            this.dgv_ImportProductManagement.RowHeadersWidth = 51;
            this.dgv_ImportProductManagement.RowTemplate.Height = 24;
            this.dgv_ImportProductManagement.Size = new System.Drawing.Size(1134, 375);
            this.dgv_ImportProductManagement.TabIndex = 5;
            this.dgv_ImportProductManagement.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ImportProductManagement.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_ImportProductManagement.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_ImportProductManagement.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_ImportProductManagement.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_ImportProductManagement.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_ImportProductManagement.ThemeStyle.GridColor = System.Drawing.Color.SeaShell;
            this.dgv_ImportProductManagement.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_ImportProductManagement.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_ImportProductManagement.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ImportProductManagement.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_ImportProductManagement.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_ImportProductManagement.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_ImportProductManagement.ThemeStyle.ReadOnly = true;
            this.dgv_ImportProductManagement.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_ImportProductManagement.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_ImportProductManagement.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_ImportProductManagement.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ImportProductManagement.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_ImportProductManagement.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_ImportProductManagement.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_ImportProductManagement.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_ImportProductManagement_CellClick);
            // 
            // ImportProductID
            // 
            this.ImportProductID.HeaderText = "Mã phiếu nhập";
            this.ImportProductID.MinimumWidth = 6;
            this.ImportProductID.Name = "ImportProductID";
            this.ImportProductID.ReadOnly = true;
            // 
            // ProductID
            // 
            this.ProductID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProductID.HeaderText = "Mã sản phẩm";
            this.ProductID.MinimumWidth = 6;
            this.ProductID.Name = "ProductID";
            this.ProductID.ReadOnly = true;
            // 
            // ProductName
            // 
            this.ProductName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProductName.HeaderText = "Tên sản phẩm";
            this.ProductName.MinimumWidth = 6;
            this.ProductName.Name = "ProductName";
            this.ProductName.ReadOnly = true;
            // 
            // TypeProductName
            // 
            this.TypeProductName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.TypeProductName.HeaderText = "Loại";
            this.TypeProductName.MinimumWidth = 6;
            this.TypeProductName.Name = "TypeProductName";
            this.TypeProductName.ReadOnly = true;
            // 
            // ProductPrice
            // 
            this.ProductPrice.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ProductPrice.HeaderText = "Giá mua";
            this.ProductPrice.MinimumWidth = 6;
            this.ProductPrice.Name = "ProductPrice";
            this.ProductPrice.ReadOnly = true;
            // 
            // CalculationUnit
            // 
            this.CalculationUnit.HeaderText = "Đơn vị tính";
            this.CalculationUnit.MinimumWidth = 6;
            this.CalculationUnit.Name = "CalculationUnit";
            this.CalculationUnit.ReadOnly = true;
            // 
            // Quantity
            // 
            this.Quantity.HeaderText = "Số lượng";
            this.Quantity.MinimumWidth = 6;
            this.Quantity.Name = "Quantity";
            this.Quantity.ReadOnly = true;
            // 
            // Supplier
            // 
            this.Supplier.HeaderText = "Nhà cung cấp";
            this.Supplier.MinimumWidth = 6;
            this.Supplier.Name = "Supplier";
            this.Supplier.ReadOnly = true;
            // 
            // dtp_fromDate
            // 
            this.dtp_fromDate.Animated = true;
            this.dtp_fromDate.BackColor = System.Drawing.Color.Transparent;
            this.dtp_fromDate.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.dtp_fromDate.BorderRadius = 15;
            this.dtp_fromDate.BorderThickness = 1;
            this.dtp_fromDate.Checked = true;
            this.dtp_fromDate.FillColor = System.Drawing.Color.LightPink;
            this.dtp_fromDate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.dtp_fromDate.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.dtp_fromDate.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dtp_fromDate.Location = new System.Drawing.Point(351, 17);
            this.dtp_fromDate.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dtp_fromDate.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtp_fromDate.Name = "dtp_fromDate";
            this.dtp_fromDate.Size = new System.Drawing.Size(289, 36);
            this.dtp_fromDate.TabIndex = 8;
            this.dtp_fromDate.Value = new System.DateTime(2023, 11, 1, 0, 0, 0, 0);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(646, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 18);
            this.label1.TabIndex = 9;
            this.label1.Text = "đến";
            // 
            // dtp_toDate
            // 
            this.dtp_toDate.Animated = true;
            this.dtp_toDate.BackColor = System.Drawing.Color.Transparent;
            this.dtp_toDate.BorderColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dtp_toDate.BorderRadius = 15;
            this.dtp_toDate.BorderThickness = 1;
            this.dtp_toDate.Checked = true;
            this.dtp_toDate.FillColor = System.Drawing.Color.LightPink;
            this.dtp_toDate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.dtp_toDate.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.dtp_toDate.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.dtp_toDate.Location = new System.Drawing.Point(687, 17);
            this.dtp_toDate.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.dtp_toDate.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.dtp_toDate.Name = "dtp_toDate";
            this.dtp_toDate.Size = new System.Drawing.Size(289, 36);
            this.dtp_toDate.TabIndex = 8;
            this.dtp_toDate.Value = new System.DateTime(2023, 11, 30, 0, 0, 0, 0);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pic_Product);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(604, 83);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 212);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Mặt hàng";
            // 
            // pic_Product
            // 
            this.pic_Product.BackColor = System.Drawing.Color.Transparent;
            this.pic_Product.BorderRadius = 10;
            this.pic_Product.Image = ((System.Drawing.Image)(resources.GetObject("pic_Product.Image")));
            this.pic_Product.ImageRotate = 0F;
            this.pic_Product.Location = new System.Drawing.Point(6, 23);
            this.pic_Product.Name = "pic_Product";
            this.pic_Product.Size = new System.Drawing.Size(293, 183);
            this.pic_Product.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic_Product.TabIndex = 17;
            this.pic_Product.TabStop = false;
            this.pic_Product.UseTransparentBackground = true;
            // 
            // guna2CustomGradientPanel3
            // 
            this.guna2CustomGradientPanel3.BackColor = System.Drawing.Color.Snow;
            this.guna2CustomGradientPanel3.BorderColor = System.Drawing.Color.LightPink;
            this.guna2CustomGradientPanel3.BorderRadius = 25;
            this.guna2CustomGradientPanel3.BorderThickness = 2;
            this.guna2CustomGradientPanel3.Controls.Add(this.lb_TotalImportCash);
            this.guna2CustomGradientPanel3.Controls.Add(this.lb_ImportQuantity);
            this.guna2CustomGradientPanel3.Controls.Add(this.guna2TextBox4);
            this.guna2CustomGradientPanel3.Controls.Add(this.label4);
            this.guna2CustomGradientPanel3.Controls.Add(this.label5);
            this.guna2CustomGradientPanel3.Location = new System.Drawing.Point(50, 83);
            this.guna2CustomGradientPanel3.Name = "guna2CustomGradientPanel3";
            this.guna2CustomGradientPanel3.Size = new System.Drawing.Size(503, 212);
            this.guna2CustomGradientPanel3.TabIndex = 25;
            // 
            // lb_TotalImportCash
            // 
            this.lb_TotalImportCash.AutoSize = true;
            this.lb_TotalImportCash.BackColor = System.Drawing.Color.White;
            this.lb_TotalImportCash.Font = new System.Drawing.Font("Tahoma", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_TotalImportCash.Location = new System.Drawing.Point(291, 65);
            this.lb_TotalImportCash.Name = "lb_TotalImportCash";
            this.lb_TotalImportCash.Size = new System.Drawing.Size(55, 57);
            this.lb_TotalImportCash.TabIndex = 2;
            this.lb_TotalImportCash.Text = "0";
            this.lb_TotalImportCash.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lb_ImportQuantity
            // 
            this.lb_ImportQuantity.AutoSize = true;
            this.lb_ImportQuantity.BackColor = System.Drawing.Color.White;
            this.lb_ImportQuantity.Font = new System.Drawing.Font("Tahoma", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_ImportQuantity.Location = new System.Drawing.Point(84, 65);
            this.lb_ImportQuantity.Name = "lb_ImportQuantity";
            this.lb_ImportQuantity.Size = new System.Drawing.Size(55, 57);
            this.lb_ImportQuantity.TabIndex = 2;
            this.lb_ImportQuantity.Text = "0";
            this.lb_ImportQuantity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // guna2TextBox4
            // 
            this.guna2TextBox4.Animated = true;
            this.guna2TextBox4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox4.DefaultText = "";
            this.guna2TextBox4.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.Enabled = false;
            this.guna2TextBox4.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.guna2TextBox4.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Location = new System.Drawing.Point(936, 65);
            this.guna2TextBox4.Margin = new System.Windows.Forms.Padding(10, 14, 10, 14);
            this.guna2TextBox4.Name = "guna2TextBox4";
            this.guna2TextBox4.PasswordChar = '\0';
            this.guna2TextBox4.PlaceholderForeColor = System.Drawing.Color.White;
            this.guna2TextBox4.PlaceholderText = "";
            this.guna2TextBox4.SelectedText = "";
            this.guna2TextBox4.Size = new System.Drawing.Size(515, 326);
            this.guna2TextBox4.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(318, 175);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Tổng tiền hàng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label5.Location = new System.Drawing.Point(61, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(150, 16);
            this.label5.TabIndex = 0;
            this.label5.Text = "Số lượng hàng nhập";
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            // 
            // btn_Refresh
            // 
            this.btn_Refresh.BackColor = System.Drawing.Color.Transparent;
            this.btn_Refresh.BorderRadius = 10;
            this.btn_Refresh.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Refresh.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Refresh.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Refresh.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Refresh.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Refresh.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Refresh.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Refresh.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Refresh.ForeColor = System.Drawing.Color.White;
            this.btn_Refresh.Image = global::MiniMarketManagement.Properties.Resources.refresh;
            this.btn_Refresh.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Refresh.ImageSize = new System.Drawing.Size(17, 17);
            this.btn_Refresh.Location = new System.Drawing.Point(935, 238);
            this.btn_Refresh.Name = "btn_Refresh";
            this.btn_Refresh.ShadowDecoration.BorderRadius = 10;
            this.btn_Refresh.ShadowDecoration.Enabled = true;
            this.btn_Refresh.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Refresh.Size = new System.Drawing.Size(177, 36);
            this.btn_Refresh.TabIndex = 24;
            this.btn_Refresh.Text = "Làm mới";
            this.btn_Refresh.Click += new System.EventHandler(this.btn_Refresh_Click);
            // 
            // guna2GradientButton1
            // 
            this.guna2GradientButton1.BackColor = System.Drawing.Color.Transparent;
            this.guna2GradientButton1.BorderRadius = 10;
            this.guna2GradientButton1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2GradientButton1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton1.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2GradientButton1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2GradientButton1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.guna2GradientButton1.FillColor2 = System.Drawing.Color.Tomato;
            this.guna2GradientButton1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2GradientButton1.ForeColor = System.Drawing.Color.White;
            this.guna2GradientButton1.Image = ((System.Drawing.Image)(resources.GetObject("guna2GradientButton1.Image")));
            this.guna2GradientButton1.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2GradientButton1.Location = new System.Drawing.Point(935, 106);
            this.guna2GradientButton1.Name = "guna2GradientButton1";
            this.guna2GradientButton1.ShadowDecoration.BorderRadius = 10;
            this.guna2GradientButton1.ShadowDecoration.Enabled = true;
            this.guna2GradientButton1.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.guna2GradientButton1.Size = new System.Drawing.Size(177, 36);
            this.guna2GradientButton1.TabIndex = 24;
            this.guna2GradientButton1.Text = "Xóa điều kiện lọc";
            this.guna2GradientButton1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.guna2GradientButton1.Click += new System.EventHandler(this.btn_DeleteFilter_Click);
            // 
            // btn_Filter
            // 
            this.btn_Filter.Animated = true;
            this.btn_Filter.BorderColor = System.Drawing.Color.Silver;
            this.btn_Filter.BorderRadius = 15;
            this.btn_Filter.BorderThickness = 1;
            this.btn_Filter.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Filter.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Filter.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Filter.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Filter.FillColor = System.Drawing.Color.LightPink;
            this.btn_Filter.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Filter.ForeColor = System.Drawing.Color.White;
            this.btn_Filter.Image = global::MiniMarketManagement.Properties.Resources.filter__1_;
            this.btn_Filter.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Filter.ImageSize = new System.Drawing.Size(18, 18);
            this.btn_Filter.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btn_Filter.Location = new System.Drawing.Point(999, 17);
            this.btn_Filter.Name = "btn_Filter";
            this.btn_Filter.Size = new System.Drawing.Size(103, 37);
            this.btn_Filter.TabIndex = 10;
            this.btn_Filter.Text = "   Lọc";
            this.btn_Filter.Click += new System.EventHandler(this.btn_Filter_Click);
            // 
            // txt_SearchProductByImportProductCartID
            // 
            this.txt_SearchProductByImportProductCartID.Animated = true;
            this.txt_SearchProductByImportProductCartID.BackColor = System.Drawing.Color.Transparent;
            this.txt_SearchProductByImportProductCartID.BorderColor = System.Drawing.Color.Silver;
            this.txt_SearchProductByImportProductCartID.BorderRadius = 15;
            this.txt_SearchProductByImportProductCartID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_SearchProductByImportProductCartID.DefaultText = "";
            this.txt_SearchProductByImportProductCartID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_SearchProductByImportProductCartID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_SearchProductByImportProductCartID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SearchProductByImportProductCartID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SearchProductByImportProductCartID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_SearchProductByImportProductCartID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SearchProductByImportProductCartID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchProductByImportProductCartID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SearchProductByImportProductCartID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_SearchProductByImportProductCartID.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchProductByImportProductCartID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SearchProductByImportProductCartID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchProductByImportProductCartID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SearchProductByImportProductCartID.IconLeft = global::MiniMarketManagement.Properties.Resources.search;
            this.txt_SearchProductByImportProductCartID.Location = new System.Drawing.Point(29, 17);
            this.txt_SearchProductByImportProductCartID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_SearchProductByImportProductCartID.Name = "txt_SearchProductByImportProductCartID";
            this.txt_SearchProductByImportProductCartID.PasswordChar = '\0';
            this.txt_SearchProductByImportProductCartID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_SearchProductByImportProductCartID.PlaceholderText = "Mã phiếu nhập hàng\r\n";
            this.txt_SearchProductByImportProductCartID.SelectedText = "";
            this.txt_SearchProductByImportProductCartID.ShadowDecoration.BorderRadius = 15;
            this.txt_SearchProductByImportProductCartID.ShadowDecoration.Enabled = true;
            this.txt_SearchProductByImportProductCartID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_SearchProductByImportProductCartID.Size = new System.Drawing.Size(279, 35);
            this.txt_SearchProductByImportProductCartID.TabIndex = 7;
            // 
            // btn_ExportListImportProduct
            // 
            this.btn_ExportListImportProduct.BackColor = System.Drawing.Color.Transparent;
            this.btn_ExportListImportProduct.BorderRadius = 10;
            this.btn_ExportListImportProduct.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_ExportListImportProduct.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_ExportListImportProduct.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_ExportListImportProduct.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_ExportListImportProduct.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_ExportListImportProduct.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_ExportListImportProduct.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_ExportListImportProduct.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ExportListImportProduct.ForeColor = System.Drawing.Color.White;
            this.btn_ExportListImportProduct.Image = global::MiniMarketManagement.Properties.Resources.export;
            this.btn_ExportListImportProduct.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_ExportListImportProduct.ImageSize = new System.Drawing.Size(17, 17);
            this.btn_ExportListImportProduct.Location = new System.Drawing.Point(935, 172);
            this.btn_ExportListImportProduct.Name = "btn_ExportListImportProduct";
            this.btn_ExportListImportProduct.ShadowDecoration.BorderRadius = 10;
            this.btn_ExportListImportProduct.ShadowDecoration.Enabled = true;
            this.btn_ExportListImportProduct.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_ExportListImportProduct.Size = new System.Drawing.Size(177, 36);
            this.btn_ExportListImportProduct.TabIndex = 31;
            this.btn_ExportListImportProduct.Text = "Xuất danh sách";
            this.btn_ExportListImportProduct.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_ExportListImportProduct.Click += new System.EventHandler(this.btn_ExportListImportProduct_Click);
            // 
            // frm_ImportManagement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.btn_ExportListImportProduct);
            this.Controls.Add(this.guna2CustomGradientPanel3);
            this.Controls.Add(this.btn_Refresh);
            this.Controls.Add(this.guna2GradientButton1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_Filter);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dtp_toDate);
            this.Controls.Add(this.dtp_fromDate);
            this.Controls.Add(this.txt_SearchProductByImportProductCartID);
            this.Controls.Add(this.dgv_ImportProductManagement);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_ImportManagement";
            this.Text = "ImportManagement";
            this.Load += new System.EventHandler(this.frm_ImportManagement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ImportProductManagement)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_Product)).EndInit();
            this.guna2CustomGradientPanel3.ResumeLayout(false);
            this.guna2CustomGradientPanel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_ImportProductManagement;
        private Guna.UI2.WinForms.Guna2TextBox txt_SearchProductByImportProductCartID;
        private Guna.UI2.WinForms.Guna2DateTimePicker dtp_fromDate;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2DateTimePicker dtp_toDate;
        private Guna.UI2.WinForms.Guna2Button btn_Filter;
        private System.Windows.Forms.GroupBox groupBox1;
        private Guna.UI2.WinForms.Guna2PictureBox pic_Product;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel guna2CustomGradientPanel3;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Label lb_ImportQuantity;
        private System.Windows.Forms.Label lb_TotalImportCash;
        private Guna.UI2.WinForms.Guna2GradientButton guna2GradientButton1;
        private Guna.UI2.WinForms.Guna2GradientButton btn_Refresh;
        private Guna.UI2.WinForms.Guna2GradientButton btn_ExportListImportProduct;
        private System.Windows.Forms.DataGridViewTextBoxColumn ImportProductID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn TypeProductName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProductPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn CalculationUnit;
        private System.Windows.Forms.DataGridViewTextBoxColumn Quantity;
        private System.Windows.Forms.DataGridViewTextBoxColumn Supplier;
    }
}
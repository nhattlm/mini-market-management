﻿namespace MiniMarketManagement.GUI.Administrator
{
    partial class frm_EmployeeManage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.dgv_Employee = new Guna.UI2.WinForms.Guna2DataGridView();
            this.dgv_EmployeeID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_EmployeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_EmployeePhone = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_EmployeeBirth = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_PositionName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.Pic_Employee = new Guna.UI2.WinForms.Guna2PictureBox();
            this.btn_Location = new Guna.UI2.WinForms.Guna2GradientButton();
            this.cmb_Position = new Guna.UI2.WinForms.Guna2ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_EmployeeBirth = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_EmployeePhone = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_EmployeeName = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_Search = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_EmployeeID = new Guna.UI2.WinForms.Guna2TextBox();
            this.btn_DeleteEmployee = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_AddOrUpdateEmployee = new Guna.UI2.WinForms.Guna2GradientButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Employee)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic_Employee)).BeginInit();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // dgv_Employee
            // 
            this.dgv_Employee.AllowUserToAddRows = false;
            this.dgv_Employee.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dgv_Employee.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_Employee.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_Employee.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_Employee.ColumnHeadersHeight = 18;
            this.dgv_Employee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_Employee.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgv_EmployeeID,
            this.dgv_EmployeeName,
            this.dgv_EmployeePhone,
            this.dgv_EmployeeBirth,
            this.dgv_PositionName});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_Employee.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_Employee.GridColor = System.Drawing.Color.Silver;
            this.dgv_Employee.Location = new System.Drawing.Point(501, 53);
            this.dgv_Employee.Name = "dgv_Employee";
            this.dgv_Employee.ReadOnly = true;
            this.dgv_Employee.RowHeadersVisible = false;
            this.dgv_Employee.RowHeadersWidth = 51;
            this.dgv_Employee.RowTemplate.Height = 24;
            this.dgv_Employee.Size = new System.Drawing.Size(621, 626);
            this.dgv_Employee.TabIndex = 0;
            this.dgv_Employee.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_Employee.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_Employee.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_Employee.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_Employee.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_Employee.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_Employee.ThemeStyle.GridColor = System.Drawing.Color.Silver;
            this.dgv_Employee.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_Employee.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_Employee.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_Employee.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_Employee.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_Employee.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_Employee.ThemeStyle.ReadOnly = true;
            this.dgv_Employee.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_Employee.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_Employee.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_Employee.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_Employee.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_Employee.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_Employee.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_Employee.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_Employee_CellClick);
            // 
            // dgv_EmployeeID
            // 
            this.dgv_EmployeeID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_EmployeeID.HeaderText = "Mã nhân viên";
            this.dgv_EmployeeID.MinimumWidth = 6;
            this.dgv_EmployeeID.Name = "dgv_EmployeeID";
            this.dgv_EmployeeID.ReadOnly = true;
            // 
            // dgv_EmployeeName
            // 
            this.dgv_EmployeeName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_EmployeeName.HeaderText = "Tên nhân viên";
            this.dgv_EmployeeName.MinimumWidth = 6;
            this.dgv_EmployeeName.Name = "dgv_EmployeeName";
            this.dgv_EmployeeName.ReadOnly = true;
            // 
            // dgv_EmployeePhone
            // 
            this.dgv_EmployeePhone.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_EmployeePhone.HeaderText = "Số điện thoại";
            this.dgv_EmployeePhone.MinimumWidth = 6;
            this.dgv_EmployeePhone.Name = "dgv_EmployeePhone";
            this.dgv_EmployeePhone.ReadOnly = true;
            // 
            // dgv_EmployeeBirth
            // 
            this.dgv_EmployeeBirth.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_EmployeeBirth.HeaderText = "Ngày sinh";
            this.dgv_EmployeeBirth.MinimumWidth = 6;
            this.dgv_EmployeeBirth.Name = "dgv_EmployeeBirth";
            this.dgv_EmployeeBirth.ReadOnly = true;
            // 
            // dgv_PositionName
            // 
            this.dgv_PositionName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dgv_PositionName.HeaderText = "Tên chức vụ";
            this.dgv_PositionName.MinimumWidth = 6;
            this.dgv_PositionName.Name = "dgv_PositionName";
            this.dgv_PositionName.ReadOnly = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(664, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(307, 34);
            this.label1.TabIndex = 0;
            this.label1.Text = "Danh sách nhân viên";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.guna2PictureBox1);
            this.groupBox1.Controls.Add(this.Pic_Employee);
            this.groupBox1.Controls.Add(this.btn_Location);
            this.groupBox1.Controls.Add(this.cmb_Position);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txt_EmployeeBirth);
            this.groupBox1.Controls.Add(this.txt_EmployeePhone);
            this.groupBox1.Controls.Add(this.txt_EmployeeName);
            this.groupBox1.Controls.Add(this.txt_Search);
            this.groupBox1.Controls.Add(this.txt_EmployeeID);
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(8, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(487, 616);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Thông tin nhân viên";
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.FillColor = System.Drawing.Color.Transparent;
            this.guna2PictureBox1.Image = global::MiniMarketManagement.Properties.Resources.laptop;
            this.guna2PictureBox1.ImageRotate = 0F;
            this.guna2PictureBox1.Location = new System.Drawing.Point(6, 420);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.Size = new System.Drawing.Size(140, 150);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.guna2PictureBox1.TabIndex = 26;
            this.guna2PictureBox1.TabStop = false;
            // 
            // Pic_Employee
            // 
            this.Pic_Employee.BorderRadius = 5;
            this.Pic_Employee.Image = global::MiniMarketManagement.Properties.Resources.image__2_2;
            this.Pic_Employee.ImageRotate = 0F;
            this.Pic_Employee.Location = new System.Drawing.Point(212, 378);
            this.Pic_Employee.Name = "Pic_Employee";
            this.Pic_Employee.Size = new System.Drawing.Size(263, 232);
            this.Pic_Employee.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Pic_Employee.TabIndex = 25;
            this.Pic_Employee.TabStop = false;
            // 
            // btn_Location
            // 
            this.btn_Location.BackColor = System.Drawing.Color.Transparent;
            this.btn_Location.BorderRadius = 10;
            this.btn_Location.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Location.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Location.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Location.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Location.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Location.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Location.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Location.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Location.ForeColor = System.Drawing.Color.White;
            this.btn_Location.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Location.Location = new System.Drawing.Point(157, 484);
            this.btn_Location.Name = "btn_Location";
            this.btn_Location.ShadowDecoration.BorderRadius = 10;
            this.btn_Location.ShadowDecoration.Enabled = true;
            this.btn_Location.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Location.Size = new System.Drawing.Size(49, 31);
            this.btn_Location.TabIndex = 0;
            this.btn_Location.Text = "...";
            this.btn_Location.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_Location.TextOffset = new System.Drawing.Point(0, -7);
            this.btn_Location.Click += new System.EventHandler(this.btn_Location_Click);
            // 
            // cmb_Position
            // 
            this.cmb_Position.BackColor = System.Drawing.Color.Transparent;
            this.cmb_Position.BorderColor = System.Drawing.Color.LightGray;
            this.cmb_Position.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.cmb_Position.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_Position.FillColor = System.Drawing.Color.WhiteSmoke;
            this.cmb_Position.FocusedColor = System.Drawing.Color.Tomato;
            this.cmb_Position.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.cmb_Position.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.cmb_Position.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cmb_Position.ForeColor = System.Drawing.Color.Black;
            this.cmb_Position.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.cmb_Position.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.cmb_Position.ItemHeight = 30;
            this.cmb_Position.Location = new System.Drawing.Point(212, 336);
            this.cmb_Position.Name = "cmb_Position";
            this.cmb_Position.Size = new System.Drawing.Size(263, 36);
            this.cmb_Position.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(36, 340);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 24);
            this.label6.TabIndex = 0;
            this.label6.Text = "Chức vụ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(36, 287);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 24);
            this.label5.TabIndex = 0;
            this.label5.Text = "Ngày sinh";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 229);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 24);
            this.label4.TabIndex = 0;
            this.label4.Text = "Số điện thoại";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(36, 174);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(153, 24);
            this.label3.TabIndex = 0;
            this.label3.Text = "Tên nhân viên";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(36, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 24);
            this.label7.TabIndex = 0;
            this.label7.Text = "Tìm kiếm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(36, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(146, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã nhân viên";
            // 
            // txt_EmployeeBirth
            // 
            this.txt_EmployeeBirth.Animated = true;
            this.txt_EmployeeBirth.BackColor = System.Drawing.Color.Transparent;
            this.txt_EmployeeBirth.BorderColor = System.Drawing.Color.LightGray;
            this.txt_EmployeeBirth.BorderRadius = 15;
            this.txt_EmployeeBirth.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_EmployeeBirth.DefaultText = "";
            this.txt_EmployeeBirth.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_EmployeeBirth.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_EmployeeBirth.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_EmployeeBirth.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_EmployeeBirth.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_EmployeeBirth.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_EmployeeBirth.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_EmployeeBirth.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_EmployeeBirth.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_EmployeeBirth.ForeColor = System.Drawing.Color.Black;
            this.txt_EmployeeBirth.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_EmployeeBirth.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_EmployeeBirth.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_EmployeeBirth.IconLeft = global::MiniMarketManagement.Properties.Resources.calendar;
            this.txt_EmployeeBirth.Location = new System.Drawing.Point(212, 279);
            this.txt_EmployeeBirth.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_EmployeeBirth.Name = "txt_EmployeeBirth";
            this.txt_EmployeeBirth.PasswordChar = '\0';
            this.txt_EmployeeBirth.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_EmployeeBirth.PlaceholderText = "DD-MM-YYYY";
            this.txt_EmployeeBirth.SelectedText = "";
            this.txt_EmployeeBirth.ShadowDecoration.BorderRadius = 15;
            this.txt_EmployeeBirth.ShadowDecoration.Enabled = true;
            this.txt_EmployeeBirth.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_EmployeeBirth.Size = new System.Drawing.Size(263, 37);
            this.txt_EmployeeBirth.TabIndex = 4;
            // 
            // txt_EmployeePhone
            // 
            this.txt_EmployeePhone.Animated = true;
            this.txt_EmployeePhone.BackColor = System.Drawing.Color.Transparent;
            this.txt_EmployeePhone.BorderColor = System.Drawing.Color.LightGray;
            this.txt_EmployeePhone.BorderRadius = 15;
            this.txt_EmployeePhone.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_EmployeePhone.DefaultText = "";
            this.txt_EmployeePhone.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_EmployeePhone.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_EmployeePhone.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_EmployeePhone.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_EmployeePhone.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_EmployeePhone.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_EmployeePhone.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_EmployeePhone.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_EmployeePhone.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_EmployeePhone.ForeColor = System.Drawing.Color.Black;
            this.txt_EmployeePhone.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_EmployeePhone.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_EmployeePhone.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_EmployeePhone.IconLeft = global::MiniMarketManagement.Properties.Resources.call;
            this.txt_EmployeePhone.Location = new System.Drawing.Point(212, 222);
            this.txt_EmployeePhone.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_EmployeePhone.Name = "txt_EmployeePhone";
            this.txt_EmployeePhone.PasswordChar = '\0';
            this.txt_EmployeePhone.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_EmployeePhone.PlaceholderText = "Số điện thoại";
            this.txt_EmployeePhone.SelectedText = "";
            this.txt_EmployeePhone.ShadowDecoration.BorderRadius = 15;
            this.txt_EmployeePhone.ShadowDecoration.Enabled = true;
            this.txt_EmployeePhone.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_EmployeePhone.Size = new System.Drawing.Size(263, 37);
            this.txt_EmployeePhone.TabIndex = 3;
            // 
            // txt_EmployeeName
            // 
            this.txt_EmployeeName.Animated = true;
            this.txt_EmployeeName.BackColor = System.Drawing.Color.Transparent;
            this.txt_EmployeeName.BorderColor = System.Drawing.Color.LightGray;
            this.txt_EmployeeName.BorderRadius = 15;
            this.txt_EmployeeName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_EmployeeName.DefaultText = "";
            this.txt_EmployeeName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_EmployeeName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_EmployeeName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_EmployeeName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_EmployeeName.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_EmployeeName.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_EmployeeName.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_EmployeeName.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_EmployeeName.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_EmployeeName.ForeColor = System.Drawing.Color.Black;
            this.txt_EmployeeName.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_EmployeeName.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_EmployeeName.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_EmployeeName.IconLeft = global::MiniMarketManagement.Properties.Resources.user__1_1;
            this.txt_EmployeeName.Location = new System.Drawing.Point(212, 165);
            this.txt_EmployeeName.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_EmployeeName.Name = "txt_EmployeeName";
            this.txt_EmployeeName.PasswordChar = '\0';
            this.txt_EmployeeName.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_EmployeeName.PlaceholderText = "Tên nhân viên";
            this.txt_EmployeeName.SelectedText = "";
            this.txt_EmployeeName.ShadowDecoration.BorderRadius = 15;
            this.txt_EmployeeName.ShadowDecoration.Enabled = true;
            this.txt_EmployeeName.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_EmployeeName.Size = new System.Drawing.Size(263, 37);
            this.txt_EmployeeName.TabIndex = 2;
            // 
            // txt_Search
            // 
            this.txt_Search.Animated = true;
            this.txt_Search.BackColor = System.Drawing.Color.Transparent;
            this.txt_Search.BorderColor = System.Drawing.Color.LightGray;
            this.txt_Search.BorderRadius = 15;
            this.txt_Search.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_Search.DefaultText = "";
            this.txt_Search.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_Search.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_Search.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_Search.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_Search.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_Search.ForeColor = System.Drawing.Color.Black;
            this.txt_Search.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_Search.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_Search.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_Search.IconLeft = global::MiniMarketManagement.Properties.Resources.search2;
            this.txt_Search.Location = new System.Drawing.Point(212, 51);
            this.txt_Search.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_Search.Name = "txt_Search";
            this.txt_Search.PasswordChar = '\0';
            this.txt_Search.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_Search.PlaceholderText = "Tìm kiếm";
            this.txt_Search.SelectedText = "";
            this.txt_Search.ShadowDecoration.BorderRadius = 15;
            this.txt_Search.ShadowDecoration.Enabled = true;
            this.txt_Search.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_Search.Size = new System.Drawing.Size(263, 37);
            this.txt_Search.TabIndex = 1;
            this.txt_Search.TextChanged += new System.EventHandler(this.txt_Search_TextChanged);
            // 
            // txt_EmployeeID
            // 
            this.txt_EmployeeID.Animated = true;
            this.txt_EmployeeID.BackColor = System.Drawing.Color.Transparent;
            this.txt_EmployeeID.BorderColor = System.Drawing.Color.LightGray;
            this.txt_EmployeeID.BorderRadius = 15;
            this.txt_EmployeeID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_EmployeeID.DefaultText = "";
            this.txt_EmployeeID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_EmployeeID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_EmployeeID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_EmployeeID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_EmployeeID.Enabled = false;
            this.txt_EmployeeID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_EmployeeID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_EmployeeID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_EmployeeID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_EmployeeID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_EmployeeID.ForeColor = System.Drawing.Color.Black;
            this.txt_EmployeeID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_EmployeeID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_EmployeeID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_EmployeeID.IconLeft = global::MiniMarketManagement.Properties.Resources.id__1_5;
            this.txt_EmployeeID.Location = new System.Drawing.Point(212, 108);
            this.txt_EmployeeID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_EmployeeID.Name = "txt_EmployeeID";
            this.txt_EmployeeID.PasswordChar = '\0';
            this.txt_EmployeeID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_EmployeeID.PlaceholderText = "Mã nhân viên";
            this.txt_EmployeeID.SelectedText = "";
            this.txt_EmployeeID.ShadowDecoration.BorderRadius = 15;
            this.txt_EmployeeID.ShadowDecoration.Enabled = true;
            this.txt_EmployeeID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_EmployeeID.Size = new System.Drawing.Size(263, 37);
            this.txt_EmployeeID.TabIndex = 1;
            // 
            // btn_DeleteEmployee
            // 
            this.btn_DeleteEmployee.BackColor = System.Drawing.Color.Transparent;
            this.btn_DeleteEmployee.BorderRadius = 10;
            this.btn_DeleteEmployee.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_DeleteEmployee.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_DeleteEmployee.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_DeleteEmployee.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_DeleteEmployee.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_DeleteEmployee.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_DeleteEmployee.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_DeleteEmployee.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btn_DeleteEmployee.ForeColor = System.Drawing.Color.White;
            this.btn_DeleteEmployee.Image = global::MiniMarketManagement.Properties.Resources.user2;
            this.btn_DeleteEmployee.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_DeleteEmployee.Location = new System.Drawing.Point(277, 631);
            this.btn_DeleteEmployee.Name = "btn_DeleteEmployee";
            this.btn_DeleteEmployee.ShadowDecoration.BorderRadius = 10;
            this.btn_DeleteEmployee.ShadowDecoration.Enabled = true;
            this.btn_DeleteEmployee.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_DeleteEmployee.Size = new System.Drawing.Size(90, 48);
            this.btn_DeleteEmployee.TabIndex = 0;
            this.btn_DeleteEmployee.Text = "Xóa";
            this.btn_DeleteEmployee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_DeleteEmployee.Click += new System.EventHandler(this.btn_DeleteEmployee_Click);
            // 
            // btn_AddOrUpdateEmployee
            // 
            this.btn_AddOrUpdateEmployee.BackColor = System.Drawing.Color.Transparent;
            this.btn_AddOrUpdateEmployee.BorderRadius = 10;
            this.btn_AddOrUpdateEmployee.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_AddOrUpdateEmployee.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_AddOrUpdateEmployee.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_AddOrUpdateEmployee.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_AddOrUpdateEmployee.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_AddOrUpdateEmployee.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_AddOrUpdateEmployee.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_AddOrUpdateEmployee.Font = new System.Drawing.Font("Tahoma", 12F);
            this.btn_AddOrUpdateEmployee.ForeColor = System.Drawing.Color.White;
            this.btn_AddOrUpdateEmployee.Image = global::MiniMarketManagement.Properties.Resources.add_friend1;
            this.btn_AddOrUpdateEmployee.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_AddOrUpdateEmployee.Location = new System.Drawing.Point(52, 631);
            this.btn_AddOrUpdateEmployee.Name = "btn_AddOrUpdateEmployee";
            this.btn_AddOrUpdateEmployee.ShadowDecoration.BorderRadius = 10;
            this.btn_AddOrUpdateEmployee.ShadowDecoration.Enabled = true;
            this.btn_AddOrUpdateEmployee.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_AddOrUpdateEmployee.Size = new System.Drawing.Size(147, 48);
            this.btn_AddOrUpdateEmployee.TabIndex = 0;
            this.btn_AddOrUpdateEmployee.Text = "Thêm/Sửa";
            this.btn_AddOrUpdateEmployee.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.btn_AddOrUpdateEmployee.Click += new System.EventHandler(this.btn_AddOrUpdateEmployee_Click);
            // 
            // frm_EmployeeManage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btn_DeleteEmployee);
            this.Controls.Add(this.btn_AddOrUpdateEmployee);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv_Employee);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_EmployeeManage";
            this.Text = "frrm_EmployeeManage";
            this.Load += new System.EventHandler(this.frm_EmployeeManage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_Employee)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Pic_Employee)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_Employee;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_EmployeeID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_EmployeeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_EmployeePhone;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_EmployeeBirth;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgv_PositionName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2TextBox txt_EmployeeID;
        private Guna.UI2.WinForms.Guna2GradientButton btn_AddOrUpdateEmployee;
        private Guna.UI2.WinForms.Guna2ComboBox cmb_Position;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2TextBox txt_EmployeeBirth;
        private Guna.UI2.WinForms.Guna2TextBox txt_EmployeePhone;
        private Guna.UI2.WinForms.Guna2TextBox txt_EmployeeName;
        private Guna.UI2.WinForms.Guna2PictureBox Pic_Employee;
        private Guna.UI2.WinForms.Guna2GradientButton btn_Location;
        private System.Windows.Forms.Label label7;
        private Guna.UI2.WinForms.Guna2TextBox txt_Search;
        private Guna.UI2.WinForms.Guna2GradientButton btn_DeleteEmployee;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
    }
}
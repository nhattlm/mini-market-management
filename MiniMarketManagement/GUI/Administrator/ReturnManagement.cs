﻿using MiniMarketManagement.BLL.Administrator;
using MiniMarketManagement.DAL.Administrator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.GUI.Administrator
{
    public partial class frm_ReturnManagement : Form
    {
        public frm_ReturnManagement()
        {
            InitializeComponent();
        }
        private void frm_ReturnManagement_Load(object sender, EventArgs e)
        {
            BLL_ReturnManagement bLL_ReturnManagement = new BLL_ReturnManagement();
            bLL_ReturnManagement.ShowDetailReturnProduct(dgv_ReturnProductManagement);
            lb_ReturnQuantity.Text = new BLL_ReturnManagement().LoadDefaultReturnQuantity().ToString();
            lb_TotalReturnMoney.Text = BLL_ReturnManagement.ConvertMoneyToString(new BLL_ReturnManagement().LoadDeafaultReturnProductMoney());
        }
        private void lb_ReturnQuantity_TextChanged(object sender, EventArgs e)
        {
            lb_ReturnQuantity.Text = new BLL_ReturnManagement().ReturnQuantity(txt_SearchProductByReturnProductCartID.Text, dtp_fromDate, dtp_toDate).ToString();
        }

        private void lb_TotalReturnMoney_TextChanged(object sender, EventArgs e)
        {
            lb_TotalReturnMoney.Text = BLL_ReturnManagement.ConvertMoneyToString(new BLL_ReturnManagement().ReturnProductMoney(txt_SearchProductByReturnProductCartID.Text, dtp_fromDate, dtp_toDate));
        }
        private void btn_Search_Click(object sender, EventArgs e)
        {
            BLL_ReturnManagement bLLImportManagement = new BLL_ReturnManagement();
            bLLImportManagement.SearchBy(txt_SearchProductByReturnProductCartID.Text, dtp_fromDate, dtp_toDate, dgv_ReturnProductManagement);
            lb_ReturnQuantity_TextChanged(sender, e);
            lb_TotalReturnMoney_TextChanged(sender, e);
            txt_SearchProductByReturnProductCartID.Text = null;
        }
        private void dgv_ReturnProductManagement_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            var row = e.RowIndex;
            if (row < 0)
            {
                return;
            }
            Image image;
            BLL_ReturnManagement bLL_ImportManagement = new BLL_ReturnManagement();
            bLL_ImportManagement.FillPicbox(row, dgv_ReturnProductManagement, out image);
            pic_Product.Image = image;
        }

        private void setEmptyImage()
        {
            const string EmptyImPath = "Resources\\image (2).png";
            pic_Product.Image = Image.FromFile(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, EmptyImPath));
        }
        private void btn_DeleteFilter_Click(object sender, EventArgs e)
        {
            BLL_ReturnManagement bLL_ReturnManagement = new BLL_ReturnManagement();
            bLL_ReturnManagement.ShowDetailReturnProduct(dgv_ReturnProductManagement);
            lb_ReturnQuantity.Text = new BLL_ReturnManagement().LoadDefaultReturnQuantity().ToString();
            lb_TotalReturnMoney.Text = BLL_ReturnManagement.ConvertMoneyToString(new BLL_ReturnManagement().LoadDeafaultReturnProductMoney());
            setEmptyImage();
        }
        private void btn_Refresh_Click(object sender, EventArgs e)
        {
            frm_ReturnManagement_Load(sender, e);
            setEmptyImage();
        }

        private void btn_ExportListImportProduct_Click(object sender, EventArgs e)
        {
            frmRP_RPM frmRP_RPM = new frmRP_RPM();
            frmRP_RPM.ShowDialog();
        }
    }
}

﻿namespace MiniMarketManagement.GUI.Administrator
{
    partial class frm_LoyalCustomers
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_LoyalCustomers));
            this.guna2Elipse1 = new Guna.UI2.WinForms.Guna2Elipse(this.components);
            this.dgv_LoyalCustomers = new Guna.UI2.WinForms.Guna2DataGridView();
            this.CustomerID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerPhoneNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustomerPoint = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txt_CustomerPhoneNumber = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_CustomerName = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_CustomerID = new Guna.UI2.WinForms.Guna2TextBox();
            this.txt_CustomerPoint = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2CustomGradientPanel1 = new Guna.UI2.WinForms.Guna2CustomGradientPanel();
            this.lb_Customers = new System.Windows.Forms.Label();
            this.lb_LoyalCustomers = new System.Windows.Forms.Label();
            this.guna2TextBox4 = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.guna2AnimateWindow1 = new Guna.UI2.WinForms.Guna2AnimateWindow(this.components);
            this.guna2AnimateWindow2 = new Guna.UI2.WinForms.Guna2AnimateWindow(this.components);
            this.txt_SearchCustomerByName = new Guna.UI2.WinForms.Guna2TextBox();
            this.btn_Delete = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_Search = new Guna.UI2.WinForms.Guna2GradientButton();
            this.btn_Update = new Guna.UI2.WinForms.Guna2GradientButton();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_LoyalCustomers)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.guna2CustomGradientPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2Elipse1
            // 
            this.guna2Elipse1.TargetControl = this;
            // 
            // dgv_LoyalCustomers
            // 
            this.dgv_LoyalCustomers.AllowUserToAddRows = false;
            this.dgv_LoyalCustomers.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            this.dgv_LoyalCustomers.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_LoyalCustomers.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.Tomato;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_LoyalCustomers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_LoyalCustomers.ColumnHeadersHeight = 18;
            this.dgv_LoyalCustomers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_LoyalCustomers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CustomerID,
            this.CustomerName,
            this.CustomerPhoneNumber,
            this.CustomerPoint});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_LoyalCustomers.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_LoyalCustomers.GridColor = System.Drawing.Color.SeaShell;
            this.dgv_LoyalCustomers.Location = new System.Drawing.Point(519, -1);
            this.dgv_LoyalCustomers.Name = "dgv_LoyalCustomers";
            this.dgv_LoyalCustomers.ReadOnly = true;
            this.dgv_LoyalCustomers.RowHeadersVisible = false;
            this.dgv_LoyalCustomers.RowHeadersWidth = 51;
            this.dgv_LoyalCustomers.RowTemplate.Height = 24;
            this.dgv_LoyalCustomers.Size = new System.Drawing.Size(616, 692);
            this.dgv_LoyalCustomers.TabIndex = 5;
            this.dgv_LoyalCustomers.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_LoyalCustomers.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.dgv_LoyalCustomers.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgv_LoyalCustomers.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.dgv_LoyalCustomers.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.dgv_LoyalCustomers.ThemeStyle.BackColor = System.Drawing.Color.WhiteSmoke;
            this.dgv_LoyalCustomers.ThemeStyle.GridColor = System.Drawing.Color.SeaShell;
            this.dgv_LoyalCustomers.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.Tomato;
            this.dgv_LoyalCustomers.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgv_LoyalCustomers.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_LoyalCustomers.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.dgv_LoyalCustomers.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.dgv_LoyalCustomers.ThemeStyle.HeaderStyle.Height = 18;
            this.dgv_LoyalCustomers.ThemeStyle.ReadOnly = true;
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.White;
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.Height = 24;
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(231)))), ((int)(((byte)(229)))), ((int)(((byte)(255)))));
            this.dgv_LoyalCustomers.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(69)))), ((int)(((byte)(94)))));
            this.dgv_LoyalCustomers.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_LoyalCustomers_CellClick);
            // 
            // CustomerID
            // 
            this.CustomerID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CustomerID.HeaderText = "Mã khách hàng";
            this.CustomerID.MinimumWidth = 6;
            this.CustomerID.Name = "CustomerID";
            this.CustomerID.ReadOnly = true;
            // 
            // CustomerName
            // 
            this.CustomerName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CustomerName.HeaderText = "Tên khách hàng";
            this.CustomerName.MinimumWidth = 6;
            this.CustomerName.Name = "CustomerName";
            this.CustomerName.ReadOnly = true;
            // 
            // CustomerPhoneNumber
            // 
            this.CustomerPhoneNumber.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CustomerPhoneNumber.HeaderText = "Số điện thoại";
            this.CustomerPhoneNumber.MinimumWidth = 6;
            this.CustomerPhoneNumber.Name = "CustomerPhoneNumber";
            this.CustomerPhoneNumber.ReadOnly = true;
            // 
            // CustomerPoint
            // 
            this.CustomerPoint.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.CustomerPoint.HeaderText = "Điểm tích lũy";
            this.CustomerPoint.MinimumWidth = 6;
            this.CustomerPoint.Name = "CustomerPoint";
            this.CustomerPoint.ReadOnly = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.txt_CustomerPhoneNumber);
            this.groupBox2.Controls.Add(this.txt_CustomerName);
            this.groupBox2.Controls.Add(this.txt_CustomerID);
            this.groupBox2.Controls.Add(this.txt_CustomerPoint);
            this.groupBox2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(12, 215);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(489, 341);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thông tin khách hàng";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(38, 201);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(123, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Số điện thoại";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(38, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tên khách hàng";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(38, 64);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 21);
            this.label9.TabIndex = 1;
            this.label9.Text = "Mã khách hàng";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(38, 271);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(79, 21);
            this.label10.TabIndex = 1;
            this.label10.Text = "Số điểm";
            // 
            // txt_CustomerPhoneNumber
            // 
            this.txt_CustomerPhoneNumber.Animated = true;
            this.txt_CustomerPhoneNumber.BackColor = System.Drawing.Color.Transparent;
            this.txt_CustomerPhoneNumber.BorderColor = System.Drawing.Color.LightGray;
            this.txt_CustomerPhoneNumber.BorderRadius = 15;
            this.txt_CustomerPhoneNumber.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_CustomerPhoneNumber.DefaultText = "";
            this.txt_CustomerPhoneNumber.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_CustomerPhoneNumber.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_CustomerPhoneNumber.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerPhoneNumber.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerPhoneNumber.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_CustomerPhoneNumber.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerPhoneNumber.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerPhoneNumber.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerPhoneNumber.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_CustomerPhoneNumber.ForeColor = System.Drawing.Color.Black;
            this.txt_CustomerPhoneNumber.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerPhoneNumber.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerPhoneNumber.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerPhoneNumber.Location = new System.Drawing.Point(193, 193);
            this.txt_CustomerPhoneNumber.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_CustomerPhoneNumber.Name = "txt_CustomerPhoneNumber";
            this.txt_CustomerPhoneNumber.PasswordChar = '\0';
            this.txt_CustomerPhoneNumber.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_CustomerPhoneNumber.PlaceholderText = "";
            this.txt_CustomerPhoneNumber.SelectedText = "";
            this.txt_CustomerPhoneNumber.ShadowDecoration.BorderRadius = 15;
            this.txt_CustomerPhoneNumber.ShadowDecoration.Enabled = true;
            this.txt_CustomerPhoneNumber.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_CustomerPhoneNumber.Size = new System.Drawing.Size(263, 35);
            this.txt_CustomerPhoneNumber.TabIndex = 3;
            // 
            // txt_CustomerName
            // 
            this.txt_CustomerName.Animated = true;
            this.txt_CustomerName.BackColor = System.Drawing.Color.Transparent;
            this.txt_CustomerName.BorderColor = System.Drawing.Color.LightGray;
            this.txt_CustomerName.BorderRadius = 15;
            this.txt_CustomerName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_CustomerName.DefaultText = "";
            this.txt_CustomerName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_CustomerName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_CustomerName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerName.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_CustomerName.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerName.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerName.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerName.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_CustomerName.ForeColor = System.Drawing.Color.Black;
            this.txt_CustomerName.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerName.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerName.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerName.Location = new System.Drawing.Point(193, 123);
            this.txt_CustomerName.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_CustomerName.Name = "txt_CustomerName";
            this.txt_CustomerName.PasswordChar = '\0';
            this.txt_CustomerName.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_CustomerName.PlaceholderText = "";
            this.txt_CustomerName.SelectedText = "";
            this.txt_CustomerName.ShadowDecoration.BorderRadius = 15;
            this.txt_CustomerName.ShadowDecoration.Enabled = true;
            this.txt_CustomerName.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_CustomerName.Size = new System.Drawing.Size(263, 35);
            this.txt_CustomerName.TabIndex = 3;
            // 
            // txt_CustomerID
            // 
            this.txt_CustomerID.Animated = true;
            this.txt_CustomerID.BackColor = System.Drawing.Color.Transparent;
            this.txt_CustomerID.BorderColor = System.Drawing.Color.LightGray;
            this.txt_CustomerID.BorderRadius = 15;
            this.txt_CustomerID.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_CustomerID.DefaultText = "";
            this.txt_CustomerID.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_CustomerID.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_CustomerID.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerID.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerID.Enabled = false;
            this.txt_CustomerID.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_CustomerID.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerID.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerID.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerID.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_CustomerID.ForeColor = System.Drawing.Color.Black;
            this.txt_CustomerID.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerID.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerID.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerID.Location = new System.Drawing.Point(193, 56);
            this.txt_CustomerID.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_CustomerID.Name = "txt_CustomerID";
            this.txt_CustomerID.PasswordChar = '\0';
            this.txt_CustomerID.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_CustomerID.PlaceholderText = "";
            this.txt_CustomerID.SelectedText = "";
            this.txt_CustomerID.ShadowDecoration.BorderRadius = 15;
            this.txt_CustomerID.ShadowDecoration.Enabled = true;
            this.txt_CustomerID.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_CustomerID.Size = new System.Drawing.Size(263, 35);
            this.txt_CustomerID.TabIndex = 3;
            // 
            // txt_CustomerPoint
            // 
            this.txt_CustomerPoint.Animated = true;
            this.txt_CustomerPoint.BackColor = System.Drawing.Color.Transparent;
            this.txt_CustomerPoint.BorderColor = System.Drawing.Color.LightGray;
            this.txt_CustomerPoint.BorderRadius = 15;
            this.txt_CustomerPoint.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_CustomerPoint.DefaultText = "";
            this.txt_CustomerPoint.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_CustomerPoint.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_CustomerPoint.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerPoint.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_CustomerPoint.Enabled = false;
            this.txt_CustomerPoint.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_CustomerPoint.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerPoint.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerPoint.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerPoint.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_CustomerPoint.ForeColor = System.Drawing.Color.Black;
            this.txt_CustomerPoint.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_CustomerPoint.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_CustomerPoint.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_CustomerPoint.Location = new System.Drawing.Point(193, 261);
            this.txt_CustomerPoint.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_CustomerPoint.Name = "txt_CustomerPoint";
            this.txt_CustomerPoint.PasswordChar = '\0';
            this.txt_CustomerPoint.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_CustomerPoint.PlaceholderText = "";
            this.txt_CustomerPoint.ReadOnly = true;
            this.txt_CustomerPoint.SelectedText = "";
            this.txt_CustomerPoint.ShadowDecoration.BorderRadius = 15;
            this.txt_CustomerPoint.ShadowDecoration.Enabled = true;
            this.txt_CustomerPoint.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_CustomerPoint.Size = new System.Drawing.Size(167, 35);
            this.txt_CustomerPoint.TabIndex = 3;
            // 
            // guna2CustomGradientPanel1
            // 
            this.guna2CustomGradientPanel1.BackColor = System.Drawing.Color.Snow;
            this.guna2CustomGradientPanel1.BorderColor = System.Drawing.Color.LightPink;
            this.guna2CustomGradientPanel1.BorderRadius = 25;
            this.guna2CustomGradientPanel1.BorderThickness = 2;
            this.guna2CustomGradientPanel1.Controls.Add(this.lb_Customers);
            this.guna2CustomGradientPanel1.Controls.Add(this.lb_LoyalCustomers);
            this.guna2CustomGradientPanel1.Controls.Add(this.guna2TextBox4);
            this.guna2CustomGradientPanel1.Controls.Add(this.label4);
            this.guna2CustomGradientPanel1.Controls.Add(this.label3);
            this.guna2CustomGradientPanel1.FillColor = System.Drawing.Color.Snow;
            this.guna2CustomGradientPanel1.FillColor2 = System.Drawing.Color.Snow;
            this.guna2CustomGradientPanel1.FillColor3 = System.Drawing.Color.Snow;
            this.guna2CustomGradientPanel1.FillColor4 = System.Drawing.Color.Snow;
            this.guna2CustomGradientPanel1.Location = new System.Drawing.Point(30, 12);
            this.guna2CustomGradientPanel1.Name = "guna2CustomGradientPanel1";
            this.guna2CustomGradientPanel1.Size = new System.Drawing.Size(471, 183);
            this.guna2CustomGradientPanel1.TabIndex = 22;
            // 
            // lb_Customers
            // 
            this.lb_Customers.AutoSize = true;
            this.lb_Customers.Font = new System.Drawing.Font("Tahoma", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Customers.Location = new System.Drawing.Point(314, 43);
            this.lb_Customers.Name = "lb_Customers";
            this.lb_Customers.Size = new System.Drawing.Size(55, 57);
            this.lb_Customers.TabIndex = 23;
            this.lb_Customers.Text = "0";
            // 
            // lb_LoyalCustomers
            // 
            this.lb_LoyalCustomers.AutoSize = true;
            this.lb_LoyalCustomers.Font = new System.Drawing.Font("Tahoma", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_LoyalCustomers.Location = new System.Drawing.Point(88, 43);
            this.lb_LoyalCustomers.Name = "lb_LoyalCustomers";
            this.lb_LoyalCustomers.Size = new System.Drawing.Size(55, 57);
            this.lb_LoyalCustomers.TabIndex = 23;
            this.lb_LoyalCustomers.Text = "0";
            // 
            // guna2TextBox4
            // 
            this.guna2TextBox4.Animated = true;
            this.guna2TextBox4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox4.DefaultText = "";
            this.guna2TextBox4.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.Enabled = false;
            this.guna2TextBox4.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Font = new System.Drawing.Font("Tahoma", 10.2F, System.Drawing.FontStyle.Bold);
            this.guna2TextBox4.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Location = new System.Drawing.Point(936, 65);
            this.guna2TextBox4.Margin = new System.Windows.Forms.Padding(10, 14, 10, 14);
            this.guna2TextBox4.Name = "guna2TextBox4";
            this.guna2TextBox4.PasswordChar = '\0';
            this.guna2TextBox4.PlaceholderForeColor = System.Drawing.Color.White;
            this.guna2TextBox4.PlaceholderText = "";
            this.guna2TextBox4.SelectedText = "";
            this.guna2TextBox4.Size = new System.Drawing.Size(515, 326);
            this.guna2TextBox4.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label4.Location = new System.Drawing.Point(288, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 16);
            this.label4.TabIndex = 0;
            this.label4.Text = "Khách vãng lai";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Verdana", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Location = new System.Drawing.Point(34, 141);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(166, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Khách hàng thân thiết";
            // 
            // txt_SearchCustomerByName
            // 
            this.txt_SearchCustomerByName.Animated = true;
            this.txt_SearchCustomerByName.BackColor = System.Drawing.Color.Transparent;
            this.txt_SearchCustomerByName.BorderColor = System.Drawing.Color.Silver;
            this.txt_SearchCustomerByName.BorderRadius = 15;
            this.txt_SearchCustomerByName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_SearchCustomerByName.DefaultText = "";
            this.txt_SearchCustomerByName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.txt_SearchCustomerByName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.txt_SearchCustomerByName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SearchCustomerByName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.txt_SearchCustomerByName.FillColor = System.Drawing.Color.WhiteSmoke;
            this.txt_SearchCustomerByName.FocusedState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SearchCustomerByName.FocusedState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchCustomerByName.FocusedState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SearchCustomerByName.Font = new System.Drawing.Font("Tahoma", 9F);
            this.txt_SearchCustomerByName.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchCustomerByName.HoverState.BorderColor = System.Drawing.Color.Tomato;
            this.txt_SearchCustomerByName.HoverState.ForeColor = System.Drawing.Color.DimGray;
            this.txt_SearchCustomerByName.HoverState.PlaceholderForeColor = System.Drawing.Color.Silver;
            this.txt_SearchCustomerByName.IconLeft = global::MiniMarketManagement.Properties.Resources.search;
            this.txt_SearchCustomerByName.Location = new System.Drawing.Point(218, 566);
            this.txt_SearchCustomerByName.Margin = new System.Windows.Forms.Padding(5, 7, 5, 7);
            this.txt_SearchCustomerByName.Name = "txt_SearchCustomerByName";
            this.txt_SearchCustomerByName.PasswordChar = '\0';
            this.txt_SearchCustomerByName.PlaceholderForeColor = System.Drawing.Color.LightGray;
            this.txt_SearchCustomerByName.PlaceholderText = "ID / Số điện thoại";
            this.txt_SearchCustomerByName.SelectedText = "";
            this.txt_SearchCustomerByName.ShadowDecoration.BorderRadius = 15;
            this.txt_SearchCustomerByName.ShadowDecoration.Enabled = true;
            this.txt_SearchCustomerByName.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.txt_SearchCustomerByName.Size = new System.Drawing.Size(250, 45);
            this.txt_SearchCustomerByName.TabIndex = 23;
            // 
            // btn_Delete
            // 
            this.btn_Delete.BackColor = System.Drawing.Color.Transparent;
            this.btn_Delete.BorderRadius = 10;
            this.btn_Delete.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Delete.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Delete.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Delete.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Delete.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Delete.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Delete.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Delete.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Delete.ForeColor = System.Drawing.Color.White;
            this.btn_Delete.Image = ((System.Drawing.Image)(resources.GetObject("btn_Delete.Image")));
            this.btn_Delete.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Delete.Location = new System.Drawing.Point(277, 634);
            this.btn_Delete.Name = "btn_Delete";
            this.btn_Delete.ShadowDecoration.BorderRadius = 10;
            this.btn_Delete.ShadowDecoration.Enabled = true;
            this.btn_Delete.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Delete.Size = new System.Drawing.Size(191, 45);
            this.btn_Delete.TabIndex = 21;
            this.btn_Delete.Text = "    Xóa khách hàng";
            this.btn_Delete.Click += new System.EventHandler(this.btn_Delete_Click);
            // 
            // btn_Search
            // 
            this.btn_Search.BackColor = System.Drawing.Color.Transparent;
            this.btn_Search.BorderRadius = 10;
            this.btn_Search.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Search.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Search.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Search.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Search.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Search.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Search.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Search.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Search.ForeColor = System.Drawing.Color.White;
            this.btn_Search.Image = global::MiniMarketManagement.Properties.Resources.search3;
            this.btn_Search.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Search.ImageSize = new System.Drawing.Size(17, 17);
            this.btn_Search.Location = new System.Drawing.Point(38, 566);
            this.btn_Search.Name = "btn_Search";
            this.btn_Search.ShadowDecoration.BorderRadius = 10;
            this.btn_Search.ShadowDecoration.Enabled = true;
            this.btn_Search.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Search.Size = new System.Drawing.Size(135, 45);
            this.btn_Search.TabIndex = 20;
            this.btn_Search.Text = "    Tìm kiếm";
            this.btn_Search.Click += new System.EventHandler(this.btn_Search_Click);
            // 
            // btn_Update
            // 
            this.btn_Update.BackColor = System.Drawing.Color.Transparent;
            this.btn_Update.BorderRadius = 10;
            this.btn_Update.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.btn_Update.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.btn_Update.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Update.DisabledState.FillColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.btn_Update.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.btn_Update.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Update.FillColor2 = System.Drawing.Color.Tomato;
            this.btn_Update.Font = new System.Drawing.Font("Tahoma", 10F);
            this.btn_Update.ForeColor = System.Drawing.Color.White;
            this.btn_Update.Image = ((System.Drawing.Image)(resources.GetObject("btn_Update.Image")));
            this.btn_Update.ImageAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.btn_Update.Location = new System.Drawing.Point(38, 634);
            this.btn_Update.Name = "btn_Update";
            this.btn_Update.ShadowDecoration.BorderRadius = 10;
            this.btn_Update.ShadowDecoration.Enabled = true;
            this.btn_Update.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 5, 5);
            this.btn_Update.Size = new System.Drawing.Size(192, 45);
            this.btn_Update.TabIndex = 20;
            this.btn_Update.Text = "    Sửa thông tin";
            this.btn_Update.Click += new System.EventHandler(this.btn_Update_Click);
            // 
            // frm_LoyalCustomers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(1134, 691);
            this.Controls.Add(this.txt_SearchCustomerByName);
            this.Controls.Add(this.guna2CustomGradientPanel1);
            this.Controls.Add(this.btn_Delete);
            this.Controls.Add(this.btn_Search);
            this.Controls.Add(this.btn_Update);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.dgv_LoyalCustomers);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frm_LoyalCustomers";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.frm_LoyalCustomers_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_LoyalCustomers)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.guna2CustomGradientPanel1.ResumeLayout(false);
            this.guna2CustomGradientPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Elipse guna2Elipse1;
        private Guna.UI2.WinForms.Guna2DataGridView dgv_LoyalCustomers;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private Guna.UI2.WinForms.Guna2TextBox txt_CustomerID;
        private Guna.UI2.WinForms.Guna2TextBox txt_CustomerPoint;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2TextBox txt_CustomerName;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2TextBox txt_CustomerPhoneNumber;
        private Guna.UI2.WinForms.Guna2GradientButton btn_Update;
        private Guna.UI2.WinForms.Guna2GradientButton btn_Delete;
        private Guna.UI2.WinForms.Guna2CustomGradientPanel guna2CustomGradientPanel1;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2AnimateWindow guna2AnimateWindow1;
        private Guna.UI2.WinForms.Guna2AnimateWindow guna2AnimateWindow2;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerPhoneNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn CustomerPoint;
        private System.Windows.Forms.Label lb_Customers;
        private System.Windows.Forms.Label lb_LoyalCustomers;
        private Guna.UI2.WinForms.Guna2TextBox txt_SearchCustomerByName;
        private Guna.UI2.WinForms.Guna2GradientButton btn_Search;
    }
}
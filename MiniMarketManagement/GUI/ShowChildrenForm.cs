﻿using Guna.UI2.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.GUI
{
    internal class ShowChildrenForm
    {
        private Form currentForm;
        private Guna2Button currentButton;

        public void ShowForm(Form frm, Guna2Button button, Guna2ShadowPanel panel)
        {
            if (currentForm != null)
            {
                currentForm.Close();
            }
            if (currentButton != null)
            {
                currentButton.Checked = false;
            }
            currentButton = button;
            currentButton.Checked = true;
            currentForm = frm;
            frm.TopLevel = false;
            frm.FormBorderStyle = FormBorderStyle.None;
            frm.Dock = DockStyle.Fill;
            panel.Controls.Add(frm);
            frm.Show();
        }
    }
}

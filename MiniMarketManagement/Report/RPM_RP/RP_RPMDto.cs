﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MiniMarketManagement.Report.RPM_RP
{
    internal class RP_RPMDto
    {
        public string ReturnProductID { get; set; }
        public string ProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductTypeName { get; set; }
        public double BuyPrice { get; set; }
        public string CalculationUnitName { get; set; }
        public double Quantity { get; set; }
        public string SupplierName { get; set; }
    }
}

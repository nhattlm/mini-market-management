﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Cashier;
namespace MiniMarketManagement.BLL.Cashier
{
    public class BLL_Register
    {
        DAL_Register dal_register = new DAL_Register();
        DAL_Payment dal_payment = new DAL_Payment();

        public void LoadDgvCustomer_And_textboxInvoiceID(Guna2DataGridView dgv_Customer, Guna2TextBox txt_InvoiceID)
        {
            try
            {
                dgv_Customer.Rows.Clear();
                var listCustomer = dal_payment.GetAllCustomer();
                foreach (var item in listCustomer)
                {
                    int index = dgv_Customer.Rows.Add();
                    dgv_Customer.Rows[index].Cells["dgv_CustomerID"].Value = item.CustomerID;
                    dgv_Customer.Rows[index].Cells["dgv_CustomerName"].Value = item.CustomerName;
                    dgv_Customer.Rows[index].Cells["dgv_CustomerPhone"].Value = item.PhoneNumber;
                    dgv_Customer.Rows[index].Cells["dgv_PointNow"].Value = item.Point;

                }
                var getLastRowInvoice = dal_register.GetLastIndexOfInvoice();
                txt_InvoiceID.Text = "I" + getLastRowInvoice;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xổ dữ liệu là: " + ex.Message);
                return;
            }
        }
        public bool CheckInvoice(Guna2TextBox txt_InvoiceID)
        {
            try
            {
                var listInvoice = dal_register.GetInvoiceSearch(txt_InvoiceID.Text);
                if (listInvoice.Count != 0)
                {
                    foreach (var item in listInvoice)
                    {
                        if (item.PriceTotal < 100000)
                        {
                            MessageBox.Show("Hóa đơn chưa đủ 100,000đ");
                            return false;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Mã hóa đơn không tồn tại!");
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình kiểm tra hóa đơn là: " + ex.Message);
                return false;
            }
        }
        public void btn_checkInvoiceID(Guna2TextBox txt_InvoiceID, Guna2TextBox txt_CustomerName, Guna2TextBox txt_CustomerPhone)
        {
            try
            {
                if (CheckInvoice(txt_InvoiceID))
                {
                    MessageBox.Show("Hóa đơn hợp lệ");
                    txt_CustomerName.Enabled = true;
                    txt_CustomerPhone.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình kiểm tra hóa đơn là: " + ex.Message);
                return;
            }
        }
        public void RegisterCustomer(Guna2TextBox txt_InvoiceID, Guna2TextBox txt_CustomerName, Guna2TextBox txt_CustomerPhone)
        {
            try
            {
                var customer = dal_payment.GetCustomer();
                var listCustommer = dal_payment.GetAllCustomer();
                int listLastCustomer = (dal_register.GetLastIndexOfCustomer() + 1);
                customer.CustomerID = "C" + listLastCustomer.ToString();
                customer.CustomerName = txt_CustomerName.Text;
                int.TryParse(txt_CustomerPhone.Text, out int phoneNumber);
                foreach (var item in listCustommer)
                {
                    if (item.PhoneNumber.Trim() == txt_CustomerPhone.Text.Trim())
                    {
                        MessageBox.Show("Số điện thoại này đã được đăng ký khách hàng thân thiết!");
                        return;
                    }
                }
                if (phoneNumber != 0)
                {
                    customer.PhoneNumber = txt_CustomerPhone.Text;
                }
                else
                {
                    MessageBox.Show("Số điện thoại phải là số nguyên và không vượt quá 10 ký tự!");
                    return;
                }
                customer.Point = 0;
                dal_payment.CustomerUpdate(customer);
                txt_InvoiceID.Text = null;
                txt_CustomerName.Text = null;
                txt_CustomerPhone.Text = null;
                txt_CustomerName.Enabled = false;
                txt_CustomerPhone.Enabled = false;
                MessageBox.Show("Đăng ký khách hàng thành công!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình đăng ký khách hàng là: " + ex.Message);
                return;
            }
        }
    }
}

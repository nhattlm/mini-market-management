﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Administrator;
using MiniMarketManagement.DAL.Cashier;
using MiniMarketManagement.DAL.Entities;

namespace MiniMarketManagement.BLL.Administrator
{
    public class BLL_CreateAccount
    {
        DAL_CreateAccount dal_CreateAccount = new DAL_CreateAccount();
        public void FillDgv_Account(Guna2DataGridView dgv_Account)
        {
            dgv_Account.Rows.Clear();
            var listProduct = dal_CreateAccount.GetAllAccount();
            foreach (var item in listProduct)
            {
                int index = dgv_Account.Rows.Add();
                dgv_Account.Rows[index].Cells["dgv_Username"].Value = item.Username;
                dgv_Account.Rows[index].Cells["dgv_Password"].Value = item.Password;
                dgv_Account.Rows[index].Cells["dgv_EmployeeID"].Value = item.EmployeeID;
            }
        }
        public void LoadCellClick_Account(object sender, DataGridViewCellEventArgs e, Guna2DataGridView dgv_Account, Guna2TextBox txt_Username, Guna2TextBox txt_Password, 
            Guna2TextBox txt_EmployeeID)
        {
            int index = e.RowIndex;
            if (index < 0)
            {
                return;
            }
            txt_Username.Text = dgv_Account.Rows[index].Cells["dgv_Username"].Value.ToString();
            txt_Password.Text = dgv_Account.Rows[index].Cells["dgv_Password"].Value.ToString();
            txt_EmployeeID.Text = dgv_Account.Rows[index].Cells["dgv_EmployeeID"].Value.ToString();
        }
        public void btn_createAccount( Guna2TextBox txt_Username, Guna2TextBox txt_Password,
            Guna2TextBox txt_EmployeeID)
        {
            var account = dal_CreateAccount.GetAccount();
            if (txt_Username.Text == "")
            {
                MessageBox.Show("Vui lòng nhập thông tin tài khoản!");
                return;
            }
            if (txt_Password.Text == "")
            {
                MessageBox.Show("Vui lòng nhập thông tin mật khẩu!");
                return;
            }

            var getListEmployeeIdFormDB = dal_CreateAccount.GetEmployee_Have_EmployeeID(txt_EmployeeID.Text);
            if (getListEmployeeIdFormDB != null)
            {
                account.Username = txt_Username.Text;
                account.Password = txt_Password.Text;
                account.EmployeeID = txt_EmployeeID.Text;
                dal_CreateAccount.NewAccount(account);
                MessageBox.Show("Đăng ký tài khoản thành công!");
                txt_EmployeeID.Text = "";
                txt_Password.Text = "";
                txt_Username.Text = "";
            }
            else
            {
                MessageBox.Show("Mã nhân viên không tồn tại!");
                txt_EmployeeID.Text = "";
                return;
            }

        }
        public void SearchAccount(Guna2DataGridView dgv_Account, Guna2TextBox txt_Search)
        {
            dgv_Account.Rows.Clear();
            var getAccount = dal_CreateAccount.GetAccountSearch(txt_Search.Text);
            foreach (var item in getAccount)
            {
                int index = dgv_Account.Rows.Add();
                dgv_Account.Rows[index].Cells["dgv_Username"].Value = item.Username;
                dgv_Account.Rows[index].Cells["dgv_Password"].Value = item.Password;
                dgv_Account.Rows[index].Cells["dgv_EmployeeID"].Value = item.EmployeeID;
            }
        }
        public bool btn_changePassword(Guna2TextBox txt_Username, Guna2TextBox txt_Password)
        {
            if (txt_Username.Text == "")
            {
                MessageBox.Show("Vui lòng chọn tài khoản để đổi mật khẩu!");
                return false;
            }
            if (txt_Password.Text == "")
            {
                MessageBox.Show("Mật khẩu không được để trống!");
                return false;
            }
            var result = MessageBox.Show("Bạn có muốn đổi mật khẩu không?", "Xác nhận đổi", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                var account = dal_CreateAccount.GetAccount_Have_Username(txt_Username.Text);
                account.Password = txt_Password.Text;
                dal_CreateAccount.AccountUpdate(account);
                return true;
            }
            return false;
        }
        public bool deleteAcount(Guna2TextBox txt_Username)
        {
            var result = MessageBox.Show("Bạn có muốn xóa Tài khoản này không?","Xác nhận xóa",MessageBoxButtons.YesNo,MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                dal_CreateAccount.DeleteAccount(txt_Username.Text);
                return true;
            }
            return false;
        }
    }
}

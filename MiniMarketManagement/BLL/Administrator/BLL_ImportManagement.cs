﻿using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Administrator;
using MiniMarketManagement.DAL.Storekeeper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.BLL.Administrator
{
    internal class BLL_ImportManagement
    {
        public void ShowDetailImportProduct(Guna2DataGridView dgv_ImportManagement)
        {
            DAL_ImportManagement dAL_ImportManagement = new DAL_ImportManagement();
            dgv_ImportManagement.Rows.Clear();
            foreach (var item in dAL_ImportManagement.GetDetailImportProductCard())
            {
                int index = dgv_ImportManagement.Rows.Add();
                dgv_ImportManagement.Rows[index].Cells["ImportProductID"].Value = item.ImportProductID;
                dgv_ImportManagement.Rows[index].Cells["ProductID"].Value = item.ProductID;
                dgv_ImportManagement.Rows[index].Cells["ProductName"].Value = item.Product.ProductName;
                dgv_ImportManagement.Rows[index].Cells["TypeProductName"].Value = item.Product.ProductType.ProductTypeName;
                dgv_ImportManagement.Rows[index].Cells["ProductPrice"].Value = item.Price;
                dgv_ImportManagement.Rows[index].Cells["CalculationUnit"].Value = item.Product.ProductType.CalculationUnitName;
                dgv_ImportManagement.Rows[index].Cells["Quantity"].Value = item.Quantity;
                dgv_ImportManagement.Rows[index].Cells["Supplier"].Value = item.Product.Supplier.SupplierName;
            }
        }
        const string ProductImPath = "Assets\\ProductImage\\";
        const string EmptyImPath = "Resources\\image (2).png";
        public void FillPicbox(int row, Guna2DataGridView dgv_ListProduct, out Image image)
        {
            string ProductID = dgv_ListProduct.Rows[row].Cells["ProductID"].Value.ToString();
            DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
            var product = dAL_ImportProduct_Storekeeper.GetProductByProductID(ProductID);
            var ImPath = product.ProductImage;
            string imagePath;
            if (string.IsNullOrEmpty(ImPath))
            {
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, EmptyImPath);
            }
            else
            {
                // Xác định đường dẫn tới hình ảnh sử dụng đường dẫn tương đối
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, ProductImPath, ImPath);
            }
            image = Image.FromFile(imagePath);
        }
        public void SearchBy(string ImportProductCartID, Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate, Guna2DataGridView dgv_ImportManagement)
        {
            DAL_ImportManagement dAL_ImportManagement = new DAL_ImportManagement();
            dgv_ImportManagement.Rows.Clear();
            foreach (var item in dAL_ImportManagement.SearchBy(ImportProductCartID, fromDate, toDate))
            {
                int index = dgv_ImportManagement.Rows.Add();
                dgv_ImportManagement.Rows[index].Cells["ImportProductID"].Value = item.ImportProductID;
                dgv_ImportManagement.Rows[index].Cells["ProductID"].Value = item.ProductID;
                dgv_ImportManagement.Rows[index].Cells["ProductName"].Value = item.Product.ProductName;
                dgv_ImportManagement.Rows[index].Cells["TypeProductName"].Value = item.Product.ProductType.ProductTypeName;
                dgv_ImportManagement.Rows[index].Cells["ProductPrice"].Value = item.Price;
                dgv_ImportManagement.Rows[index].Cells["CalculationUnit"].Value = item.Product.ProductType.CalculationUnitName;
                dgv_ImportManagement.Rows[index].Cells["Quantity"].Value = item.Quantity;
                dgv_ImportManagement.Rows[index].Cells["Supplier"].Value = item.Product.Supplier.SupplierName;
            }
        }
        public double LoadDefaultImportQuantity()
        {
            try
            {
                return new DAL_ImportManagement().LoadDefaultImportQuantity();
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra trong quá trình truy vấn cơ sở dữ liệu!", "Thông báo");
                return 0;
            }
        }
        public double LoadDeafaultImportProductMoney()
        {
            try
            {
                return new DAL_ImportManagement().LoadDeafaultImportProductMoney();
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra trong quá trình truy vấn cơ sở dữ liệu!", "Thông báo");
                return 0;
            }
        }
        public double ImportQuantity(string ImportProductCartID, Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate)
        {
            try
            {
                return new DAL_ImportManagement().ImportQuantity(ImportProductCartID, fromDate, toDate);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra trong quá trình truy vấn cơ sở dữ liệu!", "Thông báo");
                return 0;
            }
        }
        public double ImportProductMoney(string ImportProductCartID, Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate)
        {
            try
            {
                return new DAL_ImportManagement().ImportProductMoney(ImportProductCartID, fromDate, toDate);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra trong quá trình truy vấn cơ sở dữ liệu!", "Thông báo");
                return 0;
            }
        }
        public static string ConvertMoneyToString(double money)
        {
            if (money >= 1000000000)
            {
                return (money / 1000000000).ToString("0.##") + "B";
            }
            else if (money >= 1000000)
            {
                return (money / 1000000).ToString("0.##") + "M";
            }
            else if (money >= 1000)
            {
                return (money / 1000).ToString("0.##") + "K";
            }
            else
            {
                return money.ToString();
            }
        }
    }
}

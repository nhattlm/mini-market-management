﻿using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Administrator;
using MiniMarketManagement.DAL.Storekeeper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.BLL.Administrator
{
    internal class BLL_ReturnManagement
    {
        public void ShowDetailReturnProduct(Guna2DataGridView dgv_ReturnManagement)
        {
            DAL_ReturnManagement dAL_ReturnManagement = new DAL_ReturnManagement();
            dgv_ReturnManagement.Rows.Clear();
            foreach (var item in dAL_ReturnManagement.GetDetailReturnProductCard())
            {
                int index = dgv_ReturnManagement.Rows.Add();
                dgv_ReturnManagement.Rows[index].Cells["ReturnProductID"].Value = item.ReturnProductID;
                dgv_ReturnManagement.Rows[index].Cells["ProductID"].Value = item.ProductID;
                dgv_ReturnManagement.Rows[index].Cells["ProductName"].Value = item.Product.ProductName;
                dgv_ReturnManagement.Rows[index].Cells["TypeProductName"].Value = item.Product.ProductType.ProductTypeName;
                dgv_ReturnManagement.Rows[index].Cells["ProductPrice"].Value = item.price;
                dgv_ReturnManagement.Rows[index].Cells["CalculationUnit"].Value = item.Product.ProductType.CalculationUnitName;
                dgv_ReturnManagement.Rows[index].Cells["Quantity"].Value = item.Quantity;
                dgv_ReturnManagement.Rows[index].Cells["Supplier"].Value = item.Product.Supplier.SupplierName;
            }
        }
        const string ProductImPath = "Assets\\ProductImage\\";
        const string EmptyImPath = "Resources\\image (2).png";
        public void FillPicbox(int row, Guna2DataGridView dgv_ListProduct, out Image image)
        {
            string ProductID = dgv_ListProduct.Rows[row].Cells["ProductID"].Value.ToString();
            DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
            var product = dAL_ImportProduct_Storekeeper.GetProductByProductID(ProductID);
            var ImPath = product.ProductImage;
            string imagePath;
            if (string.IsNullOrEmpty(ImPath))
            {
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, EmptyImPath);
            }
            else
            {
                // Xác định đường dẫn tới hình ảnh sử dụng đường dẫn tương đối
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, ProductImPath, ImPath);
            }
            image = Image.FromFile(imagePath);
            }
        public void SearchBy(string ReturnProductCartID, Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate, Guna2DataGridView dgv_ReturnManagement)
        {
            DAL_ReturnManagement dAL_ReturnManagement = new DAL_ReturnManagement();
            dgv_ReturnManagement.Rows.Clear();
            foreach (var item in dAL_ReturnManagement.SearchBy(ReturnProductCartID, fromDate, toDate))
            {
                int index = dgv_ReturnManagement.Rows.Add();
                dgv_ReturnManagement.Rows[index].Cells["ReturnProductID"].Value = item.ReturnProductID;
                dgv_ReturnManagement.Rows[index].Cells["ProductID"].Value = item.ProductID;
                dgv_ReturnManagement.Rows[index].Cells["ProductName"].Value = item.Product.ProductName;
                dgv_ReturnManagement.Rows[index].Cells["TypeProductName"].Value = item.Product.ProductType.ProductTypeName;
                dgv_ReturnManagement.Rows[index].Cells["ProductPrice"].Value = item.price;
                dgv_ReturnManagement.Rows[index].Cells["CalculationUnit"].Value = item.Product.ProductType.CalculationUnitName;
                dgv_ReturnManagement.Rows[index].Cells["Quantity"].Value = item.Quantity;
                dgv_ReturnManagement.Rows[index].Cells["Supplier"].Value = item.Product.Supplier.SupplierName;
            }
        }
        public double LoadDefaultReturnQuantity()
        {
            try
            {
                return new DAL_ReturnManagement().LoadDefaultReturnQuantity();
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra trong quá trình truy vấn cơ sở dữ liệu!", "Thông báo");
                return 0;
            }
        }
        public double LoadDeafaultReturnProductMoney()
        {
            try
            {
                return new DAL_ReturnManagement().LoadDeafaultReturnProductMoney();
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra trong quá trình truy vấn cơ sở dữ liệu!", "Thông báo");
                return 0;
            }
        }
        public double ReturnQuantity(string ReturnProductCartID, Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate)
        {
            try
            {
                return new DAL_ReturnManagement().ReturnQuantity(ReturnProductCartID, fromDate, toDate);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra trong quá trình truy vấn cơ sở dữ liệu!", "Thông báo");
                return 0;
            }
        }
        public double ReturnProductMoney(string ReturnProductCartID, Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate)
        {            
            try
            {
                return new DAL_ReturnManagement().ReturnProductMoney(ReturnProductCartID, fromDate, toDate);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra trong quá trình truy vấn cơ sở dữ liệu!", "Thông báo");
                return 0;
            }
        }
        public static string ConvertMoneyToString(double money)
        {
            if (money >= 1000000000)
            {
                return (money / 1000000000).ToString("0.##") + "B";
            }
            else if (money >= 1000000)
            {
                return (money / 1000000).ToString("0.##") + "M";
            }
            else if (money >= 1000)
            {
                return (money / 1000).ToString("0.##") + "K";
            }
            else
            {
                return money.ToString();
            }
        }
    }
}

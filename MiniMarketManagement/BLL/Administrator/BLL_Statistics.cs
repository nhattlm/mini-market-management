﻿using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Administrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.BLL.Administrator
{
    internal class BLL_Statistics
    {
        public double GetTotalIncome()
        {
            return new DAL_Statistics().GetTotalIncome();
        }
        public double GetTotalExpense()
        {
            return new DAL_Statistics().GetTotalExpense();
        }
        public double GetTotalProfit()
        {
            return new DAL_Statistics().GetTotalProfit();
        }
        public (DateTime, DateTime) GetFirstAndLastDayOfMonthFromCmb(string[] collections, Guna2ComboBox cmb)
        {
            // Lấy giá trị tháng được chọn từ cmb
            string selectedMonth = cmb.SelectedItem.ToString();
            int getSelectedMonthNumber = Array.IndexOf(collections, selectedMonth) + 1;
            // Xác định các ngày của tháng được chọn
            DateTime firstDate = new DateTime(DateTime.Now.Year, getSelectedMonthNumber, 1);
            DateTime lastDate = firstDate.AddMonths(1).AddDays(-1);
            return (firstDate, lastDate);
        }
        public (DateTime, DateTime) GetFirstAndLastDayOfMonthForChart(string[] collections, int month)
        {
            DateTime firstDate = new DateTime(DateTime.Now.Year, month, 1);
            DateTime lastDate = firstDate.AddMonths(1).AddDays(-1);
            return (firstDate, lastDate);
        }
        public double GetTotalIncomeByMonthForChart(string[] collections, int month)
        {
            try
            {
                var (firstDate, lastDate) = GetFirstAndLastDayOfMonthForChart(collections, month);
                return new DAL_Statistics().GetTotalIncomeByMonth(firstDate, lastDate);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra khi truy xuất dữ liệu", "Thông báo");
                return 0;
            }
        }
        public double GetTotalExpenseByMonthForChart(string[] collections, int month)
        {
            try
            {
                var (firstDate, lastDate) = GetFirstAndLastDayOfMonthForChart(collections, month);
                return new DAL_Statistics().GetTotalExpenseByMonth(firstDate, lastDate);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra khi truy xuất dữ liệu", "Thông báo");
                return 0;
            }
        }
        public double GetTotalProfitByMonthForChart(string[] collections, int month)
        {
            try
            {
                var (firstDate, lastDate) = GetFirstAndLastDayOfMonthForChart(collections, month);
                return new DAL_Statistics().GetTotalProfitByMonth(firstDate, lastDate);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra khi truy xuất dữ liệu", "Thông báo");
                return 0;
            }
        }
        public (DateTime, DateTime, DateTime, DateTime) GetFirstAndLastDaysOfTwoConsecutiveMonths(string[] collections, Guna2ComboBox cmb)
        {
            string selectedMonth = cmb.SelectedItem.ToString();
            int getSelectedMonthNumber = Array.IndexOf(collections, selectedMonth) + 1;
            int getPreviousSelectedMonthNumber = getSelectedMonthNumber - 1;
            DateTime currentFirstDate = new DateTime(DateTime.Now.Year, getSelectedMonthNumber, 1);
            DateTime currentLastDate = currentFirstDate.AddMonths(1).AddDays(-1);
            DateTime previousFirstDate = new DateTime(DateTime.Now.Year, getPreviousSelectedMonthNumber, 1);
            DateTime previousLastDate = previousFirstDate.AddMonths(1).AddDays(-1);
            return (currentFirstDate, currentLastDate, previousFirstDate, previousLastDate);
        }

        public double GetTotalIncomeByMonth(string[] collections, Guna2ComboBox cmb)
        {
            try
            {
                var (firstDate, lastDate) = GetFirstAndLastDayOfMonthFromCmb(collections, cmb);
                return new DAL_Statistics().GetTotalIncomeByMonth(firstDate, lastDate);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra khi truy xuất dữ liệu", "Thông báo");
                return 0;
            }
        }
        public double GetTotalExpenseByMonth(string[] collections, Guna2ComboBox cmb)
        {
            try
            {
                var (firstDate, lastDate) = GetFirstAndLastDayOfMonthFromCmb(collections, cmb);
                return new DAL_Statistics().GetTotalExpenseByMonth(firstDate, lastDate);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra khi truy xuất dữ liệu", "Thông báo");
                return 0;
            }
        }
        public double GetTotalProfitByMonth(string[] collections, Guna2ComboBox cmb)
        {
            try
            {
                var (firstDate, lastDate) = GetFirstAndLastDayOfMonthFromCmb(collections, cmb);
                return new DAL_Statistics().GetTotalProfitByMonth(firstDate, lastDate);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra khi truy xuất dữ liệu", "Thông báo");
                return 0;
            }
        }
        public double CompareTotalIncomeWithPreviousMonth(string[] collections, Guna2ComboBox cmb)
        {
            try
            {
                var dates = GetFirstAndLastDaysOfTwoConsecutiveMonths(collections, cmb);
                return new DAL_Statistics().CompareTotalIncomeWithPreviousMonth(dates.Item1, dates.Item2, dates.Item3, dates.Item4);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra khi truy xuất dữ liệu", "Thông báo");
                return 0;
            }
        }
        public double CompareTotalExpenseWithPreviousMonth(string[] collections, Guna2ComboBox cmb)
        {
            try
            {
                var dates = GetFirstAndLastDaysOfTwoConsecutiveMonths(collections, cmb);
                return new DAL_Statistics().CompareTotalExpenseWithPreviousMonth(dates.Item1, dates.Item2, dates.Item3, dates.Item4);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra khi truy xuất dữ liệu", "Thông báo");
                return 0;
            }
        }
        public double CompareTotalProfitWithPreviousMonth(string[] collections, Guna2ComboBox cmb)
        {
            try
            {
                var dates = GetFirstAndLastDaysOfTwoConsecutiveMonths(collections, cmb);
                return new DAL_Statistics().CompareTotalProfitWithPreviousMonth(dates.Item1, dates.Item2, dates.Item3, dates.Item4);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra khi truy xuất dữ liệu", "Thông báo");
                return 0;
            }
        }
        public static string ConvertToRate(double money)
        {
            bool isNegative = money < 0;
            money = Math.Abs(money);
            if (money >= 1000000000)
            {
                return (isNegative ? "-" : "") + (money / 1000000000).ToString("0.00");
            }
            else if (money >= 1000000)
            {
                return (isNegative ? "-" : "") + (money / 1000000).ToString("0.00");
            }
            else if (money >= 1000)
            {
                return (isNegative ? "-" : "") + (money / 1000).ToString("0.00");
            }
            else
            {
                return (isNegative ? "-" : "") + money.ToString("0.00");
            }
        }
        public static string ConvertMoneyToString(double money)
        {
            bool isNegative = money < 0;
            money = Math.Abs(money);
            if (money >= 1000000000)
            {
                return (isNegative ? "-" : "") + (money / 1000000000).ToString("0.00") + "B";
            }
            else if (money >= 1000000)
            {
                return (isNegative ? "-" : "") + (money / 1000000).ToString("0.00") + "M";
            }
            else if (money >= 1000)
            {
                return (isNegative ? "-" : "") + (money / 1000).ToString("0.00") + "K";
            }
            else
            {
                return (isNegative ? "-" : "") + money.ToString();
            }
        }
    }
}

﻿using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Administrator;
using MiniMarketManagement.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.BLL.Administrator
{
    internal class BLL_Invoices
    {
        public int GetLoyalCustomerInvoices()
        {
            try
            {
                DAL_Invoices invoices = new DAL_Invoices();
                return invoices.CountLoyalCustomerInvoices();
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi khi truy vấn cơ sỏ dữ liệu!");
                return -1;
            }
        }
        public int GetCustomerInvoices()
        {
            try
            {
                DAL_Invoices invoices = new DAL_Invoices();
                return invoices.CountCustomerInvoices();
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi khi truy vấn cơ sỏ dữ liệu!");
                return -1;
            }
        }
        public void ShowInvoice(Guna2DataGridView dgv_Invoices)
        {
            DAL_Invoices dAL_Invoices = new DAL_Invoices();
            dgv_Invoices.Rows.Clear();
            foreach (var item in dAL_Invoices.GetInvoices())
            {
                int index = dgv_Invoices.Rows.Add();
                dgv_Invoices.Rows[index].Cells["InvoiceID"].Value = item.InvoiceID;
                dgv_Invoices.Rows[index].Cells["CustomerID"].Value = item.CustomerID;
                dgv_Invoices.Rows[index].Cells["EmployeeID"].Value = item.EmployeeID;
                dgv_Invoices.Rows[index].Cells["InvoiceDate"].Value = item.Date;
                dgv_Invoices.Rows[index].Cells["PriceTotal"].Value = item.PriceTotal;
                dgv_Invoices.Rows[index].Cells["MinusPrice"].Value = item.MinusPrice;
            }
        }
        public void ShowLoyalCustomerInvoices(Guna2DataGridView dgv_Invoices)
        {
            DAL_Invoices dAL_Invoices = new DAL_Invoices();
            dgv_Invoices.Rows.Clear();
            foreach (var item in dAL_Invoices.GetLoyalCustomerInvoices())
            {
                int index = dgv_Invoices.Rows.Add();
                dgv_Invoices.Rows[index].Cells["InvoiceID"].Value = item.InvoiceID;
                dgv_Invoices.Rows[index].Cells["CustomerID"].Value = item.CustomerID;
                dgv_Invoices.Rows[index].Cells["EmployeeID"].Value = item.EmployeeID;
                dgv_Invoices.Rows[index].Cells["InvoiceDate"].Value = item.Date;
                dgv_Invoices.Rows[index].Cells["PriceTotal"].Value = item.PriceTotal;
                dgv_Invoices.Rows[index].Cells["MinusPrice"].Value = item.MinusPrice;
            }
        }
        public void ShowCustomerInvoices(Guna2DataGridView dgv_Invoices)
        {
            DAL_Invoices dAL_Invoices = new DAL_Invoices();
            dgv_Invoices.Rows.Clear();
            foreach (var item in dAL_Invoices.GetCustomerInvoices())
            {
                int index = dgv_Invoices.Rows.Add();
                dgv_Invoices.Rows[index].Cells["InvoiceID"].Value = item.InvoiceID;
                dgv_Invoices.Rows[index].Cells["CustomerID"].Value = item.CustomerID;
                dgv_Invoices.Rows[index].Cells["EmployeeID"].Value = item.EmployeeID;
                dgv_Invoices.Rows[index].Cells["InvoiceDate"].Value = item.Date;
                dgv_Invoices.Rows[index].Cells["PriceTotal"].Value = item.PriceTotal;
                dgv_Invoices.Rows[index].Cells["MinusPrice"].Value = item.MinusPrice;
            }
        }
        public void FilterLoyalCustomerInvoiceChecked (CheckBox chb_FilterLoyalCustomerInvoice, CheckBox chb_FillterCustomerInvoice, Guna2DataGridView dgv_Invoices)
        {
            try
            {
                if (chb_FilterLoyalCustomerInvoice.Checked && chb_FillterCustomerInvoice.Checked == false)
                {
                    ShowLoyalCustomerInvoices(dgv_Invoices);
                }
                else
                {
                    ShowInvoice(dgv_Invoices);
                    chb_FillterCustomerInvoice.Checked = false;
                    chb_FilterLoyalCustomerInvoice.Checked = false;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi khi truy vấn cơ sỏ dữ liệu!");
            }
        }
        public void FilterCustomerInvoiceChecked(CheckBox chb_FilterLoyalCustomerInvoice, CheckBox chb_FillterCustomerInvoice, Guna2DataGridView dgv_Invoices)
        {
            try
            {
                if (chb_FillterCustomerInvoice.Checked && chb_FilterLoyalCustomerInvoice.Checked == false)
                {
                    ShowCustomerInvoices(dgv_Invoices);
                }
                else
                {
                    ShowInvoice(dgv_Invoices);
                    chb_FillterCustomerInvoice.Checked = false;
                    chb_FilterLoyalCustomerInvoice.Checked = false;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi khi truy vấn cơ sỏ dữ liệu!");
            }
        }
        public void Filter(Guna2DateTimePicker fromDate, Guna2DateTimePicker toDate, string invoiceID, string customerID, Guna2DataGridView dgv_Invoices)
        {
            DAL_Invoices dAL_Invoices = new DAL_Invoices();
            dgv_Invoices.Rows.Clear();
            foreach (var item in dAL_Invoices.Filter(fromDate, toDate, invoiceID, customerID))
            {
                int index = dgv_Invoices.Rows.Add();
                dgv_Invoices.Rows[index].Cells["InvoiceID"].Value = item.InvoiceID;
                dgv_Invoices.Rows[index].Cells["CustomerID"].Value = item.CustomerID;
                dgv_Invoices.Rows[index].Cells["EmployeeID"].Value = item.EmployeeID;
                dgv_Invoices.Rows[index].Cells["InvoiceDate"].Value = item.Date;
                dgv_Invoices.Rows[index].Cells["PriceTotal"].Value = item.PriceTotal;
                dgv_Invoices.Rows[index].Cells["MinusPrice"].Value = item.MinusPrice;
            }
        }
    }
}

﻿using System;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Administrator;
using MiniMarketManagement.DAL.Login;
using static System.Net.Mime.MediaTypeNames;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using Image = System.Drawing.Image;

namespace MiniMarketManagement.BLL.Administrator
{
    public class BLL_EmployeeManage
    {
        const string EmployeeImPath = "Assets\\EmployeeImage\\";
        const string EmptyImPath = "Resources\\image (2).png";
        DAL_EmployeeManage dal_EmployeeManage = new DAL_EmployeeManage();
        DAL_Login dAL_Login = new DAL_Login();
        public void FillDgv_Employee(Guna2DataGridView dgv_Employee)
        {
            try
            {
                dgv_Employee.Rows.Clear();
                var listProduct = dal_EmployeeManage.GetAllEmployee();
                foreach (var item in listProduct)
                {
                    int index = dgv_Employee.Rows.Add();
                    dgv_Employee.Rows[index].Cells["dgv_EmployeeID"].Value = item.EmployeeID;
                    dgv_Employee.Rows[index].Cells["dgv_EmployeeName"].Value = item.EmployeeName;
                    dgv_Employee.Rows[index].Cells["dgv_EmployeePhone"].Value = item.EmployeePhone;
                    dgv_Employee.Rows[index].Cells["dgv_EmployeeBirth"].Value = item.EmployeeBirth;
                    dgv_Employee.Rows[index].Cells["dgv_PositionName"].Value = item.Position.PositionName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xổ dữ liệu là " + ex.Message);
                return;
            }
        }
        public void FillComboBoxPosition(Guna2ComboBox cmb_Position)
        {
            try
            {
                var listPosition = dal_EmployeeManage.GetAllPosition();
                cmb_Position.DataSource = listPosition;
                cmb_Position.DisplayMember = "PositionName";
                cmb_Position.ValueMember = "PositionID";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xổ dữ liệu là" + ex.Message);
                return;
            }
        }
        public void LoadCellClick_Employee(object sender, DataGridViewCellEventArgs e, Guna2DataGridView dgv_Account, Guna2TextBox txt_EmployeeID,
            Guna2TextBox txt_EmployeeName, Guna2TextBox txt_EmployeePhone, Guna2TextBox txt_EmployeeBirth, Guna2ComboBox cmb_Position, out Image image)
        {
            try
            {
                int index = e.RowIndex;
                txt_EmployeeID.Text = dgv_Account.Rows[index].Cells["dgv_EmployeeID"].Value.ToString();
                txt_EmployeeName.Text = dgv_Account.Rows[index].Cells["dgv_EmployeeName"].Value.ToString();
                txt_EmployeePhone.Text = dgv_Account.Rows[index].Cells["dgv_EmployeePhone"].Value.ToString();
                txt_EmployeeBirth.Text = dgv_Account.Rows[index].Cells["dgv_EmployeeBirth"].Value.ToString();
                cmb_Position.Text = dgv_Account.Rows[index].Cells["dgv_PositionName"].Value.ToString();


                var ImPath = dal_EmployeeManage.GetEmployee(txt_EmployeeID.Text).EmployeeImage;
                string imagePath;
                if (string.IsNullOrEmpty(ImPath))
                {
                    string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    imagePath = Path.Combine(projectDirectory, EmptyImPath);
                }
                else
                {
                    // Xác định đường dẫn tới hình ảnh sử dụng đường dẫn tương đối
                    string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    imagePath = Path.Combine(projectDirectory, EmployeeImPath, ImPath);
                }
                image = Image.FromFile(imagePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xổ dữ liệu là" + ex.Message);
                var ImPath = dal_EmployeeManage.GetEmployee(txt_EmployeeID.Text).EmployeeImage;
                string imagePath;
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, EmptyImPath, ImPath);
                image = Image.FromFile(imagePath);
                return;
            }
        }
        public void SearchEmployee(Guna2DataGridView dgv_Employee, Guna2TextBox txt_Search)
        {
            try
            {
                dgv_Employee.Rows.Clear();
                var employee = dal_EmployeeManage.GetEmployeeSearch(txt_Search.Text);
                foreach (var item in employee)
                {
                    int index = dgv_Employee.Rows.Add();
                    dgv_Employee.Rows[index].Cells["dgv_EmployeeID"].Value = item.EmployeeID;
                    dgv_Employee.Rows[index].Cells["dgv_EmployeeName"].Value = item.EmployeeName;
                    dgv_Employee.Rows[index].Cells["dgv_EmployeePhone"].Value = item.EmployeePhone;
                    dgv_Employee.Rows[index].Cells["dgv_EmployeeBirth"].Value = item.EmployeeBirth;
                    dgv_Employee.Rows[index].Cells["dgv_PositionName"].Value = item.Position.PositionName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình tìm kiếm là" + ex.Message);
                return;
            }
        }
        public void openLocationImage(object sender, EventArgs e, Guna2PictureBox pic_Employee)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "ALL file |*.*| JPEG file | *.jpg | PNG file | *.png | Bitmap file | *.bmp ";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    var imageFile = ofd.FileName;
                    pic_Employee.Image = Image.FromFile(imageFile);

                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình mở hình ảnh là" + ex.Message);
                return;
            }
        }
        public bool btn_AddOrUpdate_Employee(Guna2TextBox txt_EmployeeID, Guna2TextBox txt_EmployeeName, Guna2TextBox txt_EmployeePhone,
            Guna2TextBox txt_EmployeeBirth, Guna2ComboBox cmb_Position, Guna2PictureBox pic_Employee)
        {
            try
            {
                bool check = false;
                var emloyeeFromDB = dal_EmployeeManage.GetEmployee(txt_EmployeeID.Text);
                int.TryParse(txt_EmployeePhone.Text, out int employeePhone);
                if (employeePhone == 0 || txt_EmployeePhone.Text.Trim().Count() != 10)
                {
                    MessageBox.Show("Số điện thoại nhân viên phải là 10 ký tự và phải là số nguyên");
                    return false;
                }
                else
                {
                    check = true;
                }
                if (txt_EmployeeBirth.Text == "")
                {
                    MessageBox.Show("Ngày sinh nhân viên không được để trống");
                    return false;
                }
                var result = MessageBox.Show("Bạn có muốn Thêm/Sửa nhân viên không", "Xác nhân Thêm/Sửa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    if (emloyeeFromDB != null)
                    {
                        emloyeeFromDB.EmployeeName = txt_EmployeeName.Text;
                        emloyeeFromDB.EmployeePhone = txt_EmployeePhone.Text;
                        emloyeeFromDB.EmployeeBirth = txt_EmployeeBirth.Text;
                        emloyeeFromDB.PositionID = cmb_Position.SelectedValue.ToString();


                        string newImageName = txt_EmployeeID.Text.Trim();
                        string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                        if (!string.IsNullOrEmpty(newImageName))
                        {
                            string imagePath = Path.Combine(projectDirectory, EmployeeImPath);
                            string OldImagePath = Path.Combine(imagePath, txt_EmployeeID.Text + ".jpg");
                            if (File.Exists(OldImagePath))
                            {
                                bool checkImage = false;
                                try
                                {
                                    // Sử dụng phương thức Delete để xóa tệp tin
                                    File.Delete(OldImagePath);
                                }
                                catch
                                {
                                    checkImage = true;
                                }
                                if (checkImage == false)
                                {
                                    Image currentImage = pic_Employee.Image;
                                    if (currentImage != null)
                                    {
                                        // Tạo đường dẫn tới hình ảnh mới
                                        string newImagePath = Path.Combine(imagePath, newImageName + ".jpg");
                                        // Lưu hình ảnh mới với tên đã thay đổi
                                        currentImage.Save(newImagePath, ImageFormat.Jpeg);
                                        checkImage = false;
                                    }
                                }
                            }
                            else
                            {
                                Image currentImage = pic_Employee.Image;
                                if (currentImage != null)
                                {
                                    // Tạo đường dẫn tới hình ảnh mới
                                    string newImagePath = Path.Combine(imagePath, newImageName + ".jpg");
                                    // Lưu hình ảnh mới với tên đã thay đổi
                                    currentImage.Save(newImagePath, ImageFormat.Jpeg);
                                }
                            }

                        }
                        emloyeeFromDB.EmployeeImage = txt_EmployeeID.Text + ".jpg";
                        dal_EmployeeManage.EmployeeAddOrUpdate(emloyeeFromDB);
                        MessageBox.Show("Sửa thông tin nhân viên thành công!");
                        txt_EmployeeID.Text = "";
                        txt_EmployeeName.Clear();
                        txt_EmployeePhone.Clear();
                        txt_EmployeeBirth.Clear();
                        cmb_Position.SelectedIndex = 0;
                        return true;
                    }
                    else
                    {
                        if (check == true)
                        {
                            var employee = dal_EmployeeManage.NewEmployee();
                            int lastEmployeeID = (dal_EmployeeManage.GetLastIndexOfEmployee() + 1);
                            string newEmployeeID = "NV" + lastEmployeeID.ToString();
                            employee.EmployeeID = newEmployeeID;
                            employee.EmployeeName = txt_EmployeeName.Text;
                            employee.EmployeePhone = txt_EmployeePhone.Text;
                            employee.EmployeeBirth = txt_EmployeeBirth.Text;
                            employee.Deleted = false;
                            employee.PositionID = cmb_Position.SelectedValue.ToString();
                            string newImageName = newEmployeeID;
                            string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                            if (!string.IsNullOrEmpty(newImageName))
                            {
                                Image currentImage = pic_Employee.Image;
                                if (currentImage != null)
                                {

                                    string imagePath = Path.Combine(projectDirectory, EmployeeImPath);
                                    // Tạo đường dẫn tới hình ảnh mới
                                    string newImagePath = Path.Combine(imagePath, newImageName + ".jpg");
                                    // Lưu hình ảnh mới với tên đã thay đổi
                                    currentImage.Save(newImagePath, ImageFormat.Jpeg);
                                }
                            }
                            employee.EmployeeImage = newEmployeeID + ".jpg";
                            dal_EmployeeManage.EmployeeAddOrUpdate(employee);
                            MessageBox.Show("Thêm nhân viên thành công!");
                            txt_EmployeeID.Clear();
                            txt_EmployeeName.Clear();
                            txt_EmployeePhone.Clear();
                            txt_EmployeeBirth.Clear();
                            cmb_Position.SelectedIndex = 0;
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình Thêm/Sửa là" + ex.Message);
                return false;
            }
        }
        public bool btn_DeleteEmployee(Guna2TextBox txt_EmployeeID)
        {
            try
            {
                var result = MessageBox.Show("Bạn có muốn xóa nhân viên không", "Xác nhân xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    dal_EmployeeManage.DeleteEmployee(txt_EmployeeID.Text);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xóa là" + ex.Message);
                return false;
            }
        }
        static string user;
        public void saveUser(string txt_Username)
        {
            user = txt_Username;
        }
        public string GetNameEmployeesByUsername()
        {
            try
            {
                var listAccount = dAL_Login.GetAccountList();
                foreach (var account in listAccount)
                {
                    if (account.Username == user)
                    {
                        return account.Employee.EmployeeName;
                    }

                }
                return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình hiện tên nhân viên là" + ex.Message);
                return null;
            }
        }
    }
}

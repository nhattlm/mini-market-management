﻿using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Administrator;
using MiniMarketManagement.GUI.Administrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniMarketManagement.BLL.Administrator
{
    internal class BLL_LoyalCustomer
    {
        public void ShowCustomerList(Guna2DataGridView dgv_LoyalCustomers)
        {
            DAL_LoyalCustomer dAL_LoyalCustomer = new DAL_LoyalCustomer();
            dgv_LoyalCustomers.Rows.Clear();
            foreach (var item in dAL_LoyalCustomer.GetCustomerList())
            {
                int index = dgv_LoyalCustomers.Rows.Add();
                dgv_LoyalCustomers.Rows[index].Cells["CustomerID"].Value = item.CustomerID;
                dgv_LoyalCustomers.Rows[index].Cells["CustomerName"].Value = item.CustomerName;
                dgv_LoyalCustomers.Rows[index].Cells["CustomerPhoneNumber"].Value = item.PhoneNumber;
                dgv_LoyalCustomers.Rows[index].Cells["CustomerPoint"].Value = item.Point;
            }
        }

        public bool IsStringNumberValid(string phoneNumber)
        {
            foreach (char c in phoneNumber)
            {
                if (c < '0' || c > '9')
                {
                    return false;
                }
            }
            return true;
        }
        public void UpdateCustomer(string CustomerID, string CustomerName, string CustomerPhoneNumber, string CustomerPoint)
        {
            DAL_LoyalCustomer dAL_LoyalCustomer = new DAL_LoyalCustomer();
            if (CustomerName == null || CustomerName.Any(char.IsDigit) || CustomerPhoneNumber == null
                || !IsStringNumberValid(CustomerPhoneNumber) || !IsStringNumberValid(CustomerPoint) || CustomerPhoneNumber.Length != 10)
            {
                MessageBox.Show("Thông tin nhập vào không đúng, vui lòng kiểm tra lại!", "Thông Báo");
                return;
            }
            try
            {
                dAL_LoyalCustomer.UpdateCustomer(CustomerID, CustomerName, CustomerPhoneNumber, float.Parse(CustomerPoint));
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra, vui lòng kiểm tra thông tin khách hàng!", "Thông Báo");
            }
        }
        public void DeleteCustomer(string CustomerID)
        {
            DAL_LoyalCustomer dAL_LoyalCustomer = new DAL_LoyalCustomer();
            try
            {
                dAL_LoyalCustomer.DeleteCustomer(CustomerID);
            }
            catch (Exception)
            {
                MessageBox.Show("Có lỗi xảy ra!", "Thông Báo");
            }
        }
        public int TotalLoyalCustomer()
        {
            try
            {
            DAL_LoyalCustomer dAL_LoyalCustomer = new DAL_LoyalCustomer();
            return dAL_LoyalCustomer.TotalLoyalCustomer();
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi khi truy vấn cơ sở dữ liệu!", "Thông báo");
                return -1;
            }
        }
        public int TotalCustomer()
        {
            try
            {
                DAL_LoyalCustomer dAL_LoyalCustomer = new DAL_LoyalCustomer();
                return dAL_LoyalCustomer.TotalCustomer();
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi khi truy vấn cơ sở dữ liệu!", "Thông báo");
                return -1;
            }
        }
        public void SearchCustomersByIDOrPhoneNumber(string Name, Guna2DataGridView dgv_LoyalCustomers)
        {
            DAL_LoyalCustomer dAL_LoyalCustomer = new DAL_LoyalCustomer();
            dgv_LoyalCustomers.Rows.Clear();
            foreach (var item in dAL_LoyalCustomer.SearchCustomersByIDOrPhoneNumber(Name))
            {
                int index = dgv_LoyalCustomers.Rows.Add();
                dgv_LoyalCustomers.Rows[index].Cells["CustomerID"].Value = item.CustomerID;
                dgv_LoyalCustomers.Rows[index].Cells["CustomerName"].Value = item.CustomerName;
                dgv_LoyalCustomers.Rows[index].Cells["CustomerPhoneNumber"].Value = item.PhoneNumber;
                dgv_LoyalCustomers.Rows[index].Cells["CustomerPoint"].Value = item.Point;
            }
        }
    }
}

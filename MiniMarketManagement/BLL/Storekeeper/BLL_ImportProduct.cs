﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Guna.UI2.WinForms;
using MiniMarketManagement.BLL.Cashier;
using MiniMarketManagement.DAL.Entities;
using MiniMarketManagement.DAL.Storekeeper;
using MiniMarketManagement.DAL.Login;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;
using System.Web.UI.Design.WebControls;
using System.Drawing.Printing;

namespace MiniMarketManagement.BLL.Storekeeper
{
    internal class BLL_ImportProduct
    {
        const string ProductImPath = "Assets\\ProductImage\\";
        const string EmptyImPath = "Resources\\image (2).png";
        public void FillDGV(Guna2DataGridView dgv_ListProduct)
        {
            try
            {
                DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
                var list = dAL_ImportProduct_Storekeeper.GetProductListinProducts();
                dgv_ListProduct.Rows.Clear();
                foreach (var item in list)
                {
                    int index = dgv_ListProduct.Rows.Add();
                    dgv_ListProduct.Rows[index].Cells["ProductID"].Value = item.ProductID;
                    dgv_ListProduct.Rows[index].Cells["Product__Name"].Value = item.ProductName;
                    dgv_ListProduct.Rows[index].Cells["ProductTypeName"].Value = item.ProductType.ProductTypeName;
                    dgv_ListProduct.Rows[index].Cells["BuyPrice"].Value = item.BuyPrice;
                    dgv_ListProduct.Rows[index].Cells["CalculationUnitName"].Value = item.ProductType.CalculationUnitName;
                    dgv_ListProduct.Rows[index].Cells["SupplierName"].Value = item.Supplier.SupplierName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xổ dữ liệu ra bảng là: " + ex.Message);
                return;
            }
        }
        public void SearchProduct(string txt, Guna2DataGridView dgv_ListProduct, string fieldID, string fieldName)
        {
            try
            {
                List<int> rowIndexesToShow = new List<int>();
                for (int i = 0; i < dgv_ListProduct.Rows.Count; i++)
                {
                    dgv_ListProduct.Rows[i].Visible = false;
                    if ((string.IsNullOrWhiteSpace(txt) || dgv_ListProduct.Rows[i].Cells[fieldID].Value.ToString().Trim().ToLower().Contains(txt.Trim().ToLower())) ||
                    (string.IsNullOrWhiteSpace(txt) || dgv_ListProduct.Rows[i].Cells[fieldName].Value.ToString().Trim().ToLower().Contains(txt.Trim().ToLower()))
                       )
                    {
                        rowIndexesToShow.Add(i);
                    }
                }
                foreach (int rowIndex in rowIndexesToShow)
                {
                    dgv_ListProduct.Rows[rowIndex].Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình tìm kiếm là: " + ex.Message);
                return;
            }
        }
        public void FillPicboxAndTxtProductIDAndTxtName(int row, Guna2DataGridView dgv_ListProduct, out Image image, out string ProductID, out string ProductName, string fieldID, string fieldName)
        {
            try
            {
                ProductID = dgv_ListProduct.Rows[row].Cells[fieldID].Value.ToString();
                ProductName = dgv_ListProduct.Rows[row].Cells[fieldName].Value.ToString();
                DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
                var product = dAL_ImportProduct_Storekeeper.GetProductByProductID(ProductID);
                var ImPath = product.ProductImage;
                string imagePath;
                if (string.IsNullOrEmpty(ImPath))
                {
                    string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    imagePath = Path.Combine(projectDirectory, EmptyImPath);
                }
                else
                {
                    // Xác định đường dẫn tới hình ảnh sử dụng đường dẫn tương đối
                    string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    imagePath = Path.Combine(projectDirectory, ProductImPath, ImPath);
                }
                image = Image.FromFile(imagePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xổ dữ liệu ra txtbox là: " + ex.Message);
                ProductID = "";
                ProductName = "";
                string projectDirectory1 = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                string imagepath1 = Path.Combine(projectDirectory1, EmptyImPath);
                image = Image.FromFile(imagepath1);
                return;
            }
            
        }
        public void ClearInputFields(out string txtID, out Image picbProduct, out string txtQuantity, out string txtName)
        {
            txtID = "";
            txtQuantity = "";
            txtName = "";
            string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string imagepath = Path.Combine(projectDirectory, EmptyImPath);
            picbProduct = Image.FromFile(imagepath);
        }
        public int GetRowIndex(Guna2DataGridView dgv_ListSelectedProduct, string ProductID, string fieldID)
        {
            for (int i = 0; i < dgv_ListSelectedProduct.Rows.Count; i++)
            {
                if (dgv_ListSelectedProduct.Rows[i].Cells[fieldID].Value.ToString().Trim() == ProductID.Trim())
                {
                    return i;
                }
            }
            return -1;
        }
        public int importProductToSelectedListByProductID(Guna2DataGridView dgv_ListSelectedProduct, string ProductID, string Quantity)
        {
            if (string.IsNullOrEmpty(ProductID))
            {
                MessageBox.Show("Vui lòng chọn ít nhất một sản phẩm trong danh sách");
                return 0;
            }
            DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
            var product = dAL_ImportProduct_Storekeeper.GetProductByProductID(ProductID);
            if (string.IsNullOrEmpty(Quantity.ToString()))
            {
                MessageBox.Show("Vui lòng không để trống số lượng");
                return 0;
            }
            double quantity;
            bool check = double.TryParse(Quantity, out quantity);
            if (!check)
            {
                MessageBox.Show("Vui lòng chỉ nhập số cho số lượng");
                return 0;
            }
            try
            {
                if (product == null)
                {
                    MessageBox.Show("Không có mã sản phẩm bạn muốn thêm!!");
                    return 0;
                }
                else
                {
                    int selectedRow = GetRowIndex(dgv_ListSelectedProduct, product.ProductID, "Product_ID");
                    if (selectedRow == -1)
                    {
                        int index = dgv_ListSelectedProduct.Rows.Add();
                        dgv_ListSelectedProduct.Rows[index].Cells["Product_ID"].Value = product.ProductID;
                        dgv_ListSelectedProduct.Rows[index].Cells["Product_Name"].Value = product.ProductName;
                        dgv_ListSelectedProduct.Rows[index].Cells["Product_Type_Name"].Value = product.ProductType.ProductTypeName;
                        dgv_ListSelectedProduct.Rows[index].Cells["Buy_Price"].Value = product.BuyPrice;
                        dgv_ListSelectedProduct.Rows[index].Cells["Calculation_Unit_Name"].Value = product.ProductType.CalculationUnitName;
                        dgv_ListSelectedProduct.Rows[index].Cells["Product_Quantity"].Value = Quantity;
                        dgv_ListSelectedProduct.Rows[index].Cells["Supplier"].Value = product.Supplier.SupplierName;
                        return 1;
                    }
                    else
                    {
                        double sum = quantity + double.Parse(dgv_ListSelectedProduct.Rows[selectedRow].Cells["Product_Quantity"].Value.ToString().Trim());
                        dgv_ListSelectedProduct.Rows[selectedRow].Cells["Product_Quantity"].Value = sum;
                        return 1;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi khi thêm đó là: " + ex.InnerException.Message);
                return 0;
            }
        }
        public int MinusQuantityOrDeleteProduct(Guna2DataGridView dgv_ListSelectedProduct, string ProductID, string Quantity)
        {
            if (string.IsNullOrEmpty(ProductID))
            {
                MessageBox.Show("Vui lòng chọn ít nhất một sản phẩm trong danh sách");
                return 0;
            }
            DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
            var product = dAL_ImportProduct_Storekeeper.GetProductByProductID(ProductID);
            if (string.IsNullOrEmpty(Quantity.ToString()))
            {
                MessageBox.Show("Vui lòng không để trống số lượng");
                return 0;
            }
            double quantity;
            bool check = double.TryParse(Quantity, out quantity);
            if (!check)
            {
                MessageBox.Show("Vui lòng chỉ nhập số cho số lượng");
                return 0;
            }
            try
            {
                int selectedRow = GetRowIndex(dgv_ListSelectedProduct, product.ProductID, "Product_ID");
                if (selectedRow == -1)
                {
                    MessageBox.Show("Không có mặt hàng bạn muốn bớt/xóa!!");
                    return 0;
                }
                else
                {
                    double quantityOfSelectedProduct = double.Parse(dgv_ListSelectedProduct.Rows[selectedRow].Cells["Product_Quantity"].Value.ToString().Trim());
                    if (quantity > quantityOfSelectedProduct)
                    {
                        MessageBox.Show("Số lượng bạn nhập lớn hơn số lượng của mặt hàng bạn đang chọn!!");
                        return 0;
                    }
                    else
                    {
                        if (quantity == quantityOfSelectedProduct)
                        {
                            var result = MessageBox.Show("Bạn nhập số lượng bằng số lượng của mặt hàng bạn dự tính mua!!\nNếu nhấn Yes sẽ xóa nó!!", "Xóa", MessageBoxButtons.YesNo);
                            if (result == DialogResult.Yes)
                            {
                                dgv_ListSelectedProduct.Rows.RemoveAt(selectedRow);
                                return 1;
                            }
                            return 0;
                        }
                        else
                        {
                            double Subtraction = quantityOfSelectedProduct - quantity;
                            dgv_ListSelectedProduct.Rows[selectedRow].Cells["Product_Quantity"].Value = Subtraction;
                            return 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi khi bớt/xóa đó là: " + ex.InnerException.Message);
                return 0;
            }
        }
        public void DisplayLowQuantityProduct(Guna2DataGridView dgv_ListProduct, string productIDField)
        {
            try
            {
                List<int> rowIndexesToShow = new List<int>();
                DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
                for (int i = 0; i < dgv_ListProduct.Rows.Count; i++)
                {
                    var product = dAL_ImportProduct_Storekeeper.GetProductByProductIDinRepository(dgv_ListProduct.Rows[i].Cells[productIDField].Value.ToString().Trim());
                    if (product == null)
                    {
                        dgv_ListProduct.Rows[i].Visible = false;
                    }
                    else
                    {
                        dgv_ListProduct.Rows[i].Visible = false;
                        if (product.ProductQuantity <= 100 && dgv_ListProduct.Rows[i].Cells[productIDField].Value.ToString().Trim() == product.ProductID.Trim())
                        {
                            rowIndexesToShow.Add(i);
                        }
                    }
                }
                foreach (int rowIndex in rowIndexesToShow)
                {
                    dgv_ListProduct.Rows[rowIndex].Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình hiển thị sản phẩm sắp hết là: " + ex.Message);
                return;
            }
        }
        public double sumBuyPriceOfSelectedProductsList(Guna2DataGridView dgv_ListSelectedProduct, string BuyPriceField, string QuantityField)
        {
            double sum = 0;
            for (int i = 0; i < dgv_ListSelectedProduct.Rows.Count; i++)
            {
                sum += (double.Parse(dgv_ListSelectedProduct.Rows[i].Cells[BuyPriceField].Value.ToString()) * double.Parse(dgv_ListSelectedProduct.Rows[i].Cells[QuantityField].Value.ToString()));
            }
            return sum;
        }
        static string user;
        public void saveUser(string txt_Username)
        {
            user = txt_Username;

        }
        public string GetEmployeeIDByUserName()
        {
            DAL_Login dAL_Login = new DAL_Login();
            var accountsList = dAL_Login.GetAccountList();
            foreach (var account in accountsList)
            {
                if (account.Username == user)
                {
                    return account.EmployeeID;
                }
            }
            return null;
        }
        List<ImportProductCard> listIPCS = new List<ImportProductCard>();
        public int AddSelectedProductsToListIPCAndDIPC(Guna2DataGridView dgv_SelectedProduct, Guna2DataGridView dgv_ListProducts)
        {
            var result = MessageBox.Show("Bạn có chắc muốn thanh toán", "Thanh toán", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                try
                {
                    DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
                    string EID = GetEmployeeIDByUserName();
                    List<ImportProductCard> listIPC = new List<ImportProductCard>();
                    List<DetailImportProductCard> listDIPC = new List<DetailImportProductCard>();
                    List<int> rowsToRemove = new List<int>(); // Danh sách các dòng cần xóa

                    int index = (dAL_ImportProduct_Storekeeper.GetLastIndexOfImportProductCard() + 1);
                    for (int row = 0; row < dgv_SelectedProduct.Rows.Count; row++)
                    {
                        var product = dAL_ImportProduct_Storekeeper.GetProductByProductID(dgv_SelectedProduct.Rows[row].Cells["Product_ID"].Value.ToString());
                        if (!listIPC.Any(ipc => ipc.SupplierID == product.SupplierID))
                        {
                            ImportProductCard IPC = new ImportProductCard();
                            IPC.ImportProductID = "IP" + index.ToString();
                            IPC.SupplierID = product.SupplierID;
                            IPC.ImportProductDate = DateTime.Now;
                            IPC.EmployeeID = EID;
                            listIPC.Add(IPC);
                            index++;
                        }

                        DetailImportProductCard DIPC = new DetailImportProductCard();
                        DIPC.ImportProductID = listIPC.Last().ImportProductID;
                        DIPC.ProductID = product.ProductID;
                        DIPC.Quantity = double.Parse(dgv_SelectedProduct.Rows[row].Cells["Product_Quantity"].Value.ToString());
                        DIPC.Price = double.Parse(dgv_SelectedProduct.Rows[row].Cells["Buy_Price"].Value.ToString());
                        DIPC.Returned = false;
                        // Kiểm tra xem sản phẩm đã có trong listDIPC chưa
                        if (!listDIPC.Any(d => d.ProductID == DIPC.ProductID))
                        {
                            listDIPC.Add(DIPC);
                            rowsToRemove.Add(row);
                        }

                        for (int i = row + 1; i < dgv_SelectedProduct.Rows.Count; i++)
                        {
                            var productI = dAL_ImportProduct_Storekeeper.GetProductByProductID(dgv_SelectedProduct.Rows[i].Cells["Product_ID"].Value.ToString());
                            if (productI.SupplierID.Trim() == product.SupplierID.Trim())
                            {
                                DetailImportProductCard DIPCI = new DetailImportProductCard();
                                DIPCI.ImportProductID = listIPC.Last().ImportProductID;
                                DIPCI.ProductID = productI.ProductID;
                                DIPCI.Quantity = double.Parse(dgv_SelectedProduct.Rows[i].Cells["Product_Quantity"].Value.ToString());
                                DIPCI.Price = double.Parse(dgv_SelectedProduct.Rows[i].Cells["Buy_Price"].Value.ToString());
                                DIPCI.Returned = false;
                                // Kiểm tra xem sản phẩm đã có trong listDIPC chưa
                                if (!listDIPC.Any(d => d.ProductID == DIPCI.ProductID))
                                {
                                    listDIPC.Add(DIPCI);
                                    rowsToRemove.Add(row);
                                }
                            }
                        }
                    }
                    foreach (var list in listIPC)
                    {
                        double sum = 0;
                        foreach (var item in listDIPC)
                        {
                            if (item.ImportProductID == list.ImportProductID)
                            {
                                sum += (item.Price * item.Quantity);
                            }
                        }
                        list.PriceTotal = sum;
                    }
                    listIPCS = listIPC;
                    if (listIPC.Count > 0)
                    {
                        dAL_ImportProduct_Storekeeper.AddImportProductCardAndDetailImportProductCard(listIPC, listDIPC);
                        FillDGV(dgv_ListProducts);
                        MessageBox.Show("Đã thanh toán thành công");
                        rowsToRemove.Sort();
                        // Xóa các dòng đã được xác định
                        for (int i = rowsToRemove.Count - 1; i >= 0; i--)
                        {
                            dgv_SelectedProduct.Rows.RemoveAt(rowsToRemove[i]);
                        }
                        return 1;
                    }
                    else
                    {
                        MessageBox.Show("Vui lòng thêm ít nhất 1 sản phẩm!!");
                        return 0;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Có lỗi trong quá trình thanh toán đó là: " + ex.Message);

                    return 0;
                }
            }
            else
            {
                return 0;
            }
        }
        public void GetListImportProductCards(out List<ImportProductCard> l)
        {
            l = listIPCS;
        }
        public void CreateImport(object sender, PrintPageEventArgs e, ImportProductCard importProductCard,
            PrintDocument printImport, PrintPreviewDialog printdiaImport)
        {
            try
            {
                DAL_ImportProduct_Storekeeper dAL_ImportProduct_ = new DAL_ImportProduct_Storekeeper();
                var import = dAL_ImportProduct_.GetImportByImportID(importProductCard.ImportProductID);
                var getListDetailImport = dAL_ImportProduct_.GetDetailImportProductByID(importProductCard.ImportProductID);


                Graphics graphics = e.Graphics;
                var w = printImport.DefaultPageSettings.PaperSize.Width;

                graphics.DrawString("Mini Marker Sinh Tố Dâu", new Font("Courier New", 30, FontStyle.Bold), Brushes.Black, new Point(100, 20));

                graphics.DrawString("HÓA ĐƠN NHẬP HÀNG", new Font("Courier New", 25, FontStyle.Bold), Brushes.Black, new Point(190, 60));

                graphics.DrawString(String.Format("Ngày {0}", DateTime.Now.ToString("dd/MM/yyyy HH:mm")),
                    new Font("Courier New", 20, FontStyle.Bold), Brushes.Black, new Point(100, 100));

                Pen blackpen = new Pen(Color.Black, 2);
                var y = 140;

                Point p1 = new Point(10, y);
                Point p2 = new Point(w - 10, y);
                graphics.DrawLine(blackpen, p1, p2);


                Font font = new Font("Courier New", 20);

                int startX = 50;
                int startY = 120;
                int offset = 40;
                // Vẽ các thông tin hóa đơn, ví dụ:
                startY += offset;
                graphics.DrawString($"Nhân viên: {import.Employee.EmployeeName}", font, Brushes.Black, new PointF(startX, startY));
                startY += offset;
                graphics.DrawString($"Nhà cung cấp: {import.Supplier.SupplierName}", font, Brushes.Black, new PointF(startX, startY));
                startY += offset;
                graphics.DrawString($"Số hóa đơn: {import.ImportProductID}", font, Brushes.Black, new PointF(startX, startY));

                startY += offset;
                graphics.DrawString("--------------------------------------------", font, Brushes.Black, new PointF(startX, startY));
                startY += offset;
                graphics.DrawString("Sản Phẩm     Giá      Số Lượng     DVT", new Font("Courier New", 20), Brushes.Black, new Point(startX, startY));


                // Vẽ các sản phẩm trong hóa đơn
                foreach (var detailImport in getListDetailImport)
                {
                    startY += offset;
                    graphics.DrawString($"{detailImport.Product.ProductName}", font, Brushes.Black, new PointF(startX, startY));
                    graphics.DrawString($"{detailImport.Product.BuyPrice.ToString().Trim()}đ", font, Brushes.Black, new PointF(startX + 222, startY));
                    graphics.DrawString($"{detailImport.Quantity}", font, Brushes.Black, new PointF(startX + 378, startY));
                    graphics.DrawString($"{detailImport.Product.ProductType.CalculationUnitName}", font, Brushes.Black, new PointF(startX + 600, startY));
                }

                startY += offset;
                graphics.DrawString("--------------------------------------------", font, Brushes.Black, new PointF(startX, startY));
                // Tổng tiền
                startY += offset;
                graphics.DrawString($"Tổng cộng: {import.PriceTotal.ToString().Trim()}đ", font, Brushes.Black, new PointF(startX, startY));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình in hóa đơn đó là: " + ex.Message);
                return;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.Design;
using System.Windows.Forms;
using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Entities;
using MiniMarketManagement.DAL.Storekeeper;
namespace MiniMarketManagement.BLL.Storekeeper
{
    internal class BLL_AddSupplierAndProducts
    {
        const string ProductImPath = "Assets\\ProductImage\\";
        const string EmptyImPath = "Resources\\image (2).png";
        public void FillDGVProduct(Guna2DataGridView dgv_ListProduct)
        {
            try
            {
                DAL_AddSupplierAndProducts_Storekeeper dAL_AddSupplierAndProducts_Storekeeper = new DAL_AddSupplierAndProducts_Storekeeper();
                var list = dAL_AddSupplierAndProducts_Storekeeper.GetProductslist();
                dgv_ListProduct.Rows.Clear();
                foreach (var item in list)
                {
                    int index = dgv_ListProduct.Rows.Add();
                    dgv_ListProduct.Rows[index].Cells["Product_ID"].Value = item.ProductID;
                    dgv_ListProduct.Rows[index].Cells["Product_Name"].Value = item.ProductName;
                    dgv_ListProduct.Rows[index].Cells["Product_Type_Name"].Value = item.ProductType.ProductTypeName;
                    dgv_ListProduct.Rows[index].Cells["Buy_Price"].Value = item.BuyPrice;
                    dgv_ListProduct.Rows[index].Cells["Supplier"].Value = item.Supplier.SupplierName;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xổ dữ liệu ra bảng product là: " + ex.Message);
                return;
            }
        }
        public void FillDGVSupplier(Guna2DataGridView dgv_ListSupplier)
        {
            try
            {
                DAL_AddSupplierAndProducts_Storekeeper dAL_AddSupplierAndProducts_Storekeeper = new DAL_AddSupplierAndProducts_Storekeeper();
                var list = dAL_AddSupplierAndProducts_Storekeeper.GetSupplierslist();
                dgv_ListSupplier.Rows.Clear();
                foreach (var item in list)
                {
                    int index = dgv_ListSupplier.Rows.Add();
                    dgv_ListSupplier.Rows[index].Cells["SupplierID"].Value = item.SupplierID;
                    dgv_ListSupplier.Rows[index].Cells["SupplierName"].Value = item.SupplierName;
                    dgv_ListSupplier.Rows[index].Cells["SupplierPhoneNumber"].Value = item.PhoneNumber;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xổ dữ liệu ra bảng supplier là: " + ex.Message);
                return;
            }
        }
        public void FillCBProductType(Guna2ComboBox comboBox)
        {
            try
            {
                DAL_AddSupplierAndProducts_Storekeeper dAL_AddSupplierAndProducts_Storekeeper = new DAL_AddSupplierAndProducts_Storekeeper();
                var list = dAL_AddSupplierAndProducts_Storekeeper.GetProductTypeslist();
                comboBox.DataSource = list;
                comboBox.DisplayMember = "ProductTypeName";
                comboBox.ValueMember = "ProductTypeID";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xổ dữ liệu ra combobox là: " + ex.Message);
                return;
            }
        }
        public void SearchProductOrSupplierByIDAOrNameOfThem(string txt, Guna2DataGridView dgv_ListProduct)
        {
            try
            {
                List<int> rowIndexesToShow = new List<int>();
                for (int i = 0; i < dgv_ListProduct.Rows.Count; i++)
                {
                    dgv_ListProduct.Rows[i].Visible = false;
                    if ((string.IsNullOrWhiteSpace(txt) || dgv_ListProduct.Rows[i].Cells["Product_ID"].Value.ToString().Trim().ToLower().Contains(txt.Trim().ToLower())) ||
                    (string.IsNullOrWhiteSpace(txt) || dgv_ListProduct.Rows[i].Cells["Product_Name"].Value.ToString().Trim().ToLower().Contains(txt.Trim().ToLower())) ||
                    (string.IsNullOrWhiteSpace(txt) || dgv_ListProduct.Rows[i].Cells["Supplier"].Value.ToString().Trim().ToLower().Contains(txt.Trim().ToLower()))
                       )
                    {
                        rowIndexesToShow.Add(i);
                    }
                }
                foreach (int rowIndex in rowIndexesToShow)
                {
                    dgv_ListProduct.Rows[rowIndex].Visible = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình tìm kiếm là: " + ex.Message);
                return;
            }
        }

        public void FillAllTxtAndPicboxOfProductExceptTxtSearchAndTxtSupplier(int row, Guna2DataGridView dgv_ListProduct,
                                                       out Image image, out string SupplierID, out string SupplierName, out string SupplierPhoneNumber
                                                       , out string productID, out string productName, out string ProductType, out string BuyPrice)
        {
            try
            {
                DAL_AddSupplierAndProducts_Storekeeper dAL_AddSupplierAndProducts_Storekeeper = new DAL_AddSupplierAndProducts_Storekeeper();
                DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
                productID = dgv_ListProduct.Rows[row].Cells["Product_ID"].Value.ToString();
                var product = dAL_ImportProduct_Storekeeper.GetProductByProductID(productID);
                var Supplier = dAL_AddSupplierAndProducts_Storekeeper.GetSupplierBySupplierID(product.SupplierID);
                SupplierID = Supplier.SupplierID;
                SupplierName = Supplier.SupplierName;
                SupplierPhoneNumber = Supplier.PhoneNumber.Trim();
                productName = dgv_ListProduct.Rows[row].Cells["Product_Name"].Value.ToString();
                ProductType = dgv_ListProduct.Rows[row].Cells["Product_Type_Name"].Value.ToString();
                BuyPrice = dgv_ListProduct.Rows[row].Cells["Buy_Price"].Value.ToString();
                var ImPath = product.ProductImage;
                string imagePath;
                if (string.IsNullOrEmpty(ImPath))
                {
                    string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    imagePath = Path.Combine(projectDirectory, EmptyImPath);
                }
                else
                {
                    // Xác định đường dẫn tới hình ảnh sử dụng đường dẫn tương đối
                    string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    imagePath = Path.Combine(projectDirectory, ProductImPath, ImPath);
                }
                image = Image.FromFile(imagePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xổ dữ liệu ra txtbox là: " + ex.Message);
                SupplierID = "";
                SupplierName = "";
                SupplierPhoneNumber = "";
                productID = "";
                productName = "";
                ProductType = "";
                BuyPrice = "";
                string projectDirectory1 = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                string imagepath1 = Path.Combine(projectDirectory1, EmptyImPath);
                image = Image.FromFile(imagepath1);
                return;
            }
        }
        public void FillAllTxtBoxOfSupplierExcepttxtProductAndTxtSearch(int row, Guna2DataGridView dgv_ListProduct, out Image image
                                                      , out string SupplierID, out string SupplierName, out string SupplierPhoneNumber)
        {
            try
            {
                DAL_AddSupplierAndProducts_Storekeeper dAL_AddSupplierAndProducts_Storekeeper = new DAL_AddSupplierAndProducts_Storekeeper();
                DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
                string productID = dgv_ListProduct.Rows[row].Cells["Product_ID"].Value.ToString();
                var product = dAL_ImportProduct_Storekeeper.GetProductByProductID(productID);
                var Supplier = dAL_AddSupplierAndProducts_Storekeeper.GetSupplierBySupplierID(product.SupplierID);
                SupplierID = Supplier.SupplierID;
                SupplierName = Supplier.SupplierName;
                SupplierPhoneNumber = Supplier.PhoneNumber.Trim();
                string imagePath;
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, EmptyImPath);
                image = Image.FromFile(imagePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xổ dữ liệu ra txtbox là: " + ex.Message);
                SupplierID = "";
                SupplierName = "";
                SupplierPhoneNumber = "";
                string projectDirectory1 = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                string imagepath1 = Path.Combine(projectDirectory1, EmptyImPath);
                image = Image.FromFile(imagepath1);
                return;
            }
        }
        public void FillAllTxtBoxOfSupplierExcepttxtProductAndTxtSearchfordgvSupplier(int row, Guna2DataGridView dgv_ListProduct, out Image image
                                                      , out string SupplierID, out string SupplierName, out string SupplierPhoneNumber)
        {
            try
            {
                DAL_AddSupplierAndProducts_Storekeeper dAL_AddSupplierAndProducts_Storekeeper = new DAL_AddSupplierAndProducts_Storekeeper();
                DAL_ImportProduct_Storekeeper dAL_ImportProduct_Storekeeper = new DAL_ImportProduct_Storekeeper();
                SupplierID = dgv_ListProduct.Rows[row].Cells["SupplierID"].Value.ToString();
                SupplierName = dgv_ListProduct.Rows[row].Cells["SupplierName"].Value.ToString();
                SupplierPhoneNumber = dgv_ListProduct.Rows[row].Cells["SupplierPhoneNumber"].Value.ToString().Trim();
                string imagePath;
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, EmptyImPath);
                image = Image.FromFile(imagePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xổ dữ liệu ra txtbox là: " + ex.Message);
                SupplierID = "";
                SupplierName = "";
                SupplierPhoneNumber = "";
                string projectDirectory1 = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                string imagepath1 = Path.Combine(projectDirectory1, EmptyImPath);
                image = Image.FromFile(imagepath1);
                return;
            }
        }
        public void ClearInputFields(out Image picbProduct, out string SupplierID, out string SupplierName, out string SupplierPhoneNumber
                                                       , out string productID, out string productName, out string ProductType, out string BuyPrice)
        {
            SupplierID = "";
            SupplierName = "";
            SupplierPhoneNumber = "";
            productID = "";
            productName = "";
            ProductType = "";
            BuyPrice = "";
            string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
            string imagepath = Path.Combine(projectDirectory, EmptyImPath);
            picbProduct = Image.FromFile(imagepath);
        }
        public string AddImg()
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "ALL file |*.*| JPEG file | *.jpg | PNG file | *.png | Bitmap file | *.bmp ";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    return ofd.FileName;
                }
                return null;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình mở lấy ảnh là: " + ex.Message);
                return null;
            }
        }
        public void openLocationImage(Guna2PictureBox pic_Product)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "ALL file |*.*| JPEG file | *.jpg | PNG file | *.png | Bitmap file | *.bmp ";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    var imageFile = ofd.FileName;
                    pic_Product.Image = Image.FromFile(imageFile);

                }
                else
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình mở hình ảnh là" + ex.Message);
                return;
            }
        }
        public int AddNewSupplierOrNewProduct(bool cbS, bool cbP, string SupplierID, string SupplierName, string SupplierPhoneNumber, string ProductID, string ProductName,
                                               string ProductTypeName, string BuyPrice, Image image)
        {
            DAL_AddSupplierAndProducts_Storekeeper dAL_AddSupplierAndProducts_Storekeeper = new DAL_AddSupplierAndProducts_Storekeeper();
            var result = MessageBox.Show("Bạn có chắc muốn thêm", "Thêm", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                if (cbS)
                {
                    if (string.IsNullOrEmpty(SupplierID) && string.IsNullOrEmpty(SupplierName) && string.IsNullOrEmpty(SupplierPhoneNumber))
                    {
                        MessageBox.Show("Vui lòng không để trống bất kì thông tin nào của nhà cung cấp!!", "Thông báo!!");
                        return 0;
                    }
                    int a;
                    bool check = int.TryParse(SupplierPhoneNumber, out a);
                    if (SupplierPhoneNumber.Length != 10)
                    {
                        MessageBox.Show("Vui lòng nhập số điện thoại có 10 chữ số!!", "Thông báo!!");
                        return 0;
                    }
                    if (!check)
                    {
                        MessageBox.Show("Vui lòng chỉ nhập số cho số điện thoại!!", "Thông báo!!");
                        return 0;
                    }
                    try
                    {
                        var suppliercheck = dAL_AddSupplierAndProducts_Storekeeper.GetSupplierBySupplierID(SupplierID);
                        if (suppliercheck != null)
                        {
                            MessageBox.Show("Nhà cung cấp bạn muốn thêm đã tồn tại!!\nCó phải bạn muốn chỉnh sửa?\nHãy nhấn nút sửa", "Thông báo");
                            return 0;
                        }
                        else
                        {
                            Supplier supplier = new Supplier();
                            supplier.SupplierID = SupplierID;
                            supplier.SupplierName = SupplierName;
                            supplier.PhoneNumber = SupplierPhoneNumber;
                            dAL_AddSupplierAndProducts_Storekeeper.AddSupplier(supplier);
                            MessageBox.Show("Thêm nhà cung cấp thành công!!");
                            return 1;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Có lỗi khi thêm nhà cung cấp đó là: " + ex.InnerException.Message);
                        return 0;
                    }
                }
                if (cbP == true)
                {
                    if (string.IsNullOrEmpty(SupplierID) && string.IsNullOrEmpty(SupplierName) && string.IsNullOrEmpty(SupplierPhoneNumber))
                    {
                        MessageBox.Show("Vui lòng chọn một nhà cung cấp!!", "Thông báo!!");
                        return 0;
                    }
                    if (string.IsNullOrEmpty(ProductID) && string.IsNullOrEmpty(ProductName) && string.IsNullOrEmpty(BuyPrice))
                    {
                        MessageBox.Show("Vui lòng không để trống bất kì thông tin nào của sản phẩm!!", "Thông báo!!");
                        return 0;
                    }
                    if (image == null)
                    {
                        MessageBox.Show("Vui lòng không để trống ảnh của sản phẩm!!", "Thông báo!!");
                        return 0;
                    }
                    bool check = double.TryParse(BuyPrice, out double price);
                    if (!check)
                    {
                        MessageBox.Show("Vui lòng chỉ nhập số cho giá mua!!", "Thông báo!!");
                        return 0;
                    }
                    try
                    {
                        var checkproduct = dAL_AddSupplierAndProducts_Storekeeper.GetProductbyProductID(ProductID);
                        if (checkproduct != null)
                        {
                            MessageBox.Show("Sản phẩm bạn muốn thêm đã tồn tại!!\nCó phải bạn muốn chỉnh sửa?\nHãy nhấn nút sửa", "Thông báo");
                            return 0;
                        }
                        else
                        {
                            var product = new Product();
                            product.ProductID = ProductID;
                            product.ProductName = ProductName;
                            product.SupplierID = SupplierID;
                            product.BuyPrice = price;
                            product.ProductTypeID = ProductTypeName;
                            string newImageName = ProductID.Trim();
                            string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                            if (!string.IsNullOrEmpty(newImageName))
                            {
                                Image currentImage = image;
                                if (currentImage != null)
                                {
                                    string imagePath = Path.Combine(projectDirectory, ProductImPath);
                                    // Tạo đường dẫn tới hình ảnh mới
                                    string newImagePath = Path.Combine(imagePath, newImageName + ".jpg");
                                    // Lưu hình ảnh mới với tên đã thay đổi
                                    currentImage.Save(newImagePath, ImageFormat.Jpeg);
                                }
                            }
                            product.ProductImage = newImageName + ".jpg";
                            dAL_AddSupplierAndProducts_Storekeeper.AddProduct(product);
                            MessageBox.Show("Thêm nhà sản phẩm thành công!!");
                            return 1;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Có lỗi khi thêm sản phẩm đó là: " + ex.InnerException.Message);
                        return 0;
                    }
                }
                MessageBox.Show("Vui lòng chọn checkbox mà bạn muốn chỉnh sửa!!", "Thông báo");
                return 0;
            }
            else
            {
                return 0;
            }
        }
        public int UpdateSupplierOrProduct(bool cbS, bool cbP, string SupplierID, string SupplierName, string SupplierPhoneNumber, string ProductID, string ProductName,
                                               string ProductTypeName, string BuyPrice, Image image)
        {
            DAL_AddSupplierAndProducts_Storekeeper dAL_AddSupplierAndProducts_Storekeeper = new DAL_AddSupplierAndProducts_Storekeeper();
            var result = MessageBox.Show("Bạn có chắc muốn chỉnh sửa", "Thêm", MessageBoxButtons.YesNo);
            if (result == DialogResult.Yes)
            {
                if (cbS)
                {
                    if (string.IsNullOrEmpty(SupplierID) && string.IsNullOrEmpty(SupplierName) && string.IsNullOrEmpty(SupplierPhoneNumber))
                    {
                        MessageBox.Show("Vui lòng không để trống bất kì thông tin nào của nhà cung cấp!!", "Thông báo!!");
                        return 0;
                    }
                    int a;
                    bool check = int.TryParse(SupplierPhoneNumber, out a);
                    if (SupplierPhoneNumber.Length != 10)
                    {
                        MessageBox.Show("Vui lòng nhập số điện thoại có 10 chữ số!!", "Thông báo!!");
                        return 0;
                    }
                    if (!check)
                    {
                        MessageBox.Show("Vui lòng chỉ nhập số cho số điện thoại!!", "Thông báo!!");
                        return 0;
                    }
                    try
                    {
                        var suppliercheck = dAL_AddSupplierAndProducts_Storekeeper.GetSupplierBySupplierID(SupplierID);
                        if (suppliercheck == null)
                        {
                            MessageBox.Show("Nhà cung cấp bạn muốn chỉnh sửa không tồn tại\nCó phải bạn muốn thêm mới\nHãy nhấn nút thêm!!", "Thông báo!!");
                            return 0;
                        }
                        else
                        {
                            dAL_AddSupplierAndProducts_Storekeeper.UpdateSupplier(SupplierID, SupplierName, SupplierPhoneNumber);
                            MessageBox.Show("Đã chỉnh sửa nhà cung cấp thành công");
                            return 1;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Có lỗi khi chỉnh sửa nhà cung cấp đó là: " + ex.InnerException.Message);
                        return 0;
                    }
                }
                if (cbP)
                {
                    if (string.IsNullOrEmpty(SupplierID) && string.IsNullOrEmpty(SupplierName) && string.IsNullOrEmpty(SupplierPhoneNumber))
                    {
                        MessageBox.Show("Vui lòng chọn một nhà cung cấp!!", "Thông báo!!");
                        return 0;
                    }
                    if (string.IsNullOrEmpty(ProductID) && string.IsNullOrEmpty(ProductName) && string.IsNullOrEmpty(BuyPrice))
                    {
                        MessageBox.Show("Vui lòng không để trống bất kì thông tin nào của sản phẩm!!", "Thông báo!!");
                        return 0;
                    }
                    if (image == null)
                    {
                        MessageBox.Show("Vui lòng không để trống ảnh của sản phẩm!!", "Thông báo!!");
                        return 0;
                    }
                    bool check = double.TryParse(BuyPrice, out double price);
                    if (!check)
                    {
                        MessageBox.Show("Vui lòng chỉ nhập số cho giá mua!!", "Thông báo!!");
                        return 0;
                    }
                    try
                    {
                        var productcheck = dAL_AddSupplierAndProducts_Storekeeper.GetProductbyProductID(ProductID);
                        if (productcheck == null)
                        {
                            MessageBox.Show("Sản phẩm bạn muốn chỉnh sửa không tồn tại\nCó phải bạn muốn thêm mới\nHãy nhấn nút thêm!!", "Thông báo!!");
                            return 0;
                        }
                        else
                        {
                            if (productcheck.SupplierID != SupplierID)
                            {
                                MessageBox.Show("Vui lòng chọn đúng nhà cung cấp ban đầu của sản phẩm!!", "Thông báo!!");
                                return 0;
                            }
                            else
                            {

                                string imgName;
                                string newImageName = ProductID.Trim() + ".jpg";
                                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                                if (!string.IsNullOrEmpty(newImageName))
                                {
                                    string imagePath = Path.Combine(projectDirectory, ProductImPath);
                                    string ImagePath = Path.Combine(imagePath, newImageName);
                                    bool checkIMGBUG = false;
                                    try
                                    {
                                        if (File.Exists(ImagePath))
                                        {
                                            // Sử dụng phương thức Delete để xóa tệp tin
                                            File.Delete(ImagePath);
                                        }
                                    }
                                    catch
                                    {
                                        checkIMGBUG = true;
                                    }
                                    if (!checkIMGBUG)
                                    {
                                        Image currentImage = image;
                                        if (currentImage != null)
                                        {
                                            currentImage.Save(ImagePath, ImageFormat.Jpeg);
                                        }
                                        checkIMGBUG = false;
                                    }
                                }
                                image.Dispose();
                                imgName = newImageName;
                                dAL_AddSupplierAndProducts_Storekeeper.UpdateProduct(ProductID, ProductName, ProductTypeName, price, imgName);
                                MessageBox.Show("Đã chỉnh sửa mặt hàng thành công");
                                return 1;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Có lỗi khi chỉnh sửa sản phẩm đó là: " + ex.InnerException.Message);
                        return 0;
                    }
                }
                MessageBox.Show("Vui lòng chọn checkbox mà bạn muốn chỉnh sửa!!", "Thông báo");
                return 0;
            }
            else
            {
                return 0;
            }
        }
    }
}

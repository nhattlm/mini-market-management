﻿using Guna.UI2.WinForms;
using MiniMarketManagement.DAL.Cashier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MiniMarketManagement.DAL.Storekeeper;
using System.IO;
using System.Windows.Forms;
using System.Drawing;
using MiniMarketManagement.DAL.Login;

namespace MiniMarketManagement.BLL.Storekeeper
{
    public class BLL_ProductManager
    {
        DAL_ProductManager_Storekeeper dal_ProductManager = new DAL_ProductManager_Storekeeper();
        const string ProductImPath = "Assets\\ProductImage\\";
        const string EmptyImPath = "Resources\\image (2).png";
        public void LoadDgvProduct(Guna2DataGridView dgv_ListProduct)
        {
            try
            {
                dgv_ListProduct.Rows.Clear();
                var listProduct = dal_ProductManager.GetAllRepository();
                foreach (var item in listProduct)
                {
                    int index = dgv_ListProduct.Rows.Add();
                    dgv_ListProduct.Rows[index].Cells["dgv_ProductID"].Value = item.ProductID;
                    dgv_ListProduct.Rows[index].Cells["dgv_ProductName"].Value = item.Product.ProductName;
                    dgv_ListProduct.Rows[index].Cells["dgv_ProductTypeName"].Value = item.Product.ProductType.ProductTypeName;
                    dgv_ListProduct.Rows[index].Cells["dgv_CalculationUnitName"].Value = item.Product.ProductType.CalculationUnitName;
                    dgv_ListProduct.Rows[index].Cells["dgv_Supplier"].Value = item.Supplier.SupplierName;
                    var getProductID_in_Repository = dal_ProductManager.GetProductID_Form_Repository(item.ProductID);
                    foreach (var i in getProductID_in_Repository)
                    {
                        dgv_ListProduct.Rows[index].Cells["dgv_SellPrice"].Value = i.SellPrice;
                        dgv_ListProduct.Rows[index].Cells["dgv_ProductQuantity"].Value = i.ProductQuantity;
                        if (i.Discount == null)
                        {
                            dgv_ListProduct.Rows[index].Cells["dgv_Discount"].Value = 0;
                        }
                        else
                        {
                            dgv_ListProduct.Rows[index].Cells["dgv_Discount"].Value = i.Discount;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình tìm kiếm là: " + ex.Message);
                return;
            }

        }
        public void LoadCellClickDgvProduct(object sender, DataGridViewCellEventArgs e, Guna2DataGridView dgv_ListProduct,
                    Guna2TextBox txt_ProductID, Guna2TextBox txt_ProductName, Guna2TextBox txt_ProductTypeName,
                    Guna2TextBox txt_CalculationUnitName, Guna2TextBox txt_Quantity, Guna2TextBox txt_SupplierName,
                    Guna2TextBox txt_SellPrice, Guna2TextBox txt_Discount, out Image image)
        {
            try
            {
                int index = e.RowIndex;
                if (index < 0)
                {
                    image = null;
                    return;
                }
                var productID = dgv_ListProduct.Rows[index].Cells["dgv_ProductID"].Value.ToString();
                txt_ProductID.Text = productID;
                txt_ProductName.Text = dgv_ListProduct.Rows[index].Cells["dgv_ProductName"].Value.ToString();
                txt_ProductTypeName.Text = dgv_ListProduct.Rows[index].Cells["dgv_ProductTypeName"].Value.ToString();
                txt_CalculationUnitName.Text = dgv_ListProduct.Rows[index].Cells["dgv_CalculationUnitName"].Value.ToString();
                txt_SupplierName.Text = dgv_ListProduct.Rows[index].Cells["dgv_Supplier"].Value.ToString();

                var getProductID_in_Repository = dal_ProductManager.GetProductID_Form_Repository(productID);
                foreach (var i in getProductID_in_Repository)
                {
                    txt_SellPrice.Text = dgv_ListProduct.Rows[index].Cells["dgv_SellPrice"].Value.ToString();
                    txt_Quantity.Text = dgv_ListProduct.Rows[index].Cells["dgv_ProductQuantity"].Value.ToString();
                    if (i.Discount == null)
                    {
                        txt_Discount.Text = dgv_ListProduct.Rows[index].Cells["dgv_Discount"].Value.ToString();
                    }
                    else
                    {
                        txt_Discount.Text = dgv_ListProduct.Rows[index].Cells["dgv_Discount"].Value.ToString();
                    }
                }

                var ImPath = dal_ProductManager.GetProduct(txt_ProductID.Text).ProductImage;
                string imagePath;
                if (string.IsNullOrEmpty(ImPath))
                {
                    string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    imagePath = Path.Combine(projectDirectory, EmptyImPath);
                }
                else
                {
                    // Xác định đường dẫn tới hình ảnh sử dụng đường dẫn tương đối
                    string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                    imagePath = Path.Combine(projectDirectory, ProductImPath, ImPath);
                }
                image = Image.FromFile(imagePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xổ dữ liệu ra txtbox là: " + ex.Message);
                string imagePath;
                string projectDirectory = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;
                imagePath = Path.Combine(projectDirectory, EmptyImPath);
                image = Image.FromFile(imagePath);
                return;
            }

        }
        public void FindProduct(Guna2DataGridView dgv_ListProduct, Guna2TextBox txt_Search)
        {
            try
            {
                dgv_ListProduct.Rows.Clear();
                var listProduct = dal_ProductManager.RepositorySearch(txt_Search.Text);
                foreach (var item in listProduct)
                {
                    int index = dgv_ListProduct.Rows.Add();
                    dgv_ListProduct.Rows[index].Cells["dgv_ProductID"].Value = item.ProductID;
                    dgv_ListProduct.Rows[index].Cells["dgv_ProductName"].Value = item.Product.ProductName;
                    dgv_ListProduct.Rows[index].Cells["dgv_ProductTypeName"].Value = item.Product.ProductType.ProductTypeName;
                    dgv_ListProduct.Rows[index].Cells["dgv_CalculationUnitName"].Value = item.Product.ProductType.CalculationUnitName;
                    dgv_ListProduct.Rows[index].Cells["dgv_Supplier"].Value = item.Supplier.SupplierName;
                    var getProductID_in_Repository = dal_ProductManager.GetProductID_Form_Repository(item.ProductID);
                    foreach (var i in getProductID_in_Repository)
                    {
                        dgv_ListProduct.Rows[index].Cells["dgv_SellPrice"].Value = i.SellPrice;
                        dgv_ListProduct.Rows[index].Cells["dgv_ProductQuantity"].Value = i.ProductQuantity;
                        if (i.Discount == null)
                        {
                            dgv_ListProduct.Rows[index].Cells["dgv_Discount"].Value = 0;
                        }
                        else
                        {
                            dgv_ListProduct.Rows[index].Cells["dgv_Discount"].Value = i.Discount;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình tìm kiếm là: " + ex.Message);
                return;
            }
        }

        public bool btn_Update_SellPrice_And_Discount(Guna2TextBox txt_ProductID, Guna2TextBox txt_SellPrice, Guna2TextBox txt_Discount)
        {
            try
            {
                var getProduct = dal_ProductManager.GetProductID_Form_Repository(txt_ProductID.Text);
                foreach (var t in getProduct)
                {
                    double.TryParse(txt_SellPrice.Text, out double sellPrice);
                    if (sellPrice == 0)
                    {
                        MessageBox.Show("Giá bán phải là số nguyên và không được để trống!");
                        return false;
                    }
                    t.SellPrice = sellPrice;
                    double.TryParse(txt_Discount.Text, out double discount);
                    if (discount == 0)
                    {
                        MessageBox.Show("% giảm giá phải là số nguyên và không được để trống!");
                        return false;
                    }
                    t.Discount = discount;
                    dal_ProductManager.RepositoryUpdate(t);
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình chỉnh sửa là: " + ex.Message);
                return false;
            }

        }
        public bool btn_Delete_Product(Guna2TextBox txt_ProductID)
        {
            try
            {
                var result = MessageBox.Show("Bạn có muốn xóa không?", "Xác nhận xóa", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    dal_ProductManager.RepositoryDelete(txt_ProductID.Text);
                }
                else
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Có lỗi trong quá trình xóa là: " + ex.Message);
                return false;
            }
        }

    }
}

